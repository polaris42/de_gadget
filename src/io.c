/****************************************************************************
Author: Volker Springel
Modified: Jared Coughlin and Ali Snedden
Date: 5/24/13

Purpose: This writes the snapshot files.

	NOTE: SPLASH expects the cooling parameters (Ne and Nh) to be written 
				BEFORE the smoothing length and the SFR to be written AFTER the 
				smoothing length. As for Stellar Age, that will require telling 
				SPLASH to expect an extra column. 

	NOTE: THis will only be tested for file type 1 and 1 file written in parallel.


****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <mpi.h>
#include <errno.h>

#ifdef HAVE_HDF5
#include <hdf5.h>
#endif

#include "allvars.h"
#include "proto.h"



/*! \file io.c
 *  \brief Routines for producing a snapshot file on disk.
 */

static int n_type[6];												//Number of LOCAL particles per type
static long long ntot_type_all[6];					//Number of TOTAL particles per type




/*! This function writes a snapshot of the particle distribution to one or
 *  several files using the selected file format.  If NumFilesPerSnapshot>1,
 *  the snapshot is distributed onto several files, several of them can be
 *  written simultaneously (up to NumFilesWrittenInParallel). Each file
 *  contains data from a group of processors.
 */
void savepositions(int num)
{
  double t0, t1;
  char buf[500];
  int i, j;
	int *temp;			//Holds particle numbers and types from each Task
	int n, filenr, gr, ngroups, masterTask, lastTask;

  t0 = second();

  if(ThisTask == 0)
    printf("\nwriting snapshot file... \n");

  /* ALI: Xinghai commented...
		#if defined(SFR) || defined(BLACK_HOLES)
    rearrange_particle_sequence();*/
  /* ensures that new tree will be constructed */
  /* All.NumForcesSinceLastDomainDecomp = 1 + 
     All.TreeDomainUpdateFrequency * All.TotNumPart;
     #endif
  */

  if(NTask < All.NumFilesPerSnapshot)
  {
    if(ThisTask == 0)
	    printf("Fatal error.\nNumber of processors must be larger or equal than All.NumFilesPerSnapshot.\n");
    endrun(0);
  }
  if(All.SnapFormat < 1 || All.SnapFormat > 3)
  {
    if(ThisTask == 0)
	    printf("Unsupported File-Format\n");
    endrun(0);
  }
	#ifndef  HAVE_HDF5
  	if(All.SnapFormat == 3)
  	{
  	  if(ThisTask == 0)
		    printf("Code wasn't compiled with HDF5 support enabled!\n");
  	  endrun(0);
  	}
	#endif


  /* determine global and local particle numbers */
  for(n = 0; n < 6; n++)
    n_type[n] = 0;

  for(n = 0; n < NumPart; n++)
    n_type[P[n].Type]++;

  /* because ntot_type_all[] is of type `long long', we cannot do a simple
   * MPI_Allreduce() to sum the total particle numbers 
   */
  temp = malloc(NTask * 6 * sizeof(int));
  MPI_Allgather(n_type, 6, MPI_INT, temp, 6, MPI_INT, MPI_COMM_WORLD);	//store in temp
  for(i = 0; i < 6; i++)
  {
    ntot_type_all[i] = 0;

		//Iterate through Tasks communicated particles numbers. Reduce
    for(j = 0; j < NTask; j++){
	    ntot_type_all[i] += temp[j * 6 + i];
		}
  }
  free(temp);


  /* assign processors to output files */
  distribute_file(All.NumFilesPerSnapshot, 0, 0, NTask - 1, &filenr, &masterTask, &lastTask);

  fill_Tab_IO_Labels();					//Only necessary for File type 2?

	//Get snapshot name
  if(All.NumFilesPerSnapshot > 1)
    sprintf(buf, "%s%s_%03d.%d", All.OutputDir, All.SnapshotFileBase, num, filenr);
  else
    sprintf(buf, "%s%s_%03d", All.OutputDir, All.SnapshotFileBase, num);

  ngroups = All.NumFilesPerSnapshot / All.NumFilesWrittenInParallel;
  if((All.NumFilesPerSnapshot % All.NumFilesWrittenInParallel))
    ngroups++;


	//Write (possibly) multiple files in Gadget format
  for(gr = 0; gr < ngroups; gr++)
  {
    if((filenr / All.NumFilesWrittenInParallel) == gr)	/* ok, it's this processor's turn */
	  write_file(buf, masterTask, lastTask);
    MPI_Barrier(MPI_COMM_WORLD);
  }

	//Write files in csv format...
	#ifdef CSVOUTPUT
		writeCsvData(buf);				//Use to check Gadget data with gadget2csv utility
	#endif


  if(ThisTask == 0)
    printf("done with snapshot.\n");

  t1 = second();

  All.CPU_Snapshot += timediff(t0, t1);

}



/*! This function fills the write buffer with particle data. New output blocks
 *  can in principle be added here. 
 * 
 *  The buffer is filled with LOCAL particle data to send to other processors
 */
void fill_write_buffer(enum iofields blocknr, int *startindex, int pc, int type)
{
  int n, k, pindex;
  float *fp;														//Points to buffer

	#ifdef LONGIDS
	  long long *ip;
	#else
	  int *ip;
	#endif

	#ifdef PERIODIC
	  FLOAT boxSize;
	#endif
	#ifdef PMGRID
	  double dt_gravkick_pm = 0;
	#endif
  double dt_gravkick, dt_hydrokick, a3inv = 1, fac1, fac2;


  if(All.ComovingIntegrationOn)
  {
    a3inv = 1 / (All.Time * All.Time * All.Time);
    fac1 = 1 / (All.Time * All.Time);
    fac2 = 1 / pow(All.Time, 3 * GAMMA - 2);
  }
  else
    a3inv = fac1 = fac2 = 1;

	#ifdef PMGRID
	  if(All.ComovingIntegrationOn)
	    dt_gravkick_pm =
	      get_gravkick_factor(All.PM_Ti_begstep, All.Ti_Current) - 
	                          get_gravkick_factor(All.PM_Ti_begstep, 
	                          (All.PM_Ti_begstep + All.PM_Ti_endstep) / 2);
	  else
	    dt_gravkick_pm = (All.Ti_Current - (All.PM_Ti_begstep + All.PM_Ti_endstep) / 2) * All.Timebase_interval;
	#endif


	//CommBuffer points to memory block of size All.BufferSize * 1024 *1024
  fp = CommBuffer;
  ip = CommBuffer;

  pindex = *startindex;									//Start at P[index] that was communicated.

  switch (blocknr)
  {
    case IO_POS:		/* positions */
      for(n = 0; n < pc; pindex++)
    	  if(P[pindex].Type == type)
  	    {
  	      for(k = 0; k < 3; k++)
	        {
        		fp[k] = P[pindex].Pos[k];
						#ifdef PERIODIC
        			boxSize = All.BoxSize;
							#ifdef LONG_X
        				if(k == 0)
        		  		boxSize = All.BoxSize * LONG_X;
							#endif
							#ifdef LONG_Y
        				if(k == 1)
        		  		boxSize = All.BoxSize * LONG_Y;
							#endif
							#ifdef LONG_Z
        				if(k == 2)
        				  boxSize = All.BoxSize * LONG_Z;
							#endif
        			while(fp[k] < 0)
        			  fp[k] += boxSize;
        			while(fp[k] >= boxSize)
        			  fp[k] -= boxSize;
						#endif
	        }
	        n++;
	        fp += 3;							//Advance to next block of 3 position data
	      }
      break;

    case IO_VEL:		/* velocities */
      for(n = 0; n < pc; pindex++)
	      if(P[pindex].Type == type)
	      {
	        if(All.ComovingIntegrationOn)
	        {
						/* I suspect below ALL the remaining particles that have
							 not been updated due to being in the "drift" portion of the integration
							 scheme.*/
	        	dt_gravkick =
	      	  							get_gravkick_factor(P[pindex].Ti_begstep,
			  	                All.Ti_Current) -	 get_gravkick_factor(P[pindex].Ti_begstep,
			  	                (P[pindex].Ti_begstep + P[pindex].Ti_endstep) / 2);
		        dt_hydrokick =
		                      get_hydrokick_factor(P[pindex].Ti_begstep,
			  	                All.Ti_Current) - get_hydrokick_factor(P[pindex].Ti_begstep,
			  	               (P[pindex].Ti_begstep + P[pindex].Ti_endstep) / 2);
	        }
	        else
	          dt_gravkick = dt_hydrokick =
		                 (All.Ti_Current - (P[pindex].Ti_begstep + P[pindex].Ti_endstep) / 2) * All.Timebase_interval;

	        for(k = 0; k < 3; k++)
	        {
		        fp[k] = P[pindex].Vel[k] + P[pindex].GravAccel[k] * dt_gravkick;
		        if(P[pindex].Type == 0)
		          fp[k] += SphP[pindex].HydroAccel[k] * dt_hydrokick;
	        }
					#ifdef PMGRID
	        	for(k = 0; k < 3; k++)
	        	  fp[k] += P[pindex].GravPM[k] * dt_gravkick_pm;
					#endif
	        for(k = 0; k < 3; k++)
	          fp[k] *= sqrt(a3inv);				/* Maintain GADGET-1 compatability, 
																					 see User Manual p40 */

	        n++;
	        fp += 3;
	      }
      break;

    case IO_ID:		/* particle ID */
      for(n = 0; n < pc; pindex++)
	      if(P[pindex].Type == type)
	      {
	        *ip++ = P[pindex].ID;
	        n++;
	      }
      break;

    case IO_MASS:		/* particle mass */
      for(n = 0; n < pc; pindex++)
	      if(P[pindex].Type == type)
	      {
	        *fp++ = P[pindex].Mass;
	        n++;
	      }
      break;

		/* FIX!!! Xinghai screwed up*/
    case IO_U:			/* internal energy, I think this is physical energy*/
      for(n = 0; n < pc; pindex++)
	      if(P[pindex].Type == type)
	      {
					#ifdef ISOTHERM_EQS
	    	    *fp++ = SphP[pindex].Entropy;  
					#else
						#ifdef OUTPUT_TEMP_FOR_U
		    			/***** here output temperature instead of internal energy *****/
	  	      	double molecular_weight;

							/*Photo-ionized even at low temps. Self shielding ignored. See
								p1214 in Schaye & Dalla Vecchia 2008 */
							#ifndef UVBACKGROUND
	    	    		if(All.InitGasTemp > 1.0e4)   /* assuming FULL ionization */
	      	  		  molecular_weight = 4 / (8 - 5 * (1 - HYDROGEN_MASSFRAC));
	        			else                          /* assuming NEUTRAL GAS */
	        			  molecular_weight = 4 / (1 + 3 * HYDROGEN_MASSFRAC);
							#endif
							#ifdef UVBACKGROUND
	      	  		molecular_weight = 4 / (8 - 5 * (1 - HYDROGEN_MASSFRAC));
							#endif		
		

	        		*fp++ = dmax(All.MinEgySpec, SphP[pindex].Entropy / GAMMA_MINUS1 * 
													pow(SphP[pindex].Density * a3inv, GAMMA_MINUS1)) * 
													molecular_weight * GAMMA_MINUS1 / (All.UnitMass_in_g / 
													All.UnitEnergy_in_cgs)/(BOLTZMANN / PROTONMASS);

						//Output Energy
						#else
							//Eqn 10 in Springel & Hernquist (2002), there is a minimum energy
	        		*fp++ = dmax(All.MinEgySpec,
		            				 SphP[pindex].Entropy / GAMMA_MINUS1 * pow(SphP[pindex].Density
												 * a3inv, GAMMA_MINUS1));
						#endif
					#endif
	        n++;
	      }
      break;

    case IO_RHO:		/* density */
      for(n = 0; n < pc; pindex++)
	      if(P[pindex].Type == type)
	      {
	        *fp++ = SphP[pindex].Density;
	        n++;
	      }
      break;





    case IO_HSML:		/* SPH smoothing length */
      for(n = 0; n < pc; pindex++)
	      if(P[pindex].Type == type)
	      {
	        *fp++ = SphP[pindex].Hsml;
	        n++;
	      }
      break;






    case IO_POT:		/* gravitational potential */
			#ifdef OUTPUTPOTENTIAL
      	for(n = 0; n < pc; pindex++)
	    	  if(P[pindex].Type == type)
	    	  {
						#ifdef OUTPUT_AGE_FOR_POTENTIAL
	        		*fp++ = P[pindex].StarAge;
						#else
	        		*fp++ = P[pindex].Potential;
						#endif
	        	n++;
	      	}
			#endif
      break;

    case IO_ACCEL:		/* acceleration */
			#ifdef OUTPUTACCELERATION
      	for(n = 0; n < pc; pindex++)
	    	  if(P[pindex].Type == type)
	    	  {
	      	  for(k = 0; k < 3; k++)
	        	  fp[k] = fac1 * P[pindex].GravAccel[k];
						#ifdef PMGRID
	        		for(k = 0; k < 3; k++)
	          		fp[k] += fac1 * P[pindex].GravPM[k];
						#endif
	        	if(P[pindex].Type == 0)
	          	for(k = 0; k < 3; k++)
	          		fp[k] += fac2 * SphP[pindex].HydroAccel[k];
	        	fp += 3;
	        	n++;
	      	}
			#endif
      break;

    case IO_DTENTR:		/* rate of change of entropy */
			#ifdef OUTPUTCHANGEOFENTROPY
      	for(n = 0; n < pc; pindex++)
	      	if(P[pindex].Type == type)
	      	{
	      	  *fp++ = SphP[pindex].DtEntropy;
	      	  n++;
	      	}
			#endif
      break;

    case IO_TSTP:		/* timestep  */
			#ifdef OUTPUTTIMESTEP
      	for(n = 0; n < pc; pindex++)
	    	  if(P[pindex].Type == type)
	    	  {
	    	    *fp++ = (P[pindex].Ti_endstep - P[pindex].Ti_begstep) * All.Timebase_interval;
	    	    n++;
	    	  }
			#endif
      break;


    case IO_RII:
      #ifdef FEEDBACK
        //Loop thru particles.
				for(n = 0; n < pc; pindex++)
				{
					if(P[pindex].Type == type)
					{
            float dN_II = (float)get_typeII_snr(pindex);

            //Actually write
						*fp++ = dN_II;//P[pindex].R_II;
						n++;
					}
				}
      #endif
      break;


    case IO_RIA:
      #ifdef FEEDBACK
				for(n = 0; n < pc; pindex++)
				{
					if(P[pindex].Type == type)
					{
            float dN_Ia = (float)get_typeIa_snr(pindex);

						*fp++ = dN_Ia;//P[pindex].R_Ia;
						n++;
					}
				}
      #endif
      break;


		
		case IO_STELLARAGE:		/* Star Age */
			#ifdef STELLARAGE
				for(n = 0; n < pc; pindex++)
				{
					if(P[pindex].Type == type)
					{
						*fp++ = P[pindex].StarAge;
						n++;
					}
				}	
			#endif
			break;



		case IO_CARBON:	/*Carbon Mass Fraction*/
			#ifdef CARBON
				for(n = 0; n < pc; pindex++)
				{
					if(P[pindex].Type == type)
					{
						*fp++ = P[pindex].CarbonMass;
						n++;
					}
				}	
			#endif
			break;

		
		case IO_NITROGEN:	/*Nitrogen Mass Fraction*/
			#ifdef NITROGEN
				for(n = 0; n < pc; pindex++)
				{
					if(P[pindex].Type == type)
					{
						*fp++ = P[pindex].NitrogenMass;
						n++;
					}
				}	
			#endif
			break;

		
		case IO_OXYGEN:	/*Oxygen Mass Fraction*/
			#ifdef OXYGEN
				for(n = 0; n < pc; pindex++)
				{
					if(P[pindex].Type == type)
					{
						*fp++ = P[pindex].OxygenMass;
						n++;
					}
				}	
			#endif
			break;

		
		case IO_FLORINE:	/*Florine Mass Fraction*/
			#ifdef FLORINE
				for(n = 0; n < pc; pindex++)
				{
					if(P[pindex].Type == type)
					{
						*fp++ = P[pindex].FlorineMass;
						n++;
					}
				}	
			#endif
			break;

		
		case IO_NEON:	/*Neon Mass Fraction*/
			#ifdef NEON
				for(n = 0; n < pc; pindex++)
				{
					if(P[pindex].Type == type)
					{
						*fp++ = P[pindex].NeonMass;
						n++;
					}
				}	
			#endif
			break;

		
		case IO_SODIUM:	/*Sodium Mass Fraction*/
			#ifdef SODIUM
				for(n = 0; n < pc; pindex++)
				{
					if(P[pindex].Type == type)
					{
						*fp++ = P[pindex].SodiumMass;
						n++;
					}
				}	
			#endif
			break;

		
		case IO_MAGNESIUM:	/*Magnesium Mass Fraction*/
			#ifdef MAGNESIUM
				for(n = 0; n < pc; pindex++)
				{
					if(P[pindex].Type == type)
					{
						*fp++ = P[pindex].MagnesiumMass;
						n++;
					}
				}	
			#endif
			break;

		
		case IO_ALUMINUM:	/*Aluminum Mass Fraction*/
			#ifdef ALUMINUM
				for(n = 0; n < pc; pindex++)
				{
					if(P[pindex].Type == type)
					{
						*fp++ = P[pindex].AluminumMass;
						n++;
					}
				}	
			#endif
			break;

		
		case IO_SILICON:	/*Silicon Mass Fraction*/
			#ifdef SILICON
				for(n = 0; n < pc; pindex++)
				{
					if(P[pindex].Type == type)
					{
						*fp++ = P[pindex].SiliconMass;
						n++;
					}
				}	
			#endif
			break;

		
		case IO_PHOSPHORUS:	/*Phosphorus Mass Fraction*/
			#ifdef PHOSPHORUS
				for(n = 0; n < pc; pindex++)
				{
					if(P[pindex].Type == type)
					{
						*fp++ = P[pindex].PhosphorusMass;
						n++;
					}
				}	
			#endif
			break;

		
		case IO_SULFUR:	/*Sulfur Mass Fraction*/
			#ifdef SULFUR
				for(n = 0; n < pc; pindex++)
				{
					if(P[pindex].Type == type)
					{
						*fp++ = P[pindex].SulfurMass;
						n++;
					}
				}	
			#endif
			break;

		
		case IO_CHLORINE:	/*Chlorine Mass Fraction*/
			#ifdef CHLORINE
				for(n = 0; n < pc; pindex++)
				{
					if(P[pindex].Type == type)
					{
						*fp++ = P[pindex].ChlorineMass;
						n++;
					}
				}	
			#endif
			break;

		
		case IO_ARGON:	/*Argon Mass Fraction*/
			#ifdef ARGON
				for(n = 0; n < pc; pindex++)
				{
					if(P[pindex].Type == type)
					{
						*fp++ = P[pindex].ArgonMass;
						n++;
					}
				}	
			#endif
			break;

		
		case IO_POTASSIUM:	/*Potassium Mass Fraction*/
			#ifdef POTASSIUM
				for(n = 0; n < pc; pindex++)
				{
					if(P[pindex].Type == type)
					{
						*fp++ = P[pindex].PotassiumMass;
						n++;
					}
				}	
			#endif
			break;

		
		case IO_CALCIUM:	/*Calcium Mass Fraction*/
			#ifdef CALCIUM
				for(n = 0; n < pc; pindex++)
				{
					if(P[pindex].Type == type)
					{
						*fp++ = P[pindex].CalciumMass;
						n++;
					}
				}	
			#endif
			break;

		
		case IO_SCANDIUM:	/*Scandium Mass Fraction*/
			#ifdef SCANDIUM
				for(n = 0; n < pc; pindex++)
				{
					if(P[pindex].Type == type)
					{
						*fp++ = P[pindex].ScandiumMass;
						n++;
					}
				}	
			#endif
			break;

		
		case IO_TITANIUM:	/*Titanium Mass Fraction*/
			#ifdef TITANIUM
				for(n = 0; n < pc; pindex++)
				{
					if(P[pindex].Type == type)
					{
						*fp++ = P[pindex].TitaniumMass;
						n++;
					}
				}	
			#endif
			break;

		
		case IO_VANADIUM:	/*Vanadium Mass Fraction*/
			#ifdef VANADIUM
				for(n = 0; n < pc; pindex++)
				{
					if(P[pindex].Type == type)
					{
						*fp++ = P[pindex].VanadiumMass;
						n++;
					}
				}	
			#endif
			break;

		
		case IO_CHROMIUM:	/*Chromium Mass Fraction*/
			#ifdef CHROMIUM
				for(n = 0; n < pc; pindex++)
				{
					if(P[pindex].Type == type)
					{
						*fp++ = P[pindex].ChromiumMass;
						n++;
					}
				}	
			#endif
			break;

		
		case IO_MANGANESE:	/*Manganese Mass Fraction*/
			#ifdef MANGANESE
				for(n = 0; n < pc; pindex++)
				{
					if(P[pindex].Type == type)
					{
						*fp++ = P[pindex].ManganeseMass;
						n++;
					}
				}	
			#endif
			break;

		
		case IO_IRON:	/*Iron Mass Fraction*/
			#ifdef IRON
				for(n = 0; n < pc; pindex++)
				{
					if(P[pindex].Type == type)
					{
						*fp++ = P[pindex].IronMass;
						n++;
					}
				}	
			#endif
			break;

		
		case IO_COBALT:	/*Cobalt Mass Fraction*/
			#ifdef COBALT
				for(n = 0; n < pc; pindex++)
				{
					if(P[pindex].Type == type)
					{
						*fp++ = P[pindex].CobaltMass;
						n++;
					}
				}	
			#endif
			break;

		
		case IO_NICKEL:	/*Nickel Mass Fraction*/
			#ifdef NICKEL
				for(n = 0; n < pc; pindex++)
				{
					if(P[pindex].Type == type)
					{
						*fp++ = P[pindex].NickelMass;
						n++;
					}
				}	
			#endif
			break;

		
		case IO_COPPER:	/*Copper Mass Fraction*/
			#ifdef COPPER
				for(n = 0; n < pc; pindex++)
				{
					if(P[pindex].Type == type)
					{
						*fp++ = P[pindex].CopperMass;
						n++;
					}
				}	
			#endif
			break;

		
		case IO_ZINC:	/*Zinc Mass Fraction*/
			#ifdef ZINC
				for(n = 0; n < pc; pindex++)
				{
					if(P[pindex].Type == type)
					{
						*fp++ = P[pindex].ZincMass;
						n++;
					}
				}	
			#endif
			break;
  }

  *startindex = pindex;
}




/*! This function tells the size of one data entry in each of the blocks
 *  defined for the output file. If one wants to add a new output-block, this
 *  function should be augmented accordingly.
 */
int get_bytes_per_blockelement(enum iofields blocknr)
{
  int bytes_per_blockelement = 0;

  switch (blocknr)
  {
    case IO_POS:
    case IO_VEL:
    case IO_ACCEL:
      bytes_per_blockelement = 3 * sizeof(float);
      break;

    case IO_ID:
			#ifdef LONGIDS
  	    bytes_per_blockelement = sizeof(long long);
			#else
      	bytes_per_blockelement = sizeof(int);
			#endif
      break;

    case IO_MASS:
    case IO_U:
    case IO_RHO:
    case IO_HSML:
    case IO_POT:
    case IO_DTENTR:
    case IO_TSTP:
		//case IO_SFR:
		case IO_RII:
		case IO_RIA:
		case IO_STELLARAGE:
		case IO_CARBON:
		case IO_NITROGEN:
		case IO_OXYGEN:
		case IO_FLORINE:
		case IO_NEON:
		case IO_SODIUM:
		case IO_MAGNESIUM:
		case IO_ALUMINUM:
		case IO_SILICON:
		case IO_PHOSPHORUS:
		case IO_SULFUR:
		case IO_CHLORINE:
		case IO_ARGON:
		case IO_POTASSIUM:
		case IO_CALCIUM:
		case IO_SCANDIUM:
		case IO_TITANIUM:
		case IO_VANADIUM:
		case IO_CHROMIUM:
		case IO_MANGANESE:
		case IO_IRON:
		case IO_COBALT:
		case IO_NICKEL:
		case IO_COPPER:
		case IO_ZINC:
      bytes_per_blockelement = sizeof(float);
      break;
  }

  return bytes_per_blockelement;
}


/*! This function returns the type of the data contained in a given block of
 *  the output file. If one wants to add a new output-block, this function
 *  should be augmented accordingly.
 */
int get_datatype_in_block(enum iofields blocknr)
{
  int typekey;

  switch (blocknr)
  {
    case IO_ID:
			#ifdef LONGIDS
      	typekey = 2;		/* native long long */
			#else
      	typekey = 0;		/* native int */
			#endif
      break;

    default:
      typekey = 1;		/* native float */
      break;
  }

  return typekey;
}


/*! This function informs about the number of elements stored per particle for
 *  the given block of the output file. If one wants to add a new
 *  output-block, this function should be augmented accordingly.
 */
int get_values_per_blockelement(enum iofields blocknr)
{
  int values = 0;

  switch (blocknr)
  {
    case IO_POS:
    case IO_VEL:
    case IO_ACCEL:
      values = 3;
      break;

    case IO_ID:
    case IO_MASS:
    case IO_U:
    case IO_RHO:
    case IO_HSML:
    case IO_POT:
    case IO_DTENTR:
    case IO_TSTP:
		//case IO_SFR:
		case IO_RII:
		case IO_RIA:
		case IO_STELLARAGE:
		case IO_CARBON:
		case IO_NITROGEN:
		case IO_OXYGEN:
		case IO_FLORINE:
		case IO_NEON:
		case IO_SODIUM:
		case IO_MAGNESIUM:
		case IO_ALUMINUM:
		case IO_SILICON:
		case IO_PHOSPHORUS:
		case IO_SULFUR:
		case IO_CHLORINE:
		case IO_ARGON:
		case IO_POTASSIUM:
		case IO_CALCIUM:
		case IO_SCANDIUM:
		case IO_TITANIUM:
		case IO_VANADIUM:
		case IO_CHROMIUM:
		case IO_MANGANESE:
		case IO_IRON:
		case IO_COBALT:
		case IO_NICKEL:
		case IO_COPPER:
		case IO_ZINC:
      values = 1;
      break;
  }

  return values;
}


/*! This function determines how many particles there are in a given block,
 *  based on the information in the header-structure.  It also flags particle
 *  types that are present in the block in the typelist array. If one wants to
 *  add a new output-block, this function should be augmented accordingly.
 *
 *	Note that this is for the GLOBAL (i.e. NOT LOCAL) number of particles.	
 */
int get_particles_in_block(enum iofields blocknr, int *typelist)
{
  int i;							//Indice
	int nall;    				//Total number of particles
	int ntot_withmasses, ngas, nstars;

  nall = 0;
  ntot_withmasses = 0;

  for(i = 0; i < 6; i++)
  {
    typelist[i] = 0;

		//sum up total number of particles and flag particle types
    if(header.npart[i] > 0)
	  {
	    nall += header.npart[i];
	    typelist[i] = 1;
	  }

		//Get number of particles with P[].Mass
    if(All.MassTable[i] == 0)
	    ntot_withmasses += header.npart[i];
  }

  ngas = header.npart[0];
  nstars = header.npart[4];


  switch (blocknr)
  {
    case IO_POS:
    case IO_VEL:
    case IO_ACCEL:
    case IO_TSTP:
    case IO_ID:
    case IO_POT:
      return nall;
      break;

		/*IO_MASS not used for types that have All.MassTable[i] > 0*/
    case IO_MASS:
      for(i = 0; i < 6; i++)
	    {
	      typelist[i] = 0;
	      if(All.MassTable[i] == 0 && header.npart[i] > 0)
	        typelist[i] = 1;
	    }
      return ntot_withmasses;
      break;

    case IO_U:
    case IO_RHO:
    case IO_HSML:
    case IO_DTENTR:
		//case IO_SFR:
      for(i = 1; i < 6; i++)
	      typelist[i] = 0;
      return ngas;
      break;

		//ALI : Fix?
		case IO_STELLARAGE:
    case IO_RII:
    case IO_RIA:
			for(i=0; i<6; i++)
			{
				typelist[i] = 0;
				if(i == 4){
					typelist[i] = 1;
				}
			}
			return nstars;
			break;

		/*Elements.  These hold for gas and stars, so need to return ngas+nstars.*/
		case IO_CARBON:
		case IO_NITROGEN:
		case IO_OXYGEN:
		case IO_FLORINE:
		case IO_NEON:
		case IO_SODIUM:
		case IO_MAGNESIUM:
		case IO_ALUMINUM:
		case IO_SILICON:
		case IO_PHOSPHORUS:
		case IO_SULFUR:
		case IO_CHLORINE:
		case IO_ARGON:
		case IO_POTASSIUM:
		case IO_CALCIUM:
		case IO_SCANDIUM:
		case IO_TITANIUM:
		case IO_VANADIUM:
		case IO_CHROMIUM:
		case IO_MANGANESE:
		case IO_IRON:
		case IO_COBALT:
		case IO_NICKEL:
		case IO_COPPER:
		case IO_ZINC:
			for(i = 0; i < 6; i++)
			{
				typelist[i] = 0;
				if(i == 0 || i == 4)
				{
					typelist[i] = 1;
				}
			}
			return (ngas + nstars);
			break;

  }

  endrun(212);
  return 0;
}



/*! This function tells whether or not a given block in the output file is
 *  present, depending on the type of simulation run and the compile-time
 *  options. If one wants to add a new output-block, this function should be
 *  augmented accordingly.
 */
int blockpresent(enum iofields blocknr)
{
	#ifndef OUTPUTPOTENTIAL
	  if(blocknr == IO_POT)
  	  return 0;
	#endif

	#ifndef OUTPUTACCELERATION
  	if(blocknr == IO_ACCEL)
    	return 0;
	#endif

	#ifndef OUTPUTCHANGEOFENTROPY
  	if(blocknr == IO_DTENTR)
    	return 0;
	#endif

	#ifndef OUTPUTTIMESTEP
  	if(blocknr == IO_TSTP)
    	return 0;
	#endif

	/*#ifndef SFR
		if(blocknr == IO_SFR)
			return 0;
	#endif*/

	#ifndef FEEDBACK
		if(blocknr == IO_RII)
			return 0;
	#endif

	#ifndef FEEDBACK
		if(blocknr == IO_RIA)
			return 0;
	#endif

	#ifndef STELLARAGE
		if(blocknr == IO_STELLARAGE)
			return 0;
	#endif

	#ifndef CARBON
		if(blocknr == IO_CARBON)
			return 0;
	#endif

	
	#ifndef NITROGEN
		if(blocknr == IO_NITROGEN)
			return 0;
	#endif

	
	#ifndef OXYGEN
		if(blocknr == IO_OXYGEN)
			return 0;
	#endif

	
	#ifndef FLORINE
		if(blocknr == IO_FLORINE)
			return 0;
	#endif

	
	#ifndef NEON
		if(blocknr == IO_NEON)
			return 0;
	#endif

	
	#ifndef SODIUM
		if(blocknr == IO_SODIUM)
			return 0;
	#endif

	
	#ifndef MAGNESIUM
		if(blocknr == IO_MAGNESIUM)
			return 0;
	#endif

	
	#ifndef ALUMINUM
		if(blocknr == IO_ALUMINUM)
			return 0;
	#endif

	
	#ifndef SILICON
		if(blocknr == IO_SILICON)
			return 0;
	#endif

	
	#ifndef PHOSPHORUS
		if(blocknr == IO_PHOSPHORUS)
			return 0;
	#endif

	
	#ifndef SULFUR
		if(blocknr == IO_SULFUR)
			return 0;
	#endif

	
	#ifndef CHLORINE
		if(blocknr == IO_CHLORINE)
			return 0;
	#endif

	
	#ifndef ARGON
		if(blocknr == IO_ARGON)
			return 0;
	#endif

	
	#ifndef POTASSIUM
		if(blocknr == IO_POTASSIUM)
			return 0;
	#endif

	
	#ifndef CALCIUM
		if(blocknr == IO_CALCIUM)
			return 0;
	#endif

	
	#ifndef SCANDIUM
		if(blocknr == IO_SCANDIUM)
			return 0;
	#endif

	
	#ifndef TITANIUM
		if(blocknr == IO_TITANIUM)
			return 0;
	#endif

	
	#ifndef VANADIUM
		if(blocknr == IO_VANADIUM)
			return 0;
	#endif

	
	#ifndef CHROMIUM
		if(blocknr == IO_CHROMIUM)
			return 0;
	#endif

	
	#ifndef MANGANESE
		if(blocknr == IO_MANGANESE)
			return 0;
	#endif

	
	#ifndef IRON
		if(blocknr == IO_IRON)
			return 0;
	#endif

	
	#ifndef COBALT
		if(blocknr == IO_COBALT)
			return 0;
	#endif

	
	#ifndef NICKEL
		if(blocknr == IO_NICKEL)
			return 0;
	#endif

	
	#ifndef COPPER
		if(blocknr == IO_COPPER)
			return 0;
	#endif

	
	#ifndef ZINC
		if(blocknr == IO_ZINC)
			return 0;
	#endif

  return 1;			/* default: present */
}




/*! This function associates a short 4-character block name with each block
 *  number.  This is stored in front of each block for snapshot
 *  FileFormat=2. If one wants to add a new output-block, this function should
 *  be augmented accordingly.
 */
void fill_Tab_IO_Labels(void)
{
  enum iofields i;

  for(i = 0; i < IO_NBLOCKS; i++)
    switch (i)
    {
      case IO_POS:
	      strncpy(Tab_IO_Labels[IO_POS], "POS ", 4);
	      break;
      case IO_VEL:
	      strncpy(Tab_IO_Labels[IO_VEL], "VEL ", 4);
	      break;
      case IO_ID:
	      strncpy(Tab_IO_Labels[IO_ID], "ID  ", 4);
	      break;
      case IO_MASS:
	      strncpy(Tab_IO_Labels[IO_MASS], "MASS", 4);
	      break;
      case IO_U:
	      strncpy(Tab_IO_Labels[IO_U], "U   ", 4);
	      break;
      case IO_RHO:
	      strncpy(Tab_IO_Labels[IO_RHO], "RHO ", 4);
	      break;
      case IO_HSML:
	      strncpy(Tab_IO_Labels[IO_HSML], "HSML", 4);
	      break;
      case IO_POT:
	      strncpy(Tab_IO_Labels[IO_POT], "POT ", 4);
	      break;
      case IO_ACCEL:
	      strncpy(Tab_IO_Labels[IO_ACCEL], "ACCE", 4);
	      break;
      case IO_DTENTR:
	      strncpy(Tab_IO_Labels[IO_DTENTR], "ENDT", 4);
	      break;
      case IO_TSTP:
	      strncpy(Tab_IO_Labels[IO_TSTP], "TSTP", 4);
	      break;
			/*case IO_SFR:
				strncpy(Tab_IO_Labels[IO_SFR], "SFR ", 4);
				break;*/	
			case IO_RII:
				strncpy(Tab_IO_Labels[IO_RII], "RII ", 4);
				break;
			case IO_RIA:
				strncpy(Tab_IO_Labels[IO_RIA], "RIA ", 4);
				break;
			case IO_STELLARAGE:
				strncpy(Tab_IO_Labels[IO_STELLARAGE], "SAGE", 4);
				break;	
			case IO_CARBON:
				strncpy(Tab_IO_Labels[IO_CARBON], "CMF ", 4);
				break;
			case IO_NITROGEN:
				strncpy(Tab_IO_Labels[IO_NITROGEN], "NMF ", 4);
				break;
			case IO_OXYGEN:
				strncpy(Tab_IO_Labels[IO_OXYGEN], "OMF ", 4);
				break;
			case IO_FLORINE:
				strncpy(Tab_IO_Labels[IO_FLORINE], "FMF ", 4);
				break;
			case IO_NEON:
				strncpy(Tab_IO_Labels[IO_NEON], "NeMF", 4);
				break;
			case IO_SODIUM:
				strncpy(Tab_IO_Labels[IO_SODIUM], "NaMF", 4);
				break;
			case IO_MAGNESIUM:
				strncpy(Tab_IO_Labels[IO_MAGNESIUM], "MgMF", 4);
				break;
			case IO_ALUMINUM:
				strncpy(Tab_IO_Labels[IO_ALUMINUM], "AlMF", 4);
				break;
			case IO_SILICON:
				strncpy(Tab_IO_Labels[IO_SILICON], "SiMF", 4);
				break;
			case IO_PHOSPHORUS:
				strncpy(Tab_IO_Labels[IO_PHOSPHORUS], "PMF ", 4);
				break;
			case IO_SULFUR:
				strncpy(Tab_IO_Labels[IO_SULFUR], "SMF ", 4);
				break;
			case IO_CHLORINE:
				strncpy(Tab_IO_Labels[IO_CHLORINE], "ClMF", 4);
				break;
			case IO_ARGON:
				strncpy(Tab_IO_Labels[IO_ARGON], "ArMF", 4);
				break;
			case IO_POTASSIUM:
				strncpy(Tab_IO_Labels[IO_POTASSIUM], "KMF ", 4);
				break;
			case IO_CALCIUM:
				strncpy(Tab_IO_Labels[IO_CALCIUM], "CaMF", 4);
				break;
			case IO_SCANDIUM:
				strncpy(Tab_IO_Labels[IO_SCANDIUM], "ScMF", 4);
				break;
			case IO_TITANIUM:
				strncpy(Tab_IO_Labels[IO_TITANIUM], "TiMF", 4);
				break;
			case IO_VANADIUM:
				strncpy(Tab_IO_Labels[IO_VANADIUM], "VMF ", 4);
				break;
			case IO_CHROMIUM:
				strncpy(Tab_IO_Labels[IO_CHROMIUM], "CrMF", 4);
				break;
			case IO_MANGANESE:
				strncpy(Tab_IO_Labels[IO_MANGANESE], "MnMF", 4);
				break;
			case IO_IRON:
				strncpy(Tab_IO_Labels[IO_IRON], "FeMF", 4);
				break;
			case IO_COBALT:
				strncpy(Tab_IO_Labels[IO_COBALT], "CoMF", 4);
				break;
			case IO_NICKEL:
				strncpy(Tab_IO_Labels[IO_NICKEL], "NiMF", 4);
				break;
			case IO_COPPER:
				strncpy(Tab_IO_Labels[IO_COPPER], "CuMF", 4);
				break;
			case IO_ZINC:
				strncpy(Tab_IO_Labels[IO_ZINC], "ZnMF", 4);
				break;
    }
}

/*! This function returns a descriptive character string that describes the
 *  name of the block when the HDF5 file format is used.  If one wants to add
 *  a new output-block, this function should be augmented accordingly.
 */
void get_dataset_name(enum iofields blocknr, char *buf)
{

  strcpy(buf, "default");

  switch (blocknr)
  {
    case IO_POS:
      strcpy(buf, "Coordinates");
      break;
    case IO_VEL:
      strcpy(buf, "Velocities");
      break;
    case IO_ID:
      strcpy(buf, "ParticleIDs");
      break;
    case IO_MASS:
      strcpy(buf, "Masses");
      break;
    case IO_U:
      strcpy(buf, "InternalEnergy");
      break;
    case IO_RHO:
      strcpy(buf, "Density");
      break;
    case IO_HSML:
      strcpy(buf, "SmoothingLength");
      break;
    case IO_POT:
      strcpy(buf, "Potential");
      break;
    case IO_ACCEL:
      strcpy(buf, "Acceleration");
      break;
    case IO_DTENTR:
      strcpy(buf, "RateOfChangeOfEntropy");
      break;
    case IO_TSTP:
      strcpy(buf, "TimeStep");
      break;
		/*case IO_SFR:
			strcpy(buf, "StarFormRate");
			break;*/
		case IO_RII:
			strcpy(buf, "RII");
			break;
		case IO_RIA:
			strcpy(buf, "RIa");
			break;
		case IO_STELLARAGE:
			strcpy(buf, "StarAge");
			break;
		case IO_CARBON:
			strcpy(buf, "CarbonMass");
			break;
		case IO_NITROGEN:
			strcpy(buf, "NitrogenMass");
			break;
		case IO_OXYGEN:
			strcpy(buf, "OxygenMass");
			break;
		case IO_FLORINE:
			strcpy(buf, "FlorineMass");
			break;
		case IO_NEON:
			strcpy(buf, "NeonMass");
			break;
		case IO_SODIUM:
			strcpy(buf, "SodiumMass");
			break;
		case IO_MAGNESIUM:
			strcpy(buf, "MagnesiumMass");
			break;
		case IO_ALUMINUM:
			strcpy(buf, "AluminumMass");
			break;
		case IO_SILICON:
			strcpy(buf, "SiliconMass");
			break;
		case IO_PHOSPHORUS:
			strcpy(buf, "PhosphorusMass");
			break;
		case IO_SULFUR:
			strcpy(buf, "SulfurMass");
			break;
		case IO_CHLORINE:
			strcpy(buf, "ChlorineMass");
			break;
		case IO_ARGON:
			strcpy(buf, "ArgonMass");
			break;
		case IO_POTASSIUM:
			strcpy(buf, "PotassiumMass");
			break;
		case IO_CALCIUM:
			strcpy(buf, "CalciumMass");
		case IO_SCANDIUM:
			strcpy(buf, "ScandiumMass");
			break;
		case IO_TITANIUM:
			strcpy(buf, "TitaniumMass");
			break;
		case IO_VANADIUM:
			strcpy(buf, "VanadiumMass");
			break;
		case IO_CHROMIUM:
			strcpy(buf, "ChromiumMass");
			break;
		case IO_MANGANESE:
			strcpy(buf, "ManganeseMass");
			break;
		case IO_IRON:
			strcpy(buf, "IronMass");
			break;
		case IO_COBALT:
			strcpy(buf, "CobaltMass");
			break;
		case IO_NICKEL:
			strcpy(buf, "NickelMass");
			break;
		case IO_COPPER:
			strcpy(buf, "CopperMass");
			break;
		case IO_ZINC:
			strcpy(buf, "ZincMass");
			break;
  }
}



/*! This function writes an actual snapshot file containing the data from
 *  processors 'writeTask' to 'lastTask'. 'writeTask' is the one that actually
 *  writes.  Each snapshot file contains a header first, then particle
 *  positions, velocities and ID's.  Particle masses are written only for
 *  those particle types with zero entry in MassTable.  After that, first the
 *  internal energies u, and then the density is written for the SPH
 *  particles.  If cooling is enabled, mean molecular weight and neutral
 *  hydrogen abundance are written for the gas particles. This is followed by
 *  the SPH smoothing length and further blocks of information, depending on
 *  included physics and compile-time flags.  If HDF5 is used, the header is
 *  stored in a group called "/Header", and the particle data is stored
 *  separately for each particle type in groups calles "/PartType0",
 *  "/PartType1", etc. The sequence of the blocks is unimportant in this case.
 */
void write_file(char *fname, int writeTask, int lastTask)
{
  int type; 										//Particle type index
	int bytes_per_blockelement;		//Per iofields element, ex: IO_POS -> 3*sizeof(float)
	int npart;										//Global number of particles in block
	int nextblock;
	int typelist[6];							//Particle types present in 'iofields' block
  int n_for_this_task;					//Buffer: Communicates n_part[type] to other processors
	int ntask, n; 
	int p;
	int pc;												//Maximum particle index in chunck to be written
	int offset = 0;								//Index tracking P[] as buffer size chuncks are written
	int task;
  int blockmaxlen;							//Max number of iofields elements that fit into buffer 
 	int ntot_type[6];							//Number of particles by type, incl. from other Tasks 
	int nn[6];
  enum iofields blocknr;				//Index for the output data blocks
  int blksize;									//Total bytes of block
  MPI_Status status;
  FILE *fd = 0;									//Output file

	#ifdef HAVE_HDF5
  	hid_t hdf5_file = 0, hdf5_grp[6], hdf5_headergrp = 0, hdf5_dataspace_memory;
  	hid_t hdf5_datatype = 0, hdf5_dataspace_in_file = 0, hdf5_dataset = 0;
  	herr_t hdf5_status;
  	hsize_t dims[2], count[2], start[2];
  	int rank, pcsum = 0;
  	char buf[500];
	#endif

	//Used to write block size prior to actual block
	#define SKIP  {my_fwrite(&blksize,sizeof(int),1,fd);}

  /* determine particle numbers of each type in file */

  if(ThisTask == writeTask)
  {
    for(n = 0; n < 6; n++)
	    ntot_type[n] = n_type[n];							//Number of LOCAL particles

    for(task = writeTask + 1; task <= lastTask; task++)
	  {
	    MPI_Recv(&nn[0], 6, MPI_INT, task, TAG_LOCALN, MPI_COMM_WORLD, &status);
	    for(n = 0; n < 6; n++)
	      ntot_type[n] += nn[n];
	  }

    for(task = writeTask + 1; task <= lastTask; task++)
	    MPI_Send(&ntot_type[0], 6, MPI_INT, task, TAG_N, MPI_COMM_WORLD);
  }
  else
  {
    MPI_Send(&n_type[0], 6, MPI_INT, writeTask, TAG_LOCALN, MPI_COMM_WORLD);
    MPI_Recv(&ntot_type[0], 6, MPI_INT, writeTask, TAG_N, MPI_COMM_WORLD, &status);
  }



  /* fill file header */

  for(n = 0; n < 6; n++)
  {
    header.npart[n] = ntot_type[n];
    header.npartTotal[n] = (unsigned int) ntot_type_all[n];
    header.npartTotalHighWord[n] = (unsigned int) (ntot_type_all[n] >> 32);
  }

  for(n = 0; n < 6; n++)
    header.mass[n] = All.MassTable[n];

  header.time = All.Time;

  if(All.ComovingIntegrationOn)
    header.redshift = 1.0 / All.Time - 1;
  else
    header.redshift = 0;

  header.flag_sfr = 0;
  header.flag_RII = 0;
  header.flag_RIa = 0;
  header.flag_feedback = 0;
  header.flag_cooling = 0;
  header.flag_stellarage = 0;
  header.flag_metals = 0;

	/*Element flags initialized to 0*/
	header.flag_Carbon = '0';	
	header.flag_Nitrogen = '0';	
	header.flag_Oxygen = '0';	
	header.flag_Florine = '0';	
	header.flag_Neon = '0';	
	header.flag_Sodium = '0';	
	header.flag_Magnesium = '0';	
	header.flag_Aluminum = '0';	
	header.flag_Silicon = '0';	
	header.flag_Phosphorus = '0';	
	header.flag_Sulfur = '0';	
	header.flag_Chlorine = '0';	
	header.flag_Argon = '0';	
	header.flag_Potassium = '0';	
	header.flag_Calcium = '0';	
	header.flag_Scandium = '0';	
	header.flag_Titanium = '0';	
	header.flag_Vanadium = '0';	
	header.flag_Chromium = '0';	
	header.flag_Manganese = '0';	
	header.flag_Iron = '0';	
	header.flag_Cobalt = '0';	
	header.flag_Nickel = '0';	
	header.flag_Copper = '0';	
	header.flag_Zinc = '0';	

	/*Commenting out below COOLING section permits one to output non-garbage 
		snapshot files. When *.flag_cooling is specified, it appears that SPLASH
		expects further data to be passed. I infer this b/c new options show up 
		in the SPLASH menu (Ne, Nh, SFR), but these are not written by 
		file_write_buffer(). Hence a better understanding of SPLASH and its file
		format is needed (and the role the header plays) in order to get non-garbage
		*/
	#ifdef COOLING
  	header.flag_cooling = 1;
	#endif
	#ifdef SFR
	  header.flag_sfr = 1;
		#ifdef STELLARAGE
	 		header.flag_stellarage = 1;
		#endif
		#ifdef FEEDBACK
	  	header.flag_feedback = 1;
      header.flag_RII = 1;
      header.flag_RIa = 1;
		#endif
		#ifdef METALS
	  	header.flag_metals = 1;
				
			/*Element flags*/
			
			#ifdef CARBON
				header.flag_Carbon = '1';
			#endif
			#ifdef NITROGEN	
				header.flag_Nitrogen = '1';
			#endif
			#ifdef OXYGEN
				header.flag_Oxygen = '1';
			#endif
			#ifdef FLORINE
				header.flag_Florine = '1';
			#endif
			#ifdef NEON
				header.flag_Neon = '1';
			#endif
			#ifdef SODIUM
				header.flag_Sodium = '1';
			#endif
			#ifdef MAGNESIUM
				header.flag_Magnesium = '1';
			#endif
			#ifdef ALUMINUM
				header.flag_Aluminum = '1';
			#endif
			#ifdef SILICON
				header.flag_Silicon = '1';
			#endif
			#ifdef PHOSPHORUS
				header.flag_Phosphorus = '1';
			#endif
			#ifdef SULFUR
				header.flag_Sulfur = '1';
			#endif
			#ifdef CHLORINE
				header.flag_Chlorine = '1';
			#endif
			#ifdef ARGON
				header.flag_Argon = '1';
			#endif
			#ifdef POTASSIUM
				header.flag_Potassium = '1';
			#endif
			#ifdef CALCIUM
				header.flag_Calcium = '1';
			#endif
			#ifdef SCANDIUM
				header.flag_Scandium = '1';
			#endif
			#ifdef TITANIUM
				header.flag_Titanium = '1';
			#endif
			#ifdef VANADIUM
				header.flag_Vanadium = '1';
			#endif
			#ifdef CHROMIUM
				header.flag_Chromium = '1';
			#endif
			#ifdef MANGANESE
				header.flag_Manganese = '1';
			#endif
			#ifdef IRON
				header.flag_Iron = '1';
			#endif
			#ifdef COBALT
				header.flag_Cobalt = '1';
			#endif
			#ifdef NICKEL
				header.flag_Nickel = '1';
			#endif
			#ifdef COPPER
				header.flag_Copper = '1';
			#endif
			#ifdef ZINC
				header.flag_Zinc = '1';
			#endif

		#endif
	#endif

  header.num_files = All.NumFilesPerSnapshot;
  header.BoxSize = All.BoxSize;
  header.Omega0 = All.Omega0;
  header.OmegaLambda = All.OmegaLambda;
  header.HubbleParam = All.HubbleParam;


  /* open file and write header */

  if(ThisTask == writeTask)
  {
    if(All.SnapFormat == 3)
	  {
			#ifdef HAVE_HDF5
	    	sprintf(buf, "%s.hdf5", fname);
	    	hdf5_file = H5Fcreate(buf, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);

	    	hdf5_headergrp = H5Gcreate(hdf5_file, "/Header", 0);

	    	for(type = 0; type < 6; type++)
	    	{
	    	  if(header.npart[type] > 0)
	    		{
	    		  sprintf(buf, "/PartType%d", type);
	    		  hdf5_grp[type] = H5Gcreate(hdf5_file, buf, 0);
	    		}
	    	}

	    	write_header_attributes_in_hdf5(hdf5_headergrp);
			#endif
	  }
    else
	  {
	    if(!(fd = fopen(fname, "w")))
	    {
	      printf("can't open file `%s' for writing snapshot.\n", fname);
	      endrun(123);
	    }

	    if(All.SnapFormat == 2)
	    {
	      blksize = sizeof(int) + 4 * sizeof(char);
	      SKIP;
	      my_fwrite("HEAD", sizeof(char), 4, fd);
	      nextblock = sizeof(header) + 2 * sizeof(int);
	      my_fwrite(&nextblock, sizeof(int), 1, fd);
	      SKIP;
	    }

	    blksize = sizeof(header);
	    SKIP;
	    my_fwrite(&header, sizeof(header), 1, fd);					//Write header
	    SKIP;
	  }
  }

  ntask = lastTask - writeTask + 1;


	//Iterate through the 11 iofields output data fields
  for(blocknr = 0; blocknr < IO_NBLOCKS; blocknr++)
  {
		//Checks output fields (iofields) against compile-time options
    if(blockpresent(blocknr))				
	  {
	    bytes_per_blockelement = get_bytes_per_blockelement(blocknr);

			//Recall BufferSize is in MB and 1MB = 1024^2, (in units of block elements)
	    blockmaxlen = ((int) (All.BufferSize * 1024 * 1024)) / bytes_per_blockelement;

			//Get number of ALL particles in the block and particle types available
	    npart = get_particles_in_block(blocknr, &typelist[0]);

		
			//IF particles exist for particle type...do below
	    if(npart > 0)
	    {
				//Allow only 1 Task to write at a time.
	      if(ThisTask == writeTask)        
	    	{
	    	  if(All.SnapFormat == 1 || All.SnapFormat == 2)
	  	    {
	  	      if(All.SnapFormat == 2)
	      		{
	      		  blksize = sizeof(int) + 4 * sizeof(char);
	      		  SKIP;
	      		  my_fwrite(Tab_IO_Labels[blocknr], sizeof(char), 4, fd);
	      		  nextblock = npart * bytes_per_blockelement + 2 * sizeof(int);
	      		  my_fwrite(&nextblock, sizeof(int), 1, fd);
	      		  SKIP;
	      		}

						//Write the block size to file.
		        blksize = npart * bytes_per_blockelement;
		        SKIP;
		      }
		    }

				//Loop through particle types. Include only those present
	      for(type = 0; type < 6; type++)
		    {
					//Only worry about particles available
		      if(typelist[type])						//typelist[] set in get_particles_in_block()
		      {
						#ifdef HAVE_HDF5
		        	if(ThisTask == writeTask && All.SnapFormat == 3 && header.npart[type] > 0)
			      	{
			      		switch (get_datatype_in_block(blocknr))
			      	  {
			      	    case 0:
			      	      hdf5_datatype = H5Tcopy(H5T_NATIVE_UINT);
			      	      break;
			     	     	case 1:
			     	      	hdf5_datatype = H5Tcopy(H5T_NATIVE_FLOAT);
			     	       	break;
			     	     	case 2:
			     	       	hdf5_datatype = H5Tcopy(H5T_NATIVE_UINT64);
			     	       	break;
			     	   	}

			       		dims[0] = header.npart[type];
			       		dims[1] = get_values_per_blockelement(blocknr);
			        	if(dims[1] == 1)
			          	rank = 1;
			        	else
			        	  rank = 2;

			        	get_dataset_name(blocknr, buf);

			        	hdf5_dataspace_in_file = H5Screate_simple(rank, dims, NULL);
			        	hdf5_dataset =
			        	        H5Dcreate(hdf5_grp[type], buf, hdf5_datatype, 
              	                  hdf5_dataspace_in_file, H5P_DEFAULT);
			        	pcsum = 0;
			      	}
						#endif

						//Iterate through all the processors. Send data to writeTask one at a time.
		        for(task = writeTask, offset = 0; task <= lastTask; task++)
			      {
							//Communicate Number of particles (n_type) to and from other processors
			        if(task == ThisTask)
			        {
			          n_for_this_task = n_type[type];

								//Broadcast local number of n_type[type] to all other processors
			          for(p = writeTask; p <= lastTask; p++){
			          	if(p != ThisTask){
			          	  MPI_Send(&n_for_this_task, 1, MPI_INT, p, TAG_NFORTHISTASK, 
                             MPI_COMM_WORLD);
									}
								}
			        }
			        else{
								//Receive particles from other 
			          MPI_Recv(&n_for_this_task, 1, MPI_INT, task, TAG_NFORTHISTASK, 
                         MPI_COMM_WORLD, &status);
							}


							//Write P[] data communitcated
			        while(n_for_this_task > 0)
			        {
			          pc = n_for_this_task;						//Get length of buffer to send

								//Enforce length of buffer
			          if(pc > blockmaxlen)
				          pc = blockmaxlen;

								//Fill buffer with ThisTasks' P[] data to send. See MPI_Send from above
			          if(ThisTask == task)
				          fill_write_buffer(blocknr, &offset, pc, type);

								//Only writing processor needs to receive data from other processors
			          if(ThisTask == writeTask && task != writeTask)
				          MPI_Recv(CommBuffer, bytes_per_blockelement * pc, MPI_BYTE, task,
				               	 TAG_PDATA, MPI_COMM_WORLD, &status);

								//All other tasks must send to the witing task
			          if(ThisTask != writeTask && task == ThisTask)
				          MPI_Ssend(CommBuffer, bytes_per_blockelement * pc, MPI_BYTE, 
                            writeTask, TAG_PDATA, MPI_COMM_WORLD);


			          if(ThisTask == writeTask)
				        {
				          if(All.SnapFormat == 3)
				          {
										#ifdef HAVE_HDF5
				            	start[0] = pcsum;
				            	start[1] = 0;

				            	count[0] = pc;
				            	count[1] = get_values_per_blockelement(blocknr);
				            	pcsum += pc;

				            	H5Sselect_hyperslab(hdf5_dataspace_in_file, H5S_SELECT_SET,
				                      			  start, NULL, count, NULL);

				            	dims[0] = pc;
				            	dims[1] = get_values_per_blockelement(blocknr);
				            	hdf5_dataspace_memory = H5Screate_simple(rank, dims, NULL);

				            	hdf5_status =
				            	   	H5Dwrite(hdf5_dataset, hdf5_datatype, hdf5_dataspace_memory,
				                    		 hdf5_dataspace_in_file, H5P_DEFAULT, CommBuffer);

				            	H5Sclose(hdf5_dataspace_memory);
										#endif
				          }
				          else{
										//Write the buffer to the output file
				            my_fwrite(CommBuffer, bytes_per_blockelement, pc, fd);
									}
				        }
								//Send the remaining part of the block
			          n_for_this_task -= pc;
			        }
			      }

						#ifdef HAVE_HDF5
		        	if(ThisTask == writeTask && All.SnapFormat == 3 && header.npart[type] > 0)
			      	{
			        	if(All.SnapFormat == 3)
			        	{
			          	H5Dclose(hdf5_dataset);
			          	H5Sclose(hdf5_dataspace_in_file);
			          	H5Tclose(hdf5_datatype);
			        	}
			      	}
						#endif
		      }
		    }

	      if(ThisTask == writeTask)
		    {
		      if(All.SnapFormat == 1 || All.SnapFormat == 2)
		        SKIP;
		    }
	    }
	  }
  }

  if(ThisTask == writeTask)
  {
    if(All.SnapFormat == 3)
	  {
			#ifdef HAVE_HDF5
	    	for(type = 5; type >= 0; type--)
	    	  if(header.npart[type] > 0)
	    	    H5Gclose(hdf5_grp[type]);
	    	H5Gclose(hdf5_headergrp);
	    	H5Fclose(hdf5_file);
			#endif
	  }
    else
	    fclose(fd);
  }
}




/*! This function writes the header information in case HDF5 is selected as
 *  file format.
 */
#ifdef HAVE_HDF5
void write_header_attributes_in_hdf5(hid_t handle)
{
  hsize_t adim[1] = { 6 };
  hid_t hdf5_dataspace, hdf5_attribute;

  hdf5_dataspace = H5Screate(H5S_SIMPLE);
  H5Sset_extent_simple(hdf5_dataspace, 1, adim, NULL);
  hdf5_attribute = H5Acreate(handle, "NumPart_ThisFile", H5T_NATIVE_INT, 
                             hdf5_dataspace, H5P_DEFAULT);
  H5Awrite(hdf5_attribute, H5T_NATIVE_UINT, header.npart);
  H5Aclose(hdf5_attribute);
  H5Sclose(hdf5_dataspace);

  hdf5_dataspace = H5Screate(H5S_SIMPLE);
  H5Sset_extent_simple(hdf5_dataspace, 1, adim, NULL);
  hdf5_attribute = H5Acreate(handle, "NumPart_Total", H5T_NATIVE_UINT, 
                             hdf5_dataspace, H5P_DEFAULT);
  H5Awrite(hdf5_attribute, H5T_NATIVE_UINT, header.npartTotal);
  H5Aclose(hdf5_attribute);
  H5Sclose(hdf5_dataspace);

  hdf5_dataspace = H5Screate(H5S_SIMPLE);
  H5Sset_extent_simple(hdf5_dataspace, 1, adim, NULL);
  hdf5_attribute = H5Acreate(handle, "NumPart_Total_HW", H5T_NATIVE_UINT, 
                             hdf5_dataspace, H5P_DEFAULT);
  H5Awrite(hdf5_attribute, H5T_NATIVE_UINT, header.npartTotalHighWord);
  H5Aclose(hdf5_attribute);
  H5Sclose(hdf5_dataspace);


  hdf5_dataspace = H5Screate(H5S_SIMPLE);
  H5Sset_extent_simple(hdf5_dataspace, 1, adim, NULL);
  hdf5_attribute = H5Acreate(handle, "MassTable", H5T_NATIVE_DOUBLE, 
                             hdf5_dataspace, H5P_DEFAULT);
  H5Awrite(hdf5_attribute, H5T_NATIVE_DOUBLE, header.mass);
  H5Aclose(hdf5_attribute);
  H5Sclose(hdf5_dataspace);

  hdf5_dataspace = H5Screate(H5S_SCALAR);
  hdf5_attribute = H5Acreate(handle, "Time", H5T_NATIVE_DOUBLE, 
                             hdf5_dataspace, H5P_DEFAULT);
  H5Awrite(hdf5_attribute, H5T_NATIVE_DOUBLE, &header.time);
  H5Aclose(hdf5_attribute);
  H5Sclose(hdf5_dataspace);

  hdf5_dataspace = H5Screate(H5S_SCALAR);
  hdf5_attribute = H5Acreate(handle, "Redshift", H5T_NATIVE_DOUBLE, 
                             hdf5_dataspace, H5P_DEFAULT);
  H5Awrite(hdf5_attribute, H5T_NATIVE_DOUBLE, &header.redshift);
  H5Aclose(hdf5_attribute);
  H5Sclose(hdf5_dataspace);

  hdf5_dataspace = H5Screate(H5S_SCALAR);
  hdf5_attribute = H5Acreate(handle, "BoxSize", H5T_NATIVE_DOUBLE, 
                             hdf5_dataspace, H5P_DEFAULT);
  H5Awrite(hdf5_attribute, H5T_NATIVE_DOUBLE, &header.BoxSize);
  H5Aclose(hdf5_attribute);
  H5Sclose(hdf5_dataspace);

  hdf5_dataspace = H5Screate(H5S_SCALAR);
  hdf5_attribute = H5Acreate(handle, "NumFilesPerSnapshot", H5T_NATIVE_INT, 
                             hdf5_dataspace, H5P_DEFAULT);
  H5Awrite(hdf5_attribute, H5T_NATIVE_INT, &header.num_files);
  H5Aclose(hdf5_attribute);
  H5Sclose(hdf5_dataspace);

  hdf5_dataspace = H5Screate(H5S_SCALAR);
  hdf5_attribute = H5Acreate(handle, "Omega0", H5T_NATIVE_DOUBLE, 
                             hdf5_dataspace, H5P_DEFAULT);
  H5Awrite(hdf5_attribute, H5T_NATIVE_DOUBLE, &header.Omega0);
  H5Aclose(hdf5_attribute);
  H5Sclose(hdf5_dataspace);

  hdf5_dataspace = H5Screate(H5S_SCALAR);
  hdf5_attribute = H5Acreate(handle, "OmegaLambda", H5T_NATIVE_DOUBLE, 
                             hdf5_dataspace, H5P_DEFAULT);
  H5Awrite(hdf5_attribute, H5T_NATIVE_DOUBLE, &header.OmegaLambda);
  H5Aclose(hdf5_attribute);
  H5Sclose(hdf5_dataspace);

  hdf5_dataspace = H5Screate(H5S_SCALAR);
  hdf5_attribute = H5Acreate(handle, "HubbleParam", H5T_NATIVE_DOUBLE, 
                             hdf5_dataspace, H5P_DEFAULT);
  H5Awrite(hdf5_attribute, H5T_NATIVE_DOUBLE, &header.HubbleParam);
  H5Aclose(hdf5_attribute);
  H5Sclose(hdf5_dataspace);

  hdf5_dataspace = H5Screate(H5S_SCALAR);
  /*hdf5_attribute = H5Acreate(handle, "Flag_Sfr", H5T_NATIVE_INT, 
                             hdf5_dataspace, H5P_DEFAULT);
  H5Awrite(hdf5_attribute, H5T_NATIVE_INT, &header.flag_sfr);*/
  H5Aclose(hdf5_attribute);
  H5Sclose(hdf5_dataspace);

  hdf5_dataspace = H5Screate(H5S_SCALAR);
  hdf5_attribute = H5Acreate(handle, "Flag_Cooling", H5T_NATIVE_INT, 
                             hdf5_dataspace, H5P_DEFAULT);
  H5Awrite(hdf5_attribute, H5T_NATIVE_INT, &header.flag_cooling);
  H5Aclose(hdf5_attribute);
  H5Sclose(hdf5_dataspace);

  hdf5_dataspace = H5Screate(H5S_SCALAR);
  hdf5_attribute = H5Acreate(handle, "Flag_StellarAge", H5T_NATIVE_INT, 
                             hdf5_dataspace, H5P_DEFAULT);
  H5Awrite(hdf5_attribute, H5T_NATIVE_INT, &header.flag_stellarage);
  H5Aclose(hdf5_attribute);
  H5Sclose(hdf5_dataspace);

  hdf5_dataspace = H5Screate(H5S_SCALAR);
  hdf5_attribute = H5Acreate(handle, "Flag_Metals", H5T_NATIVE_INT, 
                             hdf5_dataspace, H5P_DEFAULT);
  H5Awrite(hdf5_attribute, H5T_NATIVE_INT, &header.flag_metals);
  H5Aclose(hdf5_attribute);
  H5Sclose(hdf5_dataspace);

  hdf5_dataspace = H5Screate(H5S_SCALAR);
  hdf5_attribute = H5Acreate(handle, "Flag_Feedback", H5T_NATIVE_INT, 
                             hdf5_dataspace, H5P_DEFAULT);
  H5Awrite(hdf5_attribute, H5T_NATIVE_INT, &header.flag_feedback);
  H5Aclose(hdf5_attribute);
  H5Sclose(hdf5_dataspace);

  header.flag_entropy_instead_u = 0;

  hdf5_dataspace = H5Screate(H5S_SIMPLE);
  H5Sset_extent_simple(hdf5_dataspace, 1, adim, NULL);
  hdf5_attribute = H5Acreate(handle, "Flag_Entropy_ICs", H5T_NATIVE_UINT, 
                             hdf5_dataspace, H5P_DEFAULT);
  H5Awrite(hdf5_attribute, H5T_NATIVE_UINT, &header.flag_entropy_instead_u);
  H5Aclose(hdf5_attribute);
  H5Sclose(hdf5_dataspace);
}
#endif





/*! This catches I/O errors occuring for my_fwrite(). In this case we
 *  better stop.
 */
size_t my_fwrite(void *ptr, size_t size, size_t nmemb, FILE * stream)
{
  size_t nwritten;

  if((nwritten = fwrite(ptr, size, nmemb, stream)) != nmemb)
  {
    printf("I/O error (fwrite) on task=%d has occured: %s\n", ThisTask, strerror(errno));
    fflush(stdout);
    endrun(777);
  }
  return nwritten;
}


/*! This catches I/O errors occuring for fread(). In this case we
 *  better stop.
 */
size_t my_fread(void *ptr, size_t size, size_t nmemb, FILE * stream)
{
  size_t nread;

  if((nread = fread(ptr, size, nmemb, stream)) != nmemb)
  {
    printf("I/O error (fread) on task=%d has occured: %s\n", ThisTask, strerror(errno));
    fflush(stdout);
    endrun(778);
  }
  return nread;
}
