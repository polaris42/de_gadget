/*********************************************************
Author: Ali Snedden
Date: 2/13/13
NSH 341a
Univ. of Notre Dame

Purpose: 
	This takes in a 2 dimensional table and interpolates it.
	See Numerical Recipes in C (2nd edition) p. 123. 
	Below we do the bilinear interpolation that is 
	"Good enough for government work". THis is intended to
	interpolate between metallicities and temperature to get
	the necessary cooling rate (See Cloudy10 and Sutherland &
	Dopita 1993)

	Below is a reproduction of Fig. 3.6.1 in an intelligent 
	manner.


 z_1(x_1, y_1)                           z_2(x_2, y_2)
----O-----------------------------------------O-----------
    |                                         |
    |                                         |
    |                                         |
    |      O                                  |
    |    z(x,y)                               |
    |                                         |
    |                                         |
    |                                         |
    |                                         |
    |                                         | 
    |                                         |
    |                                         |
    |                                         |
----O-----------------------------------------O-----------
 z_4(x_4, y_4)                           z_3(x_3, y_3)




**********************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <mpi.h>
#include <gsl/gsl_math.h>
#include "allvars.h"
#include "proto.h"

#define LINE_LENGTH  1000

#ifdef COOLING
/*
typedef struct{
	float x;
	float y;
	float value;
} Point;
*/

/* 	table holds values, 
		xAxis and yAxis provides the axis values, 
		pt is the value to be interpolated.
*/
//float interpolate(float ** table, int nRows, int nColumns,  Point * pt);



/*int main(int argc, char * argv[])
{
	char line[LINE_LENGTH];						//Line buffer
	char * p = NULL;									//Used to parse data lines
	int nColumns = 0;									//Number of data columns
	int nRows    = 0;									//Num Data lines plus 1 for last line of header
	int i,j;
	float ** table = NULL;
	Point a;
	//Point a = {.metal = 0.89, .logT = 4.63, .cooling_rate = -1};

	//Help Section
	if(argc != 3)
	{
		fprintf(stderr, "ERROR!! see ./interpolate -h for USAGE\n\n");
		exit(1);
	}
	else if(argv[1][0] == '-' && argv[1][1] == 'h')
	{
		printf("USAGE:\n./interpolate x_value y_value\n");
		printf("\nWhere x and y values are the points on the table to be interpolated\n\n");
		exit(1);
	}

	//Initialize Point to interpolate
	a.metal = atof(argv[1]);
	a.logT = atof(argv[2]);
	a.cooling_rate = -1.0;
	
	
	//Error Check
	if(f==NULL)
	{
		fprintf(stderr, "ERROR!! File not Found");
		exit(1);
	}


	//Get table Dimensions 
	while( fgets(line, LINE_LENGTH, f) != NULL)
	{
		if(feof(f) == 0)
		{
			if(line[0] != '#' && line[0] != '/')
			{
				//Get # of columns, only need to do once
				if(nColumns == 0)
				{
					p = strtok(line, " ,");
					while(p != NULL)
					{
						p = strtok(NULL, " ,");
						if(p != NULL)
						{
							nColumns++;
						}
					}
				}
				nRows++;			
			}
		}
	}
	nRows++;                  //Add extra row for xAxis values at array[0][]

	printf("\n");

	//Allocate Table
	table = (float **)malloc(sizeof(float *) * nColumns);
	for(i=0; i< (nRows); i++)
	{
		table[i] = (float *)malloc(sizeof(float) * nRows);
	}

	
	rewind(f);
	i=0;
	j=0;	



	//Read x-axis values (Metallicities)....last line of file header 
	while( fgets(line, LINE_LENGTH, f) != NULL)
	{
		if(feof(f) == 0)
		{
			p = strtok(line, "# ,_Z");
			//Get x-axis values (Metallicities)
			if( strcmp(p, "temp") == 0 )
			{
				i = 1;
				table[0][0] = 1e20;						//This value should NEVER be accessed
				p = strtok(NULL, "# ,_Z");		//Skip 'temp'

				//Write values
				while(p != NULL && i < nColumns)
				{
					sscanf(p, "%f", &table[i][0]);
					p = strtok(NULL, "# ,_Z");
					i++;
				}
			}
		}
	}
	

	rewind(f);
	j=1;																//Skip x-axis line



	//Read file, write to table
	while( fgets(line, LINE_LENGTH, f) != NULL)
	{
		if(feof(f) == 0  &&  line[0] != '#')			//Skip header lines
		{
			p = strtok(line, " ");
			i=0;

			//Read & Write in all other lines.
			while( p != NULL && i < nColumns)
			{
				sscanf(p, "%f", &table[i][j]);
				p = strtok(NULL, " \t");
				i++;
			}

			j++;
		}
	}


	/ * /Print file into 2D array
	for(j=0; j<nRows; j++)
	{
		for(i=0; i<nColumns; i++)
		{
			printf("%.2e ",table[i][j]);
		}
		printf("\n");
	}* /

	printf("interpolate: (%.2f, %.2f) : %.3e\n", a.metal, a.logT, 
				 interpolate(table, nRows, nColumns, &a));

	printf("Number of lines: %i\nNumber of columns: %i\n",nRows,nColumns);

	return 0;
}*/


/*
	B/c my values are spaced evenly in both 'x' and 'y', I do not need to 
	have an efficient searching algorithm. If they were not uniformly spaced
	I would HAVE to develop a searching algorithm, recursive function and all.
	Why waste my time when I can make my life easier today?

	If this assumption is false for the table then you are SCREWED!


float interpolate(float ** table, int nRows, int nColumns,  Cooling_Point * pt)
{
	int i=0;									//index for z_1
	int j=0;									//index for z_1
	float t = 0;
	float u = 0;
	float step_x = 0;					//step size in x
	float step_y = 0;					//step size in y
	float min_x  = 0;
	float min_y  = 0;
	float max_x  = 0;
	float max_y  = 0;
	float result = 0;
	Cooling_Point z_1;
	Cooling_Point z_2;
	Cooling_Point z_3;
	Cooling_Point z_4;
	
	//Find the closest value
	step_x = fabs(table[1][0] - table[2][0]);
	step_y = fabs(table[0][2] - table[0][1]);
	min_x  = table[1][0];
	min_y  = table[0][1];
	max_x  = table[nColumns - 1][0];
	max_y  = table[0][nRows-1];
	

	if(pt->metal <= min_x || pt->metal >= max_x || pt->logT <= min_y || pt->logT >= max_y)
	{
		fprintf(stderr, "ERROR ::: X or Y value outside interpolation table bounds\n\n");
		exit(2);
	}
	else
	{
		i = floor((pt->metal - min_x)/step_x) + 1;				//+1, b/c table[0][0] is not a value
		j = floor((pt->logT - min_y)/step_y) + 1;				//+1, b/c table[0][0] is not a value
	}


	//Assign Points for interpolation.	
	z_4.metal = table[i][0];   z_4.logT = table[0][j+1]; z_4.cooling_rate = table[i][j+1];
	z_3.metal = table[i+1][0]; z_3.logT = table[0][j+1]; z_3.cooling_rate=table[i+1][j+1];
	z_2.metal = table[i+1][0]; z_2.logT = table[0][j];   z_2.cooling_rate = table[i+1][j];
	z_1.metal = table[i][0];   z_1.logT = table[0][j];   z_1.cooling_rate = table[i][j];

	//Print Diagnostics
	printf("(i,j): (%i, %i)\n",i,j);
	printf("(min_x, min_y) : (%.2f, %.2f)\n",min_x,min_y);
	printf("(step_x, step_y) : (%.2f, %.2f)\n",step_x, step_y);
	printf("\nz1: %.2f    z2: %.2f\nz3: %.2f   z4: %.2f\n\n", z_1.cooling_rate, z_2.cooling_rate, 
					z_3.cooling_rate, z_4.cooling_rate);


	//Interpolate. See section 3.6, Num. Recip. in C
	t = (pt->metal - z_1.metal)/(z_2.metal - z_1.metal);
	u = (pt->logT - z_1.logT)/(z_4.logT - z_1.logT);

	printf("t,u : %f %f\n",t,u);

	result = (1 - t)*(1 - u)*z_1.cooling_rate + t*(1 - u)*z_2.cooling_rate + 
					 t*u*z_3.cooling_rate + (1 - t)*u*z_4.cooling_rate;



	return result;
}

*/

#endif
