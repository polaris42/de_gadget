/************************************************
Title: dark_energy
Author: Jared Coughlin
Date: 1/16/15
Purpose: Contains all functions related to handling 
         time-dependent dark energy in gadget2


Notes:
************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <mpi.h>
#include <math.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_spline.h>
#include "allvars.h"
#include "proto.h"



/***********************
       dark_init
***********************/
void dark_init(void)
{
   // Driver function for setting up DE assets

   // Check for errors to make sure only one DE model is turned on
   dark_check();

   #ifdef SCALARFIELD
      // Both quintessence and k-essencec have the exact same 
      // procedure (though their w_vs_a.txt files will be different (and
      // how those files are generated differ, but that's outside of gadget)) 

      // Read w_vs_a.txt file
      scalar_read();

      // Tabulate the dark factor so a simple interpolation can be 
      // performed when getting H. This is done for reasons of speed.
      tabulate_dark_factor();
   #endif

   #ifdef GCG
      set_up_gcg();
   #endif
}



/***********************
      dark_check
***********************/
void dark_check(void)
{
   // This function makes sure that only one dark energy model is
   // turned on.

   int total = 0; 

   // Make sure something is chosen (and only one)
   #ifdef SCALARFIELD
      total++;
   #endif

   #ifdef GCG
      total++;
   #endif

   #ifdef DARKPARAM
      total++;
   #endif

   if((total == 0) || (total > 1))
   {
      printf("Error, DARKENERGY on but either no models chosen or\n");
      printf("more than one model is chosen!\n");
      endrun(1);
   } 
}



/***********************
      scalar_read
***********************/
void scalar_read(void)
{
   // This function reads in the w_vs_a.txt file.

   // Can only use this code if SCALARFIELD is on, b/c otherwise
   // the variables it needs from All won't be defined

   #ifdef SCALARFIELD
      FILE *read_file;          // File to be read
      int i;
      int j;
      int numcomments = 0;
      int numtotallines = 0;
      double dummy; // The w file has 3 columns: a, z, and w. This is used to hold the z data
      
      All.nlines = 0;           // Number of entries. Needed for interpolation, spline, and integral
      
      // Open file for reading
      if(!(read_file = fopen(All.dark_file, "r")))
      {
         printf("Error, could not open w_vs_a file for reading!\n");
         endrun(1);
      }

      // Get the number of lines in the file
      while(EOF != (i = getc(read_file)))
      {
         if(i == '#')
         {
            // This is a comment
            numcomments++;
         }

         if(i == '\n')
         {
            numtotallines++;
         }
      }

      // The number of data entries (All.nlines) is simply the total number of lines
      // (numtotallines) minus the number of comments (numcomments)
      All.nlines = numtotallines - numcomments;

      // Go back to the beginning of the file (go to where actual data starts)
      rewind(read_file);

      i = 0;

      // All of the comments are at the beginning of the file, so all we have to do
      // is skip the first numcomments lines.
      while(i < numcomments)
      {
         j = getc(read_file);

         if(j == '\n')
         {
            i++;
         }
      }

      // Allocate memory
      if(!(All.w = (double *)malloc(All.nlines * sizeof(double))))
      {
         printf("Error, could not allocate memory for w_array!\n");
         endrun(1);
      }

      if(!(All.scale_factor = (double *)malloc(All.nlines * sizeof(double))))
      {
         printf("Error, could not allocate memory for scale factor!\n");
         endrun(1);
      }

      if(!(All.integrand_array = (double *)malloc(All.nlines * sizeof(double))))
      {
         printf("Error, could not allocate memory for integrand array!\n");
         endrun(1);
      }

      // Actually read the data from the file
      for(i = 0; i < All.nlines; i++)
      {
         fscanf(read_file, "%lf %lf %lf\n", &All.scale_factor[i], &dummy, &All.w[i]);

         // Use the data to get the integrand of dark factor
         All.integrand_array[i] = 3.0 * (1.0 + All.w[i]) / All.scale_factor[i];
      }

      // Close the file
      fclose(read_file);
   #endif
}



/***********************
      set_up_gcg
***********************/
void set_up_gcg(void)
{
   // This probably didn't need it's own function, as all it
   // does is set some constants, but whatever.

   #ifdef GCG
      // Set omega_m_star (defined in Amendola eq. 8.240)
      All.Omega_m_star = All.GCG_B / (All.GCG_A + All.GCG_B);

      // Set up GCG_C and GCG_D. These are convenience variables of
      // my own desing. See pg. 2 of "GCG EOS" notes in the black binder
      // for where they come from

      All.GCG_C = All.Omega_m_star / (1.0 - All.Omega_m_star);

      All.GCG_D = 3.0 * (1.0 + All.GCG_Alpha);
   #endif
}



/***********************
  tabulate_dark_factor
***********************/
void tabulate_dark_factor(void)
{
   // This function creates a table of dark factor values and their 
   // corresponding scale factor values. This means that the integral 
   // only has to be done once, at the beginning of the run, and we can
   // simply interpolate within this table whenever we need to get H.

   #ifdef SCALARFIELD
      int i;
      gsl_interp_accel *accel;
      gsl_spline *spline;

      // Initialize the spline
      spline = gsl_spline_alloc(gsl_interp_cspline, All.nlines);
      accel = gsl_interp_accel_alloc();

      gsl_spline_init(spline, All.scale_factor, All.integrand_array, All.nlines);

      // Allocate memory for df_table
      if(!(All.df_table = (double *)malloc(All.nlines * sizeof(double))))
      {
         printf("Error, could not allocate memory for df_table!\n");
         endrun(1);
      }

      // Get dark factor
      for(i = 0; i < All.nlines; i++)
      {
         // Progress bar
         printf("Tabulating df table: %d of %d\n", i, All.nlines); 

         // Evaluate integ_a^1
         All.df_table[i] = gsl_spline_eval_integ(spline, All.scale_factor[i], 1.0, accel);

         // Take exponential of integ
         All.df_table[i] = exp(All.df_table[i]);
      }

      // Clean
      gsl_spline_free(spline);
      gsl_interp_accel_free(accel);

      // Set up interpolation spline for reading from df_table in get_dark_factor
      // These are variables that are global, so that I don't have to reallocate and
      // free them every time get_dark_factor is called. 
      All.df_spline = gsl_spline_alloc(gsl_interp_cspline, All.nlines);
      All.df_accel = gsl_interp_accel_alloc();
      gsl_spline_init(All.df_spline, All.scale_factor, All.df_table, All.nlines);
   #endif
}



/***********************
    get_dark_factor
***********************/
double get_dark_factor(double a)
{
   // This function uses the read-in data to calculate the dark
   // factor, which is the exp term in Amendola
   // eq. 2.84. I'm doing it with a instead of z, though.

   double dark_factor = 0.0;   // This is exp(int_a^1 integrand) in Amendola eq. 2.84

   // Quintessence and k-essence
   #ifdef SCALARFIELD
      // Interpolate within df_table to get proper value. Use a global gsl_spline to do this
      // so I don't constantly have to allocate and free every time the function is called.
      dark_factor = gsl_spline_eval(All.df_spline, a, All.df_accel);
   #endif

   // Generalized Chaplygin Gas
   #ifdef GCG
      // See "GCG EOS" notes in black binder for this deriv.

      dark_factor = (1.0 / a) * pow((pow(a, All.GCG_D) + All.GCG_C) / (1.0 + All.GCG_C), 3.0 / All.GCG_D);      
   #endif

   // Linder02 parameterization of w(a)
   #ifdef DARKPARAM
      // See my notes from 2/26/15 for this deriv

      dark_factor = pow(a, -3.0 * (1.0 + All.dp_w0 + All.dp_wa)) * exp(-3.0 * All.dp_wa * (1.0 - a));
   #endif

   return dark_factor;
}



/***********************
       de_clean
***********************/
void de_clean(void)
{
   // This function is called at the end of a run and 
   // simply frees all of the memory for the global arrays
   // associated with de.

   #ifdef SCALARFIELD
      free(All.integrand_array);
      free(All.scale_factor);
      free(All.w);
      free(All.df_table);
      gsl_interp_accel_free(All.df_accel);
      gsl_spline_free(All.df_spline);
   #endif
}
