/***************************************************************
Author: Ali Snedden
Date: //

NSH a
Dept. of Physics
Univ. of Notre Dame

Purpose:
	This file is to print out the options that I have enabled\n, to prevent
	confusion later.
****************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <mpi.h>
#include <unistd.h>

#include "allvars.h"
#include "proto.h"

void printOptionsEnabled(void)
{
	//Prevent Conflicting options.
	#ifdef ADIABATIC_SWITCH_ON_4_ALL_NGB_GAS
	#ifdef METAL_FEEDBACK_ONLY
		if(ThisTask == 0){
			fprintf(stderr, "ERROR!! ADIABATIC_SWITCH_ON_4_ALL_NGB_GAS and "
						"METAL_FEEDBACK_ONLY\nare simultaneously enabled!!! These are "
						"conflicting options. Consult the\nMakefile\n");
		}
		endrun(5021);
	#endif
	#ifdef TURNOFF_ADIABATIC_SWITCH
		if(ThisTask == 0){
			fprintf(stderr, "ERROR!! ADIABATIC_SWITCH_ON_4_ALL_NGB_GAS and "
						"TURNOFF_ADIABITC_SWITCH\nare simultaneously enabled!!! These are"
						" conflicting options. Consult the\nMakefile\n");
		}
		endrun(5021);
	#endif
	#endif

	#ifdef METAL_FEEDBACK_ONLY
	#ifdef TURNOFF_ADIABATIC_SWITCH
		if(ThisTask == 0){
			fprintf(stderr, "ERROR!! TURNOFF_ADIABATIC_SWITCH and METAL_FEEDBACK_ONLY\n"
						"are simultaneously enabled!!! These are conflicting options. Consult the\n"
						"Makefile\n");
		}
		endrun(5021);
	#endif
	#endif


	//If not using Jeans Criterion, ensure proper options are enabled.
	#ifdef NO_JEANS_CRITERION
	#ifndef NH_MIN
		if(ThisTask == 0){
			fprintf(stderr, "ERROR!! NO_JEANS_CRITERION and NH_MIN and T_MAX\n"
						"MUST be simultaneously enabled!!! Consult the Makefile\n");
		}
		endrun(5022);
	#endif
	#ifndef T_MAX
		if(ThisTask == 0){
			fprintf(stderr, "ERROR!! NO_JEANS_CRITERION and NH_MIN and T_MAX\n"
						"MUST be simultaneously enabled!!! Consult the Makefile\n");
		}
		endrun(5022);
	#endif
	#ifndef OVERDENSITY
		if(ThisTask == 0){
			fprintf(stderr, "ERROR!! NO_JEANS_CRITERION and NH_MIN and T_MAX\n"
						"MUST be simultaneously enabled!!! Consult the Makefile\n");
		}
		endrun(5022);
	#endif
	#endif

	#ifdef NH_MIN
	#ifndef NO_JEANS_CRITERION
		if(ThisTask == 0){
			fprintf(stderr, "ERROR!! NO_JEANS_CRITERION and NH_MIN and T_MAX\n"
						"MUST be simultaneously enabled!!! Consult the Makefile\n");
		}
		endrun(5022);
	#endif
	#ifndef T_MAX
		if(ThisTask == 0){
			fprintf(stderr, "ERROR!! NO_JEANS_CRITERION and NH_MIN and T_MAX\n"
						"MUST be simultaneously enabled!!! Consult the Makefile\n");
		}
		endrun(5022);
	#endif
	#ifndef OVERDENSITY
		if(ThisTask == 0){
			fprintf(stderr, "ERROR!! NO_JEANS_CRITERION and NH_MIN and T_MAX\n"
						"MUST be simultaneously enabled!!! Consult the Makefile\n");
		}
		endrun(5022);
	#endif
	#endif

	#ifdef T_MAX
	#ifndef NO_JEANS_CRITERION
		if(ThisTask == 0){
			fprintf(stderr, "ERROR!! NO_JEANS_CRITERION and NH_MIN and T_MAX\n"
						"MUST be simultaneously enabled!!! Consult the Makefile\n");
		}
		endrun(5022);
	#endif
	#ifndef NH_MIN
		if(ThisTask == 0){
			fprintf(stderr, "ERROR!! NO_JEANS_CRITERION and NH_MIN and T_MAX\n"
						"MUST be simultaneously enabled!!! Consult the Makefile\n");
		}
		endrun(5022);
	#endif
	#ifndef OVERDENSITY
		if(ThisTask == 0){
			fprintf(stderr, "ERROR!! NO_JEANS_CRITERION and NH_MIN and T_MAX\n"
						"MUST be simultaneously enabled!!! Consult the Makefile\n");
		}
		endrun(5022);
	#endif
	#endif

	#ifdef OVERDENSITY
	#ifndef NO_JEANS_CRITERION
		if(ThisTask == 0){
			fprintf(stderr, "ERROR!! NO_JEANS_CRITERION and NH_MIN and T_MAX\n"
						"MUST be simultaneously enabled!!! Consult the Makefile\n");
		}
		endrun(5022);
	#endif
	#ifndef T_MAX
		if(ThisTask == 0){
			fprintf(stderr, "ERROR!! NO_JEANS_CRITERION and NH_MIN and T_MAX\n"
						"MUST be simultaneously enabled!!! Consult the Makefile\n");
		}
		endrun(5022);
	#endif
	#ifndef NH_MIN
		if(ThisTask == 0){
			fprintf(stderr, "ERROR!! NO_JEANS_CRITERION and NH_MIN and T_MAX\n"
						"MUST be simultaneously enabled!!! Consult the Makefile\n");
		}
		endrun(5022);
	#endif
	#endif


  //Ensure only 1 IMF is selected.
  #ifdef SAL_A_IMF
  #ifdef CHABRIER_IMF
  	if(ThisTask == 0){
			fprintf(stderr, "ERROR!! SAL_A_IMF and CHABRIER_IMF\n"
						"CANNOT be simultaneously enabled!!! Consult the Makefile\n");
		}
		endrun(5022);  
  #endif
  #endif


  //Ensure only one feedback integration scheme is used, trapezoidal is default
  #ifdef USE_GSL
  	if(ThisTask == 0){
			fprintf(stderr, "ERROR!! USE_GSL may not be enabled!!!\n"
						"You must correctly add metal yields from TypeII SN at 8 M_sun, otherwise\n"
            "gsl_integration_qag() will not work b/c I currently floor the metal\n"
            "mass fraction for mass < 13 in TypeII_Zi_yield_mass_frac().  This makes\n"
            "it damn near impossible to iteratively integrate the funcitons\n"
            "integrate_pzII_imf() and integrate_1_minus_wm_minus_pzII_Z_imf() when \n"
            "the bounds dip below 13M_sun. To use the GSL functions, you need to \n"
            "somehow decide on a value for the metal yields at 8M_sun so I don't \n"
            "have to floor the metal mass frac. Using this code option will require\n"
            "commenting out this error message and adjusting feedback_w_kernal.c\n");
		}
		endrun(5022);
  #ifdef USE_SIMPSON
  	if(ThisTask == 0){
			fprintf(stderr, "ERROR!! USE_GSL and USE_SIMPSON\n"
						"CANNOT be simultaneously enabled!!! Consult the Makefile\n");
		}
		endrun(5022);
  #endif
  #endif


	if(ThisTask == 0)
	{
		/******************* Basic Operation mode of code **********************/
		#ifdef PERIODIC
			printf("PERIODIC enabled\n");
		#endif

		#ifdef    UNEQUALSOFTENINGS
 			printf("UNEQUALSOFTENINGS  enabled\n");
		#endif
	 
  
		/******************** Things that are always recommended ****************/
		#ifdef    PEANOHILBERT
			printf("PEANOHILBERT  enabled\n");
		#endif

		#ifdef    WALLCLOCK
			printf("WALLCLOCK  enabled\n");
		#endif
  
  
		/************************** Astrophysics Options *************************/
		#ifdef  COOLING       
			printf("COOLING  enabled\n");
		#endif

		#ifdef  SFR           
			printf("SFR  enabled\n");
		#endif

		#ifdef  STELLARAGE   
			printf("STELLARAGE  enabled\n");
		#endif

		#ifdef  METALS        
			printf("METALS  enabled\n");
		#endif

		#ifdef  FEEDBACK      
			printf("FEEDBACK  enabled\n");
		#endif

		#ifdef  FEEDBACK_W_SPH_KERNAL 
			printf("FEEDBACK_W_SPH_KERNAL  enabled\n");
		#endif

		#ifdef  UVBACKGROUND
			printf("UVBACKGROUND  enabled\n");
		#endif

		#ifdef  BLACK_HOLES
			printf("BLACK_HOLES  enabled\n");
		#endif

		#ifdef NO_JEANS_CRITERION
			printf("NO_JEANS_CRITERION enabled\n");
		#endif

		#ifdef NH_MIN
			printf("NH_MIN = %f enabled\n", NH_MIN);
		#endif

		#ifdef T_MAX
			printf("T_MAX = %i enabled\n", T_MAX);
		#endif

		#ifdef OVERDENSITY
			printf("OVERDENSITY = %i enabled\n", OVERDENSITY);
		#endif

    #ifdef SAL_A_IMF
      printf("SAL_A_IMF enabled\n");
    #endif

    #ifdef CHABRIER_IMF
      printf("CHABRIER_IMF enabled\n");
    #endif

    #ifdef USE_GSL
      printf("USE_GSL enabled\n");
    #endif
 
   #ifdef USE_SIMPSON
      printf("USE_SIMPSON enabled\n");
    #endif

   #ifdef N_INTEGRATION_STEPS
      printf("N_INTEGRATION_STEPS = %i enabled\n", N_INTEGRATION_STEPS);
    #endif

		#ifdef ADIABATIC_SWITCH_ON_4_ALL_NGB_GAS
			printf("ADIABATIC_SWITCH_ON_4_ALL_NGB_GAS  enabled\n");
		#endif

		#ifdef METAL_FEEDBACK_ONLY
			printf("METAL_FEEDBACK_ONLY  enabled\n");
		#endif
 
		#ifdef TURNOFF_ADIABATIC_SWITCH
			printf("TURNOFF_ADIABATIC_SWITCH  enabled\n");
		#endif


		/************************* Metals to Track Options ************************/
		#ifdef  CARBON         
			printf("CARBON  enabled\n");
		#endif

		#ifdef  NITROGEN
			printf("NITROGEN  enabled\n");
		#endif

		#ifdef  OXYGEN      
			printf("OXYGEN  enabled\n");
		#endif

		#ifdef  FLORINE
			printf("FLORINE  enabled\n");
		#endif

		#ifdef  NEON
			printf("NEON  enabled\n");
		#endif

		#ifdef  SODIUM
			printf("SODIUM  enabled\n");
		#endif

		#ifdef  MAGNESIUM  
			printf("MAGNESIUM  enabled\n");
		#endif

		#ifdef  ALUMINUM
			printf("ALUMINUM  enabled\n");
		#endif

		#ifdef  SILICON   
			printf("SILICON  enabled\n");
		#endif

		#ifdef  PHOSPHORUS
			printf("PHOSPHORUS  enabled\n");
		#endif

		#ifdef  SULFUR
			printf("SULFUR  enabled\n");
		#endif

		#ifdef  CHLORINE
			printf("CHLORINE  enabled\n");
		#endif

		#ifdef  ARGON
			printf("ARGON  enabled\n");
		#endif

		#ifdef  POTASSIUM
			printf("POTASSIUM  enabled\n");
		#endif

		#ifdef  CALCIUM       
			printf("CALCIUM  enabled\n");
		#endif

		#ifdef  SCANDIUM    
			printf("SCANDIUM  enabled\n");
		#endif

		#ifdef  TITANIUM   
			printf("TITANIUM  enabled\n");
		#endif

		#ifdef  VANADIUM
			printf("VANADIUM  enabled\n");
		#endif

		#ifdef  CHROMIUM     
			printf("CHROMIUM  enabled\n");
		#endif

		#ifdef  MANGANESE   
			printf("MANGANESE  enabled\n");
		#endif

		#ifdef  IRON       
			printf("IRON  enabled\n");
		#endif

		#ifdef  COBALT
			printf("COBALT  enabled\n");
		#endif

		#ifdef  NICKEL
			printf("NICKEL  enabled\n");
		#endif

		#ifdef  COPPER
			printf("COPPER  enabled\n");
		#endif

		#ifdef  ZINC
			printf("ZINC  enabled\n");
		#endif


		/***************************** TreePM Options ************************/
		#ifdef    PMGRID
			printf("PMGRID = %i  enabled\n", PMGRID);
		#endif

		#ifdef    PLACEHIGHRESREGION
			printf("PLACEHIGHRESREGION = %f enabled\n", PLACEHIGHRESREGION);
		#endif

		#ifdef    ENLARGEREGION
			printf("ENLARGEREGION = %f enabled\n",ENLARGEREGION);
		#endif

		#ifdef    ASMTH
			printf("ASMTH = %f enabled\n",ASMTH);
		#endif

		#ifdef    RCUT
			printf("RCUT = %f enabled\n",RCUT);
		#endif
  

		/************************ Single/Double Precision **********************/
		#ifdef    DOUBLEPRECISION      
			printf("DOUBLEPRECISION  enabled\n");
		#endif

		#ifdef    DOUBLEPRECISION_FFTW      
 			printf("DOUBLEPRECISION_FFTW  enabled\n");
		#endif
 

		/************************ Time integration options *********************/
		#ifdef    SYNCHRONIZATION
			printf("SYNCHRONIZATION  enabled\n");
		#endif

		#ifdef    FLEXSTEPS
			printf("FLEXSTEPS  enabled\n");
		#endif

		#ifdef    PSEUDOSYMMETRIC
			printf("PSEUDOSYMMETRIC  enabled\n");
		#endif

		#ifdef    NOSTOP_WHEN_BELOW_MINTIMESTEP
			printf("NOSTOP_WHEN_BELOW_MINTIMESTEP  enabled\n");
		#endif

		#ifdef    NOPMSTEPADJUSTMENT
			printf("NOPMSTEPADJUSTMENT  enabled\n");
		#endif
  

		/******************************** Output ********************************/
		#ifdef    WRITEGNUPLOT      
			printf("WRITEGNUPLOT  enabled\n");
		#endif

		#ifdef    CSVOUTPUT        
			printf("CSVOUTPUT  enabled\n");
		#endif

		#ifdef    HAVE_HDF  
			printf("HAVE_HDF  enabled\n");
		#endif

		#ifdef    OUTPUTPOTENTIAL      
			printf("OUTPUTPOTENTIAL  enabled\n");
		#endif

		#ifdef    OUTPUTACCELERATION
			printf("OUTPUTACCELERATION  enabled\n");
		#endif

		#ifdef    OUTPUTCHANGEOFENTROPY  
			printf("OUTPUTCHANGEOFENTROPY  enabled\n");
		#endif

		#ifdef    OUTPUTTIMESTEP
			printf("OUTPUTTIMESTEP  enabled\n");
		#endif

		#ifdef    OUTPUT_TEMP_FOR_U     
			printf("OUTPUT_TEMP_FOR_U  enabled\n");
		#endif

		#ifdef    OUTPUT_AGE_FOR_POTENTIAL  
			printf("OUTPUT_AGE_FOR_POTENTIAL  enabled\n");
		#endif


		/********************** Things for special behaviour ***********************/
		#ifdef    NOGRAVITY     
			printf("NOGRAVITY  enabled\n");
		#endif

		#ifdef    NOTREERND 
			printf("NOTREERND  enabled\n");
		#endif

		#ifdef    NOTYPEPREFIX_FFTW        
			printf("NOTYPEPREFIX_FFTW  enabled\n");
		#endif

		#ifdef    LONG_X
			printf("LONG_X = %f enabled\n", LONG_X);
		#endif

		#ifdef    LONG_Y
			printf("LONG_Y = %f enabled\n", LONG_Y);
		#endif

		#ifdef    LONG_Z
			printf("LONG_Z = %f enabled\n", LONG_Z);
		#endif

		#ifdef    TWODIMS
			printf("TWODIMS  enabled\n");
		#endif

		#ifdef    SPH_BND_PARTICLES
			printf("SPH_BND_PARTICLES  enabled\n");
		#endif

		#ifdef    NOVISCOSITYLIMITER
			printf("NOVISCOSITYLIMITER  enabled\n");
		#endif

		#ifdef    COMPUTE_POTENTIAL_ENERGY
			printf("COMPUTE_POTENTIAL_ENERGY  enabled\n");
		#endif

		#ifdef    LONGIDS
			printf("LONGIDS  enabled\n");
		#endif

		#ifdef    ISOTHERM_EQS
			printf("ISOTHERM_EQS  enabled\n");
		#endif

		#ifdef    ADAPTIVE_GRAVSOFT_FORGAS
			printf("ADAPTIVE_GRAVSOFT_FORGAS  enabled\n");
		#endif

		#ifdef    SELECTIVE_NO_GRAVITY
 			printf("SELECTIVE_NO_GRAVITY  enabled\n");
		#endif


		/********************** Testing and Debugging options **********************/
		#ifdef    FORCETEST
 			printf("FORCETEST  enabled\n");
		#endif

		#ifdef    MAKEGLASS
			printf("MAKEGLASS  enabled\n");
		#endif


      /********************** Dark Energy Options *************************/
      #ifdef DARKENERGY
         printf("DARKENERGY enabled\n");

         #ifdef SCALARFIELD
            printf("SCALARFIELD enabled\n");
         #endif

         #ifdef GCG
            printf("GCG enabled\n");
         #endif

         #ifdef DARKPARAM
            printf("DARKPARAM enabled\n");
         #endif
      #endif
	}
}
