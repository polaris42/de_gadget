/***************************************************************************
Author: 	Ali Snedden 
Modified: 
Date: 5/1/13

NSH 341a
Dept. of Physics
University of Notre Dame

Purpose:
	Here I am implementing the feedback from Kobayashi (2004). This was originally
	copied from hydra.c.

	Computation of feedback including distrubution of metals and entropy 
 	generation. This MUST come after computing acceleration. Since feedback
	generates entropy, it MUST occur during the same timestep as the cooling
	and other hydro forces.
	
	Kobayashi(2004) = K04
	Springel & Hernquist(2002) = SH02

	WARNING!!! Check to make sure that we are using the PHYSICAL radius!!!!!
						 As of 4/23/13, I am NOT using it.

	NOTE : stars produce metals, so the metals lost by the star is DIFFERENT
				 from the metals distributed to the gas particles.
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <mpi.h>
#include <gsl/gsl_math.h>
#include "allvars.h"
#include "proto.h"

#ifdef COOLING
#ifdef SFR
#ifdef METALS
#ifdef FEEDBACK
#ifndef FEEDBACK_W_SPH_KERNAL

/*	This contains necessary definitions used in the feedback routines. See
		 K06 : Kobayashi, 2006 ApJ 635, 1145
 		 K04 : Kobayashi, 2004 MNRAS 347, 740
 		 H02 : Heger, 2002 Springer-Verlag, 369, see arXiv:astro-ph/0112059 
 		 E04 : Eldridge & Tout, 2004 MNRAS, 353, 87  

		Must change IMF exponent and normalization manually. See feedback.c
 */
static double hubble_a;         //H(t) C&O eqn 29.122, OmegaKurvature != 0, OmegaRel=0
static double a3;
static double atime;
static double dtMass			= 0;			//Change star particle mass.							
static double dtEntropy		= 0;			//Entropy from star particle distr. to gas part.
static double dtEnergy		= 0;			//units = (internal energy / internal mass)
static double dt_Z_Mass_dist[NELEMENTS];				//Metal mass distributed to gas part.
static double dt_Z_Mass_lost[NELEMENTS];			// Metals lost by star.
static double m_2l		= 8;										// SNe II lower limit
static double m_2u		= 50;										// SNe II upper limit
static double m_2l_bh	= 25;										// Black Hole lower limit.
static double m_1l    = 3;										// SNe Ia lower limit
static double m_1u 		= 8;										// SNe Ia upper limit
static double m_1l_RG	= 0.8;									// RedGiant-WhiteDwarf system lower lim
static double m_1u_RG	= 1.5;									// RG-WD system upper limit
static double m_1l_MS	= 1.8;									// MainSeq-WhiteDwarf system lower limit
static double m_1u_MS	= 2.6;									// MS-WD system upper limit

#define N_INTEGRATION_STEPS		100		//Number of steps used in integration scheme.


#ifdef PERIODIC
static double boxSize;					//Comoving size of the volume (I think?)
static double boxHalf;

#ifdef LONG_X
static double boxSize_X, boxHalf_X;
#else
#define boxSize_X boxSize
#define boxHalf_X boxHalf
#endif
#ifdef LONG_Y
static double boxSize_Y, boxHalf_Y;
#else
#define boxSize_Y boxSize
#define boxHalf_Y boxHalf
#endif
#ifdef LONG_Z
static double boxSize_Z, boxHalf_Z;
#else
#define boxSize_Z boxSize
#define boxHalf_Z boxHalf
#endif
#endif


/*! This function is the driver routine for the calculation of feedback, including
    change in mass and entropy of star particles. NOTE star particles and DM particles
  	are intermixed together after the block of SPH particles in P[].

		NOTE: 
			It is CRUCIAL for this routine to come AFTER the hydro_force(). If it comes
			before then the change in entropy will NOT be saved. The SphP[].Entropy is 
			updated in advance_and_find_timesteps().
 */
/*void feedback(void)
{
	//Does feedback using static FeedbackRadius. Not a good method to use.
	#ifndef FEEDBACK_W_SPH_KERNAL
		get_number_of_gas_nearby();

		distribute_metals_and_energy();
	#endif

	#ifdef FEEDBACK_W_SPH_KERNAL
		update_star_hsml();
		get_feedback_normalization();
		distribute_metals_and_energy_w_kernal();
	#endif
}*/



/************************************************************************************
	 This function counts the number of gas particles near each star particle. This
	 is so we can have the correct normalization when we do the feedback. It searches
	 both the LOCAL and NON-LOCAL processors.
 ************************************************************************************/
void get_number_of_gas_nearby(void)
{
  long long ntot;					//Initial GLOBAL Number of SPH part. needing updated 
	long long ntotleft;			//GLOBAL Num SPH part. left needing updated
  int i;
	int j;									//Index for Number of processors
	int k;
	int n;
	int ngrp;
	int maxfill;						//Number of structs that fit in CommBuffer
	int source;							//Index 2go GasPartSearchDataPartialResult[] to  GasPartSearchDataIn[]
	int ndone;							//Number of particle that have been updated.
  int level;							//
	int sendTask;						//Index of sending Task
	int recvTask;						//Index of receiving Task
	int nexport;						//Total Number of LOCAL SphP to export
	int place;							//Index to go from GasPartSearchDataIn[] to P[]
  int * nbuffer;					//Number of particles in buffer.
	int * noffset;					//Given a recvTask, return corresponding sending particles location in CommBuffer
	int * nsend_local;			//Number LOCAL of particles to send to j'th Task
	int * nsend;						//Combination of all Task's nsend_local[]
	int * numlist;					//Holds 'NumStarUpdate' from each task(# SphP needing updated)
	int * ndonelist;
	FLOAT radius; //= All.FeedbackRadius / All.Time; 	//Put physical units into comoving.
	double dt;
  double tstart, tend, sumt, sumcomm;
  double timecomp = 0, timecommsumm = 0, timeimbalance = 0, sumimbalance;
	MPI_Status status;

	#ifdef PERIODIC
  	boxSize = All.BoxSize;
  	boxHalf = 0.5 * All.BoxSize;
		#ifdef LONG_X
		  boxHalf_X = boxHalf * LONG_X;
		  boxSize_X = boxSize * LONG_X;
		#endif
		#ifdef LONG_Y
		  boxHalf_Y = boxHalf * LONG_Y;
		  boxSize_Y = boxSize * LONG_Y;
		#endif
		#ifdef LONG_Z
  		boxHalf_Z = boxHalf * LONG_Z;
		  boxSize_Z = boxSize * LONG_Z;
		#endif
	#endif
	if(All.ComovingIntegrationOn){
		radius = All.FeedbackRadius / All.Time;
	}else{
		radius = All.FeedbackRadius;
	}


	phys_time = get_phys_time(All.Time);		//Write age of universe (My) to phys_time.

  /* `NumStarUpdate' = number of star particles on this processor that want a 
      force update and will also generate feedback.
			Stars should be at end of P[], but we'll search the whole damn thing anyways.
	*/
  for(n = 0, NumStarUpdate = 0; n < NumPart; n++)				// <- could speed up here
  {
		dt = phys_time - P[n].StarAge;	//Time (My) passes since star formed
    if(P[n].Type == 4 && P[n].Ti_endstep == All.Ti_Current && dt > 5.0)	
		{
    	NumStarUpdate++;
		}
  }


	//Get Number of Particles needing updated from All tasks
  numlist = malloc(NTask * sizeof(int) * NTask);
  MPI_Allgather(&NumStarUpdate, 1, MPI_INT, numlist, 1, MPI_INT, MPI_COMM_WORLD);
  for(i = 0, ntot = 0; i < NTask; i++)
    ntot += numlist[i];															//Find GLOBAL number stars to update
  free(numlist);


	//Allocate Memory
  noffset 		= malloc(sizeof(int) * NTask);	/* offsets of bunches in common list */
  nbuffer 		= malloc(sizeof(int) * NTask);
  nsend_local = malloc(sizeof(int) * NTask);
  nsend 			= malloc(sizeof(int) * NTask * NTask);
  ndonelist 	= malloc(sizeof(int) * NTask);


  i = N_gas;					/* first non-SPH particle for this task */
  ntotleft = ntot;		/* particles left for all tasks together */



	/*
		Iterate through all GLOBAL Star particles needing updated, until no more GLOBAL
		particles are left to communicate. Find number of nearby gas particles
	*/
	while(ntotleft > 0)
	{
		for(j = 0; j < NTask; j++){
			nsend_local[j] = 0;
		}

		tstart = second();


		/*
			Iterate through Stars needing an update. Count number of nearby on LOCAL
			and NON-LOCAL particles. Exportflag[] is set in 
			get_local_number_gas_part_nearby().
		*/
		for(nexport=0, ndone=0; i< NumPart && nexport< All.BunchSizeGasSearch - NTask; i++)
		{
			dt = phys_time - P[i].StarAge;	//Time (My) passes since star formed

			/* Only Active Stars over 5My old w/ nearby gas part. */
			if(P[i].Type == 4 && P[i].Ti_endstep == All.Ti_Current && dt > 5.0)	
			{
				ndone++;

				for(j=0; j<NTask; j++){
					Exportflag[j] = 0;								//Reset list of Tasks to export to
				}

				P[i].NumGasNearby = get_number_gas_part_nearby(radius, i, 0);
			

				for(j=0; j<NTask; j++)
				{
					/* Are Nearby Particles on other Processors? */
					if(Exportflag[j])									//set in get_local_number_gas_part_nearby
					{
						for(k = 0; k < 3; k++)
						{
							GasPartSearchDataIn[nexport].Pos[k] = P[i].Pos[k];
						}
						GasPartSearchDataIn[nexport].ID 	 	= P[i].ID;
						GasPartSearchDataIn[nexport].Task 	= j;
						GasPartSearchDataIn[nexport].Index = i;
		
						nexport++;
						nsend_local[j]++;
					}	
				}
			}
		}
		tend = second();
		timecomp += timediff(tstart, tend);

		//Group GasPartSearchDataIn[] by Task to be exported to, makes easier to communicat
		qsort(GasPartSearchDataIn, nexport, sizeof(struct gaspartsearchdata_in),gaspartsearch_compare_key);

		//Get offset of particles as a function of destination Task
		for(j = 1, noffset[0] = 0; j < NTask; j++){
			noffset[j] = noffset[j - 1] + nsend_local[j - 1];
		}

		tstart = second();

		/*
			Get number of particles to be exported by each Task and to which Task it 
			will be exported.
		*/
		MPI_Allgather(nsend_local, NTask, MPI_INT, nsend, NTask, MPI_INT, MPI_COMM_WORLD);
		
		tend = second();
		timeimbalance += timediff(tstart, tend);


		/* Get Number of nearby particles from NON-LOCAL particles. */
   	for(level = 1; level < (1 << PTask); level++)
	  {
	    tstart = second();
	    for(j = 0; j < NTask; j++)
			{
	      nbuffer[j] = 0;
			}

			//Complex way of sending/receiving particles between Tasks. I don't understand
	    for(ngrp = level; ngrp < (1 << PTask); ngrp++)
	    {
	      maxfill = 0;
	      for(j = 0; j < NTask; j++)
	    	{
					//Get Number of particles in GasPartSearchDataIn Buffer destined for ______
	    	  if((j ^ ngrp) < NTask)                   //This is bitwise XOR comparison
	    	    if(maxfill < nbuffer[j] + nsend[(j ^ ngrp) * NTask + j])
	    	      maxfill = nbuffer[j] + nsend[(j ^ ngrp) * NTask + j];
	    	}
	      if(maxfill >= All.BunchSizeGasSearch)
	      	break;

	      sendTask = ThisTask;
	      recvTask = ThisTask ^ ngrp;

        if(recvTask < NTask)
      	{
      	  if(nsend[ThisTask * NTask + recvTask] > 0 || 
             nsend[recvTask * NTask + ThisTask] > 0)
	        {
	          /* 
							Send and Recieve the particles. Blocks communication  until 
							send buffer is free and receive buffer is filled. The send and
							receive buffers point to different sections of the CommBuffer
						*/
	          MPI_Sendrecv(&GasPartSearchDataIn[noffset[recvTask]],
				               nsend_local[recvTask] * sizeof(struct gaspartsearchdata_in), MPI_BYTE,
				               recvTask, TAG_FEEDBACK_A, &GasPartSearchDataGet[nbuffer[ThisTask]],
				               nsend[recvTask * NTask + ThisTask] * sizeof(struct 
											 gaspartsearchdata_in), MPI_BYTE, recvTask, TAG_FEEDBACK_A, 
											 MPI_COMM_WORLD, &status);
	        }
	      }

        for(j = 0; j < NTask; j++)
	        if((j ^ ngrp) < NTask)
	          nbuffer[j] += nsend[(j ^ ngrp) * NTask + j];
      }
			tend = second();
			timecommsumm += timediff(tstart, tend);

			/* now count nearby particles for imported particles */
			tstart = second();
			for(j = 0; j < nbuffer[ThisTask]; j++)
			{
				GasPartSearchDataResult[j].NumGasNearby 	= get_number_gas_part_nearby(radius, j, 1);
				GasPartSearchDataResult[j].Task 					= GasPartSearchDataGet[j].Task;
				GasPartSearchDataResult[j].ID   					= GasPartSearchDataGet[j].ID;
				GasPartSearchDataResult[j].Pos[0] 				= GasPartSearchDataGet[j].Pos[0];
				GasPartSearchDataResult[j].Pos[1] 				= GasPartSearchDataGet[j].Pos[1];
				GasPartSearchDataResult[j].Pos[2] 				= GasPartSearchDataGet[j].Pos[2];
			}
			tend = second();
			timecomp += timediff(tstart, tend);

      /* do a block to measure imbalance */
      tstart = second();
      MPI_Barrier(MPI_COMM_WORLD);
      tend = second();
      timeimbalance += timediff(tstart, tend);


      /* get the result */
      tstart = second();
      for(j = 0; j < NTask; j++)
        nbuffer[j] = 0;
      for(ngrp = level; ngrp < (1 << PTask); ngrp++)
      {
        maxfill = 0;
        for(j = 0; j < NTask; j++)
	      {
	        if((j ^ ngrp) < NTask)
	          if(maxfill < nbuffer[j] + nsend[(j ^ ngrp) * NTask + j])
	            maxfill = nbuffer[j] + nsend[(j ^ ngrp) * NTask + j];
	      }
        if(maxfill >= All.BunchSizeGasSearch)
	        break;

        sendTask = ThisTask;
        recvTask = ThisTask ^ ngrp;

        if(recvTask < NTask)
	      {
	        if(nsend[ThisTask * NTask + recvTask] > 0 || 
             nsend[recvTask * NTask + ThisTask] > 0)
	        {
	          /*  
							Send and Recieve the Results. Blocks communication  until 
							send buffer is free and receive buffer is filled. The send and
							receive buffers point to different sections of the CommBuffer

							GasPartSearchDataResult        = sending buffer
							GasPartSearchDataPartialResult = receiving buffer
						*/
	          MPI_Sendrecv(&GasPartSearchDataResult[nbuffer[ThisTask]],
				              nsend[recvTask * NTask + ThisTask] * 
											sizeof(struct gaspartsearchdata_out), MPI_BYTE, recvTask, 
											TAG_FEEDBACK_B, &GasPartSearchDataPartialResult[noffset[recvTask]],
				              nsend_local[recvTask] * sizeof(struct gaspartsearchdata_out),
				              MPI_BYTE, recvTask, TAG_FEEDBACK_B, MPI_COMM_WORLD, &status);

	          /* add the result to the particles */
	          for(j = 0; j < nsend_local[recvTask]; j++)
		        {
							/*
								NOTE: GasPartSearchDataIn[source] and GasPartSearchDataPartialResult[source] are 
								referring to the same particles.
							*/
		          source = j + noffset[recvTask];					
		          place = GasPartSearchDataIn[source].Index;

							/* Error Check */
							if(P[place].ID == GasPartSearchDataPartialResult[source].ID)
							{
								P[place].NumGasNearby += GasPartSearchDataPartialResult[source].NumGasNearby;
							}
							else
							{
								fprintf(stderr, "ERROR!!!!\nP[%i].ID != GasPartSearchDataPartialResult"
																"[%i].ID mismatch!\n", place, source);
								endrun(668);
							}
		        }
	        }
	      }

        for(j = 0; j < NTask; j++)
	        if((j ^ ngrp) < NTask)
	          nbuffer[j] += nsend[(j ^ ngrp) * NTask + j];
      }
	    tend = second();
	    timecommsumm += timediff(tstart, tend);

      level = ngrp - 1;
		}

    //Total up the number of GLOBAL particles to update
    MPI_Allgather(&ndone, 1, MPI_INT, ndonelist, 1, MPI_INT, MPI_COMM_WORLD);
    for(j = 0; j < NTask; j++)
      ntotleft -= ndonelist[j];
	}

  free(noffset);
  free(nbuffer);
  free(nsend_local);
  free(nsend);
  free(ndonelist);

 
  /* collect some timing information */
  MPI_Reduce(&timecomp, &sumt, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
  MPI_Reduce(&timecommsumm, &sumcomm, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
  MPI_Reduce(&timeimbalance, &sumimbalance, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);

	/* Not used */
  if(ThisTask == 0)
  {
    All.CPU_FeedCompWalk += sumt / NTask;
    All.CPU_FeedCommSumm += sumcomm / NTask;
    All.CPU_FeedImbalance += sumimbalance / NTask;
  }
 tstart = second();
}




/*********************************************************************************
	Find the total number of particles within radius of P[target] 

	mode = 0   :  Local    particles, P[]
	mode = 1   :  NonLocal particles, GasPartSearchDataGet[]
**********************************************************************************/
long get_number_gas_part_nearby(FLOAT radius, int target, int mode)
{
	int startnode				= All.MaxPart;
	int n 							= 0;							//Ngblist[] index
	int j;
	long numngb					= 0; 							//Number of "nearby" gas part.
	long num_gas_nearby	= 0;							//Number of gas part w/in 'radius' of P[target]
	double dx 					= 0.0;
	double dy						= 0.0;
	double dz 					= 0.0;
	double r 						= 0.0;						// | P[target].Pos - P[j].Pos |
	FLOAT *Pos;

	/* In this case b/c boxHalf_* won't be set b/c they are set in 
		 get_number_of_gas_nearby which isn't called */
	#ifdef FEEDBACK_W_SPH_KERNAL
		#ifdef PERIODIC
  		boxSize = All.BoxSize;
  		boxHalf = 0.5 * All.BoxSize;
			#ifdef LONG_X
			  boxHalf_X = boxHalf * LONG_X;
			  boxSize_X = boxSize * LONG_X;
			#endif
			#ifdef LONG_Y
			  boxHalf_Y = boxHalf * LONG_Y;
			  boxSize_Y = boxSize * LONG_Y;
			#endif
			#ifdef LONG_Z
  			boxHalf_Z = boxHalf * LONG_Z;
		  	boxSize_Z = boxSize * LONG_Z;
			#endif
		#endif
	#endif


	if(mode == 0){
		Pos = P[target].Pos;
	}
	else{
		Pos = GasPartSearchDataGet[target].Pos;
	}


	/* Get number of LOCAL particles within 'radius' of the P[target]'s location */
	do
	{
		//numngb +=	ngb_treefind_gas_in_box(&Pos[0], radius, &startnode);
		numngb +=	ngb_treefind_variable(&Pos[0], radius, &startnode);

		/* from Ngblist find particles w/in 'radius' */
		for(n = 0; n < numngb; n++)
		{
			j = Ngblist[n];										//Get index of 'nearby' gas part.

			dx = Pos[0] - P[j].Pos[0];	
			dy = Pos[1] - P[j].Pos[1];
			dz = Pos[2] - P[j].Pos[2];

			#ifdef PERIODIC			/*  find the closest image in the given box size  */
	    	if(dx > boxHalf_X) { 		dx -= boxSize_X; 	}
	   		if(dx < -boxHalf_X){		dx += boxSize_X;	}
	    	if(dy > boxHalf_Y) {		dy -= boxSize_Y;	}
	    	if(dy < -boxHalf_Y){		dy += boxSize_Y;	}
	    	if(dz > boxHalf_Z) {		dz -= boxSize_Z;	}
	    	if(dz < -boxHalf_Z){		dz += boxSize_Z;	}
			#endif
	    r = sqrt(dx * dx + dy * dy + dz * dz);
	
			if( r <= radius)
			{
				num_gas_nearby++;
			}
		}
	}
	while(startnode >= 0);

	return num_gas_nearby;
}







/*****************************************************************************
 This is a comparison kernel for a sort routine, which is used to group
 particles that are going to be exported to the same CPU.
******************************************************************************/
int gaspartsearch_compare_key(const void *a, const void *b)
{
  if(((struct gaspartsearchdata_in *) a)->Task < (((struct gaspartsearchdata_in *) b)->Task))
    return -1;
  if(((struct gaspartsearchdata_in *) a)->Task > (((struct gaspartsearchdata_in *) b)->Task))
    return +1;
  return 0;
}




/*****************************************************************************
 This is a comparison kernel for a sort routine, which is used to group
 particles that are going to be exported to the same CPU.
******************************************************************************/
int yielddata_compare_key(const void *a, const void *b)
{
  if(((struct feedbackdata_in *) a)->Task < (((struct feedbackdata_in *) b)->Task))
    return -1;
  if(((struct feedbackdata_in *) a)->Task > (((struct feedbackdata_in *) b)->Task))
    return +1;
  return 0;
}





/********************************************************************************** 
	This function physically distributes the metals and change in entropy to the 
	gas particles near the star particles that need an update. 

	NOTE: This function is non-operational as of 4/27/13.
 **********************************************************************************/
void distribute_metals_and_energy(void)
{
  long long ntot;					//Initial GLOBAL Number of SPH part. needing updated 
	long long ntotleft;			//GLOBAL Num SPH part. left needing updated
  int i;
	int j;									//Index for Number of processors
	int k;
	int n;
	int ngrp;
	int source;							//Index 2go FeedbackDataPartialResult[] to FeedbackDataIn[]
	int place;							//Index to go from FeedbackDataIn[] to P[]
	int maxfill;						//Number of structs that fit in CommBuffer
	int ndone;							//Number of particle that have been updated.
  int level;							//
	int sendTask;						//Index of sending Task
	int recvTask;						//Index of receiving Task
	int nexport;						//Total Number of LOCAL SphP to export
	int numGasFed;					//Number of gas particles fedback
  int * nbuffer;					//Number of particles in buffer.
	int * noffset;					//Given recvTask,rtrn sending partls locatn in CommBuffer
	int * nsend_local;			//Number LOCAL of particles to send to j'th Task
	int * nsend;						//Combination of all Task's nsend_local[]
	int * numlist;					//Holds 'NumStarUpdate' from each task(# SphP needing updated)
	int * ndonelist;
	FLOAT radius;//= All.FeedbackRadius / All.Time; 	//Put physical units into comoving.
	double dt;
  double tstart, tend, sumt, sumcomm;
  double timecomp = 0, timecommsumm = 0, timeimbalance = 0, sumimbalance;
	MPI_Status status;

	#ifdef PERIODIC
  	boxSize = All.BoxSize;
  	boxHalf = 0.5 * All.BoxSize;
		#ifdef LONG_X
		  boxHalf_X = boxHalf * LONG_X;
		  boxSize_X = boxSize * LONG_X;
		#endif
		#ifdef LONG_Y
		  boxHalf_Y = boxHalf * LONG_Y;
		  boxSize_Y = boxSize * LONG_Y;
		#endif
		#ifdef LONG_Z
  		boxHalf_Z = boxHalf * LONG_Z;
		  boxSize_Z = boxSize * LONG_Z;
		#endif
	#endif
	if(All.ComovingIntegrationOn){
		radius = All.FeedbackRadius / All.Time;
	}else{
		radius = All.FeedbackRadius;
	}


	phys_time = get_phys_time(All.Time);		//Write age of universe (My) to phys_time.

  /* `NumStarUpdate' = number of star particles on this processor that want a 
      force update and will also generate feedback.
			Stars should be at end of P[], but we'll search the whole damn thing anyways.
	 */
  for(n = 0, NumStarUpdate = 0; n < NumPart; n++)				// <- could speed up here
  {
		dt = phys_time - P[n].StarAge;	//Time (My) passes since star formed
    if(P[n].Type == 4 && P[n].Ti_endstep == All.Ti_Current && dt > 5.0)	
		{
    	NumStarUpdate++;
		}
  }


	//Get Number of Particles needing updated from All tasks
  numlist = malloc(NTask * sizeof(int) * NTask);
  MPI_Allgather(&NumStarUpdate, 1, MPI_INT, numlist, 1, MPI_INT, MPI_COMM_WORLD);
  for(i = 0, ntot = 0; i < NTask; i++)
    ntot += numlist[i];															//Find GLOBAL number stars to update
  free(numlist);


	//Allocate Memory
  noffset 		= malloc(sizeof(int) * NTask);	/* offsets of bunches in common list */
  nbuffer 		= malloc(sizeof(int) * NTask);
  nsend_local = malloc(sizeof(int) * NTask);
  nsend 			= malloc(sizeof(int) * NTask * NTask);
  ndonelist 	= malloc(sizeof(int) * NTask);


  i = N_gas;					/* first non-SPH particle for this task */
  ntotleft = ntot;		/* particles left for all tasks together */


	/*
		Iterate through all GLOBAL Star particles needing updated, until no more GLOBAL
		particles are left to communicate. Find number of nearby gas particles
	*/
	while(ntotleft > 0)
	{
		for(j = 0; j < NTask; j++){
			nsend_local[j] = 0;
		}

		tstart = second();


		/*
			Iterate through LOCAL Stars needing an update. Count number of nearby on LOCAL
			and NON-LOCAL particles. Exportflag[] is set in 
			get_local_number_gas_part_nearby().
		*/
		for(nexport=0, ndone=0; i< NumPart && nexport< All.BunchSizeFeedback - NTask; i++)
		{
			dt = phys_time - P[i].StarAge;	//Time (My) passes since star formed

			/* Only Active Stars over 5My old w/ nearby gas part. */
			if(P[i].Type == 4 && P[i].Ti_endstep == All.Ti_Current && dt > 5.0)
			{
				ndone++;

				for(j=0; j<NTask; j++){
					Exportflag[j] = 0;								//Reset list of Tasks to export to
				}

				numGasFed = distribute_feedback_to_nearby_gas(radius, i, 0);
				P[i].NumGasFed = numGasFed;					//Delete later?			

				for(j=0; j<NTask; j++)
				{
					/* Are Nearby Particles on other Processors? */
					if(Exportflag[j])									//set in get_local_number_gas_part_nearby
					{
						for(k = 0; k < 3; k++)
						{
							FeedbackDataIn[nexport].Pos[k] = P[i].Pos[k];
						}
						for(k = 0; k < NELEMENTS; k++)				//NELEMENTS = 25
						{
							FeedbackDataIn[nexport].Dt_Z_Mass[k] = dt_Z_Mass_dist[k];
						}

						FeedbackDataIn[nexport].DtEnergy = dtEnergy;
						FeedbackDataIn[nexport].DtMass = dtMass;
						FeedbackDataIn[nexport].ID 	 	= P[i].ID;
						FeedbackDataIn[nexport].Task 	= j;
						FeedbackDataIn[nexport].Index = i;
						FeedbackDataIn[nexport].NumGasNearby = P[i].NumGasNearby;

						nexport++;
						nsend_local[j]++;
					}	
				}
			}
		}
		tend = second();
		timecomp += timediff(tstart, tend);

		//Group GasPartSearchDataIn[] by Task to be exported to, makes easier to communicat
		qsort(FeedbackDataIn, nexport, sizeof(struct feedbackdata_in),yielddata_compare_key);

		//Get offset of particles as a function of destination Task
		for(j = 1, noffset[0] = 0; j < NTask; j++){
			noffset[j] = noffset[j - 1] + nsend_local[j - 1];
		}

		tstart = second();

		/*
			Get number of particles to be exported by each Task and to which Task it 
			will be exported.
		*/
		MPI_Allgather(nsend_local, NTask, MPI_INT, nsend, NTask, MPI_INT, MPI_COMM_WORLD);
		
		tend = second();
		timeimbalance += timediff(tstart, tend);


		/* Get Number of nearby particles from NON-LOCAL particles. */
   	for(level = 1; level < (1 << PTask); level++)
	  {
	    tstart = second();
	    for(j = 0; j < NTask; j++)
			{
	      nbuffer[j] = 0;
			}

			//Complex way of sending/receiving particles between Tasks. I don't understand
	    for(ngrp = level; ngrp < (1 << PTask); ngrp++)
	    {
	      maxfill = 0;
	      for(j = 0; j < NTask; j++)
	    	{
					//Get Number of particles in FeedbackDataIn Buffer destined for ______
	    	  if((j ^ ngrp) < NTask)                   //This is bitwise XOR comparison
	    	    if(maxfill < nbuffer[j] + nsend[(j ^ ngrp) * NTask + j])
	    	      maxfill = nbuffer[j] + nsend[(j ^ ngrp) * NTask + j];
	    	}
	      if(maxfill >= All.BunchSizeFeedback)
	      	break;

	      sendTask = ThisTask;
	      recvTask = ThisTask ^ ngrp;

        if(recvTask < NTask)
      	{
      	  if(nsend[ThisTask * NTask + recvTask] > 0 || 
             nsend[recvTask * NTask + ThisTask] > 0)
	        {
	          /* 
							Send and Recieve the particles. Blocks communication  until 
							send buffer is free and receive buffer is filled. The send and
							receive buffers point to different sections of the CommBuffer
						*/
	          MPI_Sendrecv(&FeedbackDataIn[noffset[recvTask]],
				               nsend_local[recvTask] * sizeof(struct feedbackdata_in),MPI_BYTE,
				               recvTask, TAG_FEEDBACK_A, &FeedbackDataGet[nbuffer[ThisTask]],
				               nsend[recvTask * NTask + ThisTask] * sizeof(struct 
											 feedbackdata_in), MPI_BYTE, recvTask, TAG_FEEDBACK_A, 
											 MPI_COMM_WORLD, &status);
	        }
	      }

        for(j = 0; j < NTask; j++)
	        if((j ^ ngrp) < NTask)
	          nbuffer[j] += nsend[(j ^ ngrp) * NTask + j];
      }
			tend = second();
			timecommsumm += timediff(tstart, tend);

			/* now feedback imported particle's data to local particle's */
			tstart = second();
			for(j = 0; j < nbuffer[ThisTask]; j++)
			{
				numGasFed = distribute_feedback_to_nearby_gas(radius, j, 1);			
				FeedbackDataResult[j].Task = FeedbackDataGet[j].Task;
				FeedbackDataResult[j].ID 	 = FeedbackDataGet[j].ID;

				//Total *.NumGasFed should match *.NumGasNearby
				FeedbackDataResult[j].NumGasFed = numGasFed; ///+ FeedbackDataGet[j].NumGasFed;

			}
			tend = second();
			timecomp += timediff(tstart, tend);

      /* do a block to measure imbalance */
      tstart = second();
      MPI_Barrier(MPI_COMM_WORLD);
      tend = second();
      timeimbalance += timediff(tstart, tend);


			/* NOTE : This is unneccessary!! Feel free to delete*/
      /* get the result */
      tstart = second();
      for(j = 0; j < NTask; j++)
        nbuffer[j] = 0;
      for(ngrp = level; ngrp < (1 << PTask); ngrp++)
      {
        maxfill = 0;
        for(j = 0; j < NTask; j++)
	      {
	        if((j ^ ngrp) < NTask)
	          if(maxfill < nbuffer[j] + nsend[(j ^ ngrp) * NTask + j])
	            maxfill = nbuffer[j] + nsend[(j ^ ngrp) * NTask + j];
	      }
        if(maxfill >= All.BunchSizeFeedback)
	        break;

        sendTask = ThisTask;
        recvTask = ThisTask ^ ngrp;

        if(recvTask < NTask)
	      {
	        if(nsend[ThisTask * NTask + recvTask] > 0 || 
             nsend[recvTask * NTask + ThisTask] > 0)
	        {
	          /*  
							Send and Recieve the Results. Blocks communication  until 
							send buffer is free and receive buffer is filled. The send and
							receive buffers point to different sections of the CommBuffer

							FeedbackDataResult        = sending buffer
							FeedbackDataPartialResult = receiving buffer
						*/
	          MPI_Sendrecv(&FeedbackDataResult[nbuffer[ThisTask]],
				              nsend[recvTask * NTask + ThisTask] * 
											sizeof(struct feedbackdata_out), MPI_BYTE, recvTask, 
											TAG_FEEDBACK_B, &FeedbackDataPartialResult[noffset[recvTask]],
				              nsend_local[recvTask] * sizeof(struct feedbackdata_out),
				              MPI_BYTE, recvTask, TAG_FEEDBACK_B, MPI_COMM_WORLD, &status);

	          /* add the result to the particles */
	          for(j = 0; j < nsend_local[recvTask]; j++)
		        {
							/*
								NOTE: FeedbackDataIn[source] and FeedbackDataPartialResult[source] are 
								referring to the same particles.
							*/
		          source = j + noffset[recvTask];					
		          place = FeedbackDataIn[source].Index;

							/* Error Check */
							if(P[place].ID == FeedbackDataPartialResult[source].ID)
							{
								//FeedbackDataIn still has LOCAL numGasFed
								P[place].NumGasFed += FeedbackDataPartialResult[source].NumGasFed;
							}
							else
							{
								fprintf(stderr, "WARNING!!!!\nP[%i].ID != FeedbackDataPartialResult"
																"[%i].ID mismatch!\n", place, source);
								endrun(668);
							}
		        }
	        }
	      }

        for(j = 0; j < NTask; j++)
	        if((j ^ ngrp) < NTask)
	          nbuffer[j] += nsend[(j ^ ngrp) * NTask + j];
      }
	    tend = second();
	    timecommsumm += timediff(tstart, tend);

      level = ngrp - 1;
		}

    //Total up the number of GLOBAL particles to update
    MPI_Allgather(&ndone, 1, MPI_INT, ndonelist, 1, MPI_INT, MPI_COMM_WORLD);
    for(j = 0; j < NTask; j++)
      ntotleft -= ndonelist[j];
	}

  free(noffset);
  free(nbuffer);
  free(nsend_local);
  free(nsend);
  free(ndonelist);

 
  /* collect some timing information */
  MPI_Reduce(&timecomp, &sumt, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
  MPI_Reduce(&timecommsumm, &sumcomm, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
  MPI_Reduce(&timeimbalance, &sumimbalance, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);

	/* Not used */
  if(ThisTask == 0)
  {
    All.CPU_FeedCompWalk += sumt / NTask;
    All.CPU_FeedCommSumm += sumcomm / NTask;
    All.CPU_FeedImbalance += sumimbalance / NTask;
  }


	/* Final Error Check, Be sure Stars feed correct number of particles */
	for(j = N_gas; j < NumPart; j++)
	{
		dt = phys_time - P[j].StarAge;	//Time (My) passes since star formed

    if(P[j].Type == 4 && P[j].Ti_endstep == All.Ti_Current && dt > 5.0)	
		{
			if(P[j].NumGasFed != P[j].NumGasNearby)
			{
				fprintf(stderr, "ERROR!!!!\n%li != %li\nNumGasFed should ="
							 	" NumGasNearby\nThisTask: %i\nSee P[%i].ID = %i\n"
								"P[%i].Type: %i P[%i].StarAge: %.2f  NumPart: %i   N_gas: %i"
								"\n\n", P[j].NumGasFed, P[j].NumGasNearby, ThisTask, j, 
								P[j].ID, j, P[j].Type, j, P[j].StarAge, NumPart, N_gas);
				endrun(676); 
			}
		}
	}


 tstart = second();
}






/*****************************************************************************
	mode = 0   :  Local    particles, P[]
	mode = 1   :  NonLocal particles, GasPartSearchDataGet[]

	Distribute metals, mass, entropy to nearby particles. I am following 
	Kobayashi, C., 2004, MNRAS, 347, 740. Note that I am going to use slightly 
	different notation b/c the notation in her paper is cumbersome.

	NOTE : I work in M_sun and convert to M_sun/h later.

	NOTE : P[].TurnOffMass is used to determine the most massive stars
				 in the IMF. This gives us upper limits on our integrations.
	
	NOTE : I use 'm_u' as the Current max mass (or last timestep's turn off mass)

	NOTE : When doing integrals w/r/t WD and Type Ia SN, we WANT to keep track of the
				 stellar remnants. So the only source of WD's are 3 M_s < m < 8 M_s. 
 				 ALL the WD's are created BEFORE any Type Ia SN can occur !!!!

	NOTE : I picked m_1l_RG = 0.8 instead of 0.9 so you can run the feedback into the
				 future.

	NOTE : Looking at eqn 21 and 22, K04, I presume eqn 23, 24, 25, 27, 28, and 29 
				 are calculating FRACTIONAL masses!!

	NOTE : I have NOT implemented energy from stellar winds for 8M_s < m < 50M_s b/c
				 R_SW doesn't reach this region. I'm confused about how K04 did this. See
				 eqns 16, 17, and 30.
				 I added this.

	WARNING : I have not considered how updating either *.Entropy or *.DtEntropy for
				 		neighboring particles will affect their time integration...


        m_1l = 3     m_1u = m_2l = 8         m_2u=50                    IMF_MAX = 120
IMF |-------|-------------|---------------------|----------------------------------|
    | M.S.  |   WD Region |  Type II SN Region  |  Direct Collapse to B.H.         |
	  | R.G.  |             |                     |  SW feedback only                |
                           


******************************************************************************/
long distribute_feedback_to_nearby_gas(FLOAT radius, int target, int mode)
{
	int startnode				= All.MaxPart;
	int n 							= 0;								//Ngblist[] index
	int i,j;
	long numngb					= 0; 								//Number of "nearby" gas part.
	long num_gas_nearby	= 0;								//Num of gas part w/in 'radius' of P[target]
	long num_gas_fed		= 0;								//Num of gas particles fed
	char element[3];
	double dx 					= 0.0;
	double dy						= 0.0;
	double dz 					= 0.0;
	double r 						= 0.0;							// | P[target].Pos - P[j].Pos |
	double dt					= 0;									// CurrentTime - star_age (in mega-years)
	double star_age		= 0;									// Universe's age(mega-years)when star formed
	double m_turnoff  = 0;									// Upper-most mass ON the Main Sequence
	double metal_mass_frac = 0.0; 					// Target metal mass fraction
	double z_mass_frac = 0;									// Individual metal's mass fraction
	double metal_mass;											// Target metal mass (in internal units)
	double oldMass = 0.0;										// This used in update_Zi_mass_frac() 
	double mass = 0.0;											// Target mass
	FLOAT *pos;															// Target Position (in internal units)
	double density;													//Target density
	double m_u	 = P[target].TurnOffMass;		// Most massive stars currently, init = 120
	double upper = 0;												// Upper lim passed to integration routines
	double lower = 0;												// Lower lim passed to integration routines
	double upper1 = 0;											// Upper lim passed to integration routines
	double lower1 = 0;											// Lower lim passed to integration routines
	double e_SW 	= 0;											// Energy injected per Stellar Wind
	double e_II		= 0;											// Energy injected per Type II SN
	double e_Ia		= 0;											// Energy injected per Type Ia SN

	double R_SW				= 0;									// Rate	of SW	per SOLAR mass
	double R_II				= 0;									// Rate of Type II SN per SOLAR mass
	double R_Ia				= 0;									// Rate of Type Ia SN per SOLAR mass
	double dtMassFrac_SW	= 0;							// Stellar Wind mass fedback.
	double dtMassFrac_II	= 0;
	double dtMassFrac_Ia	= 0;
	double dt_Z_MassFrac_SW[NELEMENTS];			//Metal mass frac due to SW 
	double dt_Z_MassFrac_II[NELEMENTS];			//Metal	mass frac due to Type II
	double dt_Z_MassFrac_Ia[NELEMENTS];			//Metal mass frac due to Type Ia

	dtEnergy		= 0;
	dtMass			= 0;									
	dtEntropy		= 0;


	/* mode == 0 for local particles, mode == 1 for imported particles*/
	if(mode == 0){
		pos       			= P[target].Pos;
		star_age				= P[target].StarAge;
		metal_mass_frac	= P[target].MetalMassFrac;
		metal_mass 			= P[target].MetalMassFrac * P[target].Mass;
		mass						= P[target].Mass;
		num_gas_nearby 	= P[target].NumGasNearby;
	}
	else{
		pos 						= FeedbackDataGet[target].Pos;
		dtMass					= FeedbackDataGet[target].DtMass;
		dtEnergy				= FeedbackDataGet[target].DtEnergy;
		num_gas_nearby 	= FeedbackDataGet[target].NumGasNearby;
	
		for(i=0; i<NELEMENTS; i++)
		{
			dt_Z_Mass_dist[i] = FeedbackDataGet[target].Dt_Z_Mass[i];
		}

		//Skip Imported particles w/o near gas part.
		if(num_gas_nearby == 0){
			num_gas_fed = 0;
			return num_gas_fed;
		}
	}


	//Error Check
	if(NELEMENTS != 25)
	{
		fprintf(stderr, "ERROR!!!! NELEMENTS = %i, in allvars.h, dt_Z_Mass_dist[25]\n"
						"Adjust length of struct feedbackdata_in accordingly\n" , 
						NELEMENTS);
		endrun(675);
	}
	if(mode == 0 && (mass < 0 || star_age < 0 ))
	{
		fprintf(stderr, "ERROR!!!! P[%i].Mass = %f, %f\n",target, mass, star_age);
		endrun(676);
	}
	if(num_gas_nearby < 0)
	{
		fprintf(stderr, "ERROR!!! num_gas_nearby %li\n", num_gas_nearby);
		endrun(677);
	}



	//Get Cosmological factors
 	if(All.ComovingIntegrationOn)
 	{
 	  /* Factors for comoving integration of hydro ..... Recall All.Time = 1/(1+z) */

     #ifndef DARKENERGY
 	      hubble_a = All.Omega0 / (All.Time * All.Time * All.Time)
 	          	+ (1 - All.Omega0 - All.OmegaLambda) / (All.Time * All.Time) + 
 	            All.OmegaLambda;
     #endif

     #ifdef DARKENERGY
         #ifdef SCALARFIELD
            All.dark_factor = get_dark_factor(All.Time);
 	         hubble_a = All.Omega0 / (All.Time * All.Time * All.Time)
 	            	+ (1 - All.Omega0 - All.OmegaLambda) / (All.Time * All.Time) + 
 	               (All.OmegaLambda * All.dark_factor);
         #endif

         #ifdef DARKPARAM
            All.dark_factor = get_dark_factor(All.Time);
 	         hubble_a = All.Omega0 / (All.Time * All.Time * All.Time)
 	            	+ (1 - All.Omega0 - All.OmegaLambda) / (All.Time * All.Time) + 
 	               (All.OmegaLambda * All.dark_factor);
         #endif

         #ifdef GCG
            All.dark_factor = get_dark_factor(All.Time);
 	         hubble_a = All.Omega0 / (All.Time * All.Time * All.Time)
 	            	+ (1 - All.Omega0 - All.OmegaLambda) / (All.Time * All.Time) + 
 	               (All.OmegaLambda * All.dark_factor);
         #endif
     #endif

 	  hubble_a = All.Hubble * sqrt(hubble_a);
		a3 = (All.Time * All.Time * All.Time);
		atime = All.Time;
	}
	else
	{
		atime = hubble_a = a3 = 1.0;
	}



	//Only need to do this calculation on LOCAL task. Export the result to other tasks
	if(mode == 0)
	{
		//Get time.
		phys_time get_phys_time(All.Time);					// result is in phys_time (mega-years)
		dt = phys_time - star_age;


		//Get turnoff mass, solve eqn 26, K04 for log(m). Select physical root.
		if(dt > 5.0){											//prevent sqrt(-1)
			m_turnoff = (3.42 - sqrt(3.42*3.42 - 4.0*0.88*(10 - log10(dt * 1.0e6)))) / 1.76;
			m_turnoff = pow(10, m_turnoff);
		}else{
			//No feedback before 5 mega-years
			return 0;
		}


		//Get energy yields.
		e_SW = 0.2e51 * pow(metal_mass_frac/SolarMetalMassFrac, 0.8);   // ergs/SW
		e_II = 1.4e51 + 0.2e51;		// ergs/SN  (includes from winds eqn 17 K04)
		e_Ia = 1.3e51;						// ergs/SN	



		/************************** Feedback Rates ******************************/
	
		/* Stellar Winds */
		if(m_u > m_2u){
			upper = m_u;
			lower = dmax(m_turnoff, m_2u);
			R_SW  = integrate_imf_div_m(lower, upper);											//eqn 30 in K04
		}else{
			R_SW  = 0.0;
		}	


		/* Type II SN */
		if(m_2l <= m_u && m_u <= m_2u){
			upper = m_u;
			lower = dmax(m_turnoff, m_2l);
			R_II  = integrate_imf_div_m(lower, upper);											//eqn 31 in K04
		}
		else if(m_turnoff < m_2u && m_u > m_2u){		// Cross SW -> Type II region
			upper = m_2u;
			lower = dmax(m_turnoff, m_2l);
			R_II  = integrate_imf_div_m(lower, upper);											//eqn 31 in K04
		}else{
			R_II = 0.0;
		}


		/* Type Ia SN  */
		if(m_1l_RG <m_u && m_u <= m_1u_RG){						//Red Giant + White Dwarf system
			//WD
			upper = m_1u;														
			lower = dmax(m_turnoff, m_1l);				
			//RG
			upper1= m_u;		
			lower1= dmax(m_turnoff, m_1l_RG);					
			R_Ia  = 0.02 * integrate_imf_div_m(lower, upper) *						
										 integrate_imf_div_m(lower1, upper1);							//eqn 32 in K04 
		}
		else if(m_turnoff < m_1u_RG && m_u > m_1u_RG){	//m_turnoff crosses R.G. thresh 
			//WD
			upper = m_1u;	
			lower = dmax(m_turnoff, m_1l);
			//RG
			upper1= m_1u_RG;
			lower1= dmax(m_turnoff, m_1l_RG);
			R_Ia  = 0.02 * integrate_imf_div_m(lower, upper) *					
										 integrate_imf_div_m(lower1, upper1);							//eqn 32 in K04
		}else{
			R_Ia = 0.0;
		}

		if(m_1l_MS < m_u && m_u < m_1u_MS){				//Main Sequence + White Dwarf system
			//WD
			upper = m_1u;
			lower = dmax(m_turnoff, m_1l);
			//MS
			upper1= m_u;
			lower1= dmax(m_turnoff, m_1l_MS);
			R_Ia += 0.05 * integrate_imf_div_m(lower, upper) * 				
										 integrate_imf_div_m(lower1, upper1);							//eqn 32 in K04
		}else if(m_turnoff < m_1u_MS && m_u > m_1u_MS){		
			//WD
			upper = m_1u;
			lower = dmax(m_turnoff, m_1l);
			//MS
			upper1= m_1u_MS;
			lower1= dmax(m_turnoff, m_1l_MS);
			R_Ia += 0.05 * integrate_imf_div_m(lower, upper) *				
										 integrate_imf_div_m(lower1, upper1);							//eqn 32 in K04
		}else{
			R_Ia += 0.0;
		}

	
		//Error Check.
		if(R_Ia < 0.0 || R_II < 0.0 || R_SW < 0.0){
			fprintf(stderr, "ERROR!!! Rates can't be less than zero!\nR_SW : %.2e "
							"R_II : %.2e  R_Ia : %.2e\n", R_SW, R_II, R_Ia);
			endrun(678);
		}




		/***************************  Mass Feedback *****************************/

		/* Stellar Winds */
		if(m_u > m_2u){																//Only have SW if massive stars exist
			upper = m_u;
			lower = dmax(m_turnoff, m_2u);
			dtMassFrac_SW = pow(metal_mass_frac / SolarMetalMassFrac, 0.8) * 
									 		integrate_1_minus_wm_imf(lower, upper);					//eqn 23 in K04
		}else{
			dtMassFrac_SW = 0.0;
		}


		/* Type II SN */
		if(m_2l <= m_u && m_u <= m_2u){	
			upper = m_u;
			lower = dmax(m_turnoff, m_2l);
			dtMassFrac_II = integrate_1_minus_wm_imf(lower, upper);					//eqn 24 in K04
		}
		else if(m_turnoff < m_2u && m_u > m_2u){			//Cross SW -> Type II region
			upper = m_2u;
			lower = dmax(m_turnoff, m_2l);
			dtMassFrac_II = integrate_1_minus_wm_imf(lower, upper);
		}else{
			dtMassFrac_II = 0.0;
		}	

		//Type Ia SN 
		dtMassFrac_Ia = WhiteDwarfMass * R_Ia;														//eqn. 25 in K04



		/************************** Metal Feedback ******************************
			NOTE : I believe that eqn 27 in K04 is wrong. It doesn't make sense to 
						 include p_zmII when in the paragraph below she states that no
						 elements are produced. I fixed my stellar winds below accordingly.
		*/
		for(i=0; i<NELEMENTS; i++)												//Iterate through metals
		{
			dt_Z_MassFrac_SW[i] = 0;
			dt_Z_MassFrac_II[i] = 0;
			dt_Z_MassFrac_Ia[i] = 0;
			dt_Z_Mass_lost[i]		= 0;

			if(MetalsPresent[i].Present == 1)
			{
				strcpy(element, MetalsPresent[i].Element);
				z_mass_frac = get_Zi_mass_frac(target, element, mode);


				/* Stellar Winds */
				if(m_u > m_2u){
					upper = m_u;
					lower = dmax(m_turnoff, m_2u);
					dt_Z_MassFrac_SW[i] = pow(metal_mass_frac / SolarMetalMassFrac, 0.8) * 	
												 	z_mass_frac * integrate_1_minus_wm_imf(lower, upper);			
																																			//eqn. 27 in K04
																																
					dt_Z_Mass_lost[i] += mass * dt_Z_MassFrac_SW[i];
				}else{
					dt_Z_MassFrac_SW[i] += 0;
				}


				/* Type II SN */
				if(m_2l <= m_u && m_u <= m_2u){
					upper = m_u;
					lower = dmax(m_turnoff, m_2l);
					dt_Z_MassFrac_II[i] = integrate_pzII_imf(element, lower, upper) + 	
													integrate_1_minus_wm_minus_pzII_Z_imf(element,
													z_mass_frac, lower, upper);									//eqn. 28 in K04

					dt_Z_Mass_lost[i] += mass * integrate_1_minus_wm_minus_pzII_Z_imf(element,
													z_mass_frac, lower, upper);	
				}
				else if(m_turnoff < m_2u && m_u > m_2u){
					upper = m_2u;
					lower = dmax(m_turnoff, m_2l);
					dt_Z_MassFrac_II[i] = integrate_pzII_imf(element, lower, upper) + 	
													integrate_1_minus_wm_minus_pzII_Z_imf(element,
													z_mass_frac, lower, upper);									//eqn. 28 in K04

					dt_Z_Mass_lost[i] += mass * integrate_1_minus_wm_minus_pzII_Z_imf(element,
													z_mass_frac, lower, upper);		
				}else{
					dt_Z_MassFrac_II[i] = 0;
				}


				/* Type Ia SN */
				dt_Z_MassFrac_Ia[i] = WhiteDwarfMass * TypeIa_Zi_yield_mass_frac(element) * 
															R_Ia;																		//eqn. 29 in K04
			}
		}



		/******************** Collect results and give actual masses ********************/
		/*
			Here note that we can leave dt_Z_Mass[] and dtMass in terms of internal mass 
			units because they are mass fractions. HOWEVER, R_SW, R_II, R_Ia are the number 
			of SW and SN per SOLAR mass. This means we must convert to internal mass to 
			solar mass.

			dtEnergy units = (internal energy / internal mass).
		*/
		for(i=0; i<NELEMENTS; i++)
		{
			dt_Z_Mass_dist[i] = mass * (dt_Z_MassFrac_SW[i] + dt_Z_MassFrac_II[i] + 
														 dt_Z_MassFrac_Ia[i]);										//eqn. 22 in K04
		}
		dtMass = mass * (dtMassFrac_SW + dtMassFrac_II + dtMassFrac_Ia);	//eqn. 21 in K04
		dtEnergy = All.UnitMass_in_g / (SOLAR_MASS * All.UnitEnergy_in_cgs) * 
							 (e_SW * R_SW + e_II * R_II + e_Ia * R_Ia);							//eqn. 16 in K04
																																			



		//Update star particle mass and metal mass.
		oldMass = P[target].Mass;
		P[target].Mass -= dtMass;													//Update Total mass
		update_Zi_mass_frac(target, dt_Z_Mass_lost, -1.0, oldMass, P[target].Mass);	

	}/* endif mode == 0 */




	/* Get number of LOCAL particles within 'radius' of the P[target]'s location */
	startnode	= All.MaxPart;
	do
	{
		numngb +=	ngb_treefind_gas_in_box(&pos[0], radius, &startnode);

		/* from Ngblist find particles w/in 'radius' */
		for(n = 0; n < numngb; n++)
		{
			j = Ngblist[n];										//Get index of 'nearby' gas part.

			dx = pos[0] - P[j].Pos[0];	
			dy = pos[1] - P[j].Pos[1];
			dz = pos[2] - P[j].Pos[2];

			#ifdef PERIODIC			/*  find the closest image in the given box size  */
	    	if(dx > boxHalf_X) { 		dx -= boxSize_X; 	}
	   		if(dx < -boxHalf_X){		dx += boxSize_X;	}
	    	if(dy > boxHalf_Y) {		dy -= boxSize_Y;	}
	    	if(dy < -boxHalf_Y){		dy += boxSize_Y;	}
	    	if(dz > boxHalf_Z) {		dz -= boxSize_Z;	}
	    	if(dz < -boxHalf_Z){		dz += boxSize_Z;	}
			#endif
	    r = sqrt(dx * dx + dy * dy + dz * dz);

			//Within Feedback Radius?
			if( r <= radius)
			{
				oldMass = P[j].Mass;
				density = SphP[j].Density;

				/* 
					Now convert dtEnergy into dtEntropy (dA/dt). See initialization of 
					SphP[].Entropy in read.ic and init.c for motivation. Note that dtEnergy is in
					(ergs/internal mass units). That is why I didn't multiply by 'mass' above. 
					Also, account for dA/dt * dt/da = dA/da. 

					convert this particles' fraction of dtEnergy to its change in dtEntropy
				*/
				dtEntropy = (dtEnergy / ((double)num_gas_nearby) ) * GAMMA_MINUS1 / 
										pow(density / a3, GAMMA_MINUS1);
				dtEntropy = dtEntropy / (hubble_a * atime);						//  dA/dt -> dA/da


				//Actually update feedback..
				SphP[j].DtEntropy += dtEntropy;
				P[j].Mass			 += dtMass   	/ ( (double)num_gas_nearby);
				update_Zi_mass_frac(j, dt_Z_Mass_dist, 1.0/((double)num_gas_nearby), oldMass,
														P[j].Mass);

				num_gas_fed++;
			}
		}
	}
	while(startnode >= 0);


	/* 
		Conserve metals & mass in stars w/o any nearby gas particles. 
		Entropy is not conserved 
	*/
	if(mode == 0 && P[target].NumGasNearby == 0 )
	{
		oldMass = P[target].Mass;

		P[target].Mass += dtMass;
		update_Zi_mass_frac(target, dt_Z_Mass_dist, 1.0, oldMass, P[target].Mass);
	}



	//Check for correctness
	if(mode == 0){
		P[target].TurnOffMass = m_turnoff;
	}

	return num_gas_fed;
}




/*********************************************************************************
	 Initial Mass Function (IMF). 
	
	 :::NOTE:::  
	 If you want to change the IMF, you'll need to fix the normalization 
	 factor (0.1313) and the exponent (1.35). This is normalized for 0.05 < m < 120 
 *********************************************************************************/
double imf(double mass)									
{
	return 0.1313 * pow(mass, -1.35);  //For units M_sun
	//return 1.4873 * pow(mass, -1.35);    //For units M_sun/h
}





/*********************************************************************************
	Here is how we determine the mass of the remnant (m_rem) of a star with mass M.
  We use this for the integrals. Here are some useful references. Values for 
	Black Hole masses are like assholes, everyone's got one. H02 and E04 have the 
	best, most comprehensible data on stellar remnants. E04 contains metallicity 
	dependent information on stellar remnants. 


	K06 : Kobayashi, 2006 ApJ 635, 1145
	K04 : Kobayashi, 2004 MNRAS 347, 740
	H02 : Heger, 2002 Springer-Verlag, 369, see arXiv:astro-ph/0112059 
	E04 : Eldridge & Tout, 2004 MNRAS, 353, 87  

	Region 1, all the mass is locked up in stars that live longer than 13Gy. 
	Region 2, derived from p298 Iben & Renzini 1983. Arbitrarily picked eta = 1.
	Region 3, used table 1 to derive linear eqn. used.
	Region 4, per K04 all mass gets locked up into black holes.

			Value             ||             Regime                ||      Reason
	===========================================================================
1.	m_rem = M  		            (0.05 M_sun < M < 0.9 M_sun)      On M.S. > 13.6 Gy
2.	m_rem = 0.15*M + 0.38 		(0.90 M_sun < M < 8.0 M_sun)      p298,Iben & Renzini '83
3. 	m_rem = 0.0735*M +0.662		(8.0  M_sun < M < 25  M_sun)			p2 Figure 1, H02
4.	m_rem = 1.1*M	- 25				(25.0 M_sun < M < 50  M_sun)			p2 Figure 1, H02.
5.  m_rem = 0.6*M							(50.0 M_sun < M < 100 M_sun)			p2 Figure 1, H02
	===========================================================================
**********************************************************************************/
double remnant_mass_frac(double mass)
{
	double m_rem = 0.0;

	/* (0.05 M_sun < M <= 0.9 M_sun)   */       //Low mass M.S. stars
	if(mass <= m_1l_RG)
	{
		m_rem = mass;
	}
	
	/* (0.90 M_sun < M <= 8.0 M_sun)   */       //W.D. and companions, Type Ia SN
	if(m_1l_RG < mass && mass <= m_2l)
	{
		m_rem = 0.15 * mass + 0.38;
	}

	/* (8.0 M_sun < M <= 25 M_sun)     */       //Type II SN
	if(m_2l < mass && mass <= m_2l_bh)
	{
		m_rem = 0.0735 * mass + 0.662;
	}

	/* (25 M_sun < M <= 50 M_sun)      */       //Type II SN
	if(m_2l_bh < mass && mass <= m_2u)
	{
		m_rem = 1.10 * mass - 25.0;
	}

	/* (50 M_sun < M <= 120 M_sun)     */
	if(m_2u < mass && mass <= IMF_MAX)							//Direct collapse to B.H.
	{
		m_rem = 0.6 * mass;
	}

	return (m_rem/mass);
}




/**********************************************************************************
	This linearly interpolates the yields of the table Type_II_SN. If your table has 
	different points you will DEFINITELY need to change this function. You will also 
	need to change 'sn_yield' in allvars.h.

	Returns mass fraction of metal yield
	
	If mass < 13 M_s, we pick the same mass fraction as 13 M_s.  All other
	values are interpolated. Masses are extrapolated outside of 70 M_s. 
	Yield = 0 for masses > 120
 *********************************************************************************/
double TypeII_Zi_yield_mass_frac(char element[3], float mass)
{
	int i = -1;
	int j = 0;												//Index to iterate through *.Yields[]
	int mIndex = 0;										//Mass Index
	double x1,y1;											//Lower mass and yield for interpolation
	double x2,y2;											//Upper mass and yield for interpolation
	double m;													//Slope for interpolation
	double b;													//y-intercept for interpolation
	double zYield  = 0;						//In M_s (M_solar)
	double zMassFrac  = 0;

	
	//Enforce limits for Type II SN explosion.  8 M_s < mass < 50 M_s
	if(mass < m_2l || mass > IMF_MAX)
	{
		return 0;
	}

	i = find_SN_Table_index(element, 2);	

	//interpolate table... mIndex is the lower bound.
	j = 0;
	while(j < NMasses - 1)
	{
		//No need to interpolate.
		if(mass == SNIIMassTable[j] || mass <= SNIIMassTable[0])
		{
			zMassFrac = Type_II_SN[i].Yield[j] / SNIIMassTable[j];
			return zMassFrac;
		}
		mIndex = mass >= SNIIMassTable[j] ? j : mIndex;
		j++;
	}

	//Get points to interpolate, y = mx + b
	x1 = SNIIMassTable[mIndex];
	x2 = SNIIMassTable[mIndex + 1];
	y1 = Type_II_SN[i].Yield[mIndex];
	y2 = Type_II_SN[i].Yield[mIndex + 1];				

	//Interpolate
	m = (y2 - y1) / (x2 - x1);
	b = y1 - m*x1;
	zYield = m * mass + b;
	zMassFrac = zYield / mass;

	return zMassFrac;
}





/********************************************************************************
	Returns mass fraction of Type Ia metal yield
 ********************************************************************************/
double TypeIa_Zi_yield_mass_frac(char element[3])
{
	int i = 0;
	double zMassFrac = 0.0;
	i = find_SN_Table_index(element, 1);
	
	zMassFrac = Type_Ia_SN[i].Yield[0] / WhiteDwarfMass;

	return zMassFrac;
}





/***********************************************************************************
	Integrand used in eqn. 23 and 24 in K04. Use Trapezoidal Rule which is good enough.
	integrate[(1-remn_mass_frac)*phi*dm]
************************************************************************************/
double integrate_1_minus_wm_imf(float lower, float upper)
{
	int n		 	 		= N_INTEGRATION_STEPS;
	int i 				= 0;
	double dx 	 	= (upper - lower)/((float)n);
	double area 	= 0.0;
	double x_i		= 0.0;
	double x_j		= 0.0;
	double y_i		= 0.0;
	double y_j		= 0.0;

	x_i = lower;
	x_j = lower + dx;

	for(i=0; i<n; i++)
	{
		y_i = (1 - remnant_mass_frac(x_i)) * imf(x_i);
		y_j = (1 - remnant_mass_frac(x_j)) * imf(x_j);

		area += (x_j - x_i) * y_i + 0.5 * (x_j - x_i) * (y_j - y_i);

		x_i = x_j;
		x_j += dx;
	}

	return area;
}






/************************************************************************************
	Integrand used in eqn. 27 and 28 in K04. Use Trapezoidal Rule which is good enough.
	integrate[(1 - remn_mass_frac - metal_yield_mass_frac) * Z * phi * dm]
*************************************************************************************/
double integrate_1_minus_wm_minus_pzII_Z_imf(char element[3], double zMassFrac, 
																						 float lower, float upper)
{
	int n 			= N_INTEGRATION_STEPS;
	int i				= 0;
	double dx		= (upper - lower) / ((float)n);
	double area	= 0.0;
	double x_i	= 0.0;
	double x_j	= 0.0;
	double y_i	= 0.0;
	double y_j	= 0.0;

	x_i = lower;
	x_j = lower + dx;



	for(i=0; i<n; i++)
	{
		y_i = (1 - remnant_mass_frac(x_i) - 
						TypeII_Zi_yield_mass_frac(element, x_i)) * zMassFrac * imf(x_i);
		y_j = (1 - remnant_mass_frac(x_j) - 
						TypeII_Zi_yield_mass_frac(element, x_j)) * zMassFrac * imf(x_j);
		
		area += (x_j - x_i) * y_i + 0.5 * (x_j - x_i) * (y_j - y_i);
		
		x_i = x_j;
		x_j += dx;
	}
	
	return area;
}




/***********************************************************************************
	Integrand used in eqn 28 in K04. Use Trapezoidal Rule which is good enough. 
	integrate[(pzII * phi * dm)]
***********************************************************************************/
double integrate_pzII_imf(char element[3], float lower, float upper)
{	
	int n 			= N_INTEGRATION_STEPS;
	int i				= 0;
	double dx		= (upper - lower) / ((float)n);
	double area	= 0.0;
	double x_i	= 0.0;
	double x_j	= 0.0;
	double y_i	= 0.0;
	double y_j	= 0.0;

	x_i = lower;
	x_j = lower + dx;

	for(i=0; i<n; i++)
	{
		y_i = TypeII_Zi_yield_mass_frac(element, x_i) * imf(x_i);
		y_j = TypeII_Zi_yield_mass_frac(element, x_j) * imf(x_j);

		area += (x_j - x_i) * y_i + 0.5 * (x_j - x_i) * (y_j - y_i);

		x_i = x_j;
		x_j += dx;
	}

	return area;
}




/************************************************************************************
	Integrand used in eqn. 30, 31, 32 in K04. Use Trapezoidal Rule which is good enough.
	integrate[1/m * phi * dm]
************************************************************************************/
double integrate_imf_div_m(float lower, float upper)
{
	int n		 	 	= N_INTEGRATION_STEPS;
	int i 			= 0;
	float dx 	 	= (upper - lower)/((float)n);
	float area 	= 0.0;
	float x_i		= 0.0;
	float x_j		= 0.0;
	float y_i		= 0.0;
	float y_j		= 0.0;

	x_i = lower;
	x_j = lower + dx;

	for(i=0; i<n; i++)
	{
		y_i = imf(x_i) / x_i;
		y_j = imf(x_j) / x_j;

		area += (x_j - x_i) * y_i + 0.5 * (x_j - x_i) * (y_j - y_i);

		x_i = x_j;
		x_j += dx;
	}

	return area;
}


/************************************************************************************
	Given an element, find the appropriate index in either Type_II_SN[] or Type_Ia_SN[]

	mode == 1 for Type Ia SN
	mode == 2 for Type II SN
************************************************************************************/
int find_SN_Table_index(char element[3], int mode)
{
	int i = 0;
	int index = -1;
	int nElements = 0;

	if(mode == 1)
	{
		nElements = NTypeIaElements;
	}else if(mode == 2){
		nElements = NTypeIIElements;
	}else{
		fprintf(stderr, "ERROR!! Please specify correct 'mode' for find_SN_index\n");
		endrun(674);
	}


	while(i < nElements)
	{
		if( strcmp(element, Type_II_SN[i].Element) == 0)
		{
			//Error check, be sure element only occurs once in array
			if(index >= 0)
			{
				fprintf(stderr, "ERROR!! Element occurs multiple times in "
												"SN yields data files\n");
				endrun(672);
			}
			else
			{
				index = i;
			}
		}
		i++;
	}

	return index;
}	






/************************************************************************************
	Given a target index, it finds appropriate metal mass fraction from P[].*MassFrac
************************************************************************************/
double get_Zi_mass_frac(int target, char element[3], int mode)
{
	double metalMassFrac = -999.0;

	if(P[target].Type != 0 && P[target].Type != 4){
		fprintf(stderr, "ERROR!!! Trying to change metal abundance of particle that is "
										"neither a star nor gas particle\n");
		endrun(675);
	}



	/* mode == 0 for local particles, mode == 1 for imported particles */
	if( mode == 0 )
	{
		#ifdef CARBON
			if(strcmp(element, "C") == 0){
				metalMassFrac = P[target].CarbonMassFrac;
			}
		#endif
		#ifdef NITROGEN
			if(strcmp(element, "N") == 0){
				metalMassFrac = P[target].NitrogenMassFrac;
			}
		#endif
		#ifdef OXYGEN
			if(strcmp(element, "O") == 0){
				metalMassFrac = P[target].OxygenMassFrac;
			}
		#endif
		#ifdef FLORINE
			if(strcmp(element, "F") == 0){
				metalMassFrac = P[target].FlorineMassFrac;
			}
		#endif
		#ifdef NEON
			if(strcmp(element, "Ne") == 0){
				metalMassFrac = P[target].NeonMassFrac;
			}
		#endif
		#ifdef SODIUM
			if(strcmp(element, "Na") == 0){
				metalMassFrac = P[target].SodiumMassFrac;
			}
		#endif
		#ifdef MAGNESIUM
			if(strcmp(element, "Mg") == 0){
				metalMassFrac = P[target].MagnesiumMassFrac;
			}
		#endif
		#ifdef ALUMINUM
			if(strcmp(element, "Al") == 0){
				metalMassFrac = P[target].AluminumMassFrac;
			}
		#endif
		#ifdef SILICON
			if(strcmp(element, "Si") == 0){
				metalMassFrac = P[target].SiliconMassFrac;
			}
		#endif
		#ifdef PHOSPHORUS
			if(strcmp(element, "P") == 0){
				metalMassFrac = P[target].PhosphorusMassFrac;
			}
		#endif
		#ifdef SULFUR
			if(strcmp(element, "S") == 0){
				metalMassFrac = P[target].SulfurMassFrac;
			}
		#endif
		#ifdef CHLORINE
			if(strcmp(element, "Cl") == 0){
				metalMassFrac = P[target].ChlorineMassFrac;
			}
		#endif
		#ifdef ARGON
			if(strcmp(element, "Ar") == 0){
				metalMassFrac = P[target].ArgonMassFrac;
			}
		#endif
		#ifdef POTASSIUM
			if(strcmp(element, "K") == 0){
				metalMassFrac = P[target].PotassiumMassFrac;
			}
		#endif
		#ifdef CALCIUM
			if(strcmp(element, "Ca") == 0){
				metalMassFrac = P[target].CalciumMassFrac;
			}
		#endif
		#ifdef SCANDIUM
			if(strcmp(element, "Sc") == 0){
				metalMassFrac = P[target].ScandiumMassFrac;
			}
		#endif
		#ifdef TITANIUM
			if(strcmp(element, "Ti") == 0){
				metalMassFrac = P[target].TitaniumMassFrac;
			}
		#endif
		#ifdef VANADIUM
			if(strcmp(element, "V") == 0){
				metalMassFrac = P[target].VanadiumMassFrac;
			}
		#endif
		#ifdef CHROMIUM
			if(strcmp(element, "Cr") == 0){
				metalMassFrac = P[target].ChromiumMassFrac;
			}
		#endif
		#ifdef MANGANESE
			if(strcmp(element, "Mn") == 0){
				metalMassFrac = P[target].ManganeseMassFrac;
			}
		#endif
		#ifdef IRON
			if(strcmp(element, "Fe") == 0){
				metalMassFrac = P[target].IronMassFrac;
			}
		#endif
		#ifdef COBALT
			if(strcmp(element, "Co") == 0){
				metalMassFrac = P[target].CobaltMassFrac;
			}
		#endif
		#ifdef NICKEL
			if(strcmp(element, "Ni") == 0){
				metalMassFrac = P[target].NickelMassFrac;
			}
		#endif
		#ifdef COPPER
			if(strcmp(element, "Cu") == 0){
				metalMassFrac = P[target].CopperMassFrac;
			}
		#endif
		#ifdef ZINC
			if(strcmp(element, "Zn") == 0){
				metalMassFrac = P[target].ZincMassFrac;
			}
		#endif
	}
	return metalMassFrac;
}




/******************************************************************************
	Updates P[target].*MassFrac. If you want to update it with only partial amounts
	of the metals, adjust the fraction appropriately. This assumes that P[target].Mass
	is uptodate. 

	This also updates P[target].MetalMassFrac

	cofactor = -1.0 if you want to subract
	cofactor = +1.0 if you want to add.

	P[target] -> Particle to be updated
	dtzmass		-> Contains mass of each element to be updated.
	cofactor	-> what fraction of dtzmass to update with.
	oldmass		-> used to calculate current mass of element
	newmass		-> used to calculate element mass fraction, this contains update after 
							 dtMass;

*******************************************************************************/
void update_Zi_mass_frac(int target, double dtzmass[NELEMENTS], double cofactor, 
												 double oldmass, double newmass)
{
	int i=0;
	char element[3];
	double totalDtMetalMass = 0.0;
	double massZ = 0.0;											//Current Metal Mass

	if(P[target].Type != 0 && P[target].Type != 4){
		fprintf(stderr, "ERROR!!! Trying to change metal abundance of particle that is "
										"neither a star nor gas particle\n");
		endrun(675);
	}


	for(i=0; i<NELEMENTS; i++)
	{
		strcpy(element, MetalsPresent[i].Element);
		totalDtMetalMass += dtzmass[i];

		#ifdef CARBON
			if(strcmp(element, "C") == 0){
				massZ = P[target].CarbonMassFrac * oldmass;
				P[target].CarbonMassFrac = (massZ + cofactor * dtzmass[i]) / newmass;
			}
		#endif
		#ifdef NITROGEN
			if(strcmp(element, "N") == 0){
				massZ = P[target].NitrogenMassFrac * oldmass;
				P[target].NitrogenMassFrac = (massZ + cofactor * dtzmass[i]) / newmass;
			}
		#endif
		#ifdef OXYGEN
			if(strcmp(element, "O") == 0){
				massZ = P[target].OxygenMassFrac * oldmass;
				P[target].OxygenMassFrac = (massZ + cofactor * dtzmass[i]) / newmass;
			}
		#endif
		#ifdef FLORINE
			if(strcmp(element, "F") == 0){
				massZ = P[target].FlorineMassFrac * oldmass;
				P[target].FlorineMassFrac = (massZ + cofactor * dtzmass[i]) / newmass;
			}
		#endif
		#ifdef NEON
			if(strcmp(element, "Ne") == 0){
				massZ = P[target].NeonMassFrac * oldmass;
				P[target].NeonMassFrac = (massZ + cofactor * dtzmass[i]) / newmass;
			}
		#endif
		#ifdef SODIUM
			if(strcmp(element, "Na") == 0){
				massZ = P[target].SodiumMassFrac * oldmass;
				P[target].SodiumMassFrac = (massZ + cofactor * dtzmass[i]) / newmass;
			}
		#endif
		#ifdef MAGNESIUM
			if(strcmp(element, "Mg") == 0){
				massZ = P[target].MagnesiumMassFrac * oldmass;
				P[target].MagnesiumMassFrac = (massZ + cofactor * dtzmass[i]) / newmass;
			}
		#endif
		#ifdef ALUMINUM
			if(strcmp(element, "Al") == 0){
				massZ = P[target].AluminumMassFrac * oldmass;
				P[target].AluminumMassFrac = (massZ + cofactor * dtzmass[i]) / newmass;
			}
		#endif
		#ifdef SILICON
			if(strcmp(element, "Si") == 0){
				massZ = P[target].SiliconMassFrac * oldmass;
				P[target].SiliconMassFrac = (massZ + cofactor * dtzmass[i]) / newmass;
			}
		#endif
		#ifdef PHOSPHORUS
			if(strcmp(element, "P") == 0){
				massZ = P[target].PhosphorusMassFrac * oldmass;
				P[target].PhosphorusMassFrac =(massZ + cofactor * dtzmass[i]) / newmass;
			}
		#endif
		#ifdef SULFUR
			if(strcmp(element, "S") == 0){
				massZ = P[target].SulfurMassFrac * oldmass;
				P[target].SulfurMassFrac = (massZ + cofactor * dtzmass[i] )/ newmass;
			}
		#endif
		#ifdef CHLORINE
			if(strcmp(element, "Cl") == 0){
				massZ = P[target].ChlorineMassFrac * oldmass;
				P[target].ChlorineMassFrac = (massZ + cofactor * dtzmass[i]) / newmass;
			}
		#endif
		#ifdef ARGON
			if(strcmp(element, "Ar") == 0){
				massZ = P[target].ArgonMassFrac * oldmass;
				P[target].ArgonMassFrac = (massZ + cofactor * dtzmass[i]) / newmass;
			}
		#endif
		#ifdef POTASSIUM
			if(strcmp(element, "K") == 0){
				massZ = P[target].PotassiumMassFrac * oldmass;
				P[target].PotassiumMassFrac = (massZ + cofactor * dtzmass[i]) / newmass;
			}
		#endif
		#ifdef CALCIUM
			if(strcmp(element, "Ca") == 0){
				massZ = P[target].CalciumMassFrac * oldmass;
				P[target].CalciumMassFrac = (massZ + cofactor * dtzmass[i]) / newmass;
			}
		#endif
		#ifdef SCANDIUM
			if(strcmp(element, "Sc") == 0){
				massZ = P[target].ScandiumMassFrac * oldmass;
				P[target].ScandiumMassFrac = (massZ + cofactor * dtzmass[i]) / newmass;
			}
		#endif
		#ifdef TITANIUM
			if(strcmp(element, "Ti") == 0){
				massZ = P[target].TitaniumMassFrac * oldmass;
				P[target].TitaniumMassFrac = (massZ + cofactor * dtzmass[i]) / newmass;
			}
		#endif
		#ifdef VANADIUM
			if(strcmp(element, "V") == 0){
				massZ = P[target].VanadiumMassFrac * oldmass;
				P[target].VanadiumMassFrac = (massZ + cofactor * dtzmass[i]) / newmass;
			}
		#endif
		#ifdef CHROMIUM
			if(strcmp(element, "Cr") == 0){
				massZ = P[target].ChromiumMassFrac * oldmass;
				P[target].ChromiumMassFrac =(massZ + cofactor * dtzmass[i]) / newmass;
			}
		#endif
		#ifdef MANGANESE
			if(strcmp(element, "Mn") == 0){
				massZ = P[target].ManganeseMassFrac * oldmass;
				P[target].ManganeseMassFrac = (massZ + cofactor * dtzmass[i]) / newmass;
			}
		#endif
		#ifdef IRON
			if(strcmp(element, "Fe") == 0){
				massZ = P[target].IronMassFrac * oldmass;
				P[target].IronMassFrac = (massZ + cofactor * dtzmass[i]) / newmass;
			}
		#endif
		#ifdef COBALT
			if(strcmp(element, "Co") == 0){
				massZ = P[target].CobaltMassFrac * oldmass;
				P[target].CobaltMassFrac = (massZ + cofactor * dtzmass[i]) / newmass;
			}
		#endif
		#ifdef NICKEL
			if(strcmp(element, "Ni") == 0){
				massZ = P[target].NickelMassFrac * oldmass;
				P[target].NickelMassFrac =(massZ + cofactor * dtzmass[i])/ newmass;
			}
		#endif
		#ifdef COPPER
			if(strcmp(element, "Cu") == 0){
				massZ = P[target].CopperMassFrac * oldmass;
				P[target].CopperMassFrac = (massZ + cofactor * dtzmass[i])/ newmass;
			}
		#endif
		#ifdef ZINC
			if(strcmp(element, "Zn") == 0){
				massZ = P[target].ZincMassFrac * oldmass;
				P[target].ZincMassFrac = (massZ + cofactor * dtzmass[i]) / newmass;
			}
		#endif
	}

	P[target].MetalMassFrac = (P[target].MetalMassFrac * oldmass + 
														 cofactor * totalDtMetalMass) / newmass;


}
#endif
#endif
#endif
#endif
#endif
