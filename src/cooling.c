/*****************************************************************************
Author: Ali Snedden
Modified: Ali Snedden
Date: 9/6/13

NSH 341a
Dept. of Physics
University of Notre Dame

Purpose: This is the cooling routine that was originally placed in hydra.c.
	I am removing it from being placed in situ in hydro_evaluate() and giving
	it its own function. This will permit us to integrate it implicitly per 
	Springel, Yoshida & White (2001) eqn 51.  

	This cooling function should be able to handle UV Background and metal 
	enrichment.

******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <mpi.h>
#include <gsl/gsl_math.h>
#include "allvars.h"
#include "proto.h"

static double hubble_a;         //H(t) C&O eqn 29.122, OmegaKurvature != 0, OmegaRel=0
static double hubble_a2;        // = a^2 * H(t)


#ifdef COOLING
/* 
	Returns dA/dloga in internal code units.  rho and gas_entropy should be passed in 
	internal comoving units. They are appropriately converted in this routine.
*/
double cooling(double gas_entropy, void * params)
{
	cooling_params * p			= (cooling_params *)params;
 	int ii			 						= 0;	
 	int jj 									= 0;		
	int temp_index 					= 0;
	int metals_index 				= 0;
	int target 							= p->target; // Target particle index
	double metallicity; 						// log10( (mass_metals/m_gas) / SolarMetalMassFrac )
	double metallicity_diff = 1e10;
	double gas_temp 				= 0;
	double log10_gas_temp   = 0;
	double net_gas_cooling_rate = 0;		//Really Cooling - Heating in ergs / (cm^3 * s)
  double gas_cooling_rate = 0;
  double gas_heating_rate = 0;
	double gas_temp_diff 		= 1e10;
	double dtEntropy_cool 	= 0;
	double dloga						= p->h;	//Step size.
	double rho							= SphP[target].Density;
	double nH								= 0;
	double atime						= 0;		//Scale factor, set =1 when comoving integration =off
	#ifdef UVBACKGROUND
		int hden_index					= 0;
		int redshift_index			= 0;
		double redshift					= 0;
		double redshift_diff		= 1e10;
		double hden							= 0;
		double hden_diff				= 1e10;
	#endif
	#ifdef METALS
		FLOAT metalmassfrac;		//Particle metal mass fraction
	#endif


	#ifdef METALS
		/* Using only metals specified in Makefile present. Weird MetalNormConst and
       SolarMetalNormConst are due to fact that Cloudy uses something like metallicity.
       This is b/c it changes the fraction of metals wrt solar, while it holds n_H 
       and n_He constant. This is worked out on pages 33-36 of my 2014 Daily Notes. 
       I actually think that this may not need those constants, if it is a mistake
       it likely not too major b/c it leads to a change on the order of 0.004 in log10
       metallicity. This is irrelevant given all the other assumptions we've made */
		metalmassfrac = P[target].MetalMass / P[target].Mass;

    //Prevent -inf when taking log10(0) when INIT_METAL_TO_H_RATIO = 0
    if(metalmassfrac < LOWER_BOUND_COOLING_TABLE)
    {
		  metallicity = log10(LOWER_BOUND_COOLING_TABLE);	  	//Assume primordial abundance
    }
    else
    {
		  metallicity = log10(metalmassfrac * MetalNormConst / (SolarMetalMassFrac * 
												SolarMetalNormConst));	
    }
	#else
		//Assume all solar Metals present
		//metallicity = log10(INIT_METAL_TO_H_RATIO);				//Assume primordial abundance
		metallicity = log10(LOWER_BOUND_COOLING_TABLE);	  	//Assume primordial abundance
	#endif


	#ifdef FEEDBACK_W_SPH_KERNAL
	//Check that cooling is turned on
	if(SphP[target].cooling_on == 0){
		SphP[target].cooling_rate = 0.0;

		return (SphP[target].Entropy + (SphP[target].DtEntropy + 0.0) * dloga
				  - gas_entropy);
	}
	#endif


	//Get Cosmological factors
  if(All.ComovingIntegrationOn)
  {
    /* Factors for comoving integration of hydro ..... Recall All.Time = 1/(1+z) */
    #ifndef DARKENERGY
      hubble_a = All.Omega0 / (All.Time * All.Time * All.Time)
            	   + (1 - All.Omega0 - All.OmegaLambda) / (All.Time * All.Time) + 
                  All.OmegaLambda;
    #endif

    #ifdef DARKENERGY
      #ifdef SCALARFIELD
         // Get the dark factor which modifies the Hubble eq
         All.dark_factor = get_dark_factor(All.Time);

         // Use it to modify Hubble eq
         hubble_a = All.Omega0 / (All.Time * All.Time * All.Time)
                  + (1 - All.Omega0 - All.OmegaLambda) / (All.Time * All.Time) +
                  (All.OmegaLambda * All.dark_factor);
      #endif

      #ifdef DARKPARAM
         // Get the dark factor which modifies the Hubble eq
         All.dark_factor = get_dark_factor(All.Time);

         // Use it to modify Hubble eq
         hubble_a = All.Omega0 / (All.Time * All.Time * All.Time)
                  + (1 - All.Omega0 - All.OmegaLambda) / (All.Time * All.Time) +
                  (All.OmegaLambda * All.dark_factor);
      #endif

      #ifdef GCG
         // Get the dark factor which modifies the Hubble eq
         All.dark_factor = get_dark_factor(All.Time);

         // Use it to modify Hubble eq
         hubble_a = All.Omega0 / (All.Time * All.Time * All.Time)
                  + (1 - All.Omega0 - All.OmegaLambda) / (All.Time * All.Time) +
                  (All.OmegaLambda * All.dark_factor);
      #endif
    #endif

    hubble_a = All.Hubble * sqrt(hubble_a);
    hubble_a2 = All.Time * All.Time * hubble_a;
		atime = All.Time;
  }
  else
	{
    hubble_a = hubble_a2 = atime = 1.0;
	}

	//Get Temp and nH
	gas_temp = get_gas_temp(rho, gas_entropy);
	log10_gas_temp = log10(gas_temp);

	if(All.ComovingIntegrationOn){
		nH = rho/pow(All.Time, 3) * All.UnitDensity_in_cgs * HYDROGEN_MASSFRAC *
				 pow(All.HubbleParam, 2) / PROTONMASS;
	}else{
		nH = rho * All.UnitDensity_in_cgs * HYDROGEN_MASSFRAC / PROTONMASS;
	}




	#ifndef UVBACKGROUND
		//Get Cooling Rate from CIE_cooling_Table.
 		if(gas_temp > 1.0e4)
 		{
   		gas_temp_diff 			= 1.0e10;
	 		metallicity_diff	 	= 1.0e10;

			for(jj=0; jj<nTempsCIE; jj++){
				//Find table point closest to gas_temp
 				if (fabs(log10_gas_temp - (double)temp_CIE_array[jj]) < gas_temp_diff)
 				{
 					gas_temp_diff = fabs(log10_gas_temp - (double)temp_CIE_array[jj]);
 					temp_index = jj;
 				}
			}

			//Iterate through cooling table
 			for(ii=0; ii<nMetalsCIE; ii++){
				//Search for closest value of metallicity
				if( (fabs(metallicity - metallicity_CIE_array[ii])) < metallicity_diff)
				{
					metallicity_diff = fabs(metallicity - metallicity_CIE_array[ii]);
					metals_index = ii;
				}
 			}

	
			//Make indices bracket lower corner of cell for interpolation
			if(log10_gas_temp < temp_CIE_array[temp_index]){
				if(temp_index > 0){
					temp_index -= 1;
				}
			}
			if(metallicity < metallicity_CIE_array[metals_index]){
				if(metals_index > 0){
					metals_index -= 1;
				}
			}


			//Check to ensure array bounds are not overstepped
			if(log10_gas_temp > temp_CIE_array[nTempsCIE-1] || temp_index < 0)
			{
				/*printf("Warning! Task: %i  P[%i].ID:  %i oversteps temp_CIE_array[] bounds!!\n"
							"temp_index: %i, log10_gas_temp: %e\n",
							ThisTask, target, P[target].ID, temp_index, log10_gas_temp); */
				if(temp_index > 1){
					temp_index = nTempsCIE - 2;
				}
				if(temp_index < 0){
					fprintf(stderr, "ERROR!!! temp_index < 0\n");
					endrun(5010);
				}
			}
			if(metallicity > metallicity_CIE_array[nMetalsCIE-1] || metals_index < 0)
			{
				/*printf("Warning! Task: %i  P[%i].ID:  %i oversteps metal_CIE_array[] bounds!\n"
							"metals_index: %i  metallicity: %e",
							ThisTask, target, P[target].ID, metals_index, metallicity);*/ 
				if(metals_index > 1){
					metals_index = nMetalsCIE - 2;
				}
				if(metals_index < 0){
					fprintf(stderr, "ERROR!!! metals_index < 0\n");
					endrun(5010);
				}
			}


	
			//Interpolate. 
			gas_heating_rate  =	interpolate2D(CIE_heating_table, metals_index, temp_index, 
												                (float)metallicity, (float)gas_temp);

		  gas_cooling_rate  = interpolate2D(CIE_cooling_table, metals_index, temp_index, 
																(float)metallicity, (float)gas_temp);
			net_gas_cooling_rate = gas_heating_rate - gas_cooling_rate;

 		}
  	else{
    	net_gas_cooling_rate=0.0;				
		}
	#endif

			
	#ifdef UVBACKGROUND
		//Get Cooling Rate from UV_cooling_Table.
 		if(gas_temp > 1.0e1)
		{
   		gas_temp_diff 			= 1.0e10;
	 		metallicity_diff	 	= 1.0e10;

			for(jj=0; jj<nTempsUV; jj++)
			{
				//Find table point closest to gas_temp
 				if (fabs(log10_gas_temp - (double)temp_UV_array[jj]) < gas_temp_diff)
 				{
 					gas_temp_diff = fabs(log10_gas_temp - (double)temp_UV_array[jj]);
 					temp_index = jj;
 				}
			}

			//Iterate through cooling table
 			for(ii=0; ii<nMetalsUV; ii++)
 			{
				//Search for closest value of metallicity
				if( (fabs(metallicity - metallicity_UV_array[ii])) < metallicity_diff)
				{
					metallicity_diff = fabs(metallicity - metallicity_UV_array[ii]);
					metals_index = ii;
				}
 			}


			//Make indices bracket lower corner of cell for interpolation
			if(log10_gas_temp < temp_UV_array[temp_index]){
				if(temp_index > 0){
					temp_index -= 1;
				}
			}
			if(metallicity < metallicity_UV_array[metals_index]){
				if(metals_index > 0){
					metals_index -= 1;
				}
			}

			//Get redshift
			if(All.ComovingIntegrationOn){
				redshift = 1 / All.Time - 1;
			}else{
				redshift = mapTime2Redshift(All.Time);
			}
			hden = log10(nH); 

			// Use CIE cooling when out of UV heating regime.
			if(redshift > 8.5)
			{
				if(gas_temp >= 1.0e4)
				{
					temp_index = temp_index - tempIndexOffset;


					//Check to ensure array bounds are not overstepped
					if(log10_gas_temp > temp_CIE_array[nTempsCIE-1] || temp_index < 0)
					{
						/*printf("Warning! Task: %i  P[%i].ID:  %i oversteps temp_CIE_array[] bounds"
									"temp_index: %i  log10_gas_temp: %e!!\n", 
									ThisTask, target, P[target].ID, temp_index, log10_gas_temp);*/

						if(temp_index > 1){
							temp_index = nTempsCIE - 2;
						}
						if(temp_index < 0){
							fprintf(stderr,"ERROR!!! temp_index < 0!!!!\n");
							endrun(5010);
						}
					}
					if(metallicity > metallicity_CIE_array[nMetalsCIE-1] || metals_index < 0)
					{
						/*printf("Warning! Task: %i  P[%i].ID:  %i oversteps  metal_CIE_array[] "
									"bounds!!!\nmetals_index: %i  metallicity: %e\n", 
									ThisTask, target, P[target].ID, metals_index, metallicity); */
						if(metals_index > 1){
							metals_index = nMetalsCIE - 2;
						}
						if(metals_index < 0){
							fprintf(stderr,"ERROR!!! metals_index < 0!!!!\n");
							endrun(5010);
						}
					}

				  gas_heating_rate = interpolate2D(CIE_heating_table, metals_index, temp_index, 
																(float)metallicity, (float)gas_temp);
          gas_cooling_rate = interpolate2D(CIE_cooling_table, metals_index, temp_index, 
																(float)metallicity, (float)gas_temp);

		 			net_gas_cooling_rate = gas_heating_rate - gas_cooling_rate;

				}
				else
				{
					net_gas_cooling_rate = 0.0;
				}
			}
			//UV Heating/Cooling
			else
			{
				//Redshifts
				for(ii=0; ii<nRedshifts; ii++)
				{
					if( (fabs(redshift - redshift_array[ii])) < redshift_diff)
					{
						redshift_diff = fabs(redshift - redshift_array[ii]); 
						redshift_index = ii;
					}
				}

				//H-Densities
				for(ii=0; ii<nHden; ii++)
				{
					if( (fabs(hden - hden_array[ii])) < hden_diff)
					{
						hden_diff = fabs(hden - hden_array[ii]);
						hden_index = ii;
					}
				}


				//Make indices bracket lower corner of cell for interpolation
				if(redshift < redshift_array[redshift_index]){
					if(redshift_index > 0){
						redshift_index -= 1;
					}
				}
				if(hden < hden_array[hden_index]){
					if(hden_index > 0){
						hden_index -= 1;
					}
				}


				//Check to ensure array bounds are not overstepped
				if(log10_gas_temp > temp_UV_array[nTempsUV-1] || temp_index < 0)
				{
					/*printf("Warning! Task: %i  P[%i].ID:  %i oversteps temp_UV_array[] bounds\n"
								"temp_index: %i  log10_gas_temp: %e!!\n", 
								ThisTask, target, P[target].ID, temp_index, log10_gas_temp); */
					if(temp_index > 1){
						temp_index = nTempsUV - 2;
					}
					if(temp_index < 0){
						fprintf(stderr, "ERROR!!! temp_index < 0\n");
						endrun(5010);
					}
				}
				if(metallicity > metallicity_UV_array[nMetalsUV-1] || metals_index < 0)
				{
					/*printf("Warning! Task: %i  P[%i].ID:  %i oversteps metallicity_UV_array[]"
								" bounds!\nmetals_index: %i  metallicity: %e\n", 
								ThisTask, target, P[target].ID, metals_index, metallicity); */
					if(metals_index > 1){
						metals_index = nMetalsUV - 2;
					}
					if(metals_index < 0){
						fprintf(stderr, "ERROR!!! metals_index < 0\n");
						endrun(5010);
					}
				}
				if(hden > hden_array[nHden-1] || hden_index < 0)
				{
					/*printf("Warning! Task: %i  P[%i].ID:  %i oversteps hden_array[] bounds!\n"
								"hden_index: %i  hden: %e\n", 
								ThisTask, target, P[target].ID, hden_index, hden); */
					if(hden > 1){
						hden_index = nHden - 2;
					}
					if(hden_index < 0){
						fprintf(stderr, "ERROR!!! hden_index < 0\n");
						endrun(5010);
					}
				}

				gas_heating_rate = interpolate4D(UV_heating_table, redshift_index, hden_index, 
									         temp_index, metals_index, (float)redshift, (float)hden, 
								           (float)gas_temp, (float)metallicity);

				gas_cooling_rate = interpolate4D(UV_cooling_table, redshift_index, hden_index, 
									         temp_index, metals_index, (float)redshift, (float)hden, 
									         (float)gas_temp, (float)metallicity);
	
				net_gas_cooling_rate = gas_heating_rate - gas_cooling_rate;
			}
		}
  	else{
    	net_gas_cooling_rate=0.0;				
		}
	#endif


	//Multiply Lambda_norm (erg*cm^3/s) by (n_H)^2 toget Lambda_net (ergs/(cm^3*s)).
 	if(All.ComovingIntegrationOn){
   	net_gas_cooling_rate *= nH * nH /(All.UnitCoolingRate_in_cgs * pow(All.HubbleParam, 3));
   	gas_cooling_rate     *= nH * nH /(All.UnitCoolingRate_in_cgs * pow(All.HubbleParam, 3));
 	}else{
 		net_gas_cooling_rate *= nH * nH / All.UnitCoolingRate_in_cgs; 
 		gas_cooling_rate     *= nH * nH / All.UnitCoolingRate_in_cgs; 
	}


	//Eqn 9 in SH02...This is the physical entropy cooling (in INTERNAL code units).
 	if(All.ComovingIntegrationOn)
 	{
 		dtEntropy_cool = net_gas_cooling_rate * GAMMA_MINUS1 / 
										 pow(rho/pow(All.Time, 3), GAMMA_MINUS1 + 1);
 	}
 	else
 	{
   	dtEntropy_cool = net_gas_cooling_rate * GAMMA_MINUS1/pow(rho, GAMMA_MINUS1 + 1);
 	}


	//Convert dA/dt -> dA/da -> dA/dloga. Recall da/dt = a*H(a) and dloga/da = 1/a
 	dtEntropy_cool *= atime * hubble_a / atime; 

	//Update cooling rate
	SphP[target].cooling_rate = gas_cooling_rate;


	return (SphP[target].Entropy + (SphP[target].DtEntropy + dtEntropy_cool) * dloga
				  - gas_entropy);
}








/****************************************************************************
	This takes in a 2 dimensional table and interpolates it.
	See Numerical Recipes in C (2nd edition) p. 123. 
	Below we do the bilinear interpolation that is 
	"Good enough for government work". This is intended to
	interpolate between metallicities and temperature to get
	the necessary cooling rate (See Cloudy10 and Sutherland &
	Dopita 1993)

	Below is a reproduction of Fig. 3.6.1 in an intelligent 
	manner.


 q_4(x_1, y_2)                           q_3(x_2, y_2)
----O-----------------------------------------O-----------
    |                                         |
    |                                         |
    |                                         |
    |      O                                  |
    |    p(gas_temp, metallicity)             |
    |                                         |
    |                                         |
    |                                         |
    |                                         |
    |                                         | 
    |                                         |
    |                                         |
    |                                         |
----O-----------------------------------------O-----------
 q_1(x_1, y_1)                           q_2(x_2, y_1)


	B/c my values are spaced evenly in both 'x' and 'y', I do not need to 
	have an efficient searching algorithm. If they were not uniformly spaced
	I would HAVE to develop a searching algorithm, recursive function and all.
	Why waste my time when I can make my life easier today?

	If this assumption is false for the table then you are SCREWED!

	NOTE : This function was checked 9/13/13 and appears to be working.

	How indices map...
	i -> metals_index .... metallicity
	j -> temp_index   .... gas_temp
****************************************************************************/
double interpolate2D(float** table, int i, int j, 
														 float metallicity, float gas_temp)
{
	float t = 0;
	float u = 0;
	float result = 0;

	//Initial location in phase space
	float x1 = metallicity_CIE_array[i];
	float y1 = temp_CIE_array[j];

	//Final Location in phase space
	float x2 = metallicity_CIE_array[i+1];
	float y2 = temp_CIE_array[j+1];

	float q1 = table[i  ][j  ];
	float q2 = table[i+1][j  ];
	float q3 = table[i+1][j+1];
	float q4 = table[i  ][j+1];

	//Step size in each dimension
	float step_x = x2 - x1;					//step size in x
	float step_y = y2 - y1;					//step size in y


	//Interpolate. See section 3.6, Num. Recip. in C
	t = (metallicity - x1)/step_x;
	u = (log10(gas_temp)    - y1)/step_y;

	result = (1 - t) * (1 - u) * q1 + 
						    t  * (1 - u) * q2 + 
					      t  *      u  * q3 + 
           (1 - t) *      u  * q4;

	return result;
}







#ifdef UVBACKGROUND
/***********************************************************************
	Here i,j,k,l are the lower indices that bracket the value
  to be interpolated. Thus the point to be interpolated is between 
  (i,j,k,l) and (i+1,j+1,k+1,l+1).

	(redshift, hden, temp, metallicity) are the values of the point to 
	be evaluated.

  Often you'll have to use the "hunt" or "locate" algorithm to get 
  these indices.

  :::::: WARNING ::::::
  This program does NOT check the endpoints of the table. This is 
  presumably done when i,j,k,l are found.

	Here are how my variables and coordinates map to each other...
		x1, x2 -> redshift 		.... index : i
		y1, y2 -> hden     		.... index : j
		z1, z2 -> temp     		.... index : k
		w1, w2 -> metallicity .... index : l
************************************************************************/
double interpolate4D(float**** table, int i, int j, int k, int l,
										 float redshift, float hden, float temp, float metallicity)
{
	//Declare variables.
	float q1  = table[i  ][j  ][k  ][l  ];				//See my notes from 3/12/13
	float q2  = table[i+1][j  ][k  ][l  ];
	float q3  = table[i+1][j+1][k  ][l  ];
	float q4  = table[i+1][j+1][k+1][l  ];
	float q5  = table[i+1][j+1][k+1][l+1];
	float q6  = table[i+1][j+1][k  ][l+1];
	float q7 	= table[i+1][j  ][k+1][l  ];
	float q8 	= table[i+1][j  ][k+1][l+1];
	float q9 	= table[i+1][j  ][k  ][l+1];
	float q10 = table[i  ][j+1][k  ][l  ];
	float q11 = table[i  ][j+1][k+1][l  ];
	float q12 = table[i  ][j+1][k+1][l+1];
	float q13 = table[i  ][j+1][k  ][l+1];
	float q14 = table[i  ][j  ][k+1][l  ];
	float q15 = table[i  ][j  ][k+1][l+1];
	float q16 = table[i  ][j  ][k  ][l+1];

	//Initial location in phase space
	float x1 = redshift_array[i];										//This is x_1 in my daily notes
	float y1 = hden_array[j];
	float z1 = temp_UV_array[k];
	float w1 = metallicity_UV_array[l];

	//Final location in phase space
	float x2 = redshift_array[i+1];						//This is x_1 in my daily notes
	float y2 = hden_array[j+1];
	float z2 = temp_UV_array[k+1];
	float w2 = metallicity_UV_array[l+1];
	
	//Step sizes in each dimension
	float step_x = x2 - x1;
	float step_y = y2 - y1;
	float step_z = z2 - z1;
	float step_w = w2 - w1;

	//cofactors in x,y,z,w directions respectively see Num. Rec. .
	float a = (redshift - x1) / step_x;   					// = alpha from my daily notes
	float b = (hden - y1) / step_y;									// = beta  from my daily notes
	float g = (log10(temp) - z1) / step_z;									// = gamma from my daily notes
	float d = (metallicity - w1) / step_w;					// = delta from my daily notes

	float result = 	(1-a) * (1-b) * (1-g) * (1-d) * q1  +   
									   a  * (1-b) * (1-g) * (1-d) * q2  +   
										 a  *    b  * (1-g) * (1-d) * q3  +
									   a  *    b  *    g  * (1-d) * q4  +           
										 a  *    b  *    g  *    d  * q5  +
										 a  *    b  * (1-g) *    d  * q6  +   
										 a  * (1-b) *    g  * (1-d) * q7  +
										 a  * (1-b) *    g  *    d  * q8  +   
										 a  * (1-b) * (1-g) *    d  * q9  +
									(1-a) *    b  * (1-g) * (1-d) * q10 +   
									(1-a) *    b  *    g  * (1-d) * q11 +
									(1-a) *    b  *    g  *    d  * q12 +  	
									(1-a) *    b  * (1-g) *    d  * q13 +
									(1-a) * (1-b) *    g  * (1-d) * q14 +
									(1-a) * (1-b) *    g  *    d  * q15 +
									(1-a) * (1-b) * (1-g) *    d  * q16;
	

	return result;
}
#endif





/*
  Expects:
    gas_entropy = internal, cartesian units
    rho         = internal, comoving units
*/
double get_gas_temp(double rho, double gas_entropy) 
{
 	double molecular_weight = 0;		//Consider mu = total mass / (n_He + n_H + n_e)
 	double gas_u 						= 0;		//Internal Energy
	double gas_temp 				= 0;

	/*
		NOTE : This is cooling the gas particles using the SphP[].Pressure and SphP[].Rho
					 computed during the PREVIOUS time step where this particle was updated. 

					 If UV-Background enabled, we assume mu = 0.59 b/c the gas is still mostly
					 photoionized b/c self-shielding is ignored. See Schaye & Dalla Vecchia (2008)
					 p1214.
	*/
	#ifndef UVBACKGROUND
 		if(All.InitGasTemp > 1.0e4){   	//assuming FULL ionization 
 		  molecular_weight = 4 / (8 - 5 * (1 - HYDROGEN_MASSFRAC));
 		}else{                  				//assuming NEUTRAL GAS 
 		  molecular_weight = 4 / (1 + 3 * HYDROGEN_MASSFRAC);
		}
	#endif
	#ifdef UVBACKGROUND
		molecular_weight = 4 / (8 - 5 * (1 - HYDROGEN_MASSFRAC));
	#endif


	//NOTE: pressure = (  ) * rho^GAMMA, so rho's cancel
 	//gas_entropy = pressure/pow(rho, GAMMA);    //eqn 8 Springel & Hernquist(2002)

	// u_code units = (internal energy / internal mass)
 	if(All.ComovingIntegrationOn){             //eqn 10 SH02
 	  gas_u=gas_entropy*pow(rho/pow(All.Time, 3), GAMMA_MINUS1)/GAMMA_MINUS1;
 	}else{
   	gas_u=gas_entropy*pow(rho, GAMMA_MINUS1)/GAMMA_MINUS1;
	}


	/*	
		Assume that .... u_physical = (1/GAMMA_MINUS1)*NkT .... So ....

		u_physical = mass * All.UnitMass_in_g * AVOGADRO * k * T / 
							 (GAMMA_MINUS1 * molecular_weight)

		u_code = u_physical / (All.UnitEnergy_in_cgs * mass)

		u_code = gas_u = All.UnitMass_in_g * k * T /
						(GAMMA_MINUS1 * All.UnitEnergy_in_cgs * PROTONMASS * molecular_weight)
	 
		Solve for T, see below. NOTE: 1/PROTONMASS ~= AVOGADRO
		You can convince yourself of this by reading io.c, read_ic.c and 
		section 2.2 in GADGET2 Paper. Look at allvars.h, factors of HubbleParam cancel	
	*/
	gas_temp = (gas_u * GAMMA_MINUS1 * All.UnitEnergy_in_cgs * PROTONMASS *
						 molecular_weight) / (All.UnitMass_in_g * BOLTZMANN);


	//Can't have negative temperatures!
	if(gas_temp < 0)
	{
		gas_temp = 0.001;
	}

	return gas_temp;
}



double convert_T_to_Entropy(double rho, double log10T)
{
	double Entropy = 0.0;
	double molecular_weight = 0.0;
	double T = pow(10, log10T);

	/*
	 If UV-Background enabled, we assume mu = 0.59 b/c the gas is still mostly
	 photoionized b/c self-shielding is ignored. See Schaye & Dalla Vecchia (2007)
	 p1214.
	*/
	#ifndef UVBACKGROUND	
		if(All.InitGasTemp > 1.0e4){
			molecular_weight = 4 / (8 - 5 * (1 - HYDROGEN_MASSFRAC));
		}else{
			molecular_weight = 4 / (1 + 3 * HYDROGEN_MASSFRAC);
		}
	#endif
	#ifdef UVBACKGROUND
		molecular_weight = 4 / (8 - 5 * (1 - HYDROGEN_MASSFRAC));
	#endif

	Entropy = (BOLTZMANN * T * All.UnitMass_in_g ) / 
						(All.UnitEnergy_in_cgs * PROTONMASS * GAMMA_MINUS1 * molecular_weight);

	//Check for comoving integration
 	if(All.ComovingIntegrationOn){        
		Entropy *= GAMMA_MINUS1 / pow(rho / pow(All.Time, 3), GAMMA_MINUS1);
	}else{
		Entropy *= GAMMA_MINUS1 / pow(rho, GAMMA_MINUS1);
	}

	return Entropy;
}
#endif
