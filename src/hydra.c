/***************************************************************************
Author: Volker Springel & Xinghai Zhao
Modified: Ali Snedden
Date: 11/1/12

NSH 341a
Dept. of Physics
University of Notre Dame

Purpose: This was originally written by Springel and modified
	by Xinghai. In Xinghai's star_form.c, he did a bum job, so
	hopefully I can find his mistakes where he modified this.
	God help us.
	
	Kobayashi(2004) = K04
	Springel & Hernquist(2002) = SH02
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <mpi.h>
#include <gsl/gsl_math.h>
#include "allvars.h"
#include "proto.h"

/*! \file hydra.c
 *  \brief Computation of SPH forces and rate of entropy generation
 *
 *  This file contains the "second SPH loop", where the SPH forces are
 *  computed, and where the rate of change of entropy due to the shock heating
 *  (via artificial viscosity) is computed.
 */


static double hubble_a;         //H(t) C&O eqn 29.122, OmegaKurvature != 0, OmegaRel=0
static double atime;						// = a = scale factor = All.Time
static double hubble_a2;        // = a^2 * H(t)
static double fac_mu;           // = (a^((3*gamma-3)/2)) / a = 1 (for ideal gas)
static double fac_vsic_fix;
static double a3inv;
static double fac_egy;

#ifdef PERIODIC
static double boxSize;					//Comoving size of the volume (I think?)
static double boxHalf;

#ifdef LONG_X
static double boxSize_X, boxHalf_X;
#else
#define boxSize_X boxSize
#define boxHalf_X boxHalf
#endif
#ifdef LONG_Y
static double boxSize_Y, boxHalf_Y;
#else
#define boxSize_Y boxSize
#define boxHalf_Y boxHalf
#endif
#ifdef LONG_Z
static double boxSize_Z, boxHalf_Z;
#else
#define boxSize_Z boxSize
#define boxHalf_Z boxHalf
#endif
#endif



/*! This function is the driver routine for the calculation of hydrodynamical
 *  force and rate of change of entropy due to shock heating for all active
 *  particles .
 */
void hydro_force(void)
{
  long long ntot;					//Initial GLOBAL Number of SPH part. needing updated 
	long long ntotleft;			//GLOBAL Num SPH part. left needing updated
  int i;
	int j;									//Index for Number of processors
	int k;
	int n;
	int ngrp;
	int maxfill;
	int source;							//Index to go from HydroDataPartialResult[] to  HydroDataIn[]
	int ndone;							//Number of particle that have been updated.
  int * nbuffer;					//
	int * noffset;					//Given a recvTask, return corresponding sending particles location in CommBuffer
	int * nsend_local;			//Number LOCAL of particles to send to j'th Task
	int * nsend;						//Combination of all Task's nsend_local[]
	int * numlist;					//Holds 'NumSphUpdate' from each task(# SphP needing updated)
	int * ndonelist;
  int level;							//
	int sendTask;						//Index of sending Task
	int recvTask;						//Index of receiving Task
	int nexport;						//Total Number of LOCAL SphP to export
	int place;							//Index to go from HydroDataIn[] to SphP[]
  double soundspeed_i;
  double tstart, tend, sumt, sumcomm;
  double timecomp = 0, timecommsumm = 0, timeimbalance = 0, sumimbalance;
  MPI_Status status;

	#ifdef PERIODIC
  	boxSize = All.BoxSize;
  	boxHalf = 0.5 * All.BoxSize;
		#ifdef LONG_X
		  boxHalf_X = boxHalf * LONG_X;
		  boxSize_X = boxSize * LONG_X;
		#endif
		#ifdef LONG_Y
		  boxHalf_Y = boxHalf * LONG_Y;
		  boxSize_Y = boxSize * LONG_Y;
		#endif
		#ifdef LONG_Z
  		boxHalf_Z = boxHalf * LONG_Z;
		  boxSize_Z = boxSize * LONG_Z;
		#endif
	#endif


	//Get Cosmological factors
  if(All.ComovingIntegrationOn)
  {
    /* Factors for comoving integration of hydro ..... Recall All.Time = 1/(1+z) */

    #ifndef DARKENERGY
      hubble_a = All.Omega0 / (All.Time * All.Time * All.Time)
               	+ (1 - All.Omega0 - All.OmegaLambda) / (All.Time * All.Time) + 
                 All.OmegaLambda;
    #endif

    #ifdef DARKENERGY
         #ifdef SCALARFIELD
            All.dark_factor = get_dark_factor(All.Time);
            hubble_a = All.Omega0 / (All.Time * All.Time * All.Time)
                  	+ (1 - All.Omega0 - All.OmegaLambda) / (All.Time * All.Time) + 
                    (All.OmegaLambda * All.dark_factor);
         #endif

         #ifdef DARKPARAM
            All.dark_factor = get_dark_factor(All.Time);
            hubble_a = All.Omega0 / (All.Time * All.Time * All.Time)
                  	+ (1 - All.Omega0 - All.OmegaLambda) / (All.Time * All.Time) + 
                    (All.OmegaLambda * All.dark_factor);
         #endif

         #ifdef GCG
            All.dark_factor = get_dark_factor(All.Time);
            hubble_a = All.Omega0 / (All.Time * All.Time * All.Time)
                  	+ (1 - All.Omega0 - All.OmegaLambda) / (All.Time * All.Time) + 
                    (All.OmegaLambda * All.dark_factor);
         #endif
    #endif

    hubble_a = All.Hubble * sqrt(hubble_a);
    hubble_a2 = All.Time * All.Time * hubble_a;

    fac_mu = pow(All.Time, 3 * (GAMMA - 1) / 2) / All.Time;

    fac_egy = pow(All.Time, 3 * (GAMMA - 1));

    fac_vsic_fix = hubble_a * pow(All.Time, 3 * GAMMA_MINUS1);

    a3inv = 1 / (All.Time * All.Time * All.Time);
    atime = All.Time;
  }
  else
    hubble_a = hubble_a2 = atime = fac_mu = fac_vsic_fix = a3inv = fac_egy = 1.0;


  /* `NumSphUpdate' gives the number of particles on this processor that want a 
      force update */
  for(n = 0, NumSphUpdate = 0; n < N_gas; n++)
  {
    if(P[n].Ti_endstep == All.Ti_Current)
    	NumSphUpdate++;
  }

	//Get Number of Particles needing updated from All tasks
  numlist = malloc(NTask * sizeof(int) * NTask);
  MPI_Allgather(&NumSphUpdate, 1, MPI_INT, numlist, 1, MPI_INT, MPI_COMM_WORLD);
  for(i = 0, ntot = 0; i < NTask; i++)
    ntot += numlist[i];															//Find GLOBAL number SPH to update
  free(numlist);


	//Allocate Memory
  noffset 		= malloc(sizeof(int) * NTask);	/* offsets of bunches in common list */
  nbuffer 		= malloc(sizeof(int) * NTask);
  nsend_local = malloc(sizeof(int) * NTask);
  nsend 			= malloc(sizeof(int) * NTask * NTask);
  ndonelist 	= malloc(sizeof(int) * NTask);


  i = 0;							/* first particle for this task */
  ntotleft = ntot;		/* particles left for all tasks together */

	/* 
		Iterate through all the SPH particles needing updated. Iterate until 
		no more GLOBAL particles are left to communicate. Also this will loop
		through until all LOCAL particles have had their Hydro Force evaluated.
	*/
  while(ntotleft > 0)
  {
    for(j = 0; j < NTask; j++)				//Initialize to 0
	    nsend_local[j] = 0;


    /* do local particles and prepare export list */

    tstart = second();								//Get wall time or CPU ticks


		/* 
			Iterate thru LOCAL gas particles (i.e. at beginning of P[]). Be sure that 
			the communication buffer isn't full of SPH particles that need exported 
			to other Tasks.
			
			Calculate hydro force for LOCAL particles and prepare list of particles to 
			export to other Tasks. 

			NOTE: P[i] doesn't necessarily start at P[0]!!!!
		 */
    for(nexport = 0, ndone = 0; i < N_gas && nexport < All.BunchSizeHydro - NTask; i++)
		{ 
			//Evaluate Hydro Force for LOCAL particles needing update
	    if(P[i].Ti_endstep == All.Ti_Current)	
	    {
	      ndone++;

	      for(j = 0; j < NTask; j++)
	        Exportflag[j] = 0;

	      hydro_evaluate(i, 0);			


				/* 
					Iterate through processors which contain non-LOCAL particles that P[i] 
					will be interacting with. This was written to Exportflag[] and generated when 
					hydro_evaluate(i,0) was called.

					Save P[i] to the communication buffer for future communication with these 
					other processors. This is conserve Newton's second law. 

					So ThisTask writes P[i]'s info to be exported to the other Task
				*/
	      for(j = 0; j < NTask; j++)
	      {

					//Are there Nearby Particles on other Processors?
	      	if(Exportflag[j])					//Set in hydro_evaluate() ->  ngb_treefind_pairs()
	    	  {
		
						/* HydroDataIn points to the CommBuffer */
	    	    for(k = 0; k < 3; k++)
	  	      {
	        		HydroDataIn[nexport].Pos[k] = P[i].Pos[k];
	        		HydroDataIn[nexport].Vel[k] = SphP[i].VelPred[k];
	  	      }
	    	    HydroDataIn[nexport].Hsml = SphP[i].Hsml;
	    	    HydroDataIn[nexport].Mass = P[i].Mass;
	    	    HydroDataIn[nexport].DhsmlDensityFactor = SphP[i].DhsmlDensityFactor;
	    	    HydroDataIn[nexport].Density = SphP[i].Density;
	    	    HydroDataIn[nexport].Pressure = SphP[i].Pressure;
	    	    HydroDataIn[nexport].Timestep = P[i].Ti_endstep - P[i].Ti_begstep;

	    	    /* calculation of F1. Eqn 17 GADGET2*/
	    	    soundspeed_i = sqrt(GAMMA * SphP[i].Pressure / SphP[i].Density);
	    	    HydroDataIn[nexport].F1 = fabs(SphP[i].DivVel) /
	                            	      (fabs(SphP[i].DivVel) + SphP[i].CurlVel +
	                            	       0.0001 * soundspeed_i / SphP[i].Hsml / fac_mu);

	    	    HydroDataIn[nexport].Index = i;
	    	    HydroDataIn[nexport].Task = j;
	    	    nexport++;
	    	    nsend_local[j]++;
	    	  }
	      }
	    }
		}
    tend = second();
    timecomp += timediff(tstart, tend);

		//Group HydroDataIn[] particles by Task to exported to, makes easier to communicate
    qsort(HydroDataIn, nexport, sizeof(struct hydrodata_in), hydro_compare_key);

    for(j = 1, noffset[0] = 0; j < NTask; j++)
	    noffset[j] = noffset[j - 1] + nsend_local[j - 1];

    tstart = second();

		/*
			Get number of particles to be exported by each Task and to which Task it 
			will be exported.
		*/
    MPI_Allgather(nsend_local, NTask, MPI_INT, nsend, NTask, MPI_INT, MPI_COMM_WORLD);

    tend = second();
    timeimbalance += timediff(tstart, tend);



    /* Calculate Hydro Force for NON-LOCAL particles */

    for(level = 1; level < (1 << PTask); level++)
	  {
	    tstart = second();
	    for(j = 0; j < NTask; j++)
			{
	      nbuffer[j] = 0;
			}

			//Complex way of sending/receiving particles between Tasks. I don't understand
	    for(ngrp = level; ngrp < (1 << PTask); ngrp++)
	    {
	      maxfill = 0;
	      for(j = 0; j < NTask; j++)
	    	{
	    	  if((j ^ ngrp) < NTask)                   //This is bitwise XOR comparison
	    	    if(maxfill < nbuffer[j] + nsend[(j ^ ngrp) * NTask + j])
	    	      maxfill = nbuffer[j] + nsend[(j ^ ngrp) * NTask + j];
	    	}
	      if(maxfill >= All.BunchSizeHydro)
	      	break;

	      sendTask = ThisTask;
	      recvTask = ThisTask ^ ngrp;

        if(recvTask < NTask)
      	{
      	  if(nsend[ThisTask * NTask + recvTask] > 0 || 
             nsend[recvTask * NTask + ThisTask] > 0)
	        {
	          /* 
							Send and Recieve the particles. Blocks communication  until 
							send buffer is free and receive buffer is filled. The send and
							receive buffers point to different sections of the CommBuffer
						*/
	          MPI_Sendrecv(&HydroDataIn[noffset[recvTask]],
				               nsend_local[recvTask] * sizeof(struct hydrodata_in), MPI_BYTE,
				               recvTask, TAG_HYDRO_A, &HydroDataGet[nbuffer[ThisTask]],
				               nsend[recvTask * NTask + ThisTask] * sizeof(struct hydrodata_in),
                       MPI_BYTE, recvTask, TAG_HYDRO_A, MPI_COMM_WORLD, &status);
	        }
	      }

        for(j = 0; j < NTask; j++)
	        if((j ^ ngrp) < NTask)
	          nbuffer[j] += nsend[(j ^ ngrp) * NTask + j];
      }
      tend = second();
      timecommsumm += timediff(tstart, tend);

      /* now do the imported particles */
      tstart = second();
      for(j = 0; j < nbuffer[ThisTask]; j++)
			{
        hydro_evaluate(j, 1);		
			}
      tend = second();
      timecomp += timediff(tstart, tend);

      /* do a block to measure imbalance */
      tstart = second();
      MPI_Barrier(MPI_COMM_WORLD);
      tend = second();
      timeimbalance += timediff(tstart, tend);

      /* get the result */
      tstart = second();
      for(j = 0; j < NTask; j++)
        nbuffer[j] = 0;
      for(ngrp = level; ngrp < (1 << PTask); ngrp++)
      {
        maxfill = 0;
        for(j = 0; j < NTask; j++)
	      {
	        if((j ^ ngrp) < NTask)
	          if(maxfill < nbuffer[j] + nsend[(j ^ ngrp) * NTask + j])
	            maxfill = nbuffer[j] + nsend[(j ^ ngrp) * NTask + j];
	      }
        if(maxfill >= All.BunchSizeHydro)
	        break;

        sendTask = ThisTask;
        recvTask = ThisTask ^ ngrp;

        if(recvTask < NTask)
	      {
	        if(nsend[ThisTask * NTask + recvTask] > 0 || 
             nsend[recvTask * NTask + ThisTask] > 0)
	        {
	          /*  
							Send and Recieve the Results. Blocks communication  until 
							send buffer is free and receive buffer is filled. The send and
							receive buffers point to different sections of the CommBuffer

							HydroDataResult        = sending buffer
							HydroDataPartialResult = receiving buffer
						*/
	          MPI_Sendrecv(&HydroDataResult[nbuffer[ThisTask]],
				              nsend[recvTask * NTask + ThisTask] * sizeof(struct hydrodata_out),
				              MPI_BYTE, recvTask, TAG_HYDRO_B,
				              &HydroDataPartialResult[noffset[recvTask]],
				              nsend_local[recvTask] * sizeof(struct hydrodata_out),
				              MPI_BYTE, recvTask, TAG_HYDRO_B, MPI_COMM_WORLD, &status);

	          /* add the result to the particles */
	          for(j = 0; j < nsend_local[recvTask]; j++)
		        {
							/*
								NOTE: HydroDataIn[source] and HydroDataPartialResult[source] are 
								referring to the same particles.
							*/
		          source = j + noffset[recvTask];					
		          place = HydroDataIn[source].Index;

		          for(k = 0; k < 3; k++){
		            SphP[place].HydroAccel[k] += HydroDataPartialResult[source].Acc[k];
							}

		          SphP[place].DtEntropy += HydroDataPartialResult[source].DtEntropy;

		          if(SphP[place].MaxSignalVel < HydroDataPartialResult[source].MaxSignalVel)
							{
		            SphP[place].MaxSignalVel = 
                                   HydroDataPartialResult[source].MaxSignalVel;
							}
		        }
	        }
	      }

        for(j = 0; j < NTask; j++)
	        if((j ^ ngrp) < NTask)
	          nbuffer[j] += nsend[(j ^ ngrp) * NTask + j];
      }
	    tend = second();
	    timecommsumm += timediff(tstart, tend);

      level = ngrp - 1;
    }

		//Total up the number of GLOBAL particles to update
    MPI_Allgather(&ndone, 1, MPI_INT, ndonelist, 1, MPI_INT, MPI_COMM_WORLD);
    for(j = 0; j < NTask; j++)
      ntotleft -= ndonelist[j];
  }

  free(ndonelist);
  free(nsend);
  free(nsend_local);
  free(nbuffer);
  free(noffset);



  /* do final operations on results */
  tstart = second();


  for(i = 0; i < N_gas; i++){
    if(P[i].Ti_endstep == All.Ti_Current)
    {
			// Now SphP[].DtEntropy = dA / da
	    SphP[i].DtEntropy *= GAMMA_MINUS1 / (hubble_a2 * pow(SphP[i].Density, 
                                           GAMMA_MINUS1));
			#ifdef SPH_BND_PARTICLES
	    	if(P[i].ID == 0)
      	{
      	  SphP[i].DtEntropy = 0;
      	  for(k = 0; k < 3; k++)
      	    SphP[i].HydroAccel[k] = 0;
      	}
			#endif
    }
	}

  tend = second();
  timecomp += timediff(tstart, tend);

  /* collect some timing information */

  MPI_Reduce(&timecomp, &sumt, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
  MPI_Reduce(&timecommsumm, &sumcomm, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
  MPI_Reduce(&timeimbalance, &sumimbalance, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);

  if(ThisTask == 0)
  {
    All.CPU_HydCompWalk += sumt / NTask;
    All.CPU_HydCommSumm += sumcomm / NTask;
    All.CPU_HydImbalance += sumimbalance / NTask;
  }
}


/*! This function is the 'core' of the SPH force computation. A target
 *  particle is specified which may either be local, or reside in the
 *  communication buffer.

 *  Subscript 'i' is for the 'target' particle.
 *  Subscript 'j' is for the other particles
 * 
 *  If mode == 0, we are working with LOCAL particles. If mode == 1
 *  we are non-LOCAL particles.
 */
void hydro_evaluate(int target, int mode)
{
  int j;								//SPH index of particle "near" to target
	int k, n;
	int timestep;
	int startnode;				//All.MaxPart...Root Node
	int numngb;						//Number of sph particles w/in 1 smoothing length
  FLOAT *pos;						//target Position
	FLOAT *vel;						//target predicted velocity
  FLOAT mass;						//target mass
	FLOAT h_i;						//target current smoothing length
	FLOAT dhsmlDensityFactor;	//target correction factor in eq. of motion
	FLOAT rho;						//target density
	FLOAT pressure;				//target pressure
	//FLOAT gas_entropy;		//target entropy
	FLOAT f1;							//What is this? has form reminiscient of Eq.17
	FLOAT f2;
  double acc[3];				//
	double dtEntropy;			//Change Entropy for target
	double maxSignalVel;
  double dx, dy, dz;		//Vector components of r
	double dvx, dvy, dvz;	//Vector components of SphP[target].VelPred - SphP[j].VelPred
  double h_i2;					//target particle smoothing length
	double hinv; 
	double hinv4;
  double p_over_rho2_i;	//Eqn 7 ..... Pressure/Density_i
	double p_over_rho2_j; //Eqn 7 ..... Pressure/Density_j
	double soundspeed_i;	//target's local speed of sound
	double soundspeed_j;	//j'th particles' local sound speed
  double hfc;						//Viscoscity term or const. 
	double dwk_i;         //grad_{i}(W_{ij}) 
  double vdotr;					// = dx * vdx + dy * vdy + dz * vdz (PHYSICAL not COMOVING!)
	double vdotr2;				// = vdotr + hubble_a2 * r2  (PHYSICAL!)
	double visc;					// = viscous tensor = (Eq. 14) * (average of Eq. 17)
	double mu_ij;					// = w_ij .... see Eq. 13 in GADGET2
	double rho_ij;				// = (rho_i + rho_j)/2
	double vsig;					//Eqn 13 .... signal velocity
  double h_j;           //j'th particles' smoothing length
  double dwk_j;         //grad_{j}(W_{ij})   ?
  double r;							//Vector joining target and j'th particles
	double r2;						//r^2
	double u, hfc_visc;
	
	/*
	#ifdef COOLING
		double dtEntropy_cool = 0.0;
	#endif
	*/

	#ifndef NOVISCOSITYLIMITER
  	double dt;
	#endif

	//For local particles.
  if(mode == 0)
  {
    pos = P[target].Pos;					
    vel = SphP[target].VelPred;
    h_i = SphP[target].Hsml;
    mass = P[target].Mass;
    dhsmlDensityFactor = SphP[target].DhsmlDensityFactor;
    rho = SphP[target].Density;
    pressure = SphP[target].Pressure;
    timestep = P[target].Ti_endstep - P[target].Ti_begstep;
    soundspeed_i = sqrt(GAMMA * pressure / rho);	//P is fnct(rho), 'a' factors cancel
    f1 = fabs(SphP[target].DivVel) /    		/*Eqn 17 Paper has a typo, this is correct*/
		         (fabs(SphP[target].DivVel) + SphP[target].CurlVel +
	            0.0001 * soundspeed_i / SphP[target].Hsml / fac_mu); 
  }
	//For imported Particles.
  else
  {
    pos = HydroDataGet[target].Pos;
    vel = HydroDataGet[target].Vel;
    h_i = HydroDataGet[target].Hsml;
    mass = HydroDataGet[target].Mass;
    dhsmlDensityFactor = HydroDataGet[target].DhsmlDensityFactor;
    rho = HydroDataGet[target].Density;
    pressure = HydroDataGet[target].Pressure;
    timestep = HydroDataGet[target].Timestep;
    soundspeed_i = sqrt(GAMMA * pressure / rho);	//P is fnct(rho), 'a' factors 
    f1 = HydroDataGet[target].F1;
  }



  /* initialize variables before SPH loop is started */
  acc[0] = acc[1] = acc[2] = dtEntropy = 0;
  maxSignalVel = 0;

  p_over_rho2_i = pressure / (rho * rho) * dhsmlDensityFactor;
  h_i2 = h_i * h_i;

  /* Walk Tree. Start the actual SPH computation for this particle */
  startnode = All.MaxPart;
  do
  {
    numngb = ngb_treefind_pairs(&pos[0], h_i, &startnode);	//Number of 'close' sphP

		//Iterate through the list of 'close' SphP, calculate hydro forces
    for(n = 0; n < numngb; n++)
	  {
	    j = Ngblist[n];					//Get SphP index of nearby particle

			//Vector joining target particle and 'nearby' particle
	    dx = pos[0] - P[j].Pos[0];
	    dy = pos[1] - P[j].Pos[1];
	    dz = pos[2] - P[j].Pos[2];

			#ifdef PERIODIC			/*  find the closest image in the given box size  */
	    	if(dx > boxHalf_X)
	    	  dx -= boxSize_X;
	    	if(dx < -boxHalf_X)
	    	  dx += boxSize_X;
	    	if(dy > boxHalf_Y)
	    	  dy -= boxSize_Y;
	    	if(dy < -boxHalf_Y)
	   	  	dy += boxSize_Y;
	    	if(dz > boxHalf_Z)
	    	  dz -= boxSize_Z;
	    	if(dz < -boxHalf_Z)
	    	  dz += boxSize_Z;
			#endif
	    r2 = dx * dx + dy * dy + dz * dz;
	    h_j = SphP[j].Hsml;

			//Ensure that particle is w/in appropriate smoothing radius
	    if(r2 < h_i2 || r2 < h_j * h_j)
	    {
	      r = sqrt(r2);
	      if(r > 0)
	    	{
	    	  p_over_rho2_j = SphP[j].Pressure / (SphP[j].Density * SphP[j].Density);
	    	  soundspeed_j = sqrt(GAMMA * p_over_rho2_j * SphP[j].Density);
	    	  dvx = vel[0] - SphP[j].VelPred[0];
	    	  dvy = vel[1] - SphP[j].VelPred[1];
	    	  dvz = vel[2] - SphP[j].VelPred[2];
	    	  vdotr = dx * dvx + dy * dvy + dz * dvz;

					/*
						We want the PHYSICAL vdotr. Let v_phys = adot*x + a*xdot
						and x_phys = a * x, where x is comoving coordinate.

						(v_ij)_phys * (r_ij)_phys = a^2 * (xdot_i - xdot_j) * (x_i - x_j) + 
																				adot*a*(x_i - x_j)^2

																			= vdotr + hubble_a2 * r2
					*/
	    	  if(All.ComovingIntegrationOn){
	    	    vdotr2 = vdotr + hubble_a2 * r2;	
	    	  }else{
	    	    vdotr2 = vdotr;
					}

					//Calculate smoothing kernel derivative from target particle
	    	  if(r2 < h_i2)
	  	    {
	  	      hinv = 1.0 / h_i;
						#ifndef  TWODIMS
	  	      	hinv4 = hinv * hinv * hinv * hinv;
						#else
	  	      	hinv4 = hinv * hinv * hinv / boxSize_Z;
						#endif
	  	      u = r * hinv;
	  	      if(u < 0.5)
	        		dwk_i = hinv4 * u * (KERNEL_COEFF_3 * u - KERNEL_COEFF_4);
	  	      else
	        		dwk_i = hinv4 * KERNEL_COEFF_6 * (1.0 - u) * (1.0 - u);
	  	    }
		      else
		      {
		        dwk_i = 0;
		      }

					//Calculate smoothing kernel derivative from j'th particle
		      if(r2 < h_j * h_j)
		      {
		        hinv = 1.0 / h_j;
						#ifndef  TWODIMS
		        	hinv4 = hinv * hinv * hinv * hinv;
						#else
		       		hinv4 = hinv * hinv * hinv / boxSize_Z;
						#endif
		        u = r * hinv;
		        if(u < 0.5)
			        dwk_j = hinv4 * u * (KERNEL_COEFF_3 * u - KERNEL_COEFF_4);
		        else
			        dwk_j = hinv4 * KERNEL_COEFF_6 * (1.0 - u) * (1.0 - u);
		      }
		      else
		      {
		        dwk_j = 0;
		      }

		      if(soundspeed_i + soundspeed_j > maxSignalVel)
		        maxSignalVel = soundspeed_i + soundspeed_j;

          /* artificial viscosity */
		      if(vdotr2 < 0)	
		      {
		        mu_ij = fac_mu * vdotr2 / r;											//note: this is negative!

            vsig = soundspeed_i + soundspeed_j - 3 * mu_ij;		//eqn. 13 in GADGET2

		        if(vsig > maxSignalVel)
			        maxSignalVel = vsig;

		        rho_ij = 0.5 * (rho + SphP[j].Density);						//

						//Eqn 17, Paper has a typo, below eqn is correct.
		        f2 =  fabs(SphP[j].DivVel) / (fabs(SphP[j].DivVel) + SphP[j].CurlVel +
			     			  0.0001 * soundspeed_j / fac_mu / SphP[j].Hsml);

						//Eqn 14 multiplied by Eqn 17
		        visc = 0.25 * All.ArtBulkViscConst * vsig * (-mu_ij) / rho_ij * (f1 + f2);

		        /* .... end artificial viscosity evaluation */
						#ifndef NOVISCOSITYLIMITER
		        	/* make sure that viscous acceleration is not too large */
		        	dt = imax(timestep, (P[j].Ti_endstep - P[j].Ti_begstep)) * All.Timebase_interval;
		        	if(dt > 0 && (dwk_i + dwk_j) < 0)
			      	{
			        	visc = dmin(visc, 0.5 * fac_vsic_fix * vdotr2 /
				      	(0.5 * (mass + P[j].Mass) * (dwk_i + dwk_j) * r * dt));
			      	}
						#endif
		      }
		      else
		        visc = 0;

		      p_over_rho2_j *= SphP[j].DhsmlDensityFactor;

					//Eqn 14. The Viscous Force
		      hfc_visc = 0.5 * P[j].Mass * visc * (dwk_i + dwk_j) / r;

					//Eqn 7 + Eqn 14, divided by r. Accounts for viscous forces and P gradients
		      hfc = hfc_visc + P[j].Mass * (p_over_rho2_i * dwk_i + p_over_rho2_j * dwk_j) / r;

					//Account vector components, (xhat,yhat,zhat) = (dx/r, dy/r, dz/r)
		      acc[0] -= hfc * dx;
		      acc[1] -= hfc * dy;
		      acc[2] -= hfc * dz;
		      dtEntropy += 0.5 * hfc_visc * vdotr2;
		    }
	    }
	  }
  }
  while(startnode >= 0);

	/*
	#ifdef COOLING
		if(mode == 0)
		{
			//NOTE: pressure = (  ) * rho^GAMMA, so rho's cancel
			gas_entropy = pressure/pow(rho, GAMMA);

			dtEntropy_cool = cooling(rho, gas_entropy, target);
			dtEntropy -= dtEntropy_cool;
		}
	#endif
	*/

  /* Now collect the result at the right place */
  if(mode == 0)
  {
    for(k = 0; k < 3; k++)
	    SphP[target].HydroAccel[k] = acc[k];
    SphP[target].DtEntropy = dtEntropy;
    SphP[target].MaxSignalVel = maxSignalVel;
  }
  else
  {
    for(k = 0; k < 3; k++)
	    HydroDataResult[target].Acc[k] = acc[k];
    HydroDataResult[target].DtEntropy = dtEntropy;
    HydroDataResult[target].MaxSignalVel = maxSignalVel;
  }
}




/*! This is a comparison kernel for a sort routine, which is used to group
 *  particles that are going to be exported to the same CPU.
 */
int hydro_compare_key(const void *a, const void *b)
{
  if(((struct hydrodata_in *) a)->Task < (((struct hydrodata_in *) b)->Task))
    return -1;
  if(((struct hydrodata_in *) a)->Task > (((struct hydrodata_in *) b)->Task))
    return +1;
  return 0;
}
