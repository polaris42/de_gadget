#ifdef COOLING
#ifdef UVBACKGROUND

#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <mpi.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_integration.h>

#include "allvars.h"
#include "proto.h"


/* 
	If you want to use the UVBACKGROUND you MUST know a redshift!!! Therefor this
	will create a table that can be interpolated to convert the physical time to
	a redshift. This table will ONLY be USED when All.ComovingIntegrationOn = 0.

	Since this run uses linear time (i.e. total phys time = (All.TimeMax - All.TimeBeg) *
  All.UnitTime_in_s) we will need to use All.ScaleFactorEnd to match the end of the 
  simulation to a real redshift that can be found in the cooling table.

	AGAIN REITERATING: 
		Without using ComovingIntegration, All.Time will NOT correspond to a redshift.
		Instead it will ONLY correspond to a physical linear time 
		(i.e. dt = (All.TimeMax - All.TimeBeg) in internal time units).
		So we use this table to convert it to a redshift. 

		Might as well make it the same length as the drift tables.
*/
void init_phystime2redshift_table(void)
{
	if(All.ComovingIntegrationOn == 0)
	{
		int i;
		double cosmTimeEnd = 0;								//Cosmological time matchin End of run, in Myr
		double cosmTimeBeg = 0;								//Cosmological time matchin Beg of run, in Myr
		double Delta_t =(All.TimeMax -All.TimeBegin) * All.UnitTime_in_s / SEC_PER_MEGAYEAR;
		double ptime;																			//Physical Time in Myr

		//Get physical time bounds in Myr
		cosmTimeEnd = get_cosmological_time(All.ScaleFactorEnd);
		cosmTimeBeg = cosmTimeEnd - Delta_t;
		All.CosmTimeEnd = cosmTimeEnd;	
		All.CosmTimeBeg = cosmTimeBeg;

		//Can't find cooling or map All.ScaleFactorEnd into future.
		if(All.ScaleFactorEnd > 1.0){
			if(ThisTask == 0){
				fprintf(stderr, "ERROR!!!! All.ScaleFactorEnd = %f\nAll.ScaleFactor cannot be"
								" greater that 1.0", All.ScaleFactorEnd);
			}
			endrun(5000);
		}
		if(Delta_t > get_cosmological_time(1.0)){
			if(ThisTask == 0){
				fprintf(stderr, "ERROR!!!! DeltaTime = %f while maxPhysTimePossible = %f\n",
								Delta_t, get_cosmological_time(1.0));
			}
			endrun(5001);
		}
		if(cosmTimeBeg < 0){
			if(ThisTask == 0){
				fprintf(stderr, "ERROR!!!! cosmTimeBeg  = %f. Can't be less than 0!",
								cosmTimeBeg);
			}
			endrun(5004);
		}

		//Warn User of what he/she is doing.
		if(ThisTask == 0){
			printf("*********************************************************************\n");
			printf("WARNING!!! WARNING!!!! WARNING!!!! WARNING!!!! WARNING!!!! WARNING!!!! \n"
					"UVBACKGROUND and COOLING are both enabled in a simulation \n"
					"where the ComovingIntegrationOn = 0. Recall that the UVBACKGROUND is \n"
					"dependent on redshift. Since this run uses linear time \n"
					"(i.e. total phys time = (All.TimeMax - All.TimeBeg) * All.UnitTime_in_s)\n"
					"we will need to use All.ScaleFactorEnd to match the end of the simulation \n"
					"to a real redshift that can be found in the cooling table. You better know\n"
					"what the fuck you are doing.\n");
			printf("*********************************************************************\n");
		}


		/* Initialize Table. Match current linear time (All.TimeBegin, All.TimeMax) with
			 the corresponding cosmological time (cosmTimeBeg, cosmTimeEnd). */
		for(i=0; i<DRIFT_TABLE_LENGTH; i++)
		{
			ptime = cosmTimeBeg + i*Delta_t/(DRIFT_TABLE_LENGTH - 1);
			PhysTime2Redshift[i].redshift = get_redshift_from_phystime(ptime);
			PhysTime2Redshift[i].phystime = ptime;
		}	
	}
	else
	{
		if(ThisTask == 0){
			printf("PhysTime2Redshift table will not be used\n");
		}
	}
}



/*
	Iteratively solve for the redshift after being given a physical time in Myr.
	This must be done b/c it is a bitch to try and invert either C&O eqn 29.128 and/or 
	eqn. 29.122.

	physTime must be in My.
*/
double get_redshift_from_phystime(double physTime)
{
	int i = 0;											//Use to check for convergence
	double tol = 0.000001;					//Fractional tolerance
	double fracDiff = 10;						//Fractional difference
	double aGuess = 0.5;            //Scale Factor Guess
	double tGuess = 0;							//Resulting time from Redshift Guess
	double amin = 0;								//Search Bounds
	double amax = 1.0;							//Search Bounds
	double redshift = -1.0;

	//Iteratively solve for redshift 
	while(fracDiff > tol)
	{
		//Check for convergence
		i++;
		if(i>=100){
			if(ThisTask == 0){
				fprintf(stderr, "ERROR!!!! Finding redshift from physical time did NOT "
								"converge\n");
			}
			endrun(5002);
		}

		tGuess = get_cosmological_time(aGuess);

		//Adjust guess
		if(tGuess > physTime)
		{
			amax = aGuess;
			aGuess = (amax + amin)/2.0;
		}
		else if(tGuess < physTime)
		{
			amin = aGuess;
			aGuess = (amax + amin)/2.0;
		}

		fracDiff = fabs((tGuess - physTime)/physTime);
	}

	redshift = 1.0/aGuess - 1.0;

	return redshift;
}


/*
This function is ALMOST a redundant copy of get_phys_time().  However instead it
is used as means for getting the 'cosmological time' during a run without 
All.ComovingIntegrationOn. I can't think of a better way to do this.

Returns time in MegaYears.
*/
double get_cosmological_time(double atime)
{
	double time;

  /************************ For cosmological simulations **********************/
	#define WORKSIZE 100000
  double result, abserr;
  gsl_function F;
  gsl_integration_workspace *workspace;

  F.function = &phys_time_integrand;           //Pointing to a FUNCTION
  workspace = gsl_integration_workspace_alloc(WORKSIZE);

  //Integrate from beginning to current Time / Scale factor.
  gsl_integration_qag(&F, 0.0, atime, 1.0e-16, 1.0e-8, WORKSIZE, GSL_INTEG_GAUSS41, workspace, &result, &abserr);
   
  time = result;      

  gsl_integration_workspace_free(workspace);

	time /= SEC_PER_MEGAYEAR;
	return time;
}




/*
	Give a physical time that maps to a redshift (i.e. as is done in
	init_PhysTime2Redshift_table()), this interpolates the actual redshift value. 
	Once again this is ONLY useful when CosmovingIntegrationOn = 0 and COOLING and
	UVBACKGROUND are enabled.

	This actually does a 1D interpolation.
*/
double mapTime2Redshift(double atime)
{
	int i = 0;
	double cosmTimeEnd = 0;								//Cosmological time matchin End of run, in Myr
	double cosmTimeBeg = 0;								//Cosmological time matchin Beg of run, in Myr
	double Delta_t = (All.TimeMax - All.TimeBegin) * All.UnitTime_in_s / SEC_PER_MEGAYEAR;
	double phystime;											//Physical Time in Myr
	double redshift;
	double m,dx;

	//Error Check
	if(All.ComovingIntegrationOn != 0){
		if(ThisTask == 0){
			fprintf(stderr, "What the hell do you think you are doing!?!?!?!\n"
				"This function should only be called when ComovingIntegratoinOn != 1. "
				"mapTime2Redshift() is intended to only be used when All.Time does not easily\n"
				"map to a redshift b/c All.Time is on a LINEAR timeline\n");
		}
		endrun(5005);
	}

	cosmTimeEnd = All.CosmTimeEnd; //get_cosmological_time(All.ScaleFactorEnd);
	cosmTimeBeg = All.CosmTimeBeg; //cosmTimeEnd - Delta_t;

	phystime = cosmTimeEnd - (All.TimeMax - atime) * All.UnitTime_in_s 
														/ SEC_PER_MEGAYEAR;
	
	i = (int)floor((phystime - cosmTimeBeg)*(DRIFT_TABLE_LENGTH - 1)/ Delta_t);
	
	if(i >= DRIFT_TABLE_LENGTH){
		fprintf(stderr, "ERROR!!!! i >= DRIFT_TABLE_LENGTH!!!\ni= %i, DRIFT_TABLE_LENGTH= "
						"%i\n", i, DRIFT_TABLE_LENGTH);
		endrun(5006);
	}
	if(i+1 >= DRIFT_TABLE_LENGTH){
		fprintf(stderr, "ERROR!!! i+1 >= DRIFT_TABLE_LENGTH!!!\ni= %i, DRIFT_TABLE_LENGTH= "
						"%i\n", i+1, DRIFT_TABLE_LENGTH);
		endrun(5007);
	}

	//Interpolate
	m = (PhysTime2Redshift[i+1].redshift - PhysTime2Redshift[i].redshift) / 
			(PhysTime2Redshift[i+1].phystime - PhysTime2Redshift[i].phystime); 
	dx = phystime - PhysTime2Redshift[i].phystime;
	redshift = PhysTime2Redshift[i].redshift + m*dx; 

	return redshift;
}





#endif
#endif
