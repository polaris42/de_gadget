#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <mpi.h>

#include "allvars.h"
#include "proto.h"


/*! \file init.c
 *  \brief Code for initialisation of a simulation from initial conditions
 */


/*! This function reads the initial conditions, and allocates storage for the
 *  tree. Various variables of the particle data are initialised and An intial
 *  domain decomposition is performed. If SPH particles are present, the inial
 *  SPH smoothing lengths are determined.
 */
void init(void)
{
  int i, j;
  double a3;
	#ifdef COOLING
	#ifdef SFR
	#ifdef METALS
		int index = 0.0;
	#endif
	#endif
	#endif


  All.Time = All.TimeBegin;

  switch (All.ICFormat)
  {
    case 1:
			#if (MAKEGLASS > 1)
      	seed_glass();
			#else
      	read_ic(All.InitCondFile);
			#endif
      break;
    case 2:
    case 3:
      read_ic(All.InitCondFile);
      break;
    default:
      if(ThisTask == 0)
      	printf("ICFormat=%d not supported.\n", All.ICFormat);
      endrun(0);
  }

  All.Time = All.TimeBegin;
  All.Ti_Current = 0;

  if(All.ComovingIntegrationOn)
  {
    All.Timebase_interval = (log(All.TimeMax) - log(All.TimeBegin)) / TIMEBASE;
    a3 = All.Time * All.Time * All.Time;
  }
  else
  {
    All.Timebase_interval = (All.TimeMax - All.TimeBegin) / TIMEBASE;
    a3 = 1;
  }

  set_softenings();

  All.NumCurrentTiStep = 0;	/* setup some counters */
  All.SnapshotFileCount = 0;
  if(RestartFlag == 2)
    All.SnapshotFileCount = atoi(All.InitCondFile + strlen(All.InitCondFile) - 3) + 1;

  All.TotNumOfForces = 0;
  All.NumForcesSinceLastDomainDecomp = 0;

  if(All.ComovingIntegrationOn)
    if(All.PeriodicBoundariesOn == 1)
      check_omega();

  All.TimeLastStatistics = All.TimeBegin - All.TimeBetStatistics;

  if(All.ComovingIntegrationOn)	/*  change to new velocity variable */
  {
    for(i = 0; i < NumPart; i++)
	    for(j = 0; j < 3; j++)
	      P[i].Vel[j] *= sqrt(All.Time) * All.Time;
  }


  //Error Check
  #ifdef METALS
    #if defined(CARBON)     || defined(NITROGEN)  || defined(OXYGEN)   || \
        defined(FLORINE)    || defined(NEON)      || defined(SODIUM)   || \
        defined(MAGNESIUM)  || defined(ALUMINUM)  || defined(SILICON)  || \
        defined(PHOSPHORUS) || defined(SULFUR)    || defined(CHLORINE) || \
        defined(ARGON)      || defined(POTASSIUM) || defined(CALCIUM)  || \
        defined(SCANDIUM)   || defined(TITANIUM)  || defined(VANADIUM) || \
        defined(CHROMIUM)   || defined(MANGANESE) || defined(IRON)     || \
        defined(COBALT)     || defined(NICKEL)    || defined(COPPER)   || \
        defined(ZINC)
    #else
		  fprintf(stderr, "ERROR!!! You must specify a specific element when you specify"
											" METALS in the Makefile\n");
			endrun(678);
    #endif
  #endif

  for(i = 0; i < NumPart; i++)	/*  start-up initialization */
  {
    for(j = 0; j < 3; j++)
	    P[i].GravAccel[j] = 0;
		#ifdef PMGRID
    	for(j = 0; j < 3; j++)
	    	P[i].GravPM[j] = 0;
		#endif
    P[i].Ti_endstep = 0;
    P[i].Ti_begstep = 0;

    P[i].OldAcc = 0;
    P[i].GravCost = 1;
    P[i].Potential = 0;

		/* ALI : Will need to change when writing/reading Metals to snapshot */
		#ifdef COOLING
		#ifdef SFR
		P[i].StarAge = 0.0;
		#ifdef METALS
		P[i].NumGasNearby = 0;															// Only relevant for stars

		#ifdef FEEDBACK_W_SPH_KERNAL
		P[i].DtMass = 0.0;
		P[i].StarHsml = -1.0;
		P[i].StarHsmlLeft = -1.0;
		P[i].StarHsmlRight = -1.0;
		P[i].FeedbackNorm = -1.0;
		P[i].GasDensity		= -1.0;
		P[i].GasPressure	= -1.0;
		P[i].BlastRadius	= -1.0;
    P[i].origStarMass = -1.0;
		P[i].R_II					= -1.0;
		P[i].R_SW					= -1.0;
		P[i].R_Ia					= -1.0;
    P[i].R_AGB        = -1.0;
		#endif

		/* For each element present below, we are computing the fractional mass dependence
			 for the element and then multipling by the particle mass. This should give the
			 element mass.
		*/
		if(P[i].Type == 0)
		{
			#ifdef FEEDBACK_W_SPH_KERNAL
				SphP[i].cooling_on = 1;
				SphP[i].time_turned_off = 0.0;
			#endif

			P[i].MetalMass = 0;											// = 10^-3 * (solar metallicity)

  		#ifdef CARBON
			index = find_solar_abundances_index("C");
			P[i].CarbonMass = INIT_METAL_TO_H_RATIO * SolarAbundances[index].AtomicMass * 
											SolarAbundances[index].NumRatioToH * P[i].Mass / MetalNormConst;
			P[i].MetalMass += INIT_METAL_TO_H_RATIO * SolarAbundances[index].AtomicMass * 
											SolarAbundances[index].NumRatioToH;											
			#endif

			#ifdef NITROGEN
			index = find_solar_abundances_index("N");
			P[i].NitrogenMass = INIT_METAL_TO_H_RATIO*SolarAbundances[index].AtomicMass * 
											SolarAbundances[index].NumRatioToH * P[i].Mass / MetalNormConst;
			P[i].MetalMass += INIT_METAL_TO_H_RATIO * SolarAbundances[index].AtomicMass * 
											SolarAbundances[index].NumRatioToH;											
			#endif

			#ifdef OXYGEN
			index = find_solar_abundances_index("O");
			P[i].OxygenMass = INIT_METAL_TO_H_RATIO * SolarAbundances[index].AtomicMass * 
											SolarAbundances[index].NumRatioToH * P[i].Mass / MetalNormConst;
			P[i].MetalMass += INIT_METAL_TO_H_RATIO * SolarAbundances[index].AtomicMass * 
											SolarAbundances[index].NumRatioToH;											
			#endif

			#ifdef FLORINE
			index = find_solar_abundances_index("F");
			P[i].FlorineMass = INIT_METAL_TO_H_RATIO* SolarAbundances[index].AtomicMass * 
											SolarAbundances[index].NumRatioToH * P[i].Mass / MetalNormConst;
			P[i].MetalMass += INIT_METAL_TO_H_RATIO * SolarAbundances[index].AtomicMass * 
											SolarAbundances[index].NumRatioToH;											
			#endif

			#ifdef NEON
			index = find_solar_abundances_index("Ne");
			P[i].NeonMass = INIT_METAL_TO_H_RATIO * SolarAbundances[index].AtomicMass * 
											SolarAbundances[index].NumRatioToH * P[i].Mass / MetalNormConst;
			P[i].MetalMass += INIT_METAL_TO_H_RATIO * SolarAbundances[index].AtomicMass * 
											SolarAbundances[index].NumRatioToH;											
			#endif

			#ifdef SODIUM
			index = find_solar_abundances_index("Na");
			P[i].SodiumMass = INIT_METAL_TO_H_RATIO * SolarAbundances[index].AtomicMass * 
											SolarAbundances[index].NumRatioToH * P[i].Mass / MetalNormConst;
			P[i].MetalMass += INIT_METAL_TO_H_RATIO * SolarAbundances[index].AtomicMass * 
											SolarAbundances[index].NumRatioToH;											
			#endif

			#ifdef MAGNESIUM
			index = find_solar_abundances_index("Mg");
			P[i].MagnesiumMass =INIT_METAL_TO_H_RATIO* SolarAbundances[index].AtomicMass *
											SolarAbundances[index].NumRatioToH * P[i].Mass / MetalNormConst;
			P[i].MetalMass += INIT_METAL_TO_H_RATIO * SolarAbundances[index].AtomicMass * 
											SolarAbundances[index].NumRatioToH;											
			#endif

			#ifdef ALUMINUM
			index = find_solar_abundances_index("Al");
			P[i].AluminumMass = INIT_METAL_TO_H_RATIO* SolarAbundances[index].AtomicMass *
											SolarAbundances[index].NumRatioToH * P[i].Mass / MetalNormConst;
			P[i].MetalMass += INIT_METAL_TO_H_RATIO * SolarAbundances[index].AtomicMass * 
											SolarAbundances[index].NumRatioToH;											
			#endif

			#ifdef SILICON
			index = find_solar_abundances_index("Si");
			P[i].SiliconMass = INIT_METAL_TO_H_RATIO* SolarAbundances[index].AtomicMass * 
											SolarAbundances[index].NumRatioToH * P[i].Mass / MetalNormConst;
			P[i].MetalMass += INIT_METAL_TO_H_RATIO * SolarAbundances[index].AtomicMass * 
											SolarAbundances[index].NumRatioToH;											
			#endif

			#ifdef PHOSPHORUS
			index = find_solar_abundances_index("P");
			P[i].PhosphorusMass =INIT_METAL_TO_H_RATIO *SolarAbundances[index].AtomicMass*
											SolarAbundances[index].NumRatioToH * P[i].Mass / MetalNormConst;
			P[i].MetalMass += INIT_METAL_TO_H_RATIO * SolarAbundances[index].AtomicMass * 
											SolarAbundances[index].NumRatioToH;											
			#endif

			#ifdef SULFUR
			index = find_solar_abundances_index("S");
			P[i].SulfurMass = INIT_METAL_TO_H_RATIO * SolarAbundances[index].AtomicMass * 
											SolarAbundances[index].NumRatioToH * P[i].Mass / MetalNormConst;
			P[i].MetalMass += INIT_METAL_TO_H_RATIO * SolarAbundances[index].AtomicMass * 
											SolarAbundances[index].NumRatioToH;											
			#endif

			#ifdef CHLORINE
			index = find_solar_abundances_index("Cl");
			P[i].ChlorineMass = INIT_METAL_TO_H_RATIO*SolarAbundances[index].AtomicMass * 
											SolarAbundances[index].NumRatioToH * P[i].Mass / MetalNormConst;
			P[i].MetalMass += INIT_METAL_TO_H_RATIO * SolarAbundances[index].AtomicMass * 
											SolarAbundances[index].NumRatioToH;											
			#endif

			#ifdef ARGON
			index = find_solar_abundances_index("Ar");
			P[i].ArgonMass = INIT_METAL_TO_H_RATIO * SolarAbundances[index].AtomicMass * 
											SolarAbundances[index].NumRatioToH * P[i].Mass / MetalNormConst;
			P[i].MetalMass += INIT_METAL_TO_H_RATIO * SolarAbundances[index].AtomicMass * 
											SolarAbundances[index].NumRatioToH;											
			#endif

			#ifdef POTASSIUM
			index = find_solar_abundances_index("K");
			P[i].PotassiumMass = INIT_METAL_TO_H_RATIO*SolarAbundances[index].AtomicMass *
											SolarAbundances[index].NumRatioToH * P[i].Mass / MetalNormConst;
			P[i].MetalMass += INIT_METAL_TO_H_RATIO * SolarAbundances[index].AtomicMass * 
											SolarAbundances[index].NumRatioToH;											
			#endif

			#ifdef CALCIUM
			index = find_solar_abundances_index("Ca");
			P[i].CalciumMass = INIT_METAL_TO_H_RATIO* SolarAbundances[index].AtomicMass * 
											SolarAbundances[index].NumRatioToH * P[i].Mass / MetalNormConst;
			P[i].MetalMass += INIT_METAL_TO_H_RATIO * SolarAbundances[index].AtomicMass * 
											SolarAbundances[index].NumRatioToH;											
			#endif

			#ifdef SCANDIUM
			index = find_solar_abundances_index("Sc");
			P[i].ScandiumMass = INIT_METAL_TO_H_RATIO* SolarAbundances[index].AtomicMass *
											SolarAbundances[index].NumRatioToH * P[i].Mass / MetalNormConst;
			P[i].MetalMass += INIT_METAL_TO_H_RATIO * SolarAbundances[index].AtomicMass * 
											SolarAbundances[index].NumRatioToH;											
			#endif

			#ifdef TITANIUM
			index = find_solar_abundances_index("Ti");
			P[i].TitaniumMass = INIT_METAL_TO_H_RATIO* SolarAbundances[index].AtomicMass *
											SolarAbundances[index].NumRatioToH * P[i].Mass / MetalNormConst;
			P[i].MetalMass += INIT_METAL_TO_H_RATIO * SolarAbundances[index].AtomicMass * 
											SolarAbundances[index].NumRatioToH;											
			#endif

			#ifdef VANADIUM
			index = find_solar_abundances_index("V");
			P[i].VanadiumMass = INIT_METAL_TO_H_RATIO* SolarAbundances[index].AtomicMass *
											SolarAbundances[index].NumRatioToH * P[i].Mass / MetalNormConst;
			P[i].MetalMass += INIT_METAL_TO_H_RATIO * SolarAbundances[index].AtomicMass * 
											SolarAbundances[index].NumRatioToH;											
			#endif

			#ifdef CHROMIUM
			index = find_solar_abundances_index("Cr");
			P[i].ChromiumMass = INIT_METAL_TO_H_RATIO* SolarAbundances[index].AtomicMass *
											SolarAbundances[index].NumRatioToH * P[i].Mass / MetalNormConst;
			P[i].MetalMass += INIT_METAL_TO_H_RATIO * SolarAbundances[index].AtomicMass * 
											SolarAbundances[index].NumRatioToH;											
			#endif

			#ifdef MANGANESE
			index = find_solar_abundances_index("Mn");
			P[i].ManganeseMass= INIT_METAL_TO_H_RATIO* SolarAbundances[index].AtomicMass *
											SolarAbundances[index].NumRatioToH * P[i].Mass / MetalNormConst;
			P[i].MetalMass += INIT_METAL_TO_H_RATIO * SolarAbundances[index].AtomicMass * 
											SolarAbundances[index].NumRatioToH;											
			#endif

			#ifdef IRON
			index = find_solar_abundances_index("Fe");
			P[i].IronMass = INIT_METAL_TO_H_RATIO * SolarAbundances[index].AtomicMass * 
											SolarAbundances[index].NumRatioToH * P[i].Mass / MetalNormConst;
			P[i].MetalMass += INIT_METAL_TO_H_RATIO * SolarAbundances[index].AtomicMass * 
											SolarAbundances[index].NumRatioToH;											
			#endif
	
			#ifdef COBALT
			index = find_solar_abundances_index("Co");
			P[i].CobaltMass = INIT_METAL_TO_H_RATIO * SolarAbundances[index].AtomicMass * 
											SolarAbundances[index].NumRatioToH * P[i].Mass / MetalNormConst;
			P[i].MetalMass += INIT_METAL_TO_H_RATIO * SolarAbundances[index].AtomicMass * 
											SolarAbundances[index].NumRatioToH;											
			#endif

			#ifdef NICKEL
			index = find_solar_abundances_index("Ni");
			P[i].NickelMass = INIT_METAL_TO_H_RATIO * SolarAbundances[index].AtomicMass * 
											SolarAbundances[index].NumRatioToH * P[i].Mass / MetalNormConst;
			P[i].MetalMass += INIT_METAL_TO_H_RATIO * SolarAbundances[index].AtomicMass * 
											SolarAbundances[index].NumRatioToH;											
			#endif

			#ifdef COPPER
			index = find_solar_abundances_index("Cu");
			P[i].CopperMass = INIT_METAL_TO_H_RATIO * SolarAbundances[index].AtomicMass * 
											SolarAbundances[index].NumRatioToH * P[i].Mass / MetalNormConst;
			P[i].MetalMass += INIT_METAL_TO_H_RATIO * SolarAbundances[index].AtomicMass * 
											SolarAbundances[index].NumRatioToH;											
			#endif

			#ifdef ZINC
			index = find_solar_abundances_index("Zn");
			P[i].ZincMass = INIT_METAL_TO_H_RATIO * SolarAbundances[index].AtomicMass * 
											SolarAbundances[index].NumRatioToH * P[i].Mass / MetalNormConst;
			P[i].MetalMass += INIT_METAL_TO_H_RATIO * SolarAbundances[index].AtomicMass * 
											SolarAbundances[index].NumRatioToH;											
			#endif


			/*/Error Check
			if(P[i].MetalMass < 1e-20)
			{
				fprintf(stderr, "ERROR!!! You must specify a specific element when you specify"
												" METALS in the Makefile\n");
				endrun(678);
			}*/


			P[i].MetalMass = P[i].MetalMass * P[i].Mass /MetalNormConst;
		}
		else
		{
			P[i].MetalMass = -1e10;		//Unphysical, b/c DM particles shouldn't have metals

  		#ifdef CARBON
			P[i].CarbonMass 		= -1e10;
			#endif

			#ifdef NITROGEN
			P[i].NitrogenMass 	= -1e10;
			#endif

			#ifdef OXYGEN
			P[i].OxygenMass 		= -1e10;
			#endif

			#ifdef FLORINE
			P[i].FlorineMass 		= -1e10;
			#endif

			#ifdef NEON
			P[i].NeonMass 			= -1e10;
			#endif

			#ifdef SODIUM
			P[i].SodiumMass 		= -1e10;
			#endif

			#ifdef MAGNESIUM
			P[i].MagnesiumMass 	= -1e10;
			#endif

			#ifdef ALUMINUM
			P[i].AluminumMass 	= -1e10;
			#endif

			#ifdef SILICON
			P[i].SiliconMass 		= -1e10;
			#endif

			#ifdef PHOSPHORUS
			P[i].PhosphorusMass = -1e10;
			#endif

			#ifdef SULFUR
			P[i].SulfurMass 		= -1e10;
			#endif

			#ifdef CHLORINE
			P[i].ChlorineMass 	= -1e10; 
			#endif

			#ifdef ARGON
			P[i].ArgonMass 			= -1e10;
			#endif

			#ifdef POTASSIUM
			P[i].PotassiumMass 	= -1e10;
			#endif

			#ifdef CALCIUM
			P[i].CalciumMass 		= -1e10;
			#endif

			#ifdef SCANDIUM
			P[i].ScandiumMass 	= -1e10;
			#endif

			#ifdef TITANIUM
			P[i].TitaniumMass 	= -1e10;
			#endif

			#ifdef VANADIUM
			P[i].VanadiumMass 	= -1e10;
			#endif

			#ifdef CHROMIUM
			P[i].ChromiumMass 	= -1e10;
			#endif

			#ifdef MANGANESE
			P[i].ManganeseMass 	= -1e10;
			#endif

			#ifdef IRON
			P[i].IronMass 			= -1e10;
			#endif
	
			#ifdef COBALT
			P[i].CobaltMass 		= -1e10;
			#endif

			#ifdef NICKEL
			P[i].NickelMass 		= -1e10;
			#endif

			#ifdef COPPER
			P[i].CopperMass 		= -1e10;
			#endif

			#ifdef ZINC
			P[i].ZincMass 			= -1e10;
			#endif
		}

		P[i].TurnOffMass = IMF_MAX;					// = 120 M_sun

		#endif
		#endif
		#endif

  }

	#ifdef PMGRID
  	All.PM_Ti_endstep = All.PM_Ti_begstep = 0;
	#endif

	#ifdef FLEXSTEPS
  	All.PresentMinStep = TIMEBASE;
  	for(i = 0; i < NumPart; i++)	/*  start-up initialization */
  	{
  	  P[i].FlexStepGrp = (int) (TIMEBASE * get_random_number(P[i].ID));
  	}
	#endif


  for(i = 0; i < N_gas; i++)	/* initialize sph_properties */
  {
    for(j = 0; j < 3; j++)
	  {
	    SphP[i].VelPred[j] = P[i].Vel[j];
	    SphP[i].HydroAccel[j] = 0;
	  }

    SphP[i].DtEntropy = 0;

		#ifdef COOLING
      SphP[i].cooling_rate = 0.0;
      #ifdef SFR
       //SphP[i].Sfr = 0.0;
			#endif
		#endif

    if(RestartFlag == 0)
	  {
	    SphP[i].Hsml = 0;
	    SphP[i].Density = -1;
	  }
  }

  ngb_treeallocate(MAX_NGB);

  force_treeallocate(All.TreeAllocFactor * All.MaxPart, All.MaxPart);

  All.NumForcesSinceLastDomainDecomp = 1 + All.TotNumPart * All.TreeDomainUpdateFrequency;

  Flag_FullStep = 1;		/* to ensure that Peano-Hilber order is done */

  domain_Decomposition();	/* do initial domain decomposition (gives equal numbers of particles) */

  ngb_treebuild();		/* will build tree */

  setup_smoothinglengths();

  TreeReconstructFlag = 1;

  /* at this point, the entropy variable normally contains the 
   * internal energy, read in from the initial conditions file, unless the file
   * explicitly signals that the initial conditions contain the entropy directly. 
   * Once the density has been computed, we can convert thermal energy to entropy.
   */
	#ifndef ISOTHERM_EQS
	  if(header.flag_entropy_instead_u == 0)
	    for(i = 0; i < N_gas; i++)
	      SphP[i].Entropy = GAMMA_MINUS1 * SphP[i].Entropy / pow(SphP[i].Density / a3, GAMMA_MINUS1);
	#endif
}


/*! This routine computes the mass content of the box and compares it to the
 *  specified value of Omega-matter.  If discrepant, the run is terminated.
 */
void check_omega(void)
{
  double mass = 0, masstot, omega;
  int i;

  for(i = 0; i < NumPart; i++)
    mass += P[i].Mass;

  MPI_Allreduce(&mass, &masstot, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);

  omega =
    masstot / (All.BoxSize * All.BoxSize * All.BoxSize) / (3 * All.Hubble * All.Hubble / (8 * M_PI * All.G));

  if(fabs(omega - All.Omega0) > 1.0e-3)
  {
    if(ThisTask == 0)
	  {
	    printf("\n\nI've found something odd!\n");
	    printf
	    ("The mass content accounts only for Omega=%g,\nbut you specified Omega=%g in the parameterfile.\n",
	     omega, All.Omega0);
	    printf("\nI better stop.\n");

	    fflush(stdout);
	  }
    endrun(1);
  }
}



/*! This function is used to find an initial smoothing length for each SPH
 *  particle. It guarantees that the number of neighbours will be between
 *  desired_ngb-MAXDEV and desired_ngb+MAXDEV. For simplicity, a first guess
 *  of the smoothing length is provided to the function density(), which will
 *  then iterate if needed to find the right smoothing length.
 */
void setup_smoothinglengths(void)
{
  int i, no, p;

  if(RestartFlag == 0)
  {

    for(i = 0; i < N_gas; i++)
  	{
  	  no = Father[i];

  	  while(10 * All.DesNumNgb * P[i].Mass > Nodes[no].u.d.mass)
	    {
	      p = Nodes[no].u.d.father;

	      if(p < 0)
      		break;

	      no = p;
	    }
			#ifndef TWODIMS
	    	SphP[i].Hsml =
	      	     pow(3.0 / (4 * M_PI) * All.DesNumNgb * P[i].Mass / Nodes[no].u.d.mass, 1.0 / 3) * Nodes[no].len;
			#else
	    	SphP[i].Hsml =
	      	     pow(1.0 / (M_PI) * All.DesNumNgb * P[i].Mass / Nodes[no].u.d.mass, 1.0 / 2) * Nodes[no].len;
			#endif
	  }
  }

  density();
}


/*! If the code is run in glass-making mode, this function populates the
 *  simulation box with a Poisson sample of particles.
 */
#if (MAKEGLASS > 1)
void seed_glass(void)
{
  int i, k, n_for_this_task;
  double Range[3], LowerBound[3];
  double drandom, partmass;
  long long IDstart;

  All.TotNumPart = MAKEGLASS;
  partmass = All.Omega0 * (3 * All.Hubble * All.Hubble / (8 * M_PI * All.G))
    * (All.BoxSize * All.BoxSize * All.BoxSize) / All.TotNumPart;

  All.MaxPart = All.PartAllocFactor * (All.TotNumPart / NTask);	/* sets the maximum number of particles that may */

  allocate_memory();

  header.npartTotal[1] = All.TotNumPart;
  header.mass[1] = partmass;

  if(ThisTask == 0)
  {
    printf("\nGlass initialising\nPartMass= %g\n", partmass);
    printf("TotNumPart= %d%09d\n\n",
				     (int) (All.TotNumPart / 1000000000), (int) (All.TotNumPart % 1000000000));
  }

  /* set the number of particles assigned locally to this task */
  n_for_this_task = All.TotNumPart / NTask;

  if(ThisTask == NTask - 1)
    n_for_this_task = All.TotNumPart - (NTask - 1) * n_for_this_task;

  NumPart = 0;
  IDstart = 1 + (All.TotNumPart / NTask) * ThisTask;

  /* split the temporal domain into Ntask slabs in z-direction */

  Range[0] = Range[1] = All.BoxSize;
  Range[2] = All.BoxSize / NTask;
  LowerBound[0] = LowerBound[1] = 0;
  LowerBound[2] = ThisTask * Range[2];

  srand48(ThisTask);

  for(i = 0; i < n_for_this_task; i++)
  {
    for(k = 0; k < 3; k++)
  	{
  	  drandom = drand48();

  	  P[i].Pos[k] = LowerBound[k] + Range[k] * drandom;
  	  P[i].Vel[k] = 0;
  	}

    P[i].Mass = partmass;
    P[i].Type = 1;
    P[i].ID = IDstart + i;

    NumPart++;
  }
}
#endif


#ifdef COOLING
#ifdef SFR
#ifdef METALS
/*
	Given an element, find the appropriate index in SolarAbundances[].
*/
int find_solar_abundances_index(char element[3])
{
	int i = 0;
	int index = -1;

	while(i < NSolarElements)
	{
		if( strcmp(element, SolarAbundances[i].Element) == 0)
		{
			//Error check, be sure element only occurs once in array
			if(index >= 0)
			{
				fprintf(stderr, "ERROR!! Element occurs multiple times in "
												"Solar_Abundances.txt\n");
				exit(672);
			}
			else
			{
				index = i;
			}
		}
		i++;
	}

	return index;
}	
#endif
#endif
#endif
