/***************************************************************************
Author: 	Ali Snedden 
Modified: 
Date: 5/13/14

NSH 341a
Dept. of Physics
University of Notre Dame

Purpose:
  To create functions that will return the Type Ia and Type II Supernova rate.
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <mpi.h>
#include <gsl/gsl_math.h>
#include "allvars.h"
#include "proto.h"

#ifdef COOLING
#ifdef SFR
#ifdef METALS
#ifdef FEEDBACK
#ifdef FEEDBACK_W_SPH_KERNAL
/****************************************************************************
  Return the Type II Supernova rate M_sun / year.
****************************************************************************/
double get_typeII_snr(int target)
{
  double dN_II        = 0;            //Number of Type II SN per year.
  double star_age     = 0;
  double t            = 0;            //Star lifetime
  double t1           = 0;            // = t + dt
  double t2           = 0;            // = t - dt
  double dt           = 0.1;          //Find dN_II on interval [t-dt, t+dt]
  double origStarMass = 0;
  double m_2u         = 40;           //Upper Type II Mass limit
  double m_2l         = 8;            //Lower Type II Mass limit
  double t_m_2l       = 0;            //Lifetime of  8 M_sun star
  double t_m_2u       = 0;            //Lifetime of 40 M_sun star
  double m1           = 0;
  double m2           = 0;
  double m            = 0;

  //Error check
  if(P[target].Type != 4){
    fprintf(stderr, "ERROR!! ThisTask: %i\n"
            "Can't return Type II SNR for a P[%i].ID = %i and *.Type = %i\n",
            ThisTask, target, P[target].ID, P[target].Type);
    endrun(2000);
  }

  star_age     = P[target].StarAge;
  origStarMass = P[target].origStarMass;  
  m            = P[target].TurnOffMass;
  t            = phys_time - star_age;



  //Convert origStarMass to solar masses
  if(All.ComovingIntegrationOn)
  {
    origStarMass = origStarMass * All.UnitMass_in_g / 
                   (SOLAR_MASS * All.HubbleParam);
  }else{
    origStarMass = origStarMass * All.UnitMass_in_g / (SOLAR_MASS);
  }


  //Stellar Lifetimes
  t_m_2u = pow(10, 10.0 + (-3.42 + 0.88 * log10(m_2u)) * log10(m_2u)) / 1e6;
  t_m_2l = pow(10, 10.0 + (-3.42 + 0.88 * log10(m_2l)) * log10(m_2l)) / 1e6;

  //Stay within Type II bounds.
  if(t < t_m_2l && t > t_m_2u)
  {
    //Get max time limit
    if(t + dt > t_m_2l){
      t1 = t_m_2l;
    }else{
      t1 = t + dt;
    }

    //Get min time limit
    if(t - dt < t_m_2u){
      t2 = t_m_2u;
    }else{
      t2 = t - dt;
    }

    //Get minimum mass limit AT maximum time
    m1 = (3.42 - sqrt(3.42*3.42 - 4.0*0.88*(10 - log10(t1 * 1.0e6)))) / 1.76;
    m1 = pow(10, m1);

    //Get maximum mass limit AT minimum time
    m2 = (3.42 - sqrt(3.42*3.42 - 4.0*0.88*(10 - log10(t2 * 1.0e6)))) / 1.76;
    m2 = pow(10, m2);

            
    dN_II = origStarMass * integrate_imf_div_m(m1, m2); //eqn 31, K04
   
    //Convert to SNe per year.
    dN_II = dN_II /( (t1 - t2) * 1.0e6);

  }else{
    dN_II = 0;
  }

  /*/Diagnostics
  printf("P[%i].ID: %i  starLifeTime : %f - %f = %f\n", target, 
        P[target].ID, phys_time, P[target].StarAge, t);
  //printf("(t_m_2l, t_m_2u) : %f %f\n",t_m_2l,t_m_2u);
  //printf("(t1, t2) : %f %f\n",t1,t2);
  //printf("(m1, m2) : %f %f\n",m1,m2);
  printf("dN_II: %e\n", dN_II);
  */
  return dN_II;
}




/****************************************************************************
  Return the Type Ia Supernova rate M_sun / year.
****************************************************************************/
double get_typeIa_snr(int target)
{
  double dN_Ia        = 0;           //Number of Type II SN per year.
  double star_age     = 0;
  double dt           = 0.1;
  double  t           = 0;
  double origStarMass = 0;
  double mt           = 0;           // Turnoff Mass
  double tau          = 42.5731;     // Lifetime of 8M_sun star.
  double tmax         = 0;           // upper time limit
  double tmin         = 0;           // lower time limit
  double N_0          = 0.0013;      // (SN / M_sun), Maoz et al 2012
  double s            = 1.12;        // power law index, Vogelsberger 13 eqn10

  //Error check
  if(P[target].Type != 4){
    fprintf(stderr, "ERROR!! ThisTask: %i\n"
            "Can't return Type II SNR for a P[%i].ID = %i and *.Type = %i\n",
            ThisTask, target, P[target].ID, P[target].Type);
    endrun(2000);
  }


  star_age     = P[target].StarAge;
  origStarMass = P[target].origStarMass;  
  mt           = P[target].TurnOffMass;
  t            = phys_time - star_age;

  //Convert origStarMass to solar masses
  if(All.ComovingIntegrationOn)
  {
    origStarMass = origStarMass * All.UnitMass_in_g / 
                   (SOLAR_MASS * All.HubbleParam);
  }else{
    origStarMass = origStarMass * All.UnitMass_in_g / SOLAR_MASS;
  }

  //PIECEWISE ::: Ensure if dt < tau, returns 0.  Eqn. 10 Vogelsberger 2013
  if(t < tau){
     dN_Ia =  0;
  }else{
    
    //Ensure bounds.
    if(t - dt < tau){
      tmin = tau;
    }else{
      tmin = t - dt;
    }

    tmax = t + dt;      //Won't realistically hit max time limit.

    dN_Ia = N_0 * (s - 1) * tmax * pow(tmax / tau, -s) / ((1 - s) * tau) -
            N_0 * (s - 1) * tmin * pow(tmin / tau, -s) / ((1 - s) * tau);

    dN_Ia = origStarMass * dN_Ia /( (tmax - tmin) * 1.0e6);
  }

  /*/Diagnostics
  printf("P[%i].ID: %i  origMass: %f  tmax,tmin: %f %f   t: %f  dN_Ia: %e\n\n",
          target, P[target].ID, P[target].origStarMass, tmax, tmin, t, dN_Ia);
  */
  return dN_Ia;
}
#endif
#endif
#endif
#endif
#endif
