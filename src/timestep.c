#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <mpi.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_spline.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_roots.h>
#include "allvars.h"
#include "proto.h"

/*! file timestep.c 
 *  brief routines for 'kicking' particles in momentum space and assigning new timesteps
 */

static double fac1;					// = (1+z)^2 = 1/a^2
static double fac2;         // = (1+z)^(3*5/3 - 2) = (1+z)^3
static double fac3;					// ALI: How are these derived 
static double hubble_a;			// = H(t), C&O eqn 29.122, OmegaKurvature != 0, OmegaRel = 0
static double atime;				// = a     = scale factor = All.Time
static double a3inv;				// = 1/a^3 = 1/(scale factor^3) = 1/All.Time^3 
static double dt_displacement = 0;


/*! This function advances the system in momentum space, i.e. it does apply
 *  the 'kick' operation after the forces have been computed. Additionally, it
 *  assigns new timesteps to particles. At start-up, a half-timestep is
 *  carried out, as well as at the end of the simulation. In between, the
 *  half-step kick that ends the previous timestep and the half-step kick for
 *  the new timestep are combined into one operation.
 *
 *	Recall All.Time == a (scale factor), see run.c for convincing.
 *  C&O = Carroll and Ostlie "Introduction to Modern Astrophysics".
 * 	In 'hubble_a' OmegaRel = 0, but OmegaCurvature = 1 - OmegaTotal. This permits
 * 	using cosmologies of different curvature other than flat.
 */
void advance_and_find_timesteps(void)
{
  int i, j, no;
	int ti_step;							//Timestep (in redshift space) det. from cartesian accel. 
	int ti_min;
	int tend; 								//Midpoint in [0,TIMEBASE] of new timestep
	int tstart;								//Midpoint in [0,TIMEBASE] of previous timestep
  double dt_entr;           // = (tend - tstart) * All.Timebase_interval.... dloga
	double dt_entr2;          // = (tend - P[i].Ti_endstep) * All.Timebase_interval
	double dt_gravkick;       // = integral( da / (H(a) * a^2), {tstart,tend} ) 
	double dt_hydrokick;      // = integral( da / (H(a) a^(3*GAMMA-2)), {tstart,tend})
	double dt_gravkick2;      //
	double dt_hydrokick2;     //
	double t0;
	double t1;
  double minentropy;
	double aphys;							//Physical acceleration (in cartesian coords?)
  FLOAT dv[3];							//Temp holder for comoving velocities

	#ifdef FLEXSTEPS
  	int ti_grp;
	#endif
	#if defined(PSEUDOSYMMETRIC) && !defined(FLEXSTEPS)
  	double apred, prob;
  	int ti_step2;
	#endif
	#ifdef PMGRID
  	double dt_gravkickA;
		double dt_gravkickB;							//Long range PM gravity kick
	#endif
	#ifdef MAKEGLASS
	  double disp;
		double dispmax;
		double globmax;
		double dmean;
		double fac;
		double disp2sum;
		double globdisp2sum;
	#endif

	#ifdef COOLING
		//Initialize GSL root solver
		int iter = 0;												//Num of iteration.
		int status = 0;											//Catch integration status
		double hi;													//f(Entropy_lo)
		double lo;													//f(Entropy_hi)
		double newEntropy;
		double Entropy_lo;//= 0.5 * SphP[i].Entropy;//Bracket lower limit of rt solver
		double Entropy_hi;//= 1.1 * SphP[i].Entropy;//Bracket upper limit of rt solver
		const gsl_root_fsolver_type *T;  		//Root solver algorithm used
		gsl_root_fsolver *s; 								//Actual driver function
		gsl_function F;											//Points to function to be solved
		cooling_params params;//= {(double)dt_entr, (int)i};

		T = gsl_root_fsolver_brent;
		s = gsl_root_fsolver_alloc(T); 

		//F.function = &cooling;
		//F.params   = &params;
	#endif

  t0 = second();
  #ifdef SFR
    #ifdef FEEDBACK
	    phys_time = get_phys_time(All.Time);
    #endif
  #endif

	//Gather appropriate factors of 'a' and H(a)
  if(All.ComovingIntegrationOn)
  {
    fac1 = 1 / (All.Time * All.Time);							// = (1+z)^2
    fac2 = 1 / pow(All.Time, 3 * GAMMA - 2);			// = (1+z)^(3*5/3 - 2) = (1+z)^3
    fac3 = pow(All.Time, 3 * (1 - GAMMA) / 2.0);  // = (1+z)^(3*(1-5/3)/2) = 1/(1+z)

    #ifndef DARKENERGY
         hubble_a = All.Omega0 / (All.Time * All.Time * All.Time) //OmegaRel = 0, OmegaK != 0
       	   + (1 - All.Omega0 - All.OmegaLambda) / (All.Time * All.Time) + All.OmegaLambda;
    #endif

    #ifdef DARKENERGY
         #ifdef SCALARFIELD
            All.dark_factor = get_dark_factor(All.Time);
            hubble_a = All.Omega0 / (All.Time * All.Time * All.Time) +
               (1 - All.Omega0 - All.OmegaLambda) / (All.Time * All.Time) +
               (All.OmegaLambda * All.dark_factor);
         #endif

         #ifdef DARKPARAM
            All.dark_factor = get_dark_factor(All.Time);
            hubble_a = All.Omega0 / (All.Time * All.Time * All.Time) +
               (1 - All.Omega0 - All.OmegaLambda) / (All.Time * All.Time) +
               (All.OmegaLambda * All.dark_factor);
         #endif

         #ifdef GCG
            All.dark_factor = get_dark_factor(All.Time);
            hubble_a = All.Omega0 / (All.Time * All.Time * All.Time) +
               (1 - All.Omega0 - All.OmegaLambda) / (All.Time * All.Time) +
               (All.OmegaLambda * All.dark_factor);
         #endif
    #endif

    hubble_a = All.Hubble * sqrt(hubble_a); 			//C&O eqn 29.122
    a3inv = 1 / (All.Time * All.Time * All.Time); // = 1/a^3 = (1+z)^3
    atime = All.Time;															// = a = 1/1+z
  }
  else
    fac1 = fac2 = fac3 = hubble_a = a3inv = atime = 1;

	#ifdef NOPMSTEPADJUSTMENT
  	dt_displacement = All.MaxSizeTimestep;
	#else
  	if(Flag_FullStep || dt_displacement == 0)
  	  find_dt_displacement_constraint(hubble_a * atime * atime);  //sets dt_displacment
	#endif

	#ifdef PMGRID
  	if(All.ComovingIntegrationOn)
    	dt_gravkickB = get_gravkick_factor(All.PM_Ti_begstep, All.Ti_Current) -
      			         get_gravkick_factor(All.PM_Ti_begstep, 
															          (All.PM_Ti_begstep + All.PM_Ti_endstep) / 2);
  	else
    	dt_gravkickB = (All.Ti_Current - (All.PM_Ti_begstep + All.PM_Ti_endstep) / 2) 
                     * All.Timebase_interval;

 	 	if(All.PM_Ti_endstep == All.Ti_Current)	/* need to do long-range kick */
 	 	{
 	   	/* make sure that we reconstruct the domain/tree next time because we don't kick 
 	       the tree nodes in this case */
   	 	All.NumForcesSinceLastDomainDecomp = 1 + All.TotNumPart * All.TreeDomainUpdateFrequency;
  	}
	#endif


	#ifdef MAKEGLASS
  	for(i = 0, dispmax = 0, disp2sum = 0; i < NumPart; i++)
  	{
    	for(j = 0; j < 3; j++)
	  	{
	    	P[i].GravPM[j] *= -1;
	    	P[i].GravAccel[j] *= -1;
	    	P[i].GravAccel[j] += P[i].GravPM[j];
	    	P[i].GravPM[j] = 0;
	  	}

    	disp = sqrt(P[i].GravAccel[0] * P[i].GravAccel[0] +
	  	P[i].GravAccel[1] * P[i].GravAccel[1] + P[i].GravAccel[2] * P[i].GravAccel[2]);

    	disp *= 2.0 / (3 * All.Hubble * All.Hubble);

    	disp2sum += disp * disp;

    	if(disp > dispmax)
	    	dispmax = disp;
  	}

  	MPI_Allreduce(&dispmax, &globmax, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
  	MPI_Allreduce(&disp2sum, &globdisp2sum, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);

  	dmean = pow(P[0].Mass / (All.Omega0 * 3 * All.Hubble * All.Hubble / (8 * M_PI * All.G)), 1.0 / 3);

  	if(globmax > dmean)
    	fac = dmean / globmax;
  	else
    	fac = 1.0;

  	if(ThisTask == 0)
  	{
    	printf("\nglass-making:  dmean= %g  global disp-maximum= %g  rms= %g\n\n",
	  	        dmean, globmax, sqrt(globdisp2sum / All.TotNumPart));
    	fflush(stdout);
  	}

  	for(i = 0, dispmax = 0; i < NumPart; i++)
  	{
    	for(j = 0; j < 3; j++)
	  	{
	    	P[i].Vel[j] = 0;
	    	P[i].Pos[j] += fac * P[i].GravAccel[j] * 2.0 / (3 * All.Hubble * All.Hubble);
	    	P[i].GravAccel[j] = 0;
	  	}
  	}
	#endif



  /* Now assign new timesteps and kick */

	#ifdef FLEXSTEPS
  	if((All.Ti_Current % (4 * All.PresentMinStep)) == 0)
    	if(All.PresentMinStep < TIMEBASE)
      	All.PresentMinStep *= 2;

  	for(i = 0; i < NumPart; i++)
  	{
    	if(P[i].Ti_endstep == All.Ti_Current)
	  	{
	    	ti_step = get_timestep(i, &aphys, 0);  //Get timestep (in redshift space)

	    	/* make it a power 2 subdivision */
	    	ti_min = TIMEBASE;
	    	while(ti_min > ti_step)
	    	  ti_min >>= 1;
	    	ti_step = ti_min;

	   	 	if(ti_step < All.PresentMinStep)
	      	All.PresentMinStep = ti_step;
	  	}
  	}

  	ti_step = All.PresentMinStep;
  	MPI_Allreduce(&ti_step, &All.PresentMinStep, 1, MPI_INT, MPI_MIN, MPI_COMM_WORLD);

  	if(dt_displacement < All.MaxSizeTimestep)
    	ti_step = (int) (dt_displacement / All.Timebase_interval);
  	else
    	ti_step = (int) (All.MaxSizeTimestep / All.Timebase_interval);

  	/* make it a power 2 subdivision */
  	ti_min = TIMEBASE;
  	while(ti_min > ti_step)
  	  ti_min >>= 1;
  	All.PresentMaxStep = ti_min;


  	if(ThisTask == 0)
    	printf("Syn Range = %g  PresentMinStep = %d  PresentMaxStep = %d \n",
	   				(double) All.PresentMaxStep / All.PresentMinStep, All.PresentMinStep, 
						All.PresentMaxStep);

	#endif


	/******************************** Do Kick *********************************/
  for(i = 0; i < NumPart; i++)
  {
    if(P[i].Ti_endstep == All.Ti_Current)
	  {
	    ti_step = get_timestep(i, &aphys, 0);

	    /* make it a power 2 subdivision. Find closest power of 2 to ti_step */
	    ti_min = TIMEBASE;
	    while(ti_min > ti_step)
	      ti_min >>= 1;
	    ti_step = ti_min;

			#ifdef FLEXSTEPS
	    	ti_grp = P[i].FlexStepGrp % All.PresentMaxStep;
	    	ti_grp = (ti_grp / All.PresentMinStep) * All.PresentMinStep;
	    	ti_step = ((P[i].Ti_endstep + ti_grp + ti_step) / ti_step) * ti_step - (P[i].Ti_endstep + ti_grp);
			#else

				#ifdef PSEUDOSYMMETRIC
	    		if(P[i].Type != 0)
	    		{
	    		  if(P[i].Ti_endstep > P[i].Ti_begstep)
		  	  	{
		    	  	apred = aphys + ((aphys - P[i].AphysOld) / (P[i].Ti_endstep - P[i].Ti_begstep)) * ti_step;
		    	  	if(fabs(apred - aphys) < 0.5 * aphys)
		      		{
		        		ti_step2 = get_timestep(i, &apred, -1);
		        		ti_min = TIMEBASE;
		        		while(ti_min > ti_step2)
			        		ti_min >>= 1;
		        		ti_step2 = ti_min;

		        		if(ti_step2 < ti_step)
			      		{
			        		get_timestep(i, &apred, ti_step);
			        		prob =
			        	  	    ((apred - aphys) / (aphys - P[i].AphysOld) * (P[i].Ti_endstep -
											  P[i].Ti_begstep)) / ti_step;
			        		if(prob < get_random_number(P[i].ID))
			          		ti_step /= 2;
			      		}
		        		else if(ti_step2 > ti_step)
			      		{
			        		get_timestep(i, &apred, 2 * ti_step);
			        		prob =
			          		   ((apred - aphys) / (aphys - P[i].AphysOld) * (P[i].Ti_endstep -
											  P[i].Ti_begstep)) / ti_step;
    			    		if(prob < get_random_number(P[i].ID + 1))
    			      		ti_step *= 2;
			      		}
		      		}
		    		}
	      		P[i].AphysOld = aphys;
	    		}
				#endif

				#ifdef SYNCHRONIZATION
	    		if(ti_step > (P[i].Ti_endstep - P[i].Ti_begstep))/*timestep wants to increse*/
	    		{
	      		if(((TIMEBASE - P[i].Ti_endstep) % ti_step) > 0)
							ti_step = P[i].Ti_endstep - P[i].Ti_begstep;	/* leave at old step */
	    		}
				#endif
			#endif /* end of FLEXSTEPS */

			/* we here finish the last timestep. */
	    if(All.Ti_Current == TIMEBASE)	
	      ti_step = 0;

			/*check that we don't run beyond the end*/
	    if((TIMEBASE - All.Ti_Current) < ti_step) 
	      ti_step = TIMEBASE - All.Ti_Current;

	    tstart = (P[i].Ti_begstep + P[i].Ti_endstep) / 2;	/* midpoint of old step */
	    tend = P[i].Ti_endstep + ti_step / 2;	            /* midpoint of new step */

	    if(All.ComovingIntegrationOn)
	    {
	      dt_entr = (tend - tstart) * All.Timebase_interval;
	      dt_entr2 = (tend - P[i].Ti_endstep) * All.Timebase_interval;
	      dt_gravkick = get_gravkick_factor(tstart, tend);
	      dt_hydrokick = get_hydrokick_factor(tstart, tend);
	      dt_gravkick2 = get_gravkick_factor(P[i].Ti_endstep, tend);
	      dt_hydrokick2 = get_hydrokick_factor(P[i].Ti_endstep, tend);
	    }
	    else
      {
        dt_entr = dt_gravkick = dt_hydrokick = (tend - tstart) * All.Timebase_interval;
	      dt_gravkick2 = dt_hydrokick2 = dt_entr2 = (tend - P[i].Ti_endstep) * All.Timebase_interval;
	    }

			/* Advance to next next timestep */
	    P[i].Ti_begstep = P[i].Ti_endstep;
	    P[i].Ti_endstep = P[i].Ti_begstep + ti_step;


	    /* do the kick */

	    for(j = 0; j < 3; j++)
	    {
	      dv[j] = P[i].GravAccel[j] * dt_gravkick;
	      P[i].Vel[j] += dv[j];
	    }


			// SPH Kick
	    if(P[i].Type == 0)	/* SPH stuff */
	    {
	      for(j = 0; j < 3; j++)
		    {
		      dv[j] += SphP[i].HydroAccel[j] * dt_hydrokick;
		      P[i].Vel[j] += SphP[i].HydroAccel[j] * dt_hydrokick;

		      SphP[i].VelPred[j] =
		            P[i].Vel[j] - dt_gravkick2 * P[i].GravAccel[j] - dt_hydrokick2 * SphP[i].HydroAccel[j];
					#ifdef PMGRID
		      	SphP[i].VelPred[j] += P[i].GravPM[j] * dt_gravkickB;
					#endif
		    }


				#ifndef COOLING
	      	/* In case of cooling, we prevent that the entropy (and
	      	   hence temperature decreases by more than a factor 0.5.
						 See logic in Katz et al (1996) p23. */

	    	  if(SphP[i].DtEntropy * dt_entr > -0.5 * SphP[i].Entropy)
		  	    SphP[i].Entropy += SphP[i].DtEntropy * dt_entr;
	    	  else
		  	    SphP[i].Entropy *= 0.5;

	    	  if(All.MinEgySpec)
		  	  {
		    	  minentropy = All.MinEgySpec * GAMMA_MINUS1 / pow(SphP[i].Density * a3inv, 
												 GAMMA_MINUS1);
		    	  if(SphP[i].Entropy < minentropy)
		    	  {
		    	    SphP[i].Entropy = minentropy;
		    	    SphP[i].DtEntropy = 0;
		    	  }
		    	}

	      	/* In case the timestep increases in the new step, we
	      	   make sure that we do not 'overcool' when deriving
	      	   predicted temperatures. The maximum timespan over
	      	   which prediction can occur is ti_step/2, i.e. from
	      	   the middle to the end of the current step */

	      	dt_entr = ti_step / 2 * All.Timebase_interval;
	      	if(SphP[i].Entropy + SphP[i].DtEntropy * dt_entr < 0.5 * SphP[i].Entropy)
		    	  SphP[i].DtEntropy = -0.5 * SphP[i].Entropy / dt_entr;
				#endif


				#ifdef COOLING
					/*
						I am implicitly integrating eqn. 51 in the original GADGET paper. See 
						Springel, Yoshida & White (2001). We are also making the 'isochoric 
						approximation', which I believe is assuming 'rho' is constant. Also, 
						there is no explicit time dependence in this eqn to worry about.

						Use GSL Brent Root solving method b/c derivative does not need computed.
						Computing derivative for 4-Dimensional table would require something like
						4^3 or 4^4 interpolating points, which would be very expensive and a bitch 
						to correctly calculate.

						NOTE: the root solver can be easily changed below, see GSL user manual.
					*/
					//Initialize GSL root solver
					iter = 0;												//Num of iteration.
					status = 0;											//Catch integration status
					Entropy_lo = 0.5 * SphP[i].Entropy;
					Entropy_hi = 1.1 * SphP[i].Entropy;
					params.h = (double)dt_entr;
					params.target = (int)i;

					F.function = &cooling;
					F.params   = &params;

					lo = cooling(Entropy_lo, &params);
					hi = cooling(Entropy_hi, &params);

					//Bracket root, ensure existance. See Numerical Recipes Ch. 9 for explanation.
					while(lo * hi > 0)
					{
						Entropy_lo = 0.5 * Entropy_lo;
						Entropy_hi = 2.0 * Entropy_hi;
						lo = cooling(Entropy_lo, &params);
						hi = cooling(Entropy_hi, &params);
					}

					//set brackets for Brent's method
					gsl_root_fsolver_set(s, &F, Entropy_lo, Entropy_hi); 


					//Iterate root solver
					do
					{
						iter++;
						status 			= gsl_root_fsolver_iterate(s);
						newEntropy 	= gsl_root_fsolver_root(s);
						Entropy_lo  = gsl_root_fsolver_x_lower(s);
						Entropy_hi 	= gsl_root_fsolver_x_upper(s);
						status 			= gsl_root_test_interval(Entropy_lo, Entropy_hi, 0, 0.00001);

					}while(status == GSL_CONTINUE && iter < MAX_COOLING_ITER);


					//Check for Convergence
					if(status != GSL_SUCCESS){
						fprintf(stderr, "Cooling Intergration Failed to converge. Task: %i   "
										"P[%i].ID = %i\n", ThisTask, i, P[i].ID);
						endrun(1001);
					}



					//Check to see if cooling needs turned back on
					#ifdef FEEDBACK_W_SPH_KERNAL
						//phys_time = get_phys_time(All.Time);
    				if(phys_time - SphP[i].time_turned_off > 30.0){
		      		SphP[i].cooling_on = 1;
		     		}
					#endif

	
					//Eqn 52 of Gadget1 paper (Springel, Yoshida & White 2001)
					SphP[i].DtEntropy = (newEntropy - SphP[i].Entropy) / dt_entr;


	      	/* In case of cooling, we prevent that the entropy (and
	      	   hence temperature decreases by more than a factor 0.5.
						 See logic in Katz et al (1996) p23. */
	    	  if(SphP[i].DtEntropy * dt_entr > -0.5 * SphP[i].Entropy)
		  	    SphP[i].Entropy += SphP[i].DtEntropy * dt_entr;
	    	  else
		  	    SphP[i].Entropy *= 0.5;

	    	  if(All.MinEgySpec)
		  	  {
		    	  minentropy = All.MinEgySpec * GAMMA_MINUS1 / pow(SphP[i].Density * a3inv, 
												 GAMMA_MINUS1);
		    	  if(SphP[i].Entropy < minentropy)
		    	  {
		    	    SphP[i].Entropy = minentropy;
		    	    SphP[i].DtEntropy = 0;
		    	  }
		    	}

	      	/* In case the timestep increases in the new step, we
	      	   make sure that we do not 'overcool' when deriving
	      	   predicted temperatures. The maximum timespan over
	      	   which prediction can occur is ti_step/2, i.e. from
	      	   the middle to the end of the current step */

	      	dt_entr = ti_step / 2 * All.Timebase_interval;
	      	if(SphP[i].Entropy + SphP[i].DtEntropy * dt_entr < 0.5 * SphP[i].Entropy)
		    	  SphP[i].DtEntropy = -0.5 * SphP[i].Entropy / dt_entr;
				#endif
	    }





	    /* if tree is not going to be reconstructed, kick parent nodes dynamically.
	     */
	    if(All.NumForcesSinceLastDomainDecomp < All.TotNumPart * All.TreeDomainUpdateFrequency)
	    {
	      no = Father[i];
	      while(no >= 0)
		    {
		      for(j = 0; j < 3; j++)
		        Extnodes[no].vs[j] += dv[j] * P[i].Mass / Nodes[no].u.d.mass;

		      no = Nodes[no].u.d.father;
		    }
	    }
	  }
  }


	/* KICK long range force */
	#ifdef PMGRID
	  if(All.PM_Ti_endstep == All.Ti_Current)	/* need to do long-range kick */
  	{
    	ti_step = TIMEBASE;
    	while(ti_step > (dt_displacement / All.Timebase_interval))
	    	ti_step >>= 1;

			/* PM-timestep wants to increase */
    	if(ti_step > (All.PM_Ti_endstep - All.PM_Ti_begstep))	
	  	{
	    	/* we only increase if an integer number of steps will bring us to the end */
	    	if(((TIMEBASE - All.PM_Ti_endstep) % ti_step) > 0)
	      	ti_step = All.PM_Ti_endstep - All.PM_Ti_begstep;	/* leave at old step */
	  	}

    	if(All.Ti_Current == TIMEBASE)	/* we here finish the last timestep. */
	    	ti_step = 0;

    	tstart = (All.PM_Ti_begstep + All.PM_Ti_endstep) / 2;
    	tend = All.PM_Ti_endstep + ti_step / 2;

    	if(All.ComovingIntegrationOn)
	    	dt_gravkick = get_gravkick_factor(tstart, tend);
    	else
	    	dt_gravkick = (tend - tstart) * All.Timebase_interval;

    	All.PM_Ti_begstep = All.PM_Ti_endstep;
    	All.PM_Ti_endstep = All.PM_Ti_begstep + ti_step;

    	if(All.ComovingIntegrationOn)
	    	dt_gravkickB = -get_gravkick_factor(All.PM_Ti_begstep, (All.PM_Ti_begstep + All.PM_Ti_endstep) / 2);
    	else
	    	dt_gravkickB =
	      	     -((All.PM_Ti_begstep + All.PM_Ti_endstep) / 2 - All.PM_Ti_begstep) * All.Timebase_interval;

			// Long-Range Kick
    	for(i = 0; i < NumPart; i++)
	  	{
	    	for(j = 0; j < 3; j++)	/* do the kick */
	      	P[i].Vel[j] += P[i].GravPM[j] * dt_gravkick;

	    	if(P[i].Type == 0)
	    	{
	      	if(All.ComovingIntegrationOn)
	    		{
	    	  	dt_gravkickA = get_gravkick_factor(P[i].Ti_begstep, All.Ti_Current) -
	  	    								 get_gravkick_factor(P[i].Ti_begstep, (P[i].Ti_begstep + 
																							 P[i].Ti_endstep) / 2);
	    	  	dt_hydrokick = get_hydrokick_factor(P[i].Ti_begstep, All.Ti_Current) -
	  	    								 get_hydrokick_factor(P[i].Ti_begstep, (P[i].Ti_begstep + 
																								P[i].Ti_endstep) / 2);
	    		}
	      	else
		      	dt_gravkickA = dt_hydrokick =
		        	    (All.Ti_Current - (P[i].Ti_begstep + P[i].Ti_endstep) / 2) * All.Timebase_interval;

	      	for(j = 0; j < 3; j++)
		    	  SphP[i].VelPred[j] = P[i].Vel[j]
		             + P[i].GravAccel[j] * dt_gravkickA
		             + SphP[i].HydroAccel[j] * dt_hydrokick + P[i].GravPM[j] * dt_gravkickB;
	    	}
	  	}
  	}
	#endif
	

	#ifdef COOLING
		//Free memory
		gsl_root_fsolver_free(s);
	#endif

  t1 = second();
  All.CPU_TimeLine += timediff(t0, t1);
}




/*! This function normally (for flag==0) returns the maximum allowed timestep
 *  of a particle, expressed in terms of the integer mapping that is used to
 *  represent the total simulated timespan. The physical acceleration is
 *  returned in `aphys'. The latter is used in conjunction with the
 *  PSEUDOSYMMETRIC integration option, which also makes of the second
 *  function of get_timestep. When it is called with a finite timestep for
 *  flag, it returns the physical acceleration that would lead to this
 *  timestep, assuming timestep criterion 0.
 *
 *  NOTE: His 'physical coordinates' are what I call 'cartesian coordinates'.
 *        Also his 'physical coordinates' are still in internal units.
 *        He finds the time step (ti_step) using eqns 16 and 34.
 */
int get_timestep(int p,		      /*!< particle index */
	             	 double *aphys,	/*!< acceleration (physical units) */
		             int flag	      /*!< either 0 for normal operation, or 
                                     finite timestep to get corresponding aphys */ )
{
  double ax, ay, az;						//Accel in physical coordinates (internal units)
	double ac;
	double csnd;									//Speed of sound
  double dt = 0;								//Physical time, then ln(a)  
	double dt_courant = 0;				//Courant time condition, eqn 16
	double dt_accel;
  int ti_step;									//Time step returned. in range [0,TIMEBASE]

	#ifdef CONDUCTION
  	double dt_cond;
	#endif

	/* Here we are calculating cartesian force from the comoving force which 
     has already been calculated and stored in GravAccel[] (in comoving coordinates). 
     Recall that 
          (F_x', F_y', F_z') = m/r'^2 * (dx'/r, dy'/r, dz'/r)  
 
     where the prime denotes the cartesian coordinates and the non-prime denotes
     the comoving coordinates. Recall that r = r'/a, so...

                   F' = F/a^2 = (1+z)^2 * F
  */
  if(flag == 0)
  {
    ax = fac1 * P[p].GravAccel[0];
    ay = fac1 * P[p].GravAccel[1];
    az = fac1 * P[p].GravAccel[2];

		#ifdef PMGRID
	    ax += fac1 * P[p].GravPM[0];
	    ay += fac1 * P[p].GravPM[1];
	    az += fac1 * P[p].GravPM[2];
		#endif
    if(P[p].Type == 0)
	  {
			//Can derive from eqn 7, eqn 4 and P= A_{i} * rho^gamma
	    ax += fac2 * SphP[p].HydroAccel[0];
	    ay += fac2 * SphP[p].HydroAccel[1];
	    az += fac2 * SphP[p].HydroAccel[2];
	  }

    ac = sqrt(ax * ax + ay * ay + az * az);	/* this is now the physical acceleration */
    *aphys = ac;
  }
  else
    ac = *aphys;

  if(ac == 0)
    ac = 1.0e-30;

  switch (All.TypeOfTimestepCriterion)
  {
    case 0:
      if(flag > 0)
	    {
	      dt = flag * All.Timebase_interval;
    	  dt /= hubble_a;	/* convert dloga to physical timestep  */
    	  ac = 2 * All.ErrTolIntAccuracy * atime * All.SofteningTable[P[p].Type] 
             / (dt * dt);
    	  *aphys = ac;
    	  return flag;
    	}

      //Eqn 34 (Springel '05)
      dt = dt_accel = sqrt(2 * All.ErrTolIntAccuracy * atime * 
                           All.SofteningTable[P[p].Type] / ac);
			#ifdef ADAPTIVE_GRAVSOFT_FORGAS
      	if(P[p].Type == 0)
      		dt = dt_accel = sqrt(2 * All.ErrTolIntAccuracy * atime * SphP[p].Hsml / 2.8 
                          / ac);
			#endif
      break;

    default:
      endrun(888);
      break;
  }

  //Get minimum between eqn 34 and eqn 16....dt = min(dt,dt_courant)
  if(P[p].Type == 0)
  {
    csnd = sqrt(GAMMA * SphP[p].Pressure / SphP[p].Density);

    if(All.ComovingIntegrationOn)
	    dt_courant = 2 * All.CourantFac * All.Time * SphP[p].Hsml / (fac3 * SphP[p].MaxSignalVel);
    else
	    dt_courant = 2 * All.CourantFac * SphP[p].Hsml / SphP[p].MaxSignalVel;

		// min|dt,dt_courant|
    if(dt_courant < dt)
	    dt = dt_courant;
  }


  /* convert the physical timestep to dloga if needed. Note: If comoving integration 
     has not been selected, hubble_a=1. Recall da/dt = a * H(a) and 
     d(log(a))/da = 1/a. Rearrange and substitute.
   */
  dt *= hubble_a; 					// = d(log(a))

	//Can't have dt larger than Max Limits
  if(dt >= All.MaxSizeTimestep)
    dt = All.MaxSizeTimestep;

  if(dt >= dt_displacement)
    dt = dt_displacement;


  //If dt is invalid, exit gracefully.
  if(dt < All.MinSizeTimestep)
  {
		#ifndef NOSTOP_WHEN_BELOW_MINTIMESTEP
    	printf("warning: Timestep wants to be below the limit `MinSizeTimestep'\n");

    	if(P[p].Type == 0)
	  	{
	    	printf
	      	   	("Part-ID=%d  dt=%g dtc=%g ac=%g xyz=(%g|%g|%g)  hsml=%g  maxsignalvel=%g"
               " dt0=%g eps=%g\n",(int) P[p].ID, dt, dt_courant * hubble_a, ac, 
              P[p].Pos[0], P[p].Pos[1], P[p].Pos[2], SphP[p].Hsml, SphP[p].MaxSignalVel,
	         		sqrt(2 * All.ErrTolIntAccuracy * atime * All.SofteningTable[P[p].Type] / 
                   ac) * hubble_a, All.SofteningTable[P[p].Type]);
	  	}
    	else
	  	{
	  	  printf("Part-ID=%d  dt=%g ac=%g xyz=(%g|%g|%g)\n", (int) P[p].ID, 
        	      dt, ac, P[p].Pos[0], P[p].Pos[1], P[p].Pos[2]);
	  	}
    	fflush(stdout);
    	endrun(888);
		#endif
    dt = All.MinSizeTimestep;
	}

  ti_step = dt / All.Timebase_interval;			//	int  = double / double


	/* Time step must be non-zero AND less than the mapped timespan interval. dt is 
     invalid, exit gracefully. */
  if(!(ti_step > 0 && ti_step < TIMEBASE))
  {
    printf("\nError: A timestep of size zero was assigned on the integer timeline!\n"
	         "We better stop.\n"
	         "Task=%d Part-ID=%d dt=%g tibase=%g ti_step=%d ac=%g xyz=(%g|%g|%g)"
					 "tree=(%g|%g%g)\n\n",
	         ThisTask, (int) P[p].ID, dt, All.Timebase_interval, ti_step, ac,
	         P[p].Pos[0], P[p].Pos[1], P[p].Pos[2], P[p].GravAccel[0], 
           P[p].GravAccel[1], P[p].GravAccel[2]);
		#ifdef PMGRID
    	printf("pm_force=(%g|%g|%g)\n", P[p].GravPM[0], P[p].GravPM[1], P[p].GravPM[2]);
		#endif
    if(P[p].Type == 0)
	    printf("hydro-frc=(%g|%g|%g)\n", SphP[p].HydroAccel[0], 
             SphP[p].HydroAccel[1], SphP[p].HydroAccel[2]);

    fflush(stdout);
    endrun(818);
  }

  return ti_step;
}


/*! This function computes an upper limit ('dt_displacement') to the global
 *  timestep of the system based on the rms velocities of particles. For
 *  cosmological simulations, the criterion used is that the rms displacement
 *  should be at most a fraction MaxRMSDisplacementFac of the mean particle
 *  separation. Note that the latter is estimated using the assigned particle
 *  masses, separately for each particle type. If comoving integration is not
 *  used, the function imposes no constraint on the timestep.
 */
void find_dt_displacement_constraint(double hfac /*!<  should be  a^2*H(a)  */ )
{
  int i, j;
	int type;											//Type index
	int *temp;		
  int count[6];									// LOCAL  number of particles per type
  long long count_sum[6];				// GLOBAL number of particles per type
  double v[6];									// LOCAL  comoving sum v^2 sorted by particle type
	double v_sum[6];							// GLOBAL comoving sum v^2 sorted by particle type
	double mim[6];								// min. mass per P[type], good 4 part. w/ changing m
	double min_mass[6];						// GLOBAL minimum mass per particle type
  double dt;										// ~= dmean / (rms velocity)
	double dmean;									// Mean comoving distance between particles 
	double asmth = 0;

  dt_displacement = All.MaxSizeTimestep;

  if(All.ComovingIntegrationOn)
  {
    for(type = 0; type < 6; type++)
  	{
  	  count[type] = 0;
  	  v[type] = 0;
  	  mim[type] = 1.0e30;
  	}

		//Get sum(v^2), min mass, and count as a function of particle type
    for(i = 0; i < NumPart; i++)
  	{
  	  v[P[i].Type] += P[i].Vel[0] * P[i].Vel[0] + P[i].Vel[1] * P[i].Vel[1] + P[i].Vel[2] * P[i].Vel[2];
  	  if(mim[P[i].Type] > P[i].Mass)
  	    mim[P[i].Type] = P[i].Mass;
  	  count[P[i].Type]++;
  	}

    MPI_Allreduce(v, v_sum, 6, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    MPI_Allreduce(mim, min_mass, 6, MPI_DOUBLE, MPI_MIN, MPI_COMM_WORLD);

		//Get GLOBAL number of particles 
    temp = malloc(NTask * 6 * sizeof(int));
    MPI_Allgather(count, 6, MPI_INT, temp, 6, MPI_INT, MPI_COMM_WORLD);
    for(i = 0; i < 6; i++)
	  {
	    count_sum[i] = 0;
	    for(j = 0; j < NTask; j++)
	      count_sum[i] += temp[j * 6 + i];
	  }
    free(temp);

    for(type = 0; type < 6; type++)
	  {
	    if(count_sum[type] > 0)
	    {
				//Find dmean by rearranging eqn 8.55 in "Astrophysics in a Nutshell"
	      if(type == 0)
	      	dmean =
	    	  	pow(min_mass[type] / (All.OmegaBaryon * 3 * All.Hubble * All.Hubble / 
						(8 * M_PI * All.G)), 1.0 / 3);
	      else
	      	dmean =
	           pow(min_mass[type] /
	           ((All.Omega0 - All.OmegaBaryon) * 3 * All.Hubble * All.Hubble / 
						 (8 * M_PI * All.G)), 1.0 / 3);

				//I can't justify a^2*H(a) used below, a*dmean gets the cartesian separation.
	      dt = All.MaxRMSDisplacementFac * hfac * dmean / sqrt(v_sum[type] / count_sum[type]);

				#ifdef PMGRID
	      	asmth = All.Asmth[0];
					#ifdef PLACEHIGHRESREGION
		      	if(((1 << type) & (PLACEHIGHRESREGION)))
			      	asmth = All.Asmth[1];
					#endif
	      	if(asmth < dmean)
		      	dt = All.MaxRMSDisplacementFac * hfac * asmth / sqrt(v_sum[type] / count_sum[type]);
				#endif

	      if(ThisTask == 0)
		      printf("type=%d  dmean=%g asmth=%g minmass=%g a=%g  sqrt(<p^2>)=%g"
								 "  dlogmax=%g\n", type, dmean, asmth, min_mass[type], All.Time, 
                  sqrt(v_sum[type] / count_sum[type]), dt);

				//Set maximum time displacment
	      if(dt < dt_displacement)
		      dt_displacement = dt;
	    }
	  }

    if(ThisTask == 0)
    	printf("displacement time constraint: %g  (%g)\n", 
              dt_displacement, All.MaxSizeTimestep);
  }
}
