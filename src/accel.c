#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <mpi.h>

#include "allvars.h"
#include "proto.h"

/*! \file accel.c
 *  \brief driver routine to carry out force computation
 */


/*! This routine computes the accelerations for all active particles.
 *  First, the long-range PM force is computed if the TreePM algorithm is
 *  used and a "big" PM step is done.  Next, the gravitational tree forces
 *  are computed. This also constructs the tree, if needed.
 *
 *  If gas particles are present, the density-loop for active SPH particles
 *  is carried out. This includes an iteration on the correct number of
 *  neighbours.  Finally, the hydrodynamical forces are added.
 */
void compute_accelerations(int mode)
{
  double tstart, tend;

  if(ThisTask == 0)
  {
    printf("Start force computation...\n");
    fflush(stdout);
  }

	#ifdef PMGRID
  	if(All.PM_Ti_endstep == All.Ti_Current)
  	{
  	  tstart = second();
  	  long_range_force();
  	  tend = second();
  	  All.CPU_PM += timediff(tstart, tend);
  	}
	#endif

  tstart = second();		/* measure the time for the full force computation */

  gravity_tree();		    /* computes gravity accel. of PMGRID short range force*/

  if(All.TypeOfOpeningCriterion == 1 && All.Ti_Current == 0)
    gravity_tree();		/* For the first timestep, we redo it
				 * to allow usage of relative opening
				 * criterion for consistent accuracy.
				 */
  tend = second();
  All.CPU_Gravity += timediff(tstart, tend);

	#ifdef FORCETEST
  	gravity_forcetest();
	#endif

  if(All.TotN_gas > 0)
  {
    if(ThisTask == 0)
	  {
	    printf("Start density computation...\n");
	    fflush(stdout);
	  }

    tstart = second();
    density();		/* computes density, and pressure */
    tend = second();
    All.CPU_Hydro += timediff(tstart, tend);

    tstart = second();
    force_update_hmax();      /* tell the tree nodes the new SPH smoothing length such that they are guaranteed to hold the correct max(Hsml) */
    tend = second();
    All.CPU_Predict += timediff(tstart, tend);


    if(ThisTask == 0)
	  {
	    printf("Start hydro-force computation...\n");
	    fflush(stdout);
	  }

    tstart = second();
    hydro_force();		/* adds hydrodynamical accelerations and computes viscous entropy injection  */
		#ifdef COOLING
		#ifdef SFR
		#ifdef METALS
		#ifdef FEEDBACK
			if(ThisTask == 0)
			{
				printf("Start feedback computation...\n"); fflush(stdout);
			}
			#ifndef FEEDBACK_W_SPH_KERNAL
				/* Does feedback using static FeedbackRadius. Not a good method to use.
					 Must Explicitly turn back on if desired!*/
				//get_number_of_gas_nearby();
				//distribute_metals_and_energy();
				fprintf(stderr, "ERROR!!! You Have Selected the Feedback Method Used by\n"
								"Kobayashi 2004 using the feedback radius.  This is a BAD METHOD.\n"
								"If you really intend on using this HORRIBLE Method, you must \n"
								"explicitly enable it by uncommenting it here in accel.c. Otherwise\n"
								"please enable FEEDBACK_W_SPH_KERNAL in the Makefile. Also note\n"
                "that there are several unfixed errors within these routines. These\n"
                "errors were fixed in FEEDBACK_W_SPH_KERNAL\n");
				endrun(999);
			#endif

			#ifdef FEEDBACK_W_SPH_KERNAL
				phys_time = get_phys_time(All.Time);        // result is in phys_time
				update_star_hsml();
				get_feedback_normalization();
				distribute_metals_and_energy_w_kernal();

				/* Update Gas Mass, now that all feedback has been done. Star Mass was 
					 updated back in distribute_metals_and_energy_w_kernal() */
				int i = 0;
				for(i=0; i<N_gas; i++)
				{
					P[i].Mass += P[i].DtMass;			//Obviously really small DtMass is ineffective
					P[i].DtMass = 0;
	
					//Error Check
					if(P[i].Type != 0){
						fprintf(stderr, "ERROR!! You are trying to update the mass of a particle"
										"of Type = %i.\nThis loop is suppose to only update gas masses!", 
										P[i].Type);
						endrun(5009);
					}
				}
			#endif
		#endif
		#endif
		#endif
		#endif

    tend = second();
    All.CPU_Hydro += timediff(tstart, tend);
  }

  if(ThisTask == 0)
  {
    printf("force computation done.\n");
    fflush(stdout);
  }
}
