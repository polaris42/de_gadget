#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <mpi.h>
#include <sys/types.h>
#include <unistd.h>
#include <gsl/gsl_rng.h>

#include "allvars.h"
#include "proto.h"


/*! \file begrun.c
 *  \brief initial set-up of a simulation run
 *
 *  This file contains various functions to initialize a simulation run. In
 *  particular, the parameterfile is read in and parsed, the initial
 *  conditions or restart files are read, and global variables are
 *  initialized to their proper values.
 */


/*! This function performs the initial set-up of the simulation. First, the
 *  parameterfile is set, then routines for setting units, reading
 *  ICs/restart-files are called, auxialiary memory is allocated, etc.
 */
void begrun(void)
{
  struct global_data_all_processes all;

  if(ThisTask == 0)
  {
    printf("\nThis is Gadget, version `%s'.\n", GADGETVERSION);
    printf("\nRunning on %d processors.\n", NTask);
  }

  read_parameter_file(ParameterFile);	/* ... read in parameters for this run */

	#ifdef COOLING
  	get_CIE_cooling_table();
		#ifdef UVBACKGROUND
			get_UV_cooling_table();
		#endif
	
		#ifdef SFR
  		star_form_time = 0.0;
			#ifdef METALS
				#ifdef FEEDBACK	
					get_solar_abundances();
					get_SN_feedback_tables();
				#endif

				int i,j;
				//Initialize Metals Mass Fraction
				//Account for H and He. Ignore Li, Be, and B b/c of ultra-low abundanc
				MetalNormConst = SolarAbundances[0].NumRatioToH * SolarAbundances[0].AtomicMass
											+ SolarAbundances[1].NumRatioToH * SolarAbundances[1].AtomicMass;

				/* Two Loops needed b/c indices of MetalsPresent[] and SolarAbundances[] do not
    	 	directly map to each other.*/
				for(i=0; i<NSolarElements; i++)
  			{ 
  	  		//Search MetalsPresent
  	  		for(j=0; j<NELEMENTS; j++)
  	  		{ 
  	    		if(strcmp(SolarAbundances[i].Element, MetalsPresent[j].Element) == 0)
  	    		{ 
  	      		if(MetalsPresent[j].Present == 1)
  	      		{ 
  	        		MetalNormConst += INIT_METAL_TO_H_RATIO * 
																SolarAbundances[i].NumRatioToH 
																* SolarAbundances[i].AtomicMass;
  	      		}
  	    		}
  	  		}
  			}
			#endif
		#endif
	#endif



  allocate_commbuffers();	/* ... allocate buffer-memory for particle 
	                      			   exchange during force computation */
  set_units();


   // Initialize DE
   #ifdef DARKENERGY
      dark_init();
   #endif

	#ifdef COOLING
		#ifdef UVBACKGROUND
			init_phystime2redshift_table();
		#endif
	#endif


	#if defined(PERIODIC) && (!defined(PMGRID) || defined(FORCETEST))
  	ewald_init();
	#endif

  open_outputfiles();

  random_generator = gsl_rng_alloc(gsl_rng_ranlxd1);
  gsl_rng_set(random_generator, 42);	/* start-up seed */

	#ifdef PMGRID
	  long_range_init();
	#endif

  All.TimeLastRestartFile = CPUThisRun;

  if(RestartFlag == 0 || RestartFlag == 2)
  {
    set_random_numbers();

    init();			/* ... read in initial model */
  }
  else
  {
    all = All;		/* save global variables. (will be read from restart file) */

    restart(RestartFlag);	/* ... read restart file. Note: This also resets 
				                         all variables in the struct `All'. However, during the
                                 run, some variables in the parameter file are allowed 
                                 to be changed, if desired. These need to copied in the
                                 way below.
				                         Note:  All.PartAllocFactor is treated in restart() 
                                        separately.  
				                  */

		//NOTE: May need to adjust for FeedbackRadius.
    All.MinSizeTimestep = all.MinSizeTimestep;
    All.MaxSizeTimestep = all.MaxSizeTimestep;
    All.BufferSize = all.BufferSize;
    All.BunchSizeForce = all.BunchSizeForce;
    All.BunchSizeDensity = all.BunchSizeDensity;
    All.BunchSizeHydro = all.BunchSizeHydro;
    All.BunchSizeDomain = all.BunchSizeDomain;

    All.TimeLimitCPU = all.TimeLimitCPU;
    All.ResubmitOn = all.ResubmitOn;
    All.TimeBetSnapshot = all.TimeBetSnapshot;
    All.TimeBetStatistics = all.TimeBetStatistics;
    All.CpuTimeBetRestartFile = all.CpuTimeBetRestartFile;
    All.ErrTolIntAccuracy = all.ErrTolIntAccuracy;
    All.MaxRMSDisplacementFac = all.MaxRMSDisplacementFac;

    All.ErrTolForceAcc = all.ErrTolForceAcc;

    All.TypeOfTimestepCriterion = all.TypeOfTimestepCriterion;
    All.TypeOfOpeningCriterion = all.TypeOfOpeningCriterion;
    All.NumFilesWrittenInParallel = all.NumFilesWrittenInParallel;
    All.TreeDomainUpdateFrequency = all.TreeDomainUpdateFrequency;

    All.SnapFormat = all.SnapFormat;
    All.NumFilesPerSnapshot = all.NumFilesPerSnapshot;
    All.MaxNumNgbDeviation = all.MaxNumNgbDeviation;
    All.ArtBulkViscConst = all.ArtBulkViscConst;


    All.OutputListOn = all.OutputListOn;
    All.CourantFac = all.CourantFac;

    All.OutputListLength = all.OutputListLength;
    memcpy(All.OutputListTimes, all.OutputListTimes, 
           sizeof(double) * All.OutputListLength);


    strcpy(All.ResubmitCommand, all.ResubmitCommand);
    strcpy(All.OutputListFilename, all.OutputListFilename);
    strcpy(All.OutputDir, all.OutputDir);
    strcpy(All.RestartFile, all.RestartFile);
    strcpy(All.EnergyFile, all.EnergyFile);
    strcpy(All.InfoFile, all.InfoFile);
    strcpy(All.CpuFile, all.CpuFile);
    strcpy(All.TimingsFile, all.TimingsFile);
    strcpy(All.SnapshotFileBase, all.SnapshotFileBase);

    if(All.TimeMax != all.TimeMax)
    	readjust_timebase(All.TimeMax, all.TimeMax);
  }

	#ifdef PMGRID
  	long_range_init_regionsize();
	#endif

	//Initialize Drift Table.
  if(All.ComovingIntegrationOn)
    init_drift_table();

	//Get the time for the next SNAPSHOT file.
  if(RestartFlag == 2)
    All.Ti_nextoutput = find_next_outputtime(All.Ti_Current + 1);
  else
    All.Ti_nextoutput = find_next_outputtime(All.Ti_Current);


  All.TimeLastRestartFile = CPUThisRun;
}




/*! Computes conversion factors between internal code units and the
 *  cgs-system.
 */
void set_units(void)
{
  double meanweight;

  All.UnitTime_in_s = All.UnitLength_in_cm / All.UnitVelocity_in_cm_per_s;
  All.UnitTime_in_Megayears = All.UnitTime_in_s / SEC_PER_MEGAYEAR;

  if(All.GravityConstantInternal == 0)
    All.G = GRAVITY / pow(All.UnitLength_in_cm, 3) * All.UnitMass_in_g * pow(All.UnitTime_in_s, 2);
  else
    All.G = All.GravityConstantInternal;

  All.UnitDensity_in_cgs = All.UnitMass_in_g / pow(All.UnitLength_in_cm, 3);
  All.UnitPressure_in_cgs = All.UnitMass_in_g / All.UnitLength_in_cm / pow(All.UnitTime_in_s, 2);
  All.UnitCoolingRate_in_cgs = All.UnitPressure_in_cgs / All.UnitTime_in_s;
  All.UnitEnergy_in_cgs = All.UnitMass_in_g * pow(All.UnitLength_in_cm, 2) / pow(All.UnitTime_in_s, 2);

  /* convert some physical input parameters to internal units */

  All.Hubble = HUBBLE * All.UnitTime_in_s;

  if(ThisTask == 0)
  {
    printf("\nHubble (internal units) = %g\n", All.Hubble);
    printf("G (internal units) = %g\n", All.G);
    printf("UnitMass_in_g = %g \n", All.UnitMass_in_g);
    printf("UnitTime_in_s = %g \n", All.UnitTime_in_s);
    printf("UnitVelocity_in_cm_per_s = %g \n", All.UnitVelocity_in_cm_per_s);
    printf("UnitDensity_in_cgs = %g \n", All.UnitDensity_in_cgs);
    printf("UnitEnergy_in_cgs = %g \n", All.UnitEnergy_in_cgs);
    printf("\n");
  }

	/*Photo-ionized even at low temps. Self shielding ignored. See
		p1214 in Schaye & Dalla Vecchia 2008 */
	#ifndef UVBACKGROUND
 		if(All.InitGasTemp > 1.0e4)   /* assuming FULL ionization */
 		  meanweight = 4 / (8 - 5 * (1 - HYDROGEN_MASSFRAC));
		else                          /* assuming NEUTRAL GAS */
		  meanweight = 4 / (1 + 3 * HYDROGEN_MASSFRAC);
	#endif
	#ifdef UVBACKGROUND
 		meanweight = 4 / (8 - 5 * (1 - HYDROGEN_MASSFRAC));
	#endif		
	

 // meanweight = 4.0 / (1 + 3 * HYDROGEN_MASSFRAC);	/* note: we assume neutral gas here */

	#ifdef ISOTHERM_EQS
	  All.MinEgySpec = 0;
	#else
	  All.MinEgySpec = 1 / meanweight * (1.0 / GAMMA_MINUS1) * (BOLTZMANN / PROTONMASS) * All.MinGasTemp;
	  All.MinEgySpec *= All.UnitMass_in_g / All.UnitEnergy_in_cgs;
	#endif

}



/*!  This function opens various log-files that report on the status and
 *   performance of the simulstion. On restart from restart-files
 *   (start-option 1), the code will append to these files.
 */
void open_outputfiles(void)
{
  char mode[2], buf[200];

  if(ThisTask != 0)		/* only the root processor writes to the log files */
    return;

  if(RestartFlag == 0)
    strcpy(mode, "w");
  else
    strcpy(mode, "a");


  sprintf(buf, "%s%s", All.OutputDir, All.CpuFile);
  if(!(FdCPU = fopen(buf, mode)))
  {
    printf("error in opening file '%s'\n", buf);
    endrun(1);
  }

  sprintf(buf, "%s%s", All.OutputDir, All.InfoFile);
  if(!(FdInfo = fopen(buf, mode)))
  {
    printf("error in opening file '%s'\n", buf);
    endrun(1);
  }

  sprintf(buf, "%s%s", All.OutputDir, All.EnergyFile);
  if(!(FdEnergy = fopen(buf, mode)))
  {
    printf("error in opening file '%s'\n", buf);
    endrun(1);
  }

  sprintf(buf, "%s%s", All.OutputDir, All.TimingsFile);
  if(!(FdTimings = fopen(buf, mode)))
  {
    printf("error in opening file '%s'\n", buf);
    endrun(1);
  }

	#ifdef FORCETEST
	  if(RestartFlag == 0)
	  {
	    sprintf(buf, "%s%s", All.OutputDir, "forcetest.txt");
	    if(!(FdForceTest = fopen(buf, "w")))
	  	{
	  	  printf("error in opening file '%s'\n", buf);
	  	  endrun(1);
	  	}
	    fclose(FdForceTest);
	  }
	#endif
}


/*!  This function closes the global log-files.
 */
void close_outputfiles(void)
{
  if(ThisTask != 0)		/* only the root processor writes to the log files */
    return;

  fclose(FdCPU);
  fclose(FdInfo);
  fclose(FdEnergy);
  fclose(FdTimings);
	#ifdef FORCETEST
	  fclose(FdForceTest);
	#endif
}




/*! This function parses the parameterfile in a simple way.  Each paramater
 *  is defined by a keyword (`tag'), and can be either of type double, int,
 *  or character string.  The routine makes sure that each parameter
 *  appears exactly once in the parameterfile, otherwise error messages are
 *  produced that complain about the missing parameters.
 */
void read_parameter_file(char *fname)
{
	#define DOUBLE 1
	#define STRING 2
	#define INT 3
	#define MAXTAGS 300               //Maximum number of parameters

  FILE *fd, *fdout;                 //Parameter file and Parameter-used file resp.
  char buf[200];
	char buf1[200];										//Parameter field, same as tag[][], ex: MinGasTemp
	char buf2[200];
	char buf3[400];
  int i, j, nt;
  int id[MAXTAGS];                  //Specify parameter data-type(i.e. int, float etc.)
  void *addr[MAXTAGS];              //Address of elements of All.*
  char tag[MAXTAGS][50];						//Parameter field, ex: MinGasTemp
  int  errorFlag = 0;


  if(sizeof(long long) != 8)
  {
    if(ThisTask == 0)
	    printf("\nType `long long' is not 64 bit on this platform. Stopping.\n\n");
    endrun(0);
  }

  if(sizeof(int) != 4)
  {
    if(ThisTask == 0)
    	printf("\nType `int' is not 32 bit on this platform. Stopping.\n\n");
    endrun(0);
  }

  if(sizeof(float) != 4)
  {
    if(ThisTask == 0)
    	printf("\nType `float' is not 32 bit on this platform. Stopping.\n\n");
    endrun(0);
  }

  if(sizeof(double) != 8)
  {
    if(ThisTask == 0)
    	printf("\nType `double' is not 64 bit on this platform. Stopping.\n\n");
    endrun(0);
  }


  if(ThisTask == 0)		/* read parameter file on process 0 */
  {
    nt = 0;

    strcpy(tag[nt], "InitCondFile");
    addr[nt] = All.InitCondFile;
    id[nt++] = STRING;

    strcpy(tag[nt], "OutputDir");
    addr[nt] = All.OutputDir;
    id[nt++] = STRING;

    strcpy(tag[nt], "SnapshotFileBase");
    addr[nt] = All.SnapshotFileBase;
    id[nt++] = STRING;

    strcpy(tag[nt], "EnergyFile");
    addr[nt] = All.EnergyFile;
    id[nt++] = STRING;

    strcpy(tag[nt], "CpuFile");
    addr[nt] = All.CpuFile;
    id[nt++] = STRING;

    strcpy(tag[nt], "InfoFile");
    addr[nt] = All.InfoFile;
    id[nt++] = STRING;

    strcpy(tag[nt], "TimingsFile");
    addr[nt] = All.TimingsFile;
    id[nt++] = STRING;

    strcpy(tag[nt], "RestartFile");
    addr[nt] = All.RestartFile;
    id[nt++] = STRING;

    strcpy(tag[nt], "ResubmitCommand");
    addr[nt] = All.ResubmitCommand;
    id[nt++] = STRING;

    strcpy(tag[nt], "OutputListFilename");
    addr[nt] = All.OutputListFilename;
    id[nt++] = STRING;

    strcpy(tag[nt], "OutputListOn");
    addr[nt] = &All.OutputListOn;
    id[nt++] = INT;

    strcpy(tag[nt], "Omega0");
    addr[nt] = &All.Omega0;
    id[nt++] = DOUBLE;

    strcpy(tag[nt], "OmegaBaryon");
    addr[nt] = &All.OmegaBaryon;
    id[nt++] = DOUBLE;

    strcpy(tag[nt], "OmegaLambda");
    addr[nt] = &All.OmegaLambda;
    id[nt++] = DOUBLE;

    strcpy(tag[nt], "HubbleParam");
    addr[nt] = &All.HubbleParam;
    id[nt++] = DOUBLE;

    strcpy(tag[nt], "BoxSize");
    addr[nt] = &All.BoxSize;
    id[nt++] = DOUBLE;

    strcpy(tag[nt], "PeriodicBoundariesOn");
    addr[nt] = &All.PeriodicBoundariesOn;
    id[nt++] = INT;

    strcpy(tag[nt], "TimeOfFirstSnapshot");
    addr[nt] = &All.TimeOfFirstSnapshot;
    id[nt++] = DOUBLE;

    strcpy(tag[nt], "CpuTimeBetRestartFile");
    addr[nt] = &All.CpuTimeBetRestartFile;
    id[nt++] = DOUBLE;

    strcpy(tag[nt], "TimeBetStatistics");
    addr[nt] = &All.TimeBetStatistics;
    id[nt++] = DOUBLE;

    strcpy(tag[nt], "TimeBegin");
    addr[nt] = &All.TimeBegin;
    id[nt++] = DOUBLE;

    strcpy(tag[nt], "TimeMax");
    addr[nt] = &All.TimeMax;
    id[nt++] = DOUBLE;

    strcpy(tag[nt], "TimeBetSnapshot");
    addr[nt] = &All.TimeBetSnapshot;
    id[nt++] = DOUBLE;

    strcpy(tag[nt], "UnitVelocity_in_cm_per_s");
    addr[nt] = &All.UnitVelocity_in_cm_per_s;
    id[nt++] = DOUBLE;

    strcpy(tag[nt], "UnitLength_in_cm");
    addr[nt] = &All.UnitLength_in_cm;
    id[nt++] = DOUBLE;

    strcpy(tag[nt], "UnitMass_in_g");
    addr[nt] = &All.UnitMass_in_g;
    id[nt++] = DOUBLE;

    strcpy(tag[nt], "TreeDomainUpdateFrequency");
    addr[nt] = &All.TreeDomainUpdateFrequency;
    id[nt++] = DOUBLE;

    strcpy(tag[nt], "ErrTolIntAccuracy");
    addr[nt] = &All.ErrTolIntAccuracy;
    id[nt++] = DOUBLE;

    strcpy(tag[nt], "ErrTolTheta");
    addr[nt] = &All.ErrTolTheta;
    id[nt++] = DOUBLE;

    strcpy(tag[nt], "ErrTolForceAcc");
    addr[nt] = &All.ErrTolForceAcc;
    id[nt++] = DOUBLE;

    strcpy(tag[nt], "MinGasHsmlFractional");
    addr[nt] = &All.MinGasHsmlFractional;
    id[nt++] = DOUBLE;

    strcpy(tag[nt], "MaxSizeTimestep");
    addr[nt] = &All.MaxSizeTimestep;
    id[nt++] = DOUBLE;

    strcpy(tag[nt], "MinSizeTimestep");
    addr[nt] = &All.MinSizeTimestep;
    id[nt++] = DOUBLE;

    strcpy(tag[nt], "MaxRMSDisplacementFac");
    addr[nt] = &All.MaxRMSDisplacementFac;
    id[nt++] = DOUBLE;

    strcpy(tag[nt], "ArtBulkViscConst");
    addr[nt] = &All.ArtBulkViscConst;
    id[nt++] = DOUBLE;

    strcpy(tag[nt], "CourantFac");
    addr[nt] = &All.CourantFac;
    id[nt++] = DOUBLE;

    strcpy(tag[nt], "DesNumNgb");
    addr[nt] = &All.DesNumNgb;
    id[nt++] = DOUBLE;

    strcpy(tag[nt], "MaxNumNgbDeviation");
    addr[nt] = &All.MaxNumNgbDeviation;
    id[nt++] = DOUBLE;

    strcpy(tag[nt], "ComovingIntegrationOn");
    addr[nt] = &All.ComovingIntegrationOn;
    id[nt++] = INT;

    strcpy(tag[nt], "ICFormat");
    addr[nt] = &All.ICFormat;
    id[nt++] = INT;

    strcpy(tag[nt], "SnapFormat");
    addr[nt] = &All.SnapFormat;
    id[nt++] = INT;

    strcpy(tag[nt], "NumFilesPerSnapshot");
    addr[nt] = &All.NumFilesPerSnapshot;
    id[nt++] = INT;

    strcpy(tag[nt], "NumFilesWrittenInParallel");
    addr[nt] = &All.NumFilesWrittenInParallel;
    id[nt++] = INT;

    strcpy(tag[nt], "ResubmitOn");
    addr[nt] = &All.ResubmitOn;
    id[nt++] = INT;

    strcpy(tag[nt], "TypeOfTimestepCriterion");
    addr[nt] = &All.TypeOfTimestepCriterion;
    id[nt++] = INT;

    strcpy(tag[nt], "TypeOfOpeningCriterion");
    addr[nt] = &All.TypeOfOpeningCriterion;
    id[nt++] = INT;

    strcpy(tag[nt], "TimeLimitCPU");
    addr[nt] = &All.TimeLimitCPU;
    id[nt++] = DOUBLE;

    strcpy(tag[nt], "SofteningHalo");
    addr[nt] = &All.SofteningHalo;
    id[nt++] = DOUBLE;

    strcpy(tag[nt], "SofteningDisk");
    addr[nt] = &All.SofteningDisk;
    id[nt++] = DOUBLE;

    strcpy(tag[nt], "SofteningBulge");
    addr[nt] = &All.SofteningBulge;
    id[nt++] = DOUBLE;

    strcpy(tag[nt], "SofteningGas");
    addr[nt] = &All.SofteningGas;
    id[nt++] = DOUBLE;

    strcpy(tag[nt], "SofteningStars");
    addr[nt] = &All.SofteningStars;
    id[nt++] = DOUBLE;

    strcpy(tag[nt], "SofteningBndry");
    addr[nt] = &All.SofteningBndry;
    id[nt++] = DOUBLE;

    strcpy(tag[nt], "SofteningHaloMaxPhys");
    addr[nt] = &All.SofteningHaloMaxPhys;
    id[nt++] = DOUBLE;

    strcpy(tag[nt], "SofteningDiskMaxPhys");
    addr[nt] = &All.SofteningDiskMaxPhys;
    id[nt++] = DOUBLE;

    strcpy(tag[nt], "SofteningBulgeMaxPhys");
    addr[nt] = &All.SofteningBulgeMaxPhys;
    id[nt++] = DOUBLE;

    strcpy(tag[nt], "SofteningGasMaxPhys");
    addr[nt] = &All.SofteningGasMaxPhys;
    id[nt++] = DOUBLE;

    strcpy(tag[nt], "SofteningStarsMaxPhys");
    addr[nt] = &All.SofteningStarsMaxPhys;
    id[nt++] = DOUBLE;

    strcpy(tag[nt], "SofteningBndryMaxPhys");
    addr[nt] = &All.SofteningBndryMaxPhys;
    id[nt++] = DOUBLE;

    strcpy(tag[nt], "BufferSize");
    addr[nt] = &All.BufferSize;
    id[nt++] = INT;

    strcpy(tag[nt], "PartAllocFactor");
    addr[nt] = &All.PartAllocFactor;
    id[nt++] = DOUBLE;

    strcpy(tag[nt], "TreeAllocFactor");
    addr[nt] = &All.TreeAllocFactor;
    id[nt++] = DOUBLE;

    strcpy(tag[nt], "GravityConstantInternal");
    addr[nt] = &All.GravityConstantInternal;
    id[nt++] = DOUBLE;

    strcpy(tag[nt], "InitGasTemp");
    addr[nt] = &All.InitGasTemp;
    id[nt++] = DOUBLE;

    strcpy(tag[nt], "MinGasTemp");
    addr[nt] = &All.MinGasTemp;
    id[nt++] = DOUBLE;

		/* Below are some new parameters that I added. Keep in mind you cannot 
			 bracket them in #ifdef's b/c they must be ALWAYS read from the parameter 
			 file. This is why their corresponding elements in All.* aren't bracketed 
			 either. */
		strcpy(tag[nt], "FeedbackRadius");						//ALI added, might not be used
	 	addr[nt] = &All.FeedbackRadius;
   	id[nt++] = DOUBLE;

		strcpy(tag[nt], "ScaleFactorEnd");						//ALI added, might not be used
	 	addr[nt] = &All.ScaleFactorEnd;
   	id[nt++] = DOUBLE;

		strcpy(tag[nt], "StarDesNumNgb");						//ALI added, might not be used
	 	addr[nt] = &All.StarDesNumNgb;
   	id[nt++] = DOUBLE;

		strcpy(tag[nt], "StarMaxNumNgbDeviation");		//ALI added, might not be used
	 	addr[nt] = &All.StarMaxNumNgbDeviation;
   	id[nt++] = DOUBLE;

		strcpy(tag[nt], "MinStarHsmlFractional");		//ALI added, might not be used
	 	addr[nt] = &All.MinStarHsmlFractional;
   	id[nt++] = DOUBLE;

      /***************** 
        DE PARAMETERS: Jared added
      *****************/
      strcpy(tag[nt], "DarkFile");
      addr[nt] = All.dark_file;
      id[nt++] = STRING;

      strcpy(tag[nt], "DPW0");
      addr[nt] = &All.dp_w0;
      id[nt++] = DOUBLE;

      strcpy(tag[nt], "DPWA");
      addr[nt] = &All.dp_wa;
      id[nt++] = DOUBLE;

      strcpy(tag[nt], "GCG_Alpha");
      addr[nt] = &All.GCG_Alpha;
      id[nt++] = DOUBLE;

      strcpy(tag[nt], "GCG_A");
      addr[nt] = &All.GCG_A;
      id[nt++] = DOUBLE;

      strcpy(tag[nt], "GCG_B");
      addr[nt] = &All.GCG_B;
      id[nt++] = DOUBLE;

      // Jared added: gadget_src_prefix. This option holds the path to the gadget
      // src directory. The reason for this is that a lot of the required files for
      // feedback and cooling were read in simply as "./src/file", which is annoying
      // if the simulation directory structure is different. This just allows more
      // flexibility.
      strcpy(tag[nt], "gadget_src_prefix");
      addr[nt] = &All.gadget_src_prefix;
      id[nt++] = STRING;

		//Read parameter file.
    if((fd = fopen(fname, "r")))
  	{
 	    sprintf(buf, "%s%s", fname, "-usedvalues");
			
			//Write parameter usedvalues file
	    if(!(fdout = fopen(buf, "w")))
	    {
	      printf("error opening file '%s' \n", buf);
	      errorFlag = 1;
	    }
	    else
	    {
	      while(!feof(fd))
		    {
		      *buf = 0;
		      fgets(buf, 200, fd);
		      if(sscanf(buf, "%s%s%s", buf1, buf2, buf3) < 2)  //Skip lines w/o values
		        continue;

		      if(buf1[0] == '%')                //Skip comments
		        continue;

		      for(i = 0, j = -1; i < nt; i++)
		        if(strcmp(buf1, tag[i]) == 0)   //Find appropriate tag[i] matching buf1
		        {
		        	j = i;
			        tag[i][0] = 0;
			        break;
		        }

          //Write parameter values to the All.* struct and param-usedvalues file.
		      if(j >= 0)
		      {
		        switch (id[j])
			      {
			        case DOUBLE:
			          *((double *) addr[j]) = atof(buf2);    //Write to All.*
			          fprintf(fdout, "%-35s%g\n", buf1, *((double *) addr[j]));
			          break;
			        case STRING:
			          strcpy(addr[j], buf2);
			          fprintf(fdout, "%-35s%s\n", buf1, buf2);
			          break;
			        case INT:
			          *((int *) addr[j]) = atoi(buf2);       //Write to All.*
			          fprintf(fdout, "%-35s%d\n", buf1, *((int *) addr[j]));
			          break;
			      }
		      }
		      else
		      {
		        fprintf(stdout, 
                   "Error in file %s:   Tag '%s' not allowed or multiple defined.\n",
			              fname, buf1);
		        errorFlag = 1;
		      }
		    }
	      fclose(fd);
	      fclose(fdout);

	      i = strlen(All.OutputDir);
	      if(i > 0)
		      if(All.OutputDir[i - 1] != '/')
		        strcat(All.OutputDir, "/");

	      sprintf(buf1, "%s%s", fname, "-usedvalues");
	      sprintf(buf2, "%s%s", All.OutputDir, "parameters-usedvalues");
	      sprintf(buf3, "cp %s %s", buf1, buf2);
	      system(buf3);
	    }
	  }
    else
	  {
	    printf("\nParameter file %s not found.\n\n", fname);
	    errorFlag = 2;
	  }

    if(errorFlag != 2)
	    for(i = 0; i < nt; i++)
	    {
	      if(*tag[i])
	      {
	      	printf("Error. I miss a value for tag '%s' in parameter file '%s'.\n", 
                 tag[i], fname);
		      errorFlag = 1;
	      }
	    }

    if(All.OutputListOn && errorFlag == 0)
	    errorFlag += read_outputlist(All.OutputListFilename);
    else
	    All.OutputListLength = 0;
  }


  MPI_Bcast(&errorFlag, 1, MPI_INT, 0, MPI_COMM_WORLD);

  if(errorFlag)
  {
    MPI_Finalize();
    exit(0);
  }

  /* now communicate the relevant parameters to the other processes */
  MPI_Bcast(&All, sizeof(struct global_data_all_processes), MPI_BYTE, 0, 
            MPI_COMM_WORLD);


  if(All.NumFilesWrittenInParallel < 1)
  {
    if(ThisTask == 0)
    	printf("NumFilesWrittenInParallel MUST be at least 1\n");
    endrun(0);
  }

  if(All.NumFilesWrittenInParallel > NTask)
  {
    if(ThisTask == 0)
    	printf("NumFilesWrittenInParallel MUST be smaller than number of processors\n");
    endrun(0);
  }

	#ifdef PERIODIC
	  if(All.PeriodicBoundariesOn == 0)
	  {
	    if(ThisTask == 0)
	  	{
	  	  printf("Code was compiled with periodic boundary conditions switched on.\n");
	  	  printf("You must set `PeriodicBoundariesOn=1', or recompile the code.\n");
	  	}
	    endrun(0);
	  }
	#else
	  if(All.PeriodicBoundariesOn == 1)
	  {
	    if(ThisTask == 0)
		  {
		    printf("Code was compiled with periodic boundary conditions switched off.\n");
		    printf("You must set `PeriodicBoundariesOn=0', or recompile the code.\n");
		  }
	    endrun(0);
	  }
	#endif


  if(All.TypeOfTimestepCriterion >= 1)
  {
    if(ThisTask == 0)
	  {
	    printf("The specified timestep criterion\n");
	    printf("is not valid\n");
	  }
    endrun(0);
  }

	#if defined(LONG_X) ||  defined(LONG_Y) || defined(LONG_Z)
		#ifndef NOGRAVITY
  		if(ThisTask == 0)
  		{
    		printf("Code was compiled with LONG_X/Y/Z, but not with NOGRAVITY.\n");
    		printf("Stretched periodic boxes are not implemented for gravity yet.\n");
  		}
  		endrun(0);
		#endif
	#endif

	#undef DOUBLE
	#undef STRING
	#undef INT
	#undef MAXTAGS
}


/*! this function reads a table with a list of desired output times. The
 *  table does not have to be ordered in any way, but may not contain more
 *  than MAXLEN_OUTPUTLIST entries.
 */
int read_outputlist(char *fname)
{
  FILE *fd;

  if(!(fd = fopen(fname, "r")))
  {
    printf("can't read output list in file '%s'\n", fname);
    return 1;
  }

  All.OutputListLength = 0;
  do
  {
    if(fscanf(fd, " %lg ", &All.OutputListTimes[All.OutputListLength]) == 1)
	    All.OutputListLength++;
    else
	    break;
  }
  while(All.OutputListLength < MAXLEN_OUTPUTLIST);

  fclose(fd);

  printf("\nfound %d times in output-list.\n", All.OutputListLength);

  return 0;
}


/*! If a restart from restart-files is carried out where the TimeMax
 *  variable is increased, then the integer timeline needs to be
 *  adjusted. The approach taken here is to reduce the resolution of the
 *  integer timeline by factors of 2 until the new final time can be
 *  reached within TIMEBASE.
 */
void readjust_timebase(double TimeMax_old, double TimeMax_new)
{
  int i;
  long long ti_end;

  if(ThisTask == 0)
  {
    printf("\nAll.TimeMax has been changed in the parameterfile\n");
    printf("Need to adjust integer timeline\n\n\n");
  }

  if(TimeMax_new < TimeMax_old)
  {
    if(ThisTask == 0)
	    printf("\nIt is not allowed to reduce All.TimeMax\n\n");
    endrun(556);
  }

  if(All.ComovingIntegrationOn)
    ti_end = log(TimeMax_new / All.TimeBegin) / All.Timebase_interval;
  else
    ti_end = (TimeMax_new - All.TimeBegin) / All.Timebase_interval;

  while(ti_end > TIMEBASE)
  {
    All.Timebase_interval *= 2.0;

    ti_end /= 2;
    All.Ti_Current /= 2;

		#ifdef PMGRID
    	All.PM_Ti_begstep /= 2;
    	All.PM_Ti_endstep /= 2;
		#endif

    for(i = 0; i < NumPart; i++)
  	{
  	  P[i].Ti_begstep /= 2;
  	  P[i].Ti_endstep /= 2;
  	}
  }

  All.TimeMax = TimeMax_new;
}


#ifdef COOLING
#ifdef SFR
#ifdef METALS
#ifdef FEEDBACK
/************************************************************************************
	This get's the solar abundances from Solar_Abundances.txt
************************************************************************************/
void get_solar_abundances(void)
{
	int i;
	int linelen = 1000;
	int nElements = 0;
	char line[linelen];
	char * p = NULL;
    char temp_name[MAXLEN_FILENAME];
    sprintf(temp_name, "%s/src/Solar_Abundances.txt", All.gadget_src_prefix);
	FILE * f = fopen(temp_name,"r");

	//Error Check
	if(f == NULL)
	{
		fprintf(stderr, "ERROR!!! Solar_Abundances.txt Not Found!\n");
		exit(671);
	}
	
	//Get number of lines in file.  
	while( fgets(line, linelen, f) != NULL)
	{
		if(feof(f) == 0)
		{
			if(line[0] != '#' && line[0] != '/')
			{
				nElements++;
			}
		}
	}

	SolarAbundances = (struct solar_abundances *)malloc(sizeof(struct solar_abundances)
																											 * nElements);
	i = 0;

	//Read data in file
	rewind(f);
	while( fgets(line, linelen, f) != NULL)
	{
		if(feof(f) == 0)
		{
			if(line[0] != '#' && line[0] != '/' && i < nElements)
			{
				p = strtok(line, " \n\t");				//Get element
				strcpy(SolarAbundances[i].Element, p);
				p = strtok(NULL, " \n\t");
				sscanf(p,"%f", &SolarAbundances[i].AtomicMass);	
				p = strtok(NULL, " \n\t");
				sscanf(p,"%f", &SolarAbundances[i].NumRatioToH);

				i++;
			}
		}
	}

	NSolarElements = nElements;
	fclose(f);
}







/*************************************************************************************
	Get the SN yields.  File should be formatted in 4 columns, Species, Type II, 
	Type Ia(W7), Type Ia(W70). Mathews says to only use Type Ia (W7) and Type II.
**************************************************************************************/
void get_SN_feedback_tables(void)
{
	int i,j;
	int linelen = 1000;
	int nElements = 0;
	char line[linelen];
	char * p = NULL;
    char temp_nameIa[MAXLEN_FILENAME];
    char temp_nameII[MAXLEN_FILENAME];
    sprintf(temp_nameIa, "%s/src/SN_TypeIa_yields.dat", All.gadget_src_prefix);
    sprintf(temp_nameII, "%s/src/SN_TypeII_yields.dat", All.gadget_src_prefix);
	FILE * f_Ia = fopen(temp_nameIa,"r");
	FILE * f_II = fopen(temp_nameII,"r");

	//Error Check
	if(f_Ia == NULL || f_II == NULL)
	{
		fprintf(stderr, "ERROR!!! SN_yields.dat Not Found!\n");
		exit(668);
	}
	
	//Get number of lines in Type Ia yields file.  
	while( fgets(line, linelen, f_Ia) != NULL)
	{
		if(feof(f_Ia) == 0)
		{
			//Get number of elements
			if(line[0] != '#' && line[0] != '/')
			{
				nElements++;
			}
		}
	}

	NTypeIaElements = nElements;
	Type_Ia_SN = (struct sn_yield *)malloc(sizeof(struct sn_yield) * nElements);
	i = 0;

	//Read data in Type Ia yields file
	rewind(f_Ia);
	while( fgets(line, linelen, f_Ia) != NULL)
	{
		if(feof(f_Ia) == 0)
		{
			if(line[0] != '#' && line[0] != '/' && i < nElements)
			{
				p = strtok(line, " \n\t");				//Get element
				strcpy(Type_Ia_SN[i].Element, p);
				p = strtok(NULL, " \n\t");
				sscanf(p,"%f", &Type_Ia_SN[i].Yield[0]);	//Get yield

				//Set other Yields to null values
				Type_Ia_SN[i].Yield[1] = -999.9;
				Type_Ia_SN[i].Yield[2] = -999.9;
				Type_Ia_SN[i].Yield[3] = -999.9;
				Type_Ia_SN[i].Yield[4] = -999.9;
				Type_Ia_SN[i].Yield[5] = -999.9;
				Type_Ia_SN[i].Yield[6] = -999.9;

				i++;
			}
		}
	}

	

	nElements = 0;

	//Get number of lines in Type II yields file.  
	while( fgets(line, linelen, f_II) != NULL)
	{
		if(feof(f_II) == 0)
		{
			//Get number of elements
			if(line[0] != '#' && line[0] != '/')
			{
				nElements++;
			}
		}
	}

	NTypeIIElements = nElements;
	Type_II_SN = (struct sn_yield *)malloc(sizeof(struct sn_yield) * nElements);
	i = 0;


	//Read data in Type II yields file
	rewind(f_II);
	while( fgets(line, linelen, f_II) != NULL)
	{
		if(feof(f_II) == 0)
		{
			if(line[0] != '#' && line[0] != '/' && i < nElements)
			{
				p = strtok(line, " \n\t");				//Get element
				strcpy(Type_II_SN[i].Element, p);
				p = strtok(NULL, " \n\t");
				sscanf(p,"%f", &Type_II_SN[i].Yield[0]);	//Get 13 M_s yield
				p = strtok(NULL, " \n\t");
				sscanf(p,"%f", &Type_II_SN[i].Yield[1]);	//Get 15 M_s yield
				p = strtok(NULL, " \n\t");
				sscanf(p,"%f", &Type_II_SN[i].Yield[2]);	//Get 18 M_s yield
				p = strtok(NULL, " \n\t");
				sscanf(p,"%f", &Type_II_SN[i].Yield[3]);	//Get 20 M_s yield
				p = strtok(NULL, " \n\t");
				sscanf(p,"%f", &Type_II_SN[i].Yield[4]);	//Get 25 M_s yield
				p = strtok(NULL, " \n\t");
				sscanf(p,"%f", &Type_II_SN[i].Yield[5]);	//Get 30 M_s yield
				p = strtok(NULL, " \n\t");
				sscanf(p,"%f", &Type_II_SN[i].Yield[6]);	//Get 40 M_s yield
					
				i++;
			}
		}
	}

	//Check the tables for Type_II and Type_Ia are same length as NElements (= 25)
	if(NTypeIIElements != NTypeIaElements)
	{
		fprintf(stderr, "ERROR!! SN yield tables are different lengths!\n");
		exit(673);
	}
	if(NTypeIIElements != NELEMENTS || NTypeIaElements != NELEMENTS)
	{	
		fprintf(stderr, "ERROR!! You may need to adjust the length of"
						" dt_Z_Mass[] in the struct feedbackdata_in, \nespecially if"
						"NTypeIIElements > 25.  NTypeIIElements = %i\n", NTypeIIElements);
		exit(674);
	}

	//NElements = NTypeIIElements;

	WhiteDwarfMass = 0.0;
	//Get mass of White Dwarf Type Ia progenitor.
	for(i=0; i<NTypeIaElements; i++)
	{
		WhiteDwarfMass += Type_Ia_SN[i].Yield[0];
	}



	//List the metals present
	MetalsPresent = (struct metals_present *)malloc(sizeof(struct metals_present) * NELEMENTS);
	for(i=0; i<NELEMENTS; i++)												//Copy element yield
	{
		strcpy(MetalsPresent[i].Element, Type_II_SN[i].Element);
		MetalsPresent[i].Present = 0;

		if(strcmp(MetalsPresent[i].Element, "C") == 0){
			#ifdef CARBON
				MetalsPresent[i].Present = 1;
			#endif
		}
		if(strcmp(MetalsPresent[i].Element, "N") == 0){
			#ifdef NITROGEN
				MetalsPresent[i].Present = 1;
			#endif
		}
		if(strcmp(MetalsPresent[i].Element, "O") == 0){
			#ifdef OXYGEN
				MetalsPresent[i].Present = 1;
			#endif
		}
		if(strcmp(MetalsPresent[i].Element, "F") == 0){
			#ifdef FLORINE
				MetalsPresent[i].Present = 1;
			#endif
		}
		if(strcmp(MetalsPresent[i].Element, "Ne") == 0){
			#ifdef NEON 
				MetalsPresent[i].Present = 1;
			#endif
		}
		if(strcmp(MetalsPresent[i].Element, "Na") == 0){
			#ifdef SODIUM
				MetalsPresent[i].Present = 1;
			#endif
		}
		if(strcmp(MetalsPresent[i].Element, "Mg") == 0){
			#ifdef MAGNESIUM
				MetalsPresent[i].Present = 1;
			#endif
		}
		if(strcmp(MetalsPresent[i].Element, "Al") == 0){
			#ifdef ALUMINUM
				MetalsPresent[i].Present = 1;
			#endif
		}
		if(strcmp(MetalsPresent[i].Element, "Si") == 0){
			#ifdef SILICON
				MetalsPresent[i].Present = 1;
			#endif
		}
		if(strcmp(MetalsPresent[i].Element, "P") == 0){
			#ifdef PHOSPHORUS
				MetalsPresent[i].Present = 1;
			#endif
		}
		if(strcmp(MetalsPresent[i].Element, "S") == 0){
			#ifdef SULFUR
				MetalsPresent[i].Present = 1;
			#endif
		}
		if(strcmp(MetalsPresent[i].Element, "Cl") == 0){
			#ifdef CHLORINE
				MetalsPresent[i].Present = 1;
			#endif
		}
		if(strcmp(MetalsPresent[i].Element, "Ar") == 0){
			#ifdef ARGON
				MetalsPresent[i].Present = 1;
			#endif
		}
		if(strcmp(MetalsPresent[i].Element, "K") == 0){
			#ifdef POTASSIUM
				MetalsPresent[i].Present = 1;
			#endif
		}
		if(strcmp(MetalsPresent[i].Element, "Ca") == 0){
			#ifdef CALCIUM
				MetalsPresent[i].Present = 1;
			#endif
		}
		if(strcmp(MetalsPresent[i].Element, "Sc") == 0){
			#ifdef SCANDIUM
				MetalsPresent[i].Present = 1;
			#endif
		}
		if(strcmp(MetalsPresent[i].Element, "Ti") == 0){
			#ifdef TITANIUM
				MetalsPresent[i].Present = 1;
			#endif
		}
		if(strcmp(MetalsPresent[i].Element, "V") == 0){
			#ifdef VANADIUM
				MetalsPresent[i].Present = 1;
			#endif
		}
		if(strcmp(MetalsPresent[i].Element, "Cr") == 0){
			#ifdef CHROMIUM
				MetalsPresent[i].Present = 1;
			#endif
		}
		if(strcmp(MetalsPresent[i].Element, "Mn") == 0){
			#ifdef MANGANESE
				MetalsPresent[i].Present = 1;
			#endif
		}
		if(strcmp(MetalsPresent[i].Element, "Fe") == 0){
			#ifdef IRON
				MetalsPresent[i].Present = 1;
			#endif
		}
		if(strcmp(MetalsPresent[i].Element, "Co") == 0){
			#ifdef COBALT
				MetalsPresent[i].Present = 1;
			#endif
		}
		if(strcmp(MetalsPresent[i].Element, "Ni") == 0){
			#ifdef NICKEL
				MetalsPresent[i].Present = 1;
			#endif
		}
		if(strcmp(MetalsPresent[i].Element, "Cu") == 0){
			#ifdef COPPER
				MetalsPresent[i].Present = 1;
			#endif
		}
		if(strcmp(MetalsPresent[i].Element, "Zn") == 0){
			#ifdef COPPER
				MetalsPresent[i].Present = 1;
			#endif
		}
	}

	
	SolarMetalNormConst = 0;
	SolarMetalMassFrac = 0;

	//Account for H and He. Ignore Li, Be, and B b/c of ultra-low abundances.
	SolarMetalNormConst = SolarAbundances[0].NumRatioToH * SolarAbundances[0].AtomicMass
 											+ SolarAbundances[1].NumRatioToH * SolarAbundances[1].AtomicMass;

	/*
		Get appropriate "Solar Metal Mass Fraction" based on the metals present. This is 
		necessary b/c if we don't track ALL the elements (see Makefile). So the nominal 
		Solar Metal Mass Frac (~0.013765) would be too high. We need to adjust the 
		SolarMetalMassFrac so it is the sum of the metals (at solar abundances) of 
		only the ones we are following.

		Two Loops needed b/c indices of MetalsPresent[] and SolarAbundances[] do not
		directly map to each other.
	*/
	for(i=0; i<NSolarElements; i++)
	{
		//totalElementMass += SolarAbundances[i].NumRatioToH * SolarAbundances[i].AtomicMass;
		//Search MetalsPresent
		for(j=0; j<NELEMENTS; j++)
		{
			if(strcmp(SolarAbundances[i].Element, MetalsPresent[j].Element) == 0)
			{
				if(MetalsPresent[j].Present == 1)
				{
					SolarMetalNormConst += SolarAbundances[i].NumRatioToH * 
																	SolarAbundances[i].AtomicMass;
					SolarMetalMassFrac += SolarAbundances[i].NumRatioToH * 
																SolarAbundances[i].AtomicMass;
				}
			}
		}
	}

	SolarMetalMassFrac = SolarMetalMassFrac / SolarMetalNormConst;

	fclose(f_Ia);
	fclose(f_II);
}
#endif
#endif
#endif
#endif




#ifdef COOLING
/*
	Here we use a heating / cooling table generated by CLOUDY v10.00. It is sorted by 
	log10 metallicity with respect to solar. So the entry with metallicity = -3, is 
	really 1/1000 Z_solar.

	Also note that these cooling rates are in units of Lambda_norm (ergs * cm^3 / s).
	Because I calculated the cooling rates at n_H = 1 (cm^-3), 
			Lamda_net (ergs / (cm^3 * s)) / (n_H)^2 = Lambda_norm (ergs * cm^3 / s)

	Here is the Cloudy script used to generate the below table.
								CMB redshift 0
								metals -3.0 log
								stop zone 1
								hden 0
								print last
								set trimming -10
								iterate to convergence
								constant temperature, t=10000.0 K [linear]
								coronal equilibrium, t=10000.0 K [linear]
								no photoionization
								save cooling "metal-3.0/cc400.col"
								save continuum file = "metal-3.0/input_spectrum.con"


	NOTE:	This is the Collisionally Ionized Equilibrium cooling table. It gives
	the heating and cooling for each metallicity from -3.0 -> 1.5. See file header
	for details. For similar cooling table see...
	~/Research/yt-i386/src/enzo-hg-stable/input/zcool_sd93.dat

	NOTE: Metal_Cooling_Rate_wo_UV.txt, has format of temp then alternating
	heat_*Z cool_*Z.  So nColumns - 1 should be an even number. Also we 
	read the last line of the header to get the metallicities.
*/
void get_CIE_cooling_table(void)
{
	int i,j;													//Indices
	int lineLen = 1000;									//Length of line read from file
	char line[lineLen];									//Line read from file
	char * p = NULL;										//Use to parse data lines
	float min_metal = 1e10;
	float max_metal = -1e10;
	float cur_metal = 1e10;							//Current metal value in table
	float prev_metal = 1e10;						//Previous metal value in table
	FILE * f = NULL;
    char temp_name[MAXLEN_FILENAME];

	nRows    = 0;
	nColumns = 0;	
	nTempsCIE= 0;												//Rows in table
	nMetalsCIE  = 0;												//Columns in table
    sprintf(temp_name, "%s/src/Metal_Cooling_Rate_wo_UV.txt", All.gadget_src_prefix);
	f = fopen(temp_name,"r");

	
	//Error Check
	if(f == NULL)
	{
		fprintf(stderr, "ERROR!!    Cooling File Not Found!\n\n");
		exit(667);
	}


	//Get number of Columns and Rows in file.
	while( fgets(line, lineLen, f) != NULL)
	{
		if(feof(f) == 0)
		{
			if(line[0] != '#' && line[0] != '/')
			{
				// Get # of columns (nMetals), only need to do once
				if(nColumns == 0)
				{
					p = strtok(line, " ,");
					while( p != NULL)
					{
						p = strtok(NULL, " ,");
						if( p != NULL)
						{
							nColumns++;		
						}
					}
				}
				nRows++;							
			}
		}
	}


	/*	Get cooling/heating table dimensions. Recall that we are '0' indexing, so if 
			there are 39 columns (including temp), then nColumns == 38.*/
	if( (nColumns)%2 != 0){
		fprintf(stderr, "ERROR!! Incorrect Number of heating and cooling lines\n");
		fprintf(stderr, "nColumns: %i    nRows: %i\n",nColumns,nRows);
		exit(668);
	}else{
		nMetalsCIE 	= (nColumns)/2;		//Should be even number..	
		nTempsCIE = nRows;					//Delete the extra line
	}


	//Allocate Tables 
	metallicity_CIE_array  = (float *)malloc(sizeof(float) * nMetalsCIE);	
	temp_CIE_array    = (float *)malloc(sizeof(float) * nTempsCIE);	
	CIE_cooling_table = (float **)malloc(sizeof(float *) * nMetalsCIE);
	CIE_heating_table = (float **)malloc(sizeof(float *) * nMetalsCIE);

	if(	metallicity_CIE_array == NULL || temp_CIE_array == NULL ||  		 //Error Check
			CIE_cooling_table == NULL || CIE_heating_table == NULL)
	{
		fprintf(stderr, "Memory allocation error in begrun.c\n");
		exit(669);
	}

	for(i=0; i<nMetalsCIE; i++)
	{
		CIE_cooling_table[i] = (float *)malloc(sizeof(float) * nTempsCIE);
		CIE_heating_table[i] = (float *)malloc(sizeof(float) * nTempsCIE);
		
		if(CIE_cooling_table[i] == NULL || CIE_heating_table[i] == NULL){  //Error Check
			fprintf(stderr, "Memory allocation error in begrun.c\n");
			exit(669);
		}
	}



	rewind(f);
	i = 0;
	j = 0;



	//Get metallicity values. See last line of header
	while( fgets(line, lineLen, f) != NULL)
	{
		if(feof(f) == 0)
		{
			p = strtok(line, "# ,ch_Z");
			if( strcmp(p, "temp") == 0 )
			{
				p = strtok(NULL, " ,ch_Z");				//skip "temp"
				
				while(p != NULL && i < nColumns)
				{
					sscanf(p, "%f", &cur_metal);
					p = strtok(NULL, " ,ch_Z");				//skip "temp"
				
					//There are heating AND cooling rates per 1 metalicity, skip repeats
					if(cur_metal != prev_metal)
					{
						min_metal = min_metal > cur_metal ? cur_metal : min_metal;
						max_metal = max_metal < cur_metal ? cur_metal : max_metal;

						metallicity_CIE_array[i] = cur_metal;
						i++;
					}
					prev_metal = cur_metal;
					//p = strtok(NULL, "# ,ch_Z");
				}
				
				break;														//Got metals. Skip rest of file.
			}
		}
	} 

	rewind(f);
	i = 0;
	j = 0;

	//Now actually read file. Skip ALL header lines. Write values to tables
	while( fgets(line, lineLen, f) != NULL)
	{
		if(feof(f) == 0 && line[0] != '#')
		{
			p = strtok(line, " ");
			i=0;

			sscanf(p, "%f", &temp_CIE_array[j]); 	//First element is always temperature.
			p = strtok(NULL, " \t");

			//Read & Write both cooling and heating at the same time.
			while( p != NULL && i < nColumns)
			{
				sscanf(p, "%f", &CIE_heating_table[i][j]);
				p = strtok(NULL, " \t");
				sscanf(p, "%f", &CIE_cooling_table[i][j]);
				p = strtok(NULL, " \t");
				i++;
			}

			j++;
		}
	}

	printf("Cooling Table Read\n");
	printf("Heating & Cooling table dimensions %i x %i\n", nMetalsCIE, nTempsCIE);

	fflush(stdout);
	fclose(f);
}


#ifdef UVBACKGROUND
/********************************************************************************
Here is a sample Cloudy (C10)script used to generate 
			CMB redshift 0.00
      table HM05 z=0.00
      print last
      hden -1.00
      metals 0.0 log
      stop zone 1
      set trimming -10
      constant temperature, t=10000.0 K [linear]
      iterate to convergence
      save cooling "z_0.00/hden_-1.00/metal_0.00/cc400.col"
      save continuum file = "z_0.00/hden_-1.00/metal_0.00/input_spectrum.con"

Note the cooling rates are in units are in Lambda_norm = (ergs * cm^3 / s). I
converted these from in my python script that I used to run CLOUDY.

WARNING!!
I may have to hard wire (YUCK!) the values to read in this table b/c I don't feel 
like taking the time to make it generallizable.

WARNING!!
I generated the low temperature (10K < T < 10^4K) heat/cooling tables separately 
from the regular heat/cooling tables (10^4K < T < 10^8.5) because one may want to 
include low temperature physics (i.e. molecular cooling etc) separately from 
the regular effects. This is ugly, but allows you to easily try new low temperature
physics without having to rerun the whole damn temperature range (which would take
over a week using my python scripts!).

*********************************************************************************/
void get_UV_cooling_table(void)
{	
	int i;															//Redshift Index
	int j;															//Density  Index
	int k;															//Temperature Index
	int l;															//Metal    Index
	int lineLen = 1000;									//Length of line read from file
	int nz, nh, nt, nm;									//Number of redshift, hden, metals, temp
	char line[lineLen];									//Line read from file
	char * p = NULL;										//Use to parse data lines
	char filePath[256];
	char lowTempFilePath[256];
	float z,h;
	
	//Unfortunately, I have hard coded this. This should match the UV cooling files used.
	float z_min = 0.00;									//Redshift min
	float z_max = 8.50;									//Redshift max
	float dz		= 0.50;									//Redshift step
	float h_min = -10.0;								//Hdensity min
	float h_max = 6.00;									//Hdensity max
	float dh		= 0.50;									//Hdensity step
	float t_min	= 1.00;									//log10(temp) min
	float t_max = 8.50;									//log10(temp) max
	float dt		= 0.05;									//temperature step
	float m_min = -3.00;								//Metallicity min
	float m_max = 1.50;									//Metallicity max
	float dm 		= 0.25;									//Metal step

	float maxLowTemp = 3.95;						//After reading low temp files, T = maxLowTemp

	FILE * f = NULL;										//File for T > 10^4
	FILE * f2 = NULL;										//File for T < 10^4

	//Add 0.001 to ensure these numbers are slightly greater than desired int
	nz = (int)((z_max - z_min) / dz + 1.001);
	nh = (int)((h_max - h_min) / dh + 1.001);
	nt = (int)((t_max - t_min) / dt + 1.001);
	nm = (int)((m_max - m_min) / dm + 1.001);

	//Set Global Variables	
	nTempsUV 	 = nt;											
	nMetalsUV  = nm;												
	nHden 	 	 = nh;
	nRedshifts = nz;
	tempIndexOffset = nTempsUV - nRows;  	//Diff. 'tween num. of temps in CIE and UV



	//Allocate Memory
	UV_cooling_table = (float ****)malloc(sizeof(float ***) * nz);
	UV_heating_table = (float ****)malloc(sizeof(float ***) * nz);
	for(i = 0; i < nz; i++)
	{
		UV_cooling_table[i] = (float ***)malloc(sizeof(float **) * nh);
		UV_heating_table[i] = (float ***)malloc(sizeof(float **) * nh);
	}
	for(i = 0; i < nz; i++)
	{
		for(j = 0; j < nh; j++)
		{
			UV_cooling_table[i][j] = (float **)malloc(sizeof(float *) * nt);
			UV_heating_table[i][j] = (float **)malloc(sizeof(float *) * nt);
		}
	}
	for(i = 0; i < nz; i++)
	{
		for(j = 0; j < nh; j++)
		{
			for(k = 0; k < nt; k++)
			{
				UV_cooling_table[i][j][k] = (float *)malloc(sizeof(float) * nm);
				UV_heating_table[i][j][k] = (float *)malloc(sizeof(float) * nm);
			}
		}
	}


	redshift_array 				= (float *)malloc(sizeof(float) * nz);
	hden_array  					= (float *)malloc(sizeof(float) * nh);
	metallicity_UV_array 	= (float *)malloc(sizeof(float) * nm);
	temp_UV_array 				= (float *)malloc(sizeof(float) * nt);


	//Read files.
	for(i = 0; i < nz; i++)
	{
		z = z_min + i * dz;	

		for(j = 0; j < nh; j++)
		{
			h = h_min + j * dh;
			sprintf(filePath,"%s/src/cooling_w_UV/z_%.2f/hden_%.2f/Metal_Cooling_Rate.txt", 
							All.gadget_src_prefix, z, h);
			sprintf(lowTempFilePath,
							"%s/src/cooling_w_UV_low_temp/z_%.2f/hden_%.2f/Metal_Cooling_Rate.txt", 
							All.gadget_src_prefix, z, h);
			f = fopen(filePath, "r");
			f2 = fopen(lowTempFilePath, "r");
			if(f == NULL || f2 == NULL){
				printf("%i %i\n",i,j);  fflush(stdout);
			}

			k = 0;



			//Read LOW TEMP file (i.e. T < 10^4). Skip ALL header lines.
			while( fgets(line, lineLen, f2) != NULL)
			{
				if(feof(f2) == 0 && line[0] != '#')
				{
					p = strtok(line, " ");
					l=0;
					
					if( k >= nt )
					{
						fprintf(stderr, "ERROR!! Cooling array bounds overstepped\n");
						endrun(679);
					}
					sscanf(p, "%f", &temp_UV_array[k]); 	//First element is always temperature.
					p = strtok(NULL, " \t");

					//Read & Write both cooling and heating at the same time.
					while( p != NULL && l < nm)
					{
						sscanf(p, "%f", &UV_heating_table[i][j][k][l]);
						p = strtok(NULL, " \t");
						sscanf(p, "%f", &UV_cooling_table[i][j][k][l]);
						p = strtok(NULL, " \t");

						l++;      //update metals
					}
					k++; 				//update temp
				}
			} //End while loop


			//Check that entire low Temp file have been read. temp_UV_array[k-1] == maxLowTemp
			if(temp_UV_array[k-1] <= maxLowTemp+0.001 && temp_UV_array[k-1] >= maxLowTemp - 0.001)
			{
				//Do Nothing!
			}	
			else
			{
				fprintf(stderr, "ERROR!!! Low Temperature cooling file misread!!\n\n");
				fprintf(stderr, "See %s\n\n", lowTempFilePath);
				fflush(stdout);
				endrun(679);
			}


			//Read HIGH TEMP file (i.e. T > 10^4). Skip ALL header lines.
			while( fgets(line, lineLen, f) != NULL)
			{
				if(feof(f) == 0 && line[0] != '#')
				{
					p = strtok(line, " ");
					l=0;
					
					if( k >= nt )
					{
						fprintf(stderr, "ERROR!! Cooling array bounds overstepped\n");
						endrun(679);
					}
					sscanf(p, "%f", &temp_UV_array[k]); 	//First element is always temperature.
					p = strtok(NULL, " \t");

					//Read & Write both cooling and heating at the same time.
					while( p != NULL && l < nm)
					{
						sscanf(p, "%f", &UV_heating_table[i][j][k][l]);
						p = strtok(NULL, " \t");
						sscanf(p, "%f", &UV_cooling_table[i][j][k][l]);
						p = strtok(NULL, " \t");

						l++;      //update metals
					}
					k++; 				//update temp
				}
			} //End while loop
		
			hden_array[j] = h;
			fclose(f);
			fclose(f2);
		}
	
		redshift_array[i] = z;
	}


	//Initialize Metallicity array separately
	for(l = 0; l < nm; l++)
	{
		metallicity_UV_array[l] = m_min + l * dm;
	}
}
#endif
#endif

