/*************************************************************************************
Author: Ali Snedden 
Date: 	10/8/13

Dep. of Physics
University of Notre Dame

Purpose: 
	This is adapted from density.c. Below we want to implement mass, metal
	and energy feedback from supernovae.  This requires 3 searches tree walks.

	1. Iteratively search the tree and assign smoothing length to 
		 the star particles.

	2. Search the tree for SPH particles within the smoothing length.
		 Communicate neighbor particle info back to local particle / processor.
		 -> Calculate Normalization (eqn. 6 in Stinson et al. 2006)

	3. Search tree again. Distribute metals and energy.


NOTE: When you want to implement AGB stars, look at Wiersma et al. (2009).
  Its appendix provides a great place to start.



IMPORTANT DISCUSSION OF GSL LIBRARY:
  If the gsl_integration_qag() is used, at some point the following error is thrown
  "ERROR: roundoff error prevents tolerance from being achieved". I believe this
  is due to my flooring of the zMassFrac in TypeII_Zi_yield_mass_frac() when
  8 <= mass < 13. This is b/c I don't have data SN yield data for that range.

  So instead I use gsl_integration_glfixed(). It appears to give similarly decent
  results. I left in the lines related to gsl_integration_qag() to permit further
  testing in the future.
**************************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <mpi.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_spline.h>
#include <gsl/gsl_integration.h>


#include "allvars.h"
#include "proto.h"

#ifdef COOLING
#ifdef SFR
#ifdef METALS
#ifdef FEEDBACK
#ifdef FEEDBACK_W_SPH_KERNAL

/*	This contains necessary definitions used in the feedback routines. See
		 K06 : Kobayashi, 2006 ApJ 635, 1145
 		 K04 : Kobayashi, 2004 MNRAS 347, 740
 		 H02 : Heger, 2002 Springer-Verlag, 369, see arXiv:astro-ph/0112059 
 		 E04 : Eldridge & Tout, 2004 MNRAS, 353, 87  

		Must change IMF exponent and normalization manually. See feedback.c
 */
static double hubble_a;         //H(t) C&O eqn 29.122, OmegaKurvature != 0, OmegaRel=0
static double a3;
static double atime;
static double dtMass			= 0;			//Change star particle mass.							
static double dtEntropy		= 0;			//Entropy from star particle distr. to gas part.
static double dtEnergy		= 0;			//units = (internal energy / internal mass)
static double dt_Z_Mass_dist[NELEMENTS];		//Metal mass distributed to gas part.
static double dt_Z_Mass_lost[NELEMENTS];		// Metals lost by star.

//NOTE : If these values change, you'll need to change case IO_RII and IO_RIA in io.c
static double m_2u		= 40;									// SNe II upper limit
static double m_2l_bh	= 25;									// Black Hole lower limit.
static double m_ecsn  = 10;                 // "e- capture SN", No metals are produced 
static double m_2l		= 8;							  	// SNe II lower limit
static double m_sun   = 1;                  // Mass of sun in solar masses
static double m_1l_RG	= 0.8;								// RedGiant-WhiteDwarf system lower lim

//If not defined in Makefile, specify here
#ifndef N_INTEGRATION_STEPS 
#define N_INTEGRATION_STEPS		100		//Number of steps used in integration scheme.
#endif

#ifdef USE_GSL
//static gsl_integration_workspace *workspace;
//#define WORKSIZE 1000               //Memory allocated for GSL qag workspace
#define GL_TABLE_LEN 250
static gsl_integration_glfixed_table * gl_table; //Gauss-Legendre abscissae and weights
static gsl_function F;
static gsl_feedback_integ_params params;
#endif


#ifdef PERIODIC
static double boxSize, boxHalf;

#ifdef LONG_X
static double boxSize_X, boxHalf_X;
#else
#define boxSize_X boxSize
#define boxHalf_X boxHalf
#endif
#ifdef LONG_Y
static double boxSize_Y, boxHalf_Y;
#else
#define boxSize_Y boxSize
#define boxHalf_Y boxHalf
#endif
#ifdef LONG_Z
static double boxSize_Z, boxHalf_Z;
#else
#define boxSize_Z boxSize
#define boxHalf_Z boxHalf
#endif
#endif


/*******************************************************************************
	This function computes the smoothing length of star particles.
 	the number of neighbours in the current smoothing radius. 
 	If a particle with its smoothing region is fully inside the
 	local domain, it is not exported to the other processors. The function
 	also detects particles that have a number of neighbours outside the
 	allowed tolerance range. For these particles, the smoothing length is
 	adjusted accordingly, and the density computation is executed again.
 	Note that the smoothing length is not allowed to fall below the lower
 	bound set by MinStarHsml.
 ********************************************************************************/
void update_star_hsml(void)
{
  long long ntot;						//Total (global) number of P needing updated
	long long ntotleft;				//Number of P left to update
  int *noffset;							//Given a recvTask, return corresponding sending part. location in CommBuffer
	int *nbuffer;							//Number of particles in buffer
	int *nsend;								//Combination of all Task's nsend_local[]
	int *nsend_local;					//Number of LOCAL particles to send to j'th Task
	int *numlist; 						//Holds 'NumStarUpdate' from each task (# Star needing updated
	int *ndonelist;						//
  int i, j, k, n;
	int ndone;								//Number of P done updating
	int npleft;
	int maxfill;							//Number of structs that fit in CommBuffer
	int source;
	int iter = 0;
  int level;
	int ngrp;
	int sendTask;							//Index of sending Task
	int recvTask;							//Index of receiving Task
	int place;								//
	int nexport;
  double dt;				
	double tstart;
	double tend;
	double tstart_ngb = 0;
	double tend_ngb = 0;
  double sumt;
	double sumcomm;
	double timengb;
	double sumtimengb;
  double timecomp = 0;
	double timeimbalance = 0;
	double timecommsumm = 0;
	double sumimbalance;
  MPI_Status status;

	#ifdef PERIODIC
  	boxSize = All.BoxSize;
  	boxHalf = 0.5 * All.BoxSize;
		#ifdef LONG_X
  		boxHalf_X = boxHalf * LONG_X;
  		boxSize_X = boxSize * LONG_X;
		#endif
		#ifdef LONG_Y
		  boxHalf_Y = boxHalf * LONG_Y;
		  boxSize_Y = boxSize * LONG_Y;
		#endif
		#ifdef LONG_Z
		  boxHalf_Z = boxHalf * LONG_Z;
		  boxSize_Z = boxSize * LONG_Z;
		#endif
	#endif


  /* Will be used by all integration schemes throughout this file. Freed in
     distribute_metals_and_energy_w_kernal() */
  #ifdef USE_GSL
    //workspace = gsl_integration_workspace_alloc(WORKSIZE);  
    gl_table = gsl_integration_glfixed_table_alloc(GL_TABLE_LEN);
  #endif


	phys_time = get_phys_time(All.Time);    //Write age of universe (My) to phys_time.


  /* `NumStarUpdate' = number of star particles on this processor that want a 
      force update and will also generate feedback.
      I could place Stars at end of P[], but we'll search the whole damn thing anyways.
  */
  for(n = 0, NumStarUpdate = 0; n < NumPart; n++)				// <- could speed up here
  {
    P[n].StarHsmlLeft = P[n].StarHsmlRight = 0;
		dt = phys_time - P[n].StarAge;	//Time (My) passes since star formed

    if(P[n].Type == 4 && P[n].Ti_endstep == All.Ti_Current && dt > 5.0)	
		{
			/* Check that there is actual feedback to be done. This is only done once 
				 and used throughout the rest of the feedback routines. */
			get_feedback_rates(n);				
		
			if(P[n].R_II > 0.0 || P[n].R_Ia > 0.0 || P[n].R_SW > 0.0 || P[n].R_AGB > 0.0){
    		NumStarUpdate++;
			}
		}
  }


	//Find total (global) number of P needing updated
  numlist = malloc(NTask * sizeof(int) * NTask);
  MPI_Allgather(&NumStarUpdate, 1, MPI_INT, numlist, 1, MPI_INT, MPI_COMM_WORLD);
  for(i = 0, ntot = 0; i < NTask; i++)
    ntot += numlist[i];
  free(numlist);


	//Allocate memory
  noffset = malloc(sizeof(int) * NTask);	/* offsets of bunches in common list */
  nbuffer = malloc(sizeof(int) * NTask);
  nsend_local = malloc(sizeof(int) * NTask);
  nsend = malloc(sizeof(int) * NTask * NTask);
  ndonelist = malloc(sizeof(int) * NTask);


  /* 	
		Iterate through all GLOBAL Star Particles needing updated, until no more 
		GLOBAL particles are left to communicate. We will repeat the whole thing for 
		those particles where we didn't find enough neighbours.
	*/
  do
  {
    i = N_gas;					/* first non-SPH particle begin with this index */
    ntotleft = ntot;		/* particles left for all tasks together */

    while(ntotleft > 0)
	  {
	    for(j = 0; j < NTask; j++)
	      nsend_local[j] = 0;

	    /* do local particles and prepare export list */
	    tstart = second();

			/*
				Iterate through stars needing an update. Count number of nearby on LOCAL and 
				NON-LOCAL particles. Exportflag is set in get_local_number_gas_part_nearby()
			*/
	    for(nexport=0, ndone=0; i<NumPart && nexport< All.BunchSizeGasSearch - NTask; i++)
			{
				dt = phys_time - P[i].StarAge;			//Time (My) passes since star formed

				/* Only Active Stars over 5My old w/ nearby gas part. */
	      if(P[i].Type == 4 && P[i].Ti_endstep == All.Ti_Current && dt > 5.0 &&
					(P[i].R_II > 0.0 || P[i].R_Ia > 0.0 || P[i].R_SW > 0.0 || P[i].R_AGB > 0.0))
	      {
	      	ndone++;

	      	for(j = 0; j < NTask; j++){
	      	  Exportflag[j] = 0;							//Reset list of Tasks to export to
					}

	      	P[i].NumGasNearby = get_number_gas_part_nearby(P[i].StarHsml, i, 0);	

	      	for(j = 0; j < NTask; j++)
	    	  {
	    	    if(Exportflag[j])
		        {
							for(k=0; k<3; k++)
							{
								GasPartSearchDataIn[nexport].Pos[k] = P[i].Pos[k];
							}
							GasPartSearchDataIn[nexport].ID   	= P[i].ID;
							GasPartSearchDataIn[nexport].Task 	= j;
							GasPartSearchDataIn[nexport].Index	=	i;
							GasPartSearchDataIn[nexport].StarHsml	=	P[i].StarHsml;

	        		nexport++;
	        		nsend_local[j]++;
		        }
		      }
	      }
			}
	    tend = second();
	    timecomp += timediff(tstart, tend);

			//Group GasPartSearchDataIn[] by Task to be exported to, makes easier to communicate
	    qsort(GasPartSearchDataIn, nexport, sizeof(struct gaspartsearchdata_in), gaspartsearch_compare_key);

			//Get offset of particles as a function of destination Task
	    for(j = 1, noffset[0] = 0; j < NTask; j++)
	      noffset[j] = noffset[j - 1] + nsend_local[j - 1];

	    tstart = second();

			/*
				Get number of particles to be exported by each Task and to which Task it 
				will be exported.
			*/
	    MPI_Allgather(nsend_local, NTask, MPI_INT, nsend, NTask, MPI_INT, MPI_COMM_WORLD);

	    tend = second();
	    timeimbalance += timediff(tstart, tend);


	    /* Get Number of nearby particles from NON-LOCAL particles. */
	    for(level = 1; level < (1 << PTask); level++)
	    {
	      tstart = second();
	      for(j = 0; j < NTask; j++){
		      nbuffer[j] = 0;
				}

				//Complex way of sending/receiving particles between Tasks. I don't understand
	      for(ngrp = level; ngrp < (1 << PTask); ngrp++)
		    {
		      maxfill = 0;
		      for(j = 0; j < NTask; j++)
		      {
						//Get Number of particles in GasPartSearchDataIn Buffer destined for ______
		        if((j ^ ngrp) < NTask)									//This is bitwise XOR comparison
		        	if(maxfill < nbuffer[j] + nsend[(j ^ ngrp) * NTask + j])
		        	  maxfill = nbuffer[j] + nsend[(j ^ ngrp) * NTask + j];
		      }
		      if(maxfill >= All.BunchSizeGasSearch)
		        break;

		      sendTask = ThisTask;
		      recvTask = ThisTask ^ ngrp;

		      if(recvTask < NTask)
		      {
		        if(nsend[ThisTask * NTask + recvTask] > 0 || nsend[recvTask * NTask + 
               ThisTask] > 0)
		      	{
	          	/* 
								Send and Recieve the particles. Blocks communication  until 
								send buffer is free and receive buffer is filled. The send and
								receive buffers point to different sections of the CommBuffer
							*/
		      	  MPI_Sendrecv(&GasPartSearchDataIn[noffset[recvTask]],
		         		       nsend_local[recvTask] * sizeof(struct gaspartsearchdata_in), 
											 MPI_BYTE, recvTask, TAG_FEEDBACK_A,
		         		       &GasPartSearchDataGet[nbuffer[ThisTask]],
		        		       nsend[recvTask * NTask + ThisTask] * sizeof(struct 
											 gaspartsearchdata_in), MPI_BYTE, recvTask, TAG_FEEDBACK_A, 
											 MPI_COMM_WORLD, &status);
			      }
		      }

		      for(j = 0; j < NTask; j++)
		        if((j ^ ngrp) < NTask)
		          nbuffer[j] += nsend[(j ^ ngrp) * NTask + j];
		    }
	      tend = second();
	      timecommsumm += timediff(tstart, tend);

				/* now count nearby particles for imported particles */
	      tstart = second();
	      for(j = 0; j < nbuffer[ThisTask]; j++){
					GasPartSearchDataResult[j].NumGasNearby 	= get_number_gas_part_nearby(
								GasPartSearchDataGet[j].StarHsml, j, 1);
					GasPartSearchDataResult[j].Task 					= GasPartSearchDataGet[j].Task;
					GasPartSearchDataResult[j].ID   					= GasPartSearchDataGet[j].ID;
					GasPartSearchDataResult[j].Pos[0] 				= GasPartSearchDataGet[j].Pos[0];
					GasPartSearchDataResult[j].Pos[1] 				= GasPartSearchDataGet[j].Pos[1];
					GasPartSearchDataResult[j].Pos[2] 				= GasPartSearchDataGet[j].Pos[2];
				}

	      tend = second();
	      timecomp += timediff(tstart, tend);

	      /* do a block to explicitly measure imbalance */
	      tstart = second();
	      MPI_Barrier(MPI_COMM_WORLD);
	      tend = second();
	      timeimbalance += timediff(tstart, tend);

	      /* get the result */
	      tstart = second();
	      for(j = 0; j < NTask; j++)
	      	nbuffer[j] = 0;
	      for(ngrp = level; ngrp < (1 << PTask); ngrp++)
	  	  {
	  	    maxfill = 0;
	  	    for(j = 0; j < NTask; j++)
		      {
		        if((j ^ ngrp) < NTask)
	        		if(maxfill < nbuffer[j] + nsend[(j ^ ngrp) * NTask + j])
	        		  maxfill = nbuffer[j] + nsend[(j ^ ngrp) * NTask + j];
		      }
	  	    if(maxfill >= All.BunchSizeGasSearch)
	  	      break;

	  	    sendTask = ThisTask;
		      recvTask = ThisTask ^ ngrp;

		      if(recvTask < NTask)
		      {
		        if(nsend[ThisTask * NTask + recvTask] > 0 || nsend[recvTask * NTask + 
               ThisTask] > 0)
			      {
		          /*  
								Send and Recieve the Results. Blocks communication  until 
								send buffer is free and receive buffer is filled. The send and
								receive buffers point to different sections of the CommBuffer

								GasPartSearchDataResult        = sending buffer
								GasPartSearchDataPartialResult = receiving buffer
							*/
			        MPI_Sendrecv(&GasPartSearchDataResult[nbuffer[ThisTask]],
				             nsend[recvTask * NTask + ThisTask] * 
										 sizeof(struct gaspartsearchdata_out),
				             MPI_BYTE, recvTask, TAG_FEEDBACK_B,
		      		       &GasPartSearchDataPartialResult[noffset[recvTask]],
				             nsend_local[recvTask] * sizeof(struct gaspartsearchdata_out),
				             MPI_BYTE, recvTask, TAG_FEEDBACK_B, MPI_COMM_WORLD, &status);

			        /* add the result to the particles */
			        for(j = 0; j < nsend_local[recvTask]; j++)
			        {
								/*
									NOTE: GasPartSearchDataIn[source] and 
									GasPartSearchDataPartialResult[source] are referring to the same 
									particles.
								*/
			          source = j + noffset[recvTask];
			          place = GasPartSearchDataIn[source].Index;

								/* Error Check */
								if(P[place].ID == GasPartSearchDataPartialResult[source].ID)
								{
									P[place].NumGasNearby += GasPartSearchDataPartialResult[source].NumGasNearby;
								}
								else
								{
									fprintf(stderr, "ERROR!!!!\nP[%i].ID != GasPartSearchDataPartialResult"
																	"[%i].ID mismatch!\n", place, source);
									endrun(668);
								}
			        }
			      }
		      }

		      for(j = 0; j < NTask; j++)
		        if((j ^ ngrp) < NTask)
		          nbuffer[j] += nsend[(j ^ ngrp) * NTask + j];
		    }
	      tend = second();
	      timecommsumm += timediff(tstart, tend);

	      level = ngrp - 1;
	    }

			//Total up the number of GLOBAL particles to update
	    MPI_Allgather(&ndone, 1, MPI_INT, ndonelist, 1, MPI_INT, MPI_COMM_WORLD);
	    for(j = 0; j < NTask; j++)
	      ntotleft -= ndonelist[j];
	  }



    /* do final operations on results */
    tstart = second();
    for(i = 0, npleft = 0; i < NumPart; i++)
  	{
			dt = phys_time - P[i].StarAge;

  	  if(P[i].Type == 4 && P[i].Ti_endstep == All.Ti_Current && dt > 5.0 &&
				(P[i].R_II > 0.0 || P[i].R_Ia > 0.0 || P[i].R_SW > 0.0 || P[i].R_AGB > 0.0))
	    {
	      /* check whether we had enough neighbours, adjust P[].StarHsml accordingly*/
	      if(P[i].NumGasNearby < (All.StarDesNumNgb - All.StarMaxNumNgbDeviation) ||
		        (P[i].NumGasNearby > (All.StarDesNumNgb + All.StarMaxNumNgbDeviation)
		        && P[i].StarHsml > (1.01 * All.MinStarHsml)))
		    {
		      /* need to redo this particle */
		      npleft++;

		      if(P[i].StarHsmlLeft > 0 && P[i].StarHsmlRight > 0)
		        if((P[i].StarHsmlRight - P[i].StarHsmlLeft) < 1.0e-3 * P[i].StarHsmlLeft)
		        {
			        /* this one should be ok */
			        npleft--;
			        P[i].Ti_endstep = -P[i].Ti_endstep - 1;	/* Mark as inactive */
			        continue;
		        }

		      if(P[i].NumGasNearby < (All.StarDesNumNgb - All.StarMaxNumNgbDeviation))
		        P[i].StarHsmlLeft = dmax(P[i].StarHsml, P[i].StarHsmlLeft);
		      else
		      {
		        if(P[i].StarHsmlRight != 0)
			      {
			        if(P[i].StarHsml < P[i].StarHsmlRight)
			          P[i].StarHsmlRight = P[i].StarHsml;
			      }
		        else
			        P[i].StarHsmlRight = P[i].StarHsml;
		      }

		      if(iter >= MAXITER - 10)
		      {
		        printf("i=%d task=%d ID=%d Hsml=%g Left=%g Right=%g Ngbs=%g"
									 " Right-Left=%g\n   pos=(%g|%g|%g)\n",
			            i, ThisTask, (int) P[i].ID, P[i].StarHsml, P[i].StarHsmlLeft, 
									P[i].StarHsmlRight, (float)P[i].NumGasNearby, 
									P[i].StarHsmlRight - P[i].StarHsmlLeft, P[i].Pos[0], P[i].Pos[1], 
									P[i].Pos[2]);
		        fflush(stdout);
		      }

		      if(P[i].StarHsmlRight > 0 && P[i].StarHsmlLeft > 0){
						//Average volume between the two limits
		        P[i].StarHsml = pow(0.5 * (pow(P[i].StarHsmlLeft, 3) + 
																			 pow(P[i].StarHsmlRight, 3)), 1.0 / 3);	
					}
		      else
		      {
		        if(P[i].StarHsmlRight == 0 && P[i].StarHsmlLeft == 0)
		        	endrun(8188);	/* can't occur */

		        /***** this is to limit h not to over shoot *****/

		        double fac;

		        if(P[i].StarHsmlRight == 0 && P[i].StarHsmlLeft > 0)
		      	{
		      	  if(P[i].Type == 4 && fabs(P[i].NumGasNearby - All.StarDesNumNgb) < 
                 0.5 * All.StarDesNumNgb)
			        {

			          fac = 1 - (P[i].NumGasNearby - All.StarDesNumNgb) / (NUMDIMS * 
													P[i].NumGasNearby); //CHECK FOR CORRECTNESS!!!

			          if(fac > 1.26) 
				          fac = 1.26; 
			          if(fac < 1/1.26) 
				          fac = 1/1.26; 
			          P[i].StarHsml *= fac; 

			          /*****   
                P[i].StarHsml *= 1 - (P[i].NumGasNearby -
				                      All.StarDesNumNgb) / (NUMDIMS * P[i].NumGasNearby) * 
                              P[i].DhsmlDensityFactor; 
                *****/
			        }
			        else
			          P[i].StarHsml *= 1.26;
			      }

		        if(P[i].StarHsmlRight > 0 && P[i].StarHsmlLeft == 0)
			      {
			        if(P[i].Type == 4 && fabs(P[i].NumGasNearby - All.StarDesNumNgb) < 
								 0.5 * All.StarDesNumNgb)
			        {

			          fac = 1 - (P[i].NumGasNearby - All.StarDesNumNgb) / (NUMDIMS * 
													P[i].NumGasNearby); //CHECK FOR CORRECTNESS!!!

			          if(fac > 1.26) 
			          	fac = 1.26; 
			          if(fac < 1/1.26) 
			          	fac = 1/1.26; 
			          P[i].StarHsml *= fac; 

			          /*****			      P[i].StarHsml *=
			               	1 - (P[i].NumGasNearby -
			               	All.StarDesNumNgb) / (NUMDIMS * P[i].NumGasNearby) * 
                      P[i].DhsmlDensityFactor; 
                *****/
			        }
			        else
			          P[i].StarHsml /= 1.26;
			      }
		      }

		      if(P[i].StarHsml < All.MinStarHsml)
		        P[i].StarHsml = All.MinStarHsml;
		    }
	      else
		      P[i].Ti_endstep = -P[i].Ti_endstep - 1;	/* Mark as inactive */
	    }
	  }
    tend = second();
    timecomp += timediff(tstart, tend);


    numlist = malloc(NTask * sizeof(int) * NTask);
    MPI_Allgather(&npleft, 1, MPI_INT, numlist, 1, MPI_INT, MPI_COMM_WORLD);
    for(i = 0, ntot = 0; i < NTask; i++)
	    ntot += numlist[i];
    free(numlist);

    if(ntot > 0)
	  {
	    if(iter == 0)
	      tstart_ngb = second();

	    iter++;

	    if(iter > 0 && ThisTask == 0)
	    {
	      printf("ngb iteration %d: need to repeat for %d%09d particles.\n", 
               iter, (int) (ntot / 1000000000), (int) (ntot % 1000000000));
	      fflush(stdout);
	    }

	    if(iter > MAXITER)
	    {
	      printf("failed to converge in neighbour iteration in density()\n");
	      fflush(stdout);
	      endrun(1155);
	    }
	  }
    else
	    tend_ngb = second();
  }
  while(ntot > 0);


  /* mark as active again */
  for(i = 0; i < NumPart; i++)
    if(P[i].Ti_endstep < 0)
      P[i].Ti_endstep = -P[i].Ti_endstep - 1;

  free(ndonelist);
  free(nsend);
  free(nsend_local);
  free(nbuffer);
  free(noffset);


  /* collect some timing information */
  if(iter > 0)
    timengb = timediff(tstart_ngb, tend_ngb);
  else
    timengb = 0;

  MPI_Reduce(&timengb, &sumtimengb, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
  MPI_Reduce(&timecomp, &sumt, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
  MPI_Reduce(&timecommsumm, &sumcomm, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
  MPI_Reduce(&timeimbalance, &sumimbalance, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);

  if(ThisTask == 0)
  {
    All.CPU_HydCompWalk += sumt / NTask;
    All.CPU_HydCommSumm += sumcomm / NTask;
    All.CPU_HydImbalance += sumimbalance / NTask;
    All.CPU_EnsureNgb += sumtimengb / NTask;
  }
}



/*********************************************************************************
	Find the total number of particles within radius of P[target] 

	mode = 0   :  Local    particles, P[]
	mode = 1   :  NonLocal particles, GasPartSearchDataGet[]
**********************************************************************************/
long get_number_gas_part_nearby(FLOAT radius, int target, int mode)
{
	int startnode				= All.MaxPart;
	int n 							= 0;							//Ngblist[] index
	int j;
	long numngb					= 0; 							//Number of "nearby" gas part.
	long num_gas_nearby	= 0;							//Number of gas part w/in 'radius' of P[target]
	double dx 					= 0.0;
	double dy						= 0.0;
	double dz 					= 0.0;
	double r 						= 0.0;						// | P[target].Pos - P[j].Pos |
	FLOAT *Pos;

	/* In this case b/c boxHalf_* won't be set b/c they are set in 
		 get_number_of_gas_nearby which isn't called */
	#ifdef FEEDBACK_W_SPH_KERNAL
		#ifdef PERIODIC
  		boxSize = All.BoxSize;
  		boxHalf = 0.5 * All.BoxSize;
			#ifdef LONG_X
			  boxHalf_X = boxHalf * LONG_X;
			  boxSize_X = boxSize * LONG_X;
			#endif
			#ifdef LONG_Y
			  boxHalf_Y = boxHalf * LONG_Y;
			  boxSize_Y = boxSize * LONG_Y;
			#endif
			#ifdef LONG_Z
  			boxHalf_Z = boxHalf * LONG_Z;
		  	boxSize_Z = boxSize * LONG_Z;
			#endif
		#endif
	#endif


	if(mode == 0){
		Pos = P[target].Pos;
	}
	else{
		Pos = GasPartSearchDataGet[target].Pos;
	}


	/* Get number of LOCAL particles within 'radius' of the P[target]'s location */
	do
	{
		numngb =	ngb_treefind_variable(&Pos[0], radius, &startnode);

		/* from Ngblist find particles w/in 'radius' */
		for(n = 0; n < numngb; n++)
		{
			j = Ngblist[n];										//Get index of 'nearby' gas part.

			dx = Pos[0] - P[j].Pos[0];	
			dy = Pos[1] - P[j].Pos[1];
			dz = Pos[2] - P[j].Pos[2];

			#ifdef PERIODIC			/*  find the closest image in the given box size  */
	    	if(dx > boxHalf_X) { 		dx -= boxSize_X; 	}
	   		if(dx < -boxHalf_X){		dx += boxSize_X;	}
	    	if(dy > boxHalf_Y) {		dy -= boxSize_Y;	}
	    	if(dy < -boxHalf_Y){		dy += boxSize_Y;	}
	    	if(dz > boxHalf_Z) {		dz -= boxSize_Z;	}
	    	if(dz < -boxHalf_Z){		dz += boxSize_Z;	}
			#endif
	    r = sqrt(dx * dx + dy * dy + dz * dz);
	
			if( r <= radius)
			{
				num_gas_nearby++;
			}
		}
	}
	while(startnode >= 0);

	return num_gas_nearby;
}










/*******************************************************************************
	This function computes the normalization constant as determined by eqn. 6 in 
	Stinson et al. (2006).  Here we sum up the m_j * W(r_j - r_i, h_i).  

	So we only need to pass the smoothing length (hsml) and star position to the other
	processors.
 ********************************************************************************/
void get_feedback_normalization(void)
{
  long long ntot;						//Total (global) number of P needing updated
	long long ntotleft;				//Number of P left to update
  int *noffset;							//Given a recvTask, return corresponding sending part. location in CommBuffer
	int *nbuffer;							//Number of particles in buffer
	int *nsend;								//Combination of all Task's nsend_local[]
	int *nsend_local;					//Number of LOCAL particles to send to j'th Task
	int *numlist; 						//Holds 'NumStarUpdate' from each task (# Star needing updated
	int *ndonelist;						//
  int i, j, k, n;
	int ndone;								//Number of P done updating
	int maxfill;							//Number of structs that fit in CommBuffer
	int source;
  int level;
	int ngrp;
	int sendTask;							//Index of sending Task
	int recvTask;							//Index of receiving Task
	int place;								//
	int nexport;
	double gasdensity = 0.0;	//Use SPH kernal to compute
	double gaspressure = 0.0;	//Use SPH kernal to estimate
  double dt;				
	double tstart;
	double tend;
  double sumt;
	double sumcomm;
  double timecomp = 0;
	double timeimbalance = 0;
	double timecommsumm = 0;
	double sumimbalance;
  MPI_Status status;

	#ifdef PERIODIC
  	boxSize = All.BoxSize;
  	boxHalf = 0.5 * All.BoxSize;
		#ifdef LONG_X
  		boxHalf_X = boxHalf * LONG_X;
  		boxSize_X = boxSize * LONG_X;
		#endif
		#ifdef LONG_Y
		  boxHalf_Y = boxHalf * LONG_Y;
		  boxSize_Y = boxSize * LONG_Y;
		#endif
		#ifdef LONG_Z
		  boxHalf_Z = boxHalf * LONG_Z;
		  boxSize_Z = boxSize * LONG_Z;
		#endif
	#endif

	phys_time = get_phys_time(All.Time);    //Write age of universe (My) to phys_time.


  /* `NumStarUpdate' = number of star particles on this processor that want a 
      force update and will also generate feedback.
      Stars should be at end of P[], but we'll search the whole damn thing anyways.
  */
  for(n = 0, NumStarUpdate = 0; n < NumPart; n++)				// <- could speed up here
  {
		dt = phys_time - P[n].StarAge;	//Time (My) passes since star formed

    if(P[n].Type == 4 && P[n].Ti_endstep == All.Ti_Current && dt > 5.0)	
		{
			/* Check that there is actual feedback to be done. This is only done once 
				 and used throughout the rest of the feedback routines. */
			if(P[n].R_II > 0.0 || P[n].R_Ia > 0.0 || P[n].R_SW > 0.0 || P[n].R_AGB > 0.0){
    		NumStarUpdate++;
			}
		}
  }


	//Find total (global) number of P needing updated
  numlist = malloc(NTask * sizeof(int) * NTask);
  MPI_Allgather(&NumStarUpdate, 1, MPI_INT, numlist, 1, MPI_INT, MPI_COMM_WORLD);
  for(i = 0, ntot = 0; i < NTask; i++)
    ntot += numlist[i];
  free(numlist);


	//Allocate memory
  noffset = malloc(sizeof(int) * NTask);	/* offsets of bunches in common list */
  nbuffer = malloc(sizeof(int) * NTask);
  nsend_local = malloc(sizeof(int) * NTask);
  nsend = malloc(sizeof(int) * NTask * NTask);
  ndonelist = malloc(sizeof(int) * NTask);



  /* 	
		Iterate through all GLOBAL Star Particles needing updated, until no more 
		GLOBAL particles are left to communicate. 
	*/
  i = N_gas;					/* first non-SPH particle beginn with this index */
  ntotleft = ntot;		/* particles left for all tasks together */

  while(ntotleft > 0)
  {
    for(j = 0; j < NTask; j++)
      nsend_local[j] = 0;

    /* do local particles and prepare export list */
    tstart = second();

		/*
			Iterate through stars needing an update. Count number of nearby on LOCAL and 
			NON-LOCAL particles. Exportflag is set in get_feedback_normalization_constant()
		*/
    for(nexport=0, ndone=0; i<NumPart && nexport< All.BunchSizeNormalization - NTask; 
		i++)
		{
			dt = phys_time - P[i].StarAge;			//Time (My) passes since star formed

			/* Only Active Stars over 5My old */
      if(P[i].Type == 4 && P[i].Ti_endstep == All.Ti_Current && dt > 5.0 &&
				(P[i].R_II > 0.0 || P[i].R_Ia > 0.0 || P[i].R_SW > 0.0 || P[i].R_AGB > 0.0))
      {
      	ndone++;

      	for(j = 0; j < NTask; j++){
      	  Exportflag[j] = 0;							//Reset list of Tasks to export to
				}

      	P[i].FeedbackNorm = get_feedback_normalization_constant(P[i].StarHsml, &gasdensity,
																																&gaspressure, i, 0);	
				P[i].GasPressure = gaspressure;
				P[i].GasDensity  = gasdensity;

      	for(j = 0; j < NTask; j++)
    	  {
    	    if(Exportflag[j])
	        {
						for(k=0; k<3; k++)
						{
							FeedbackNormDataIn[nexport].Pos[k] = P[i].Pos[k];
						}
						FeedbackNormDataIn[nexport].ID   	= P[i].ID;
						FeedbackNormDataIn[nexport].Task 	= j;
						FeedbackNormDataIn[nexport].Index	=	i;
						FeedbackNormDataIn[nexport].StarHsml	=	P[i].StarHsml;

        		nexport++;
        		nsend_local[j]++;
	        }
	      }
      }
		}
    tend = second();
    timecomp += timediff(tstart, tend);

		//Group FeedbackNormDataIn[] by Task to be exported to, makes easiar to communicate
    qsort(FeedbackNormDataIn, nexport, sizeof(struct feedbacknormdata_in), 
					feedbacknorm_compare_key);

		//Get offset of particles as a function of destination Task
    for(j = 1, noffset[0] = 0; j < NTask; j++)
      noffset[j] = noffset[j - 1] + nsend_local[j - 1];

    tstart = second();

		/*
			Get number of particles to be exported by each Task and to which Task it 
			will be exported.
		*/
    MPI_Allgather(nsend_local, NTask, MPI_INT, nsend, NTask, MPI_INT, MPI_COMM_WORLD);

    tend = second();
    timeimbalance += timediff(tstart, tend);


    /* Get Number of nearby particles from NON-LOCAL particles. */
    for(level = 1; level < (1 << PTask); level++)
    {
      tstart = second();
      for(j = 0; j < NTask; j++){
	      nbuffer[j] = 0;
			}

			//Complex way of sending / receiving particles between Tasks. I don't understand
      for(ngrp = level; ngrp < (1 << PTask); ngrp++)
	    {
	      maxfill = 0;
	      for(j = 0; j < NTask; j++)
	      {
					//Get Number of particles in FeedbackNormDataIn Buffer destined for _________
	        if((j ^ ngrp) < NTask)											//This is bitwise XOR comparison
	        	if(maxfill < nbuffer[j] + nsend[(j ^ ngrp) * NTask + j])
	        	  maxfill = nbuffer[j] + nsend[(j ^ ngrp) * NTask + j];
	      }
	      if(maxfill >= All.BunchSizeNormalization)
	        break;

	      sendTask = ThisTask;
	      recvTask = ThisTask ^ ngrp;

	      if(recvTask < NTask)
	      {
	        if(nsend[ThisTask * NTask + recvTask] > 0 || nsend[recvTask * NTask + 
              ThisTask] > 0)
	      	{
          	/* 
							Send and Recieve the particles. Blocks communication  until 
							send buffer is free and receive buffer is filled. The send and
							receive buffers point to different sections of the CommBuffer
						*/
	      	  MPI_Sendrecv(&FeedbackNormDataIn[noffset[recvTask]],
	         		       nsend_local[recvTask] * sizeof(struct feedbacknormdata_in), 
										 MPI_BYTE, recvTask, TAG_NORM_A,
	         		       &FeedbackNormDataGet[nbuffer[ThisTask]],
	        		       nsend[recvTask * NTask + ThisTask] * sizeof(struct 
										 feedbacknormdata_in), MPI_BYTE, recvTask, TAG_NORM_A, 
										 MPI_COMM_WORLD, &status);
		      }
	      }

	      for(j = 0; j < NTask; j++)
	        if((j ^ ngrp) < NTask)
	          nbuffer[j] += nsend[(j ^ ngrp) * NTask + j];
	    }
      tend = second();
      timecommsumm += timediff(tstart, tend);

			/* now count nearby particles for imported particles*/
      tstart = second();
      for(j = 0; j < nbuffer[ThisTask]; j++){
				FeedbackNormDataResult[j].FeedbackNorm 	= get_feedback_normalization_constant(
							FeedbackNormDataGet[j].StarHsml, &gasdensity, &gaspressure, j, 1);
				FeedbackNormDataResult[j].GasDensity    = gasdensity;
				FeedbackNormDataResult[j].GasPressure   = gaspressure;
				FeedbackNormDataResult[j].Task 					= FeedbackNormDataGet[j].Task;
				FeedbackNormDataResult[j].ID   					= FeedbackNormDataGet[j].ID;
				FeedbackNormDataResult[j].Pos[0] 				= FeedbackNormDataGet[j].Pos[0];
				FeedbackNormDataResult[j].Pos[1] 				= FeedbackNormDataGet[j].Pos[1];
				FeedbackNormDataResult[j].Pos[2] 				= FeedbackNormDataGet[j].Pos[2];
			}

      tend = second();
      timecomp += timediff(tstart, tend);

      /* do a block to explicitly measure imbalance */
      tstart = second();
      MPI_Barrier(MPI_COMM_WORLD);
      tend = second();
      timeimbalance += timediff(tstart, tend);

      /* get the result */
      tstart = second();
      for(j = 0; j < NTask; j++)
      	nbuffer[j] = 0;
      for(ngrp = level; ngrp < (1 << PTask); ngrp++)
  	  {
  	    maxfill = 0;
  	    for(j = 0; j < NTask; j++)
	      {
	        if((j ^ ngrp) < NTask)
        		if(maxfill < nbuffer[j] + nsend[(j ^ ngrp) * NTask + j])
        		  maxfill = nbuffer[j] + nsend[(j ^ ngrp) * NTask + j];
	      }
  	    if(maxfill >= All.BunchSizeNormalization)
  	      break;

  	    sendTask = ThisTask;
	      recvTask = ThisTask ^ ngrp;

	      if(recvTask < NTask)
	      {
	        if(nsend[ThisTask * NTask + recvTask] > 0 || nsend[recvTask * NTask + 
              ThisTask] > 0)
		      {
	          /*  
							Send and Recieve the Results. Blocks communication  until 
							send buffer is free and receive buffer is filled. The send and
							receive buffers point to different sections of the CommBuffer

							FeedbackNormDataResult        = sending buffer
							FeedbackNormDataPartialResult = receiving buffer
						*/
		        MPI_Sendrecv(&FeedbackNormDataResult[nbuffer[ThisTask]],
			             nsend[recvTask * NTask + ThisTask] * 
									 sizeof(struct feedbacknormdata_out),
			             MPI_BYTE, recvTask, TAG_NORM_B,
	      		       &FeedbackNormDataPartialResult[noffset[recvTask]],
			             nsend_local[recvTask] * sizeof(struct feedbacknormdata_out),
			             MPI_BYTE, recvTask, TAG_NORM_B, MPI_COMM_WORLD, &status);

		        /* add the result to the particles */
		        for(j = 0; j < nsend_local[recvTask]; j++)
		        {
							/*
								NOTE: FeedbackNormDataIn[source] and FeedbackNormPartialResult[source]
								are referring to the same particles.
							*/
		          source = j + noffset[recvTask];
		          place = FeedbackNormDataIn[source].Index;

							/* Error Check */
							if(P[place].ID == FeedbackNormDataPartialResult[source].ID)
							{
								P[place].FeedbackNorm += 
																		FeedbackNormDataPartialResult[source].FeedbackNorm;
								P[place].GasDensity += FeedbackNormDataPartialResult[source].GasDensity;
								P[place].GasPressure += FeedbackNormDataPartialResult[source].GasPressure;
							}
							else
							{
								fprintf(stderr, "ERROR!!!!\nP[%i].ID != FeedbackNormDataPartialResult"
																"[%i].ID mismatch!\n", place, source);
								endrun(668);
							}
		        }
		      }
	      }

	      for(j = 0; j < NTask; j++)
	        if((j ^ ngrp) < NTask)
	          nbuffer[j] += nsend[(j ^ ngrp) * NTask + j];
	    }
      tend = second();
      timecommsumm += timediff(tstart, tend);

      level = ngrp - 1;
    }

		//Total up the number of GLOBAL particles to update
    MPI_Allgather(&ndone, 1, MPI_INT, ndonelist, 1, MPI_INT, MPI_COMM_WORLD);
    for(j = 0; j < NTask; j++)
      ntotleft -= ndonelist[j];
  }


  free(ndonelist);
  free(nsend);
  free(nsend_local);
  free(nbuffer);
  free(noffset);


 	/* collect some timing information */ 
  MPI_Reduce(&timecomp, &sumt, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
  MPI_Reduce(&timecommsumm, &sumcomm, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
  MPI_Reduce(&timeimbalance, &sumimbalance, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);

	/* Not used */
  if(ThisTask == 0)
  {
    All.CPU_NormCompWalk += sumt / NTask;
    All.CPU_NormCommSumm += sumcomm / NTask;
    All.CPU_NormImbalance += sumimbalance / NTask;
  }
}






/*********************************************************************************
	Find the Normalization Constant of particles within radius of P[target] 

	mode = 0   :  Local    particles, P[]
	mode = 1   :  NonLocal particles, FeedbackNormDataGet[]

	NMIA = "Numerical Methods in Astrophysics" by Bodenheimer et al.
	
	Compute partial pressures and densities so that the Blast wave radius can be 
	computed.
**********************************************************************************/
FLOAT get_feedback_normalization_constant(FLOAT h, double * gasdensity, 
			double * gaspressure, int target, int mode)
{
	int startnode				= All.MaxPart;
	int n 							= 0;							//Ngblist[] index
	int j;
	long numngb					= 0;
	double density			= 0.0;
	double pressure			= 0.0;
	double rh						= 0.0;						// = r / h
	double w						= 0.0;						//Evaluated `smoothing kernel
	double norm					= 0.0;						//Normalization Constant
	double dx 					= 0.0;
	double dy						= 0.0;
	double dz 					= 0.0;
	double r 						= 0.0;						// | P[target].Pos - P[j].Pos |
	FLOAT *Pos;

	/* In this case b/c boxHalf_* won't be set b/c they are set in 
		 get_number_of_gas_nearby which isn't called */
	#ifdef FEEDBACK_W_SPH_KERNAL
		#ifdef PERIODIC
  		boxSize = All.BoxSize;
  		boxHalf = 0.5 * All.BoxSize;
			#ifdef LONG_X
			  boxHalf_X = boxHalf * LONG_X;
			  boxSize_X = boxSize * LONG_X;
			#endif
			#ifdef LONG_Y
			  boxHalf_Y = boxHalf * LONG_Y;
			  boxSize_Y = boxSize * LONG_Y;
			#endif
			#ifdef LONG_Z
  			boxHalf_Z = boxHalf * LONG_Z;
		  	boxSize_Z = boxSize * LONG_Z;
			#endif
		#endif
	#endif


	if(mode == 0){
		Pos = P[target].Pos;
	}
	else{
		Pos = FeedbackNormDataGet[target].Pos;
	}


	/* Get number of LOCAL particles within 'h' of the P[target]'s location */
	do
	{
		numngb =	ngb_treefind_variable(&Pos[0], h, &startnode);

		/* from Ngblist find particles w/in 'radius' */
		for(n = 0; n < numngb; n++)
		{
			j = Ngblist[n];										//Get index of 'nearby' gas part.

			dx = Pos[0] - P[j].Pos[0];	
			dy = Pos[1] - P[j].Pos[1];
			dz = Pos[2] - P[j].Pos[2];

			#ifdef PERIODIC			/*  find the closest image in the given box size  */
	    	if(dx > boxHalf_X) { 		dx -= boxSize_X; 	}
	   		if(dx < -boxHalf_X){		dx += boxSize_X;	}
	    	if(dy > boxHalf_Y) {		dy -= boxSize_Y;	}
	    	if(dy < -boxHalf_Y){		dy += boxSize_Y;	}
	    	if(dz > boxHalf_Z) {		dz -= boxSize_Z;	}
	    	if(dz < -boxHalf_Z){		dz += boxSize_Z;	}
			#endif
	    r = sqrt(dx * dx + dy * dy + dz * dz);
			rh = r / h;

			/* Compute Normalization Constant. See eqn 6 in Stinson et al. (2006)
				 Use GADGET2, eqn 4 for smoothing kernel
			 */
			if(rh <= 0.5){
				w = 1.0 + 6.0 * (rh * (rh * (rh - 1)));
			}
			if(0.5 < rh && rh <= 1.0){
				w = 2.0 * (1.0 - rh) * (1.0 - rh) * (1.0 - rh);
			}
			if(rh > 1.0){
				w = 0.0;
			}
			w *= 8.0 / (M_PI * h * h * h);

			pressure += SphP[j].Pressure * P[j].Mass * w / SphP[j].Density;		//eqn 4.8 in NMIA
			density  += w * P[j].Mass;					//eqn 5, Springel (2005)			
			norm 		 += w * P[j].Mass;					//eqn 6, Stinson et al. (2006)
		}
	}
	while(startnode >= 0);

	*gaspressure = pressure;
	*gasdensity  = density;
	return (FLOAT)norm;
}







/*****************************************************************************
 This is a comparison kernel for a sort routine, which is used to group
 particles that are going to be exported to the same CPU.
******************************************************************************/
int feedbacknorm_compare_key(const void *a, const void *b)
{
  if(((struct feedbacknormdata_in *) a)->Task < (((struct feedbacknormdata_in *) b)->Task))
    return -1;
  if(((struct feedbacknormdata_in *) a)->Task > (((struct feedbacknormdata_in *) b)->Task))
    return +1;
  return 0;
}







/********************************************************************************** 
	This function physically distributes the metals and change in entropy to the 
	gas particles near the star particles that need an update. 
 **********************************************************************************/
void distribute_metals_and_energy_w_kernal(void)
{
  long long ntot;					//Initial GLOBAL Number of SPH part. needing updated 
	long long ntotleft;			//GLOBAL Num SPH part. left needing updated
  int i;
	int j;									//Index for Number of processors
	int k;
	int n;
	int ngrp;
	int source;							//Index 2go FeedbackDataPartialResult[] to FeedbackDataIn[]
	int place;							//Index to go from FeedbackDataIn[] to P[]
	int maxfill;						//Number of structs that fit in CommBuffer
	int ndone;							//Number of particle that have been updated.
  int level;							//
	int sendTask;						//Index of sending Task
	int recvTask;						//Index of receiving Task
	int nexport;						//Total Number of LOCAL SphP to export
	int numGasFed;					//Number of gas particles fedback
  int * nbuffer;					//Number of particles in buffer.
	int * noffset;					//Given recvTask,rtrn sending partls locatn in CommBuffer
	int * nsend_local;			//Number LOCAL of particles to send to j'th Task
	int * nsend;						//Combination of all Task's nsend_local[]
	int * numlist;					//Holds 'NumStarUpdate' from each task(# SphP needing updated)
	int * ndonelist;
	double dt;
  double tstart, tend, sumt, sumcomm;
  double timecomp = 0, timecommsumm = 0, timeimbalance = 0, sumimbalance;
	MPI_Status status;

	#ifdef PERIODIC
  	boxSize = All.BoxSize;
  	boxHalf = 0.5 * All.BoxSize;
		#ifdef LONG_X
		  boxHalf_X = boxHalf * LONG_X;
		  boxSize_X = boxSize * LONG_X;
		#endif
		#ifdef LONG_Y
		  boxHalf_Y = boxHalf * LONG_Y;
		  boxSize_Y = boxSize * LONG_Y;
		#endif
		#ifdef LONG_Z
  		boxHalf_Z = boxHalf * LONG_Z;
		  boxSize_Z = boxSize * LONG_Z;
		#endif
	#endif

	phys_time = get_phys_time(All.Time);		//Write age of universe (My) to phys_time.

  /* `NumStarUpdate' = number of star particles on this processor that want a 
      force update and will also generate feedback.
			Stars should be at end of P[], but we'll search the whole damn thing anyways.
	 */
  for(n = 0, NumStarUpdate = 0; n < NumPart; n++)				// <- could speed up here
  {
		dt = phys_time - P[n].StarAge;	//Time (My) passes since star formed
    if(P[n].Type == 4 && P[n].Ti_endstep == All.Ti_Current && dt > 5.0)	
		{
			if(P[n].R_II > 0.0 || P[n].R_Ia > 0.0 || P[n].R_SW > 0.0 || P[n].R_AGB > 0.0){
    		NumStarUpdate++;
			}
		}
  }


	//Get Number of Particles needing updated from All tasks
  numlist = malloc(NTask * sizeof(int) * NTask);
  MPI_Allgather(&NumStarUpdate, 1, MPI_INT, numlist, 1, MPI_INT, MPI_COMM_WORLD);
  for(i = 0, ntot = 0; i < NTask; i++)
    ntot += numlist[i];															//Find GLOBAL number stars to update
  free(numlist);


	//Allocate Memory
  noffset 		= malloc(sizeof(int) * NTask);	/* offsets of bunches in common list */
  nbuffer 		= malloc(sizeof(int) * NTask);
  nsend_local = malloc(sizeof(int) * NTask);
  nsend 			= malloc(sizeof(int) * NTask * NTask);
  ndonelist 	= malloc(sizeof(int) * NTask);


  i = N_gas;					/* first non-SPH particle for this task */
  ntotleft = ntot;		/* particles left for all tasks together */


	/*
		Iterate through all GLOBAL Star particles needing updated, until no more GLOBAL
		particles are left to communicate. Find number of nearby gas particles
	*/
	while(ntotleft > 0)
	{
		for(j = 0; j < NTask; j++){
			nsend_local[j] = 0;
		}

		tstart = second();


		/*
			Iterate through LOCAL Stars needing an update. Count number of nearby on LOCAL
			and NON-LOCAL particles. Exportflag[] is set in 
			get_local_number_gas_part_nearby().
		*/
		for(nexport=0, ndone=0; i< NumPart && nexport< All.BunchSizeFeedback - NTask; i++)
		{
			dt = phys_time - P[i].StarAge;	//Time (My) passes since star formed

			/* Only Active Stars over 5My old w/ nearby gas part. */
			if(P[i].Type == 4 && P[i].Ti_endstep == All.Ti_Current && dt > 5.0 && 
				(P[i].R_II > 0.0 || P[i].R_Ia > 0.0 || P[i].R_SW > 0.0 || P[i].R_AGB > 0.0))
			{
				ndone++;

				for(j=0; j<NTask; j++){
					Exportflag[j] = 0;								//Reset list of Tasks to export to
				}

				numGasFed = distribute_feedback_to_nearby_gas_w_kernal(P[i].StarHsml, i, 0);
				P[i].NumGasFed = numGasFed;					//Delete later?			

				for(j=0; j<NTask; j++)
				{
					/* Are Nearby Particles on other Processors? */
					if(Exportflag[j])									//set in get_local_number_gas_part_nearby
					{
						for(k = 0; k < 3; k++)
						{
							FeedbackDataIn[nexport].Pos[k] = P[i].Pos[k];
						}
						for(k = 0; k < NELEMENTS; k++)				//NELEMENTS = 25
						{
							FeedbackDataIn[nexport].Dt_Z_Mass[k] = dt_Z_Mass_dist[k];
						}

						FeedbackDataIn[nexport].DtEnergy = dtEnergy;
						FeedbackDataIn[nexport].DtMass = dtMass;
						FeedbackDataIn[nexport].ID 	 	= P[i].ID;
						FeedbackDataIn[nexport].Task 	= j;
						FeedbackDataIn[nexport].Index = i;
						FeedbackDataIn[nexport].FeedbackNorm = P[i].FeedbackNorm;
						FeedbackDataIn[nexport].StarHsml  = P[i].StarHsml;
						FeedbackDataIn[nexport].NumGasNearby = P[i].NumGasNearby;
						FeedbackDataIn[nexport].R_II	= P[i].R_II;
						FeedbackDataIn[nexport].BlastRadius	= P[i].BlastRadius;

						nexport++;
						nsend_local[j]++;
					}	
				}
			}
		}
		tend = second();
		timecomp += timediff(tstart, tend);

		//Group GasPartSearchDataIn[] by Task to be exported to, makes easier to communicat
		qsort(FeedbackDataIn, nexport, sizeof(struct feedbackdata_in),yielddata_compare_key);

		//Get offset of particles as a function of destination Task
		for(j = 1, noffset[0] = 0; j < NTask; j++){
			noffset[j] = noffset[j - 1] + nsend_local[j - 1];
		}

		tstart = second();

		/*
			Get number of particles to be exported by each Task and to which Task it 
			will be exported.
		*/
		MPI_Allgather(nsend_local, NTask, MPI_INT, nsend, NTask, MPI_INT, MPI_COMM_WORLD);
		
		tend = second();
		timeimbalance += timediff(tstart, tend);


		/* Get Number of nearby particles from NON-LOCAL particles. */
   	for(level = 1; level < (1 << PTask); level++)
	  {
	    tstart = second();
	    for(j = 0; j < NTask; j++)
			{
	      nbuffer[j] = 0;
			}

			//Complex way of sending/receiving particles between Tasks. I don't understand
	    for(ngrp = level; ngrp < (1 << PTask); ngrp++)
	    {
	      maxfill = 0;
	      for(j = 0; j < NTask; j++)
	    	{
					//Get Number of particles in FeedbackDataIn Buffer destined for ______
	    	  if((j ^ ngrp) < NTask)                   //This is bitwise XOR comparison
	    	    if(maxfill < nbuffer[j] + nsend[(j ^ ngrp) * NTask + j])
	    	      maxfill = nbuffer[j] + nsend[(j ^ ngrp) * NTask + j];
	    	}
	      if(maxfill >= All.BunchSizeFeedback)
	      	break;

	      sendTask = ThisTask;
	      recvTask = ThisTask ^ ngrp;

        if(recvTask < NTask)
      	{
      	  if(nsend[ThisTask * NTask + recvTask] > 0 || 
             nsend[recvTask * NTask + ThisTask] > 0)
	        {
	          /* 
							Send and Recieve the particles. Blocks communication  until 
							send buffer is free and receive buffer is filled. The send and
							receive buffers point to different sections of the CommBuffer
						*/
	          MPI_Sendrecv(&FeedbackDataIn[noffset[recvTask]],
				               nsend_local[recvTask] * sizeof(struct feedbackdata_in),MPI_BYTE,
				               recvTask, TAG_FEEDBACK_A, &FeedbackDataGet[nbuffer[ThisTask]],
				               nsend[recvTask * NTask + ThisTask] * sizeof(struct 
											 feedbackdata_in), MPI_BYTE, recvTask, TAG_FEEDBACK_A, 
											 MPI_COMM_WORLD, &status);
	        }
	      }

        for(j = 0; j < NTask; j++)
	        if((j ^ ngrp) < NTask)
	          nbuffer[j] += nsend[(j ^ ngrp) * NTask + j];
      }
			tend = second();
			timecommsumm += timediff(tstart, tend);

			/* now feedback imported particle's data to local particle's */
			tstart = second();
			for(j = 0; j < nbuffer[ThisTask]; j++)
			{
				numGasFed = distribute_feedback_to_nearby_gas_w_kernal(FeedbackDataGet[j].StarHsml,
										 j, 1);			
				FeedbackDataResult[j].Task = FeedbackDataGet[j].Task;
				FeedbackDataResult[j].ID 	 = FeedbackDataGet[j].ID;

				//Total *.NumGasFed should match *.NumGasNearby
				FeedbackDataResult[j].NumGasFed = numGasFed; ///+ FeedbackDataGet[j].NumGasFed;

			}
			tend = second();
			timecomp += timediff(tstart, tend);

      /* do a block to measure imbalance */
      tstart = second();
      MPI_Barrier(MPI_COMM_WORLD);
      tend = second();
      timeimbalance += timediff(tstart, tend);


			/* NOTE : This is unneccessary!! Feel free to delete*/
      /* get the result */
      tstart = second();
      for(j = 0; j < NTask; j++)
        nbuffer[j] = 0;
      for(ngrp = level; ngrp < (1 << PTask); ngrp++)
      {
        maxfill = 0;
        for(j = 0; j < NTask; j++)
	      {
	        if((j ^ ngrp) < NTask)
	          if(maxfill < nbuffer[j] + nsend[(j ^ ngrp) * NTask + j])
	            maxfill = nbuffer[j] + nsend[(j ^ ngrp) * NTask + j];
	      }
        if(maxfill >= All.BunchSizeFeedback)
	        break;

        sendTask = ThisTask;
        recvTask = ThisTask ^ ngrp;

        if(recvTask < NTask)
	      {
	        if(nsend[ThisTask * NTask + recvTask] > 0 || 
             nsend[recvTask * NTask + ThisTask] > 0)
	        {
	          /*  
							Send and Recieve the Results. Blocks communication  until 
							send buffer is free and receive buffer is filled. The send and
							receive buffers point to different sections of the CommBuffer

							FeedbackDataResult        = sending buffer
							FeedbackDataPartialResult = receiving buffer
						*/
	          MPI_Sendrecv(&FeedbackDataResult[nbuffer[ThisTask]],
				              nsend[recvTask * NTask + ThisTask] * 
											sizeof(struct feedbackdata_out), MPI_BYTE, recvTask, 
											TAG_FEEDBACK_B, &FeedbackDataPartialResult[noffset[recvTask]],
				              nsend_local[recvTask] * sizeof(struct feedbackdata_out),
				              MPI_BYTE, recvTask, TAG_FEEDBACK_B, MPI_COMM_WORLD, &status);

	          /* add the result to the particles */
	          for(j = 0; j < nsend_local[recvTask]; j++)
		        {
							/*
								NOTE: FeedbackDataIn[source] and FeedbackDataPartialResult[source] are 
								referring to the same particles.
							*/
		          source = j + noffset[recvTask];					
		          place = FeedbackDataIn[source].Index;

							/* Error Check */
							if(P[place].ID == FeedbackDataPartialResult[source].ID)
							{
								//FeedbackDataIn still has LOCAL numGasFed
								P[place].NumGasFed += FeedbackDataPartialResult[source].NumGasFed;
							}
							else
							{
								fprintf(stderr, "WARNING!!!!\nP[%i].ID != FeedbackDataPartialResult"
																"[%i].ID mismatch!\n", place, source);
								endrun(668);
							}
		        }
	        }
	      }

        for(j = 0; j < NTask; j++)
	        if((j ^ ngrp) < NTask)
	          nbuffer[j] += nsend[(j ^ ngrp) * NTask + j];
      }
	    tend = second();
	    timecommsumm += timediff(tstart, tend);

      level = ngrp - 1;
		}

    //Total up the number of GLOBAL particles to update
    MPI_Allgather(&ndone, 1, MPI_INT, ndonelist, 1, MPI_INT, MPI_COMM_WORLD);
    for(j = 0; j < NTask; j++)
      ntotleft -= ndonelist[j];
	}

  free(noffset);
  free(nbuffer);
  free(nsend_local);
  free(nsend);
  free(ndonelist);
  #ifdef USE_GSL
    //gsl_integration_workspace_free(workspace);
    gsl_integration_glfixed_table_free(gl_table);
  #endif 

  /* collect some timing information */
  MPI_Reduce(&timecomp, &sumt, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
  MPI_Reduce(&timecommsumm, &sumcomm, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
  MPI_Reduce(&timeimbalance, &sumimbalance, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);

	/* Not used */
  if(ThisTask == 0)
  {
    All.CPU_FeedCompWalk += sumt / NTask;
    All.CPU_FeedCommSumm += sumcomm / NTask;
    All.CPU_FeedImbalance += sumimbalance / NTask;
  }


	/* Final Error Check, Be sure Stars feed correct number of particles */
	for(j = N_gas; j < NumPart; j++)
	{
		dt = phys_time - P[j].StarAge;	//Time (My) passes since star formed

    if(P[j].Type == 4 && P[j].Ti_endstep == All.Ti_Current && dt > 5.0 &&
			(P[j].R_II > 0.0 || P[j].R_Ia > 0.0 || P[j].R_SW > 0.0 || P[j].R_AGB > 0.0))
		{
			if(P[j].NumGasFed != P[j].NumGasNearby)
			{
				fprintf(stderr, "ERROR!!!!\n%li != %li\nNumGasFed should ="
							 	" NumGasNearby\nThisTask: %i\nSee P[%i].ID = %i\n"
								"P[%i].Type: %i P[%i].StarAge: %.2f  NumPart: %i   N_gas: %i"
								"\n\n", P[j].NumGasFed, P[j].NumGasNearby, ThisTask, j, 
								P[j].ID, j, P[j].Type, j, P[j].StarAge, NumPart, N_gas);
				endrun(676); 
			}
		}
	}


 tstart = second();
}









/*****************************************************************************
	mode = 0   :  Local    particles, P[]
	mode = 1   :  NonLocal particles, GasPartSearchDataGet[]

	Distribute metals, mass, entropy to nearby particles. I am following 
	Kobayashi, C., 2004, MNRAS, 347, 740. Note that I am going to use slightly 
	different notation b/c the notation in her paper is cumbersome.

	NOTE : I work in M_sun and convert to M_sun/h later.

	NOTE : P[].TurnOffMass is used to determine the most massive stars
				 in the IMF. This gives us upper limits on our integrations.
	
	NOTE : I use 'm_u' as the Current max mass (or last timestep's turn off mass)

	NOTE : When doing integrals w/r/t WD and Type Ia SN, we WANT to keep track of the
				 stellar remnants. So the only source of WD's are 3 M_s < m < 8 M_s. 
 				 ALL the WD's are created BEFORE any Type Ia SN can occur !!!!

	NOTE : I picked m_1l_RG = 0.8 instead of 0.9 so you can run the feedback into the
				 future.

	NOTE : Looking at eqn 21 and 22, K04, I presume eqn 23, 24, 25, 27, 28, and 29 
				 are calculating FRACTIONAL masses!!

	NOTE : I have NOT implemented energy from stellar winds for 8M_s < m < 50M_s b/c
				 R_SW doesn't reach this region. I'm confused about how K04 did this. See
				 eqns 16, 17, and 30.
				 I added this.

	WARNING : I have not considered how updating either *.Entropy or *.DtEntropy for
				 		neighboring particles will affect their time integration...


        m_1l = 3     m_1u = m_2l = 8         m_2u=50                    IMF_MAX = 100
IMF |-------|-------------|---------------------|----------------------------------|
    | M.S.  |   WD Region |  Type II SN Region  |  Direct Collapse to B.H.         |
	  | R.G.  |             |                     |  SW feedback only                |
                           


******************************************************************************/
long distribute_feedback_to_nearby_gas_w_kernal(FLOAT starHsml, int target, int mode)
{
	int startnode				= All.MaxPart;
	int n 							= 0;								//Ngblist[] index
	int i,j;
	long numngb					= 0; 								//Number of "nearby" gas part.
	long num_gas_nearby	= 0;								//Num of gas part w/in 'radius' of P[target]
	long num_gas_fed		= 0;								//Num of gas particles fed
	char element[3];
	double cofactor			= 0.0;							//Distributed fraction, eqn 6, Stinson '06
	double w						= 0.0;							//Smoothing Kernal value at P[j]
	double rh 					= 0.0;							// = r / hsml
	double dx 					= 0.0;
	double dy						= 0.0;
	double dz 					= 0.0;
	double r 						= 0.0;							// | P[target].Pos - P[j].Pos |
	double dt						= 0;								// CurrentTime - star_age (in mega-years)
	double star_age			= 0;								// Universe's age(mega-years)when star formed
	double m_turnoff  	= 0;								// Upper-most mass ON the Main Sequence
	double feedbackNorm = 0.0;							// Feedback Norm., eqn 6 Stinson et al 2006
	double metal_mass_frac = 0.0; 					// Target metal mass fraction
	double z_mass_frac 	= 0;								// Element massfrac, should b const wrt time
	double metal_mass;											// Target metal mass (in internal units)
	double oldMass 			= 0.0;							// This used in update_Zi_mass_frac() 
	double mass 				= 0.0;							// Target mass
	FLOAT *pos;															// Target Position (in internal units)
	double density;													//Target density
	double m_u	 	= 0;											// Most massive stars currently, init = 100
	double upper 	= 0;											// Upper lim passed to integration routines
	double lower 	= 0;											// Lower lim passed to integration routines
	//double upper1 = 0;											// Upper lim passed to integration routines
	//double lower1 = 0;											// Lower lim passed to integration routines
	double e_SW 	= 0;											// Energy injected per Stellar Wind
	double e_II		= 0;											// Energy injected per Type II SN
	double e_Ia		= 0;											// Energy injected per Type Ia SN

	double R_SW				= 0;									// Rate	of SW	per SOLAR mass
	double R_II				= 0;									// Rate of Type II SN per SOLAR mass
	double R_Ia				= 0;									// Rate of Type Ia SN per SOLAR mass
  double R_AGB      = 0;
	double dtMassFrac_SW	= 0;							// Stellar Wind mass fedback.
	double dtMassFrac_II	= 0;
	double dtMassFrac_Ia	= 0;
  double dtMassFrac_AGB = 0;
	double dt_Z_MassFrac_SW[NELEMENTS];			//Metal mass frac due to SW 
	double dt_Z_MassFrac_II[NELEMENTS];			//Metal	mass frac due to Type II
	double dt_Z_MassFrac_Ia[NELEMENTS];			//Metal mass frac due to Type Ia
	double dt_Z_MassFrac_AGB[NELEMENTS];		//Metal mass frac due to Type Ia
	double dtEnergy_phys_II = 0.0;				  //Physical change in energy from TypeII SNe
	double pressure_phys = 0.0;
	double nH						 = 0.0;							//Hydrogen Density
	double blastRadius	 = 0.0;							//Blast radius of SN

	dtEnergy		= 0;
	dtMass			= 0;									
	dtEntropy		= 0;


	/* mode == 0 for local particles, mode == 1 for imported particles*/
	if(mode == 0){
		pos       			= P[target].Pos;
		star_age				= P[target].StarAge;
		metal_mass_frac = P[target].MetalMass / P[target].Mass;
		metal_mass 			= P[target].MetalMass;
		mass						= P[target].origStarMass;
		num_gas_nearby 	= P[target].NumGasNearby;
		feedbackNorm		= P[target].FeedbackNorm;
		m_u 						= P[target].TurnOffMass;
	}
	else{
		pos 						= FeedbackDataGet[target].Pos;
		dtMass					= FeedbackDataGet[target].DtMass;
		dtEnergy				= FeedbackDataGet[target].DtEnergy;
		num_gas_nearby 	= FeedbackDataGet[target].NumGasNearby;
		feedbackNorm		= FeedbackDataGet[target].FeedbackNorm;	
		R_II						= FeedbackDataGet[target].R_II;
		blastRadius			= FeedbackDataGet[target].BlastRadius;

		for(i=0; i<NELEMENTS; i++)
		{
			dt_Z_Mass_dist[i] = FeedbackDataGet[target].Dt_Z_Mass[i];
		}
	}


	//Error Check
	if(NELEMENTS != 25)
	{
		fprintf(stderr, "ERROR!!!! NELEMENTS = %i, in allvars.h, dt_Z_Mass_dist[25]\n"
						"Adjust length of struct feedbackdata_in accordingly\n" , 
						NELEMENTS);
		endrun(675);
	}
	if(mode == 0 && (mass < 0 || star_age < 0 ))
	{
		fprintf(stderr, "ERROR!!!! P[%i].origStarMass = %f, %f\n",target, mass, star_age);
		endrun(676);
	}
	if(num_gas_nearby < 0)
	{
		fprintf(stderr, "ERROR!!! mode : %i   target : %i  num_gas_nearby %li\n", 
						mode, target, num_gas_nearby);
		endrun(677);
	}



	//Get Cosmological factors
 	if(All.ComovingIntegrationOn)
 	{
 	  /* Factors for comoving integration of hydro ..... Recall All.Time = 1/(1+z) */

     #ifndef DARKENERGY
 	      hubble_a = All.Omega0 / (All.Time * All.Time * All.Time)
 	             	+ (1 - All.Omega0 - All.OmegaLambda) / (All.Time * All.Time) + 
 	               All.OmegaLambda;
     #endif

     #ifdef DARKENERGY
         #ifdef SCALARFIELD
            All.dark_factor = get_dark_factor(All.Time);
 	         hubble_a = All.Omega0 / (All.Time * All.Time * All.Time)
 	                	+ (1 - All.Omega0 - All.OmegaLambda) / (All.Time * All.Time) + 
 	                  (All.OmegaLambda * All.dark_factor);
         #endif

         #ifdef DARKPARAM
            All.dark_factor = get_dark_factor(All.Time);
 	         hubble_a = All.Omega0 / (All.Time * All.Time * All.Time)
 	                	+ (1 - All.Omega0 - All.OmegaLambda) / (All.Time * All.Time) + 
 	                  (All.OmegaLambda * All.dark_factor);
         #endif

         #ifdef GCG
            All.dark_factor = get_dark_factor(All.Time);
 	         hubble_a = All.Omega0 / (All.Time * All.Time * All.Time)
 	                	+ (1 - All.Omega0 - All.OmegaLambda) / (All.Time * All.Time) + 
 	                  (All.OmegaLambda * All.dark_factor);
         #endif
     #endif

 	  hubble_a = All.Hubble * sqrt(hubble_a);
		a3 = (All.Time * All.Time * All.Time);
		atime = All.Time;
	}
	else
	{
		atime = hubble_a = a3 = 1.0;
	}



	//Only need to do this calculation on LOCAL task. Export the result to other tasks
	if(mode == 0)
	{
		//Get time.
		dt = phys_time - star_age;			//already found phys_time


		//Get turnoff mass, solve eqn 26, K04 for log(m). Select physical root.
		if(dt > 5.0){											//prevent sqrt(-1)
			m_turnoff = (3.42 - sqrt(3.42*3.42 - 4.0*0.88*(10 - log10(dt * 1.0e6)))) / 1.76;
			m_turnoff = pow(10, m_turnoff);
		}else{
			//No feedback before 5 mega-years
			return 0;
		}


		//Get energy yields. 
		e_SW = 0.2e51 * pow(metal_mass_frac / SolarMetalMassFrac , 0.8);   // ergs/SW
		e_II = 1.4e51 + 0.2e51;		// ergs/SN  (includes from winds eqn 17 K04)
		e_Ia = 1.3e51;						// ergs/SN	

		/************************** Feedback Rates ******************************/
		//get_feedback_rates(target);		//Ran earlier in update_star_hsml()
		R_II  = P[target].R_II;
		R_Ia  = P[target].R_Ia;
		R_SW  = P[target].R_SW;
    R_AGB = P[target].R_AGB;
	



		/***************************  Mass Feedback *****************************/

		/* Stellar Winds */
		if(m_u > m_2u){														//Only have SW if massive stars exist
			upper = m_u;
			lower = dmax(m_turnoff, m_2u);
			dtMassFrac_SW = pow(metal_mass_frac / SolarMetalMassFrac , 0.8) * 
									 		integrate_1_minus_wm_imf(lower, upper);					//eqn 23 in K04
		}else{
			dtMassFrac_SW = 0.0;
		}


		/* Type II SN */
		if(m_2l <= m_u && m_u <= m_2u){	
			upper = m_u;
			lower = dmax(m_turnoff, m_2l);
			dtMassFrac_II = integrate_1_minus_wm_imf(lower, upper);					//eqn 24 in K04
		}
		else if(m_turnoff < m_2u && m_u > m_2u){			//Cross SW -> Type II region
			upper = m_2u;
			lower = dmax(m_turnoff, m_2l);
			dtMassFrac_II = integrate_1_minus_wm_imf(lower, upper);
		}else{
			dtMassFrac_II = 0.0;
		}	


		/* Type Ia SN */
		dtMassFrac_Ia = WhiteDwarfMass * R_Ia;														//eqn. 25 in K04


    /* AGB and sAGB */
    if(m_u < m_2l){
      upper = m_u;
      lower = dmax(m_turnoff, m_1l_RG);
      dtMassFrac_AGB = integrate_1_minus_wm_imf(lower, upper);
    }
    else if(m_turnoff < m_2l && m_u > m_2l){    //Cross Type II -> AGB region
      upper = m_2l;
      lower = dmax(m_turnoff, m_1l_RG);
      dtMassFrac_AGB = integrate_1_minus_wm_imf(lower, upper);
    }else{
      dtMassFrac_AGB = 0.0;
    }




		/************************** Metal Feedback ******************************
			NOTE : I believe that eqn 27 in K04 is wrong. It doesn't make sense to 
						 include p_zmII when in the paragraph below she states that no
						 elements are produced. I fixed my stellar winds below accordingly.
		*/
		for(i=0; i<NELEMENTS; i++)												//Iterate through metals
		{
			dt_Z_MassFrac_SW[i]  = 0;
			dt_Z_MassFrac_II[i]  = 0;
			dt_Z_MassFrac_Ia[i]  = 0;
			dt_Z_MassFrac_AGB[i] = 0;
			dt_Z_Mass_lost[i]		 = 0;

			if(MetalsPresent[i].Present == 1)
			{
				strcpy(element, MetalsPresent[i].Element);
				z_mass_frac = get_Zi_mass_frac(target, element, mode);


				/* Stellar Winds */
				if(m_u > m_2u){
					upper = m_u;
					lower = dmax(m_turnoff, m_2u);
					dt_Z_MassFrac_SW[i] = pow(metal_mass_frac / SolarMetalMassFrac, 0.8) * 
												 	z_mass_frac * integrate_1_minus_wm_imf(lower, upper);			
																																			//eqn. 27 in K04
																																
					dt_Z_Mass_lost[i] += mass * dt_Z_MassFrac_SW[i];
				}else{
					dt_Z_MassFrac_SW[i] += 0;
				}


				/* Type II SN */
				if(m_2l <= m_u && m_u <= m_2u){
					upper = m_u;
					lower = dmax(m_turnoff, m_2l);
					dt_Z_MassFrac_II[i] = integrate_pzII_imf(element, lower, upper) + 	
													integrate_1_minus_wm_minus_pzII_Z_imf(element,
													z_mass_frac, lower, upper);									//eqn. 28 in K04

					dt_Z_Mass_lost[i] += mass * z_mass_frac * integrate_1_minus_wm_imf(lower, 
                               upper);	
				}
				else if(m_turnoff < m_2u && m_u > m_2u){
					upper = m_2u;
					lower = dmax(m_turnoff, m_2l);
					dt_Z_MassFrac_II[i] = integrate_pzII_imf(element, lower, upper) + 	
													integrate_1_minus_wm_minus_pzII_Z_imf(element,
													z_mass_frac, lower, upper);									//eqn. 28 in K04

					dt_Z_Mass_lost[i] += mass * z_mass_frac * integrate_1_minus_wm_imf(lower, 
                               upper);	
				}else{
					dt_Z_MassFrac_II[i] = 0;
				}


				/* Type Ia SN */
				dt_Z_MassFrac_Ia[i] = WhiteDwarfMass * TypeIa_Zi_yield_mass_frac(element) * 
															R_Ia;																		//eqn. 29 in K04
				dt_Z_Mass_lost[i] += mass * dtMassFrac_Ia * z_mass_frac;


        /* AGB and sAGB */
        if(m_u < m_2l){
          upper = m_u;
          lower = dmax(m_turnoff, m_1l_RG);
					dt_Z_MassFrac_AGB[i] =  z_mass_frac * integrate_1_minus_wm_imf(lower, upper);

					dt_Z_Mass_lost[i] += mass * dt_Z_MassFrac_AGB[i];
        }
        else if(m_turnoff < m_2l && m_u > m_2l){    //Cross Type II -> AGB region
          upper = m_2l;
          lower = dmax(m_turnoff, m_1l_RG);
					dt_Z_MassFrac_AGB[i] =  z_mass_frac * integrate_1_minus_wm_imf(lower, upper);

					dt_Z_Mass_lost[i] += mass * dt_Z_MassFrac_AGB[i];
        }else{
          dt_Z_MassFrac_AGB[i] = 0;
        }
			}
		}



		/******************** Collect results and give actual masses ********************/
		/*
			Here note that we can leave dt_Z_Mass[] and dtMass in terms of internal mass 
			units because they are mass fractions. HOWEVER, R_SW, R_II, R_Ia are the number 
			of SW and SN per SOLAR mass. This means we must convert to internal mass to 
			solar mass.

			dtEnergy units = (internal energy / internal mass).
		*/
		for(i=0; i<NELEMENTS; i++)
		{
			dt_Z_Mass_dist[i] = mass * (dt_Z_MassFrac_SW[i] + dt_Z_MassFrac_II[i] + 
												 dt_Z_MassFrac_Ia[i] + dt_Z_MassFrac_AGB[i]);	//eqn. 22 in K04
		}
		dtMass = mass * (dtMassFrac_SW + dtMassFrac_II + dtMassFrac_Ia + 
                     dtMassFrac_AGB);                                 //eqn. 21 in K04
		dtEnergy = All.UnitMass_in_g / (SOLAR_MASS * All.UnitEnergy_in_cgs) * 
							 (e_SW * R_SW + e_II * R_II + e_Ia * R_Ia);							//eqn. 16 in K04
	

		/* Compute Blast Wave radius. Eqn 9 in Stinson et al 2006. Recall that pressure
       has a goofy-ass hybrid of comoving and cartesian coordinates and that 
       P_code = P_cart * a^(3*GAMMA). */			
		if(All.ComovingIntegrationOn)
    {
			dtEnergy_phys_II = mass * e_II * R_II * All.UnitMass_in_g / 
                         (All.HubbleParam * SOLAR_MASS);
			pressure_phys = P[target].GasPressure * All.UnitPressure_in_cgs *
										pow(All.HubbleParam, 2) / pow(All.Time, 3*GAMMA);
			nH = P[target].GasDensity / pow(All.Time, 3) * All.UnitDensity_in_cgs * 
					 HYDROGEN_MASSFRAC * pow(All.HubbleParam, 2) / PROTONMASS;
		}
    else
    {
			dtEnergy_phys_II = mass * e_II * R_II * All.UnitMass_in_g / SOLAR_MASS;
			pressure_phys = P[target].GasPressure * All.UnitPressure_in_cgs;
			nH = P[target].GasDensity * All.UnitDensity_in_cgs * HYDROGEN_MASSFRAC 
					 / PROTONMASS;
		}
		//Blast Radius is in physical pc.
		blastRadius = pow(10, 1.74) * pow(dtEnergy_phys_II / 1.0e51, 0.32) * 
									pow(nH, -0.16) * pow(pressure_phys * 1.0e-4 / BOLTZMANN, -0.20);

		//Convert pc -> comoving cm -> comoving internal units.
		if(All.ComovingIntegrationOn){
			blastRadius *= (CM_PER_PC * All.HubbleParam) / (All.UnitLength_in_cm * All.Time);
		}else{
			blastRadius *= (CM_PER_PC) / (All.UnitLength_in_cm);
		}

		P[target].R_II				= R_II;		//Save for exported SN feedback
		P[target].BlastRadius = blastRadius;

		/* Update star particle mass and metal mass. We can do that now.*/
		oldMass = P[target].Mass;
		P[target].DtMass = 0;													//Already subtracting mass, -dtMass;
		P[target].Mass -= dtMass;											//Update Total mass
		update_Zi_mass(target, dt_Z_Mass_lost, -1.0);	

	}/* endif mode == 0 */




	/* Get number of LOCAL particles within 'radius' of the P[target]'s location */
	startnode	= All.MaxPart;
	do
	{
		numngb =	ngb_treefind_variable(&pos[0], starHsml, &startnode);

		/* from Ngblist find particles w/in 'hsml' */
		for(n = 0; n < numngb; n++)
		{
			j = Ngblist[n];										//Get index of 'nearby' gas part.

			dx = pos[0] - P[j].Pos[0];	
			dy = pos[1] - P[j].Pos[1];
			dz = pos[2] - P[j].Pos[2];

			#ifdef PERIODIC			/*  find the closest image in the given box size  */
	    	if(dx > boxHalf_X) { 		dx -= boxSize_X; 	}
	   		if(dx < -boxHalf_X){		dx += boxSize_X;	}
	    	if(dy > boxHalf_Y) {		dy -= boxSize_Y;	}
	    	if(dy < -boxHalf_Y){		dy += boxSize_Y;	}
	    	if(dz > boxHalf_Z) {		dz -= boxSize_Z;	}
	    	if(dz < -boxHalf_Z){		dz += boxSize_Z;	}
			#endif
	    r = sqrt(dx * dx + dy * dy + dz * dz);

			//Within Feedback Radius?
			if( r <= starHsml)
			{
				rh = r / starHsml;

				if(rh < 0.5){
					w = 1.0 + 6.0 * (rh * (rh * (rh - 1)));
				}
				if(0.5 < rh && rh <= 1.0){
					w = 2.0 * (1.0 - rh) * (1.0 - rh) * (1.0 - rh);
				}
				if(rh > 1.0){
					fprintf(stderr, "ERROR!!! r/h > 1.0 should NEVER happen Here!!\n");
					endrun(1001);
				}

				w *= 8.0 / (M_PI * starHsml * starHsml * starHsml);
				cofactor = P[j].Mass * w / feedbackNorm;		//eqn 6 in Stinson et al. 2006


				oldMass = P[j].Mass;
				density = SphP[j].Density;

				/* 
					Now convert dtEnergy into dtEntropy (dA/dt). See initialization of 
					SphP[].Entropy in read.ic and init.c for motivation. Note that dtEnergy is in
					(ergs/internal mass units). That is why I didn't multiply by 'mass' above. 
					Also, account for dA/dt * dt/da = dA/da. 

					convert this particles' fraction of dtEnergy to its change in dtEntropy
				*/
				dtEntropy = (dtEnergy * cofactor) * GAMMA_MINUS1 / 
										pow(density / a3, GAMMA_MINUS1);
				dtEntropy = dtEntropy /(hubble_a * atime) * atime;//dA/dt -> dA/da -> dA/dloga



				//Turn off cooling ?
				if(r < blastRadius && R_II > 0.0)
				{
					SphP[j].cooling_on = 0;
					SphP[j].time_turned_off = phys_time;
				}


	
				/************************** DIAGNOSTIC OPTIONS *****************************/
				/* These options are computationally very wasteful. Since they are suppose to 
					 be diagnostics, I didn't want to waste the time and risk introducing bugs
					 by trying to make it efficient. Essentially all the regular feedback 
					 computations are done and not used. I forced these options to be exclusive
					 in printOptionsEnabled() */
				#ifdef ADIABATIC_SWITCH_ON_4_ALL_NGB_GAS
				if(R_II > 0.0)
				{
					SphP[j].cooling_on = 0;
					SphP[j].time_turned_off = phys_time;
				}
				#endif
				#ifdef METAL_FEEDBACK_ONLY
					dtEntropy = 0;
					SphP[j].cooling_on = 1;
				#endif
				#ifdef TURNOFF_ADIABATIC_SWITCH
					SphP[j].cooling_on = 1;
				#endif	

				//Insurance on Preventing dangerous situation. These options are exclusive.
				#if defined(ADIABATIC_SWITCH_ON_4_ALL_NGB_GAS) && defined(METAL_FEEDBACK_ONLY)
					fprintf(stderr, "ERROR!!! ADIABATIC_SWITCH_ON_4_ALL_NGB_GAS && "
									"METAL_FEEDBACK_ONLY simultaneously enabled\n");
					endrun(5023);
				#endif
				#if defined(ADIABATIC_SWITCH_ON_4_ALL_NGB_GAS) && defined(TURNOFF_ADIABATIC_SWITCH)
					fprintf(stderr, "ERROR!!! ADIABATIC_SWITCH_ON_4_ALL_NGB_GAS && "
									"TURNOFF_ADIABATIC_SWITCH simultaneously enabled\n");
					endrun(5024);
				#endif
				#if defined(METAL_FEEDBACK_ONLY) && defined(TURNOFF_ADIABATIC_SWITCH)
					fprintf(stderr, "ERROR!!! METAL_FEEDBACK_ONLY && TURNOFF_ADIABATIC_SWITCH "
									"simultaneously enabled\n");
					endrun(5022);
				#endif
				/*********************** END DIAGNOSTIC OPTIONS ***************************/
		

	

				/* Actually update feedback. Update P[j].Mass AFTER all the feedback has 
					 been gotten. If it is updated here, it will fuck up the cofactor
					 b/c the cofactor will not be the same as when feedbackNorm
					 was computed back in get_feedback_normalization() */
				SphP[j].DtEntropy += dtEntropy;
				P[j].DtMass += dtMass * cofactor;				//Will add to P[j].Mass later
				//P[j].Mass			 += dtMass * cofactor;
				update_Zi_mass(j, dt_Z_Mass_dist, cofactor);

				num_gas_fed++;
			}
		}
	}
	while(startnode >= 0);


	/* 
		Conserve metals & mass in stars w/o any nearby gas particles. 
		Entropy is not conserved 
	*/
	if(mode == 0 && P[target].NumGasNearby == 0 )
	{
		fprintf(stderr, "ERROR!!! Task %i, P[target].NumGasNearby Should NEVER = 0!!!!\n"
						"See P[%i].ID = %i", ThisTask, target, P[target].ID); 
		endrun(1002);
	}



	//Check for correctness
	if(mode == 0){
		P[target].TurnOffMass = m_turnoff;
	}

	return num_gas_fed;
}






/*****************************************************************************
 This is a comparison kernel for a sort routine, which is used to group
 particles that are going to be exported to the same CPU.
******************************************************************************/
int gaspartsearch_compare_key(const void *a, const void *b)
{
  if(((struct gaspartsearchdata_in *) a)->Task < (((struct gaspartsearchdata_in *) b)->Task))
    return -1;
  if(((struct gaspartsearchdata_in *) a)->Task > (((struct gaspartsearchdata_in *) b)->Task))
    return +1;
  return 0;
}



/*****************************************************************************
 This is a comparison kernel for a sort routine, which is used to group
 particles that are going to be exported to the same CPU.
******************************************************************************/
int yielddata_compare_key(const void *a, const void *b)
{
  if(((struct feedbackdata_in *) a)->Task < (((struct feedbackdata_in *) b)->Task))
    return -1;
  if(((struct feedbackdata_in *) a)->Task > (((struct feedbackdata_in *) b)->Task))
    return +1;
  return 0;
}






/*********************************************************************************
	 Initial Mass Function (IMF). 
	
	:::NOTE:::  
	If you want to change the IMF, you'll need to fix the normalization 
	factor (0.1313) and the exponent (1.35). This is normalized for 0.1 < m < 100 

	Note that we are using the definition of the IMF used "Galaxy Formation and 
	Evolution" by Mo et al. See his eqn 9.34. We assume \int(\phi * m dm) and 
	we build in the extra 'm' in the exponent.

  This IMF is really = m * \phi(m).
  
  If your IMF is piecewise, be sure that it is continuous at the junctures!!!!
*********************************************************************************/
double imf(double mass)									
{
	//See Baldry & Glazebrook (2003) for Salpeter A IMF. See eqn 3.
	#ifdef SAL_A_IMF
	if(mass > 0.5){
		return 0.2231820829 * pow(mass, -1.35);  //For units M_sun
	}else{
		return 0.4022859113 * pow(mass, -0.5);
	}
	#elif defined(CHABRIER_IMF)
  //See Vogelsberger 2013, eqn 3 and Chabrier 2003, Table 2.
  if(mass > 1.0){
    return 0.2379119496 * pow(mass, -1.3);
  }else{
    return 0.8524635577 * exp(-pow(log10(mass / 0.079), 2.0) * 1.05019953);
  }
  #else
		return 0.1716363709 * pow(mass, -1.35);  //For units M_sun
	#endif
}





/*********************************************************************************
	Here is how we determine the mass of the remnant (m_rem) of a star with mass M.
  We use this for the integrals. Here are some useful references. Values for 
	Black Hole masses are like assholes, everyone's got one. H02 and E04 have the 
	best, most comprehensible data on stellar remnants. E04 contains metallicity 
	dependent information on stellar remnants. 

  NOTES (before adding Solar Metallicity): 
    1. I don't think the AGB regime is ever reached for this function b/c I don't
       include any R_AGB that calls this function.
    2. H02 assumes 0 metallicity, which means that conflicts with eqn 23
       and eqn 27 in K04. The remnant mass frac should be for Z_solar metallicity.
    3. Iben & Renzini regime won't work at 8M_sun b/c the remnant will exceed the 
       Chandrasekar limit.

  LOW METALLICITY REFS.
  ---------------------
	K06 : Kobayashi, 2006 ApJ 653, 1145
	K04 : Kobayashi, 2004 MNRAS 347, 740................m > 50, all He-core mass = B.H.
	H02 : Heger, 2002 Springer-Verlag, 369............. see Fig. 1.
  I83 : Iben & Renzini 1983.......................... see p298 pick eta = 1.

  SOLAR METALLICITY REFS
  ----------------------
	E04 : Eldridge & Tout, 2004 MNRAS, 353, 87  
  M01 : Marigo 2001, A&A 370, 194...............see Figure 8 for AGB & sAGB regime
  W95 : Woosley & Weaver, 1995..................see Figure 7 for Type II regime
  P98 : Portinari et al. 1998, ApJS 101, 181....see Figure 8 for B.H. regime

			Value                    ||           Regime            ||      Reason
	===========================================================================
1.	m_rem = 1                    (0.1  M_sun < M < 0.8 M_sun)      On M.S. > 13.6 Gy
2.	m_rem = -0.58*M + 1.13       (0.80 M_sun < M < 1.0 M_sun)      M01, Fig 8
3. 	m_rem = -0.058*M + 0.608     (1.0  M_sun < M < 8.0 M_sun)      M01, Fig 8
4.	m_rem = -4.49e-3*M +0.18	   (8.0  M_sun < M < 25  M_sun)			 K06, Table 1
5.  m_rem = -8.2e-4*M + 0.0881   (25.0 M_sun < M < 40 M_sun)       K06, Table 1
6.  m_rem = -5.68e-4*M + 7.8e-2  (40.0 M_sun < M < 100 M_sun)      P98, Fig 8
	===========================================================================
**********************************************************************************/
double remnant_mass_frac(double mass)
{
	double m_rem = 0.0;

	/* (0.1 M_sun < M <= 0.8 M_sun)   */      //Low mass M.S. stars
	if(mass <= m_1l_RG)
	{
		m_rem = 1;               

    fprintf(stderr, "ERROR?!?!? Something weird happened. You managed to evolve \n"
            "star particle for ~ 21Gyr. For a normal cosmological simulation, that \n"
            "is simply too damn long. If you really want to do this and do it  \n"
            "correctly, you will need to adjust the remnant mass fraction and \n" 
            "possibly other functions to include the new region. \n\n\n"
            "If this was NOT your intention,then something has gone terribly wrong\n");
    endrun(1111);
	}


	/* (0.80 M_sun < M <= 1.0 M_sun)   */     //W.D. and companions, Type Ia SN
	if(m_1l_RG < mass && mass <= m_sun)
	{
		m_rem = -0.58 * mass + 1.13;
	}
	
	/* (1.0 M_sun < M <= 8.0 M_sun)   */     //W.D. and companions, Type Ia SN
	if(m_sun < mass && mass <= m_2l)
	{
		m_rem = -0.058 * mass + 0.608;
	}

	/* (8.0 M_sun < M <= 25 M_sun)     */     //Type II SN
	if(m_2l < mass && mass <= m_2l_bh)
	{
		m_rem = -0.00449 * mass + 0.18;
	}

	/* (25 M_sun < M <= 40 M_sun)      */     //Type II SN
	if(m_2l_bh < mass && mass <= m_2u)
	{
		m_rem = -8.2e-4 * mass + 0.0881;
	}

	/* (40 M_sun < M <= 100 M_sun)     */
	if(m_2u < mass && mass <= IMF_MAX)	      //Winds blow off most mass.Collapse to  BH
	{
		m_rem = -5.683e-4 * mass + 7.8e-2;
	}

	return m_rem;
}


#ifdef USE_SIMPSON
/*********************************************************************************
This function is usefull in breaking up the Simpson's Rule integration so that it
doesn't try to interpolate across the discrete boundaries in remnant_mass_frac().
This increases its overall accuracy.

Given a mass, this function will return the lower bound of the current step of
remnant_mass_frac().
*********************************************************************************/
double lower_bound_in_cur_step_of_remnant_mass_frac(double mass)
{
	/* (0.1 M_sun < M <= 0.8 M_sun)   */       //Low mass M.S. stars
	if(mass <= m_1l_RG){
	  return IMF_MIN;
	}
	
	/* (0.80 M_sun < M <= 1.0 M_sun)   */       //W.D. and companions, Type Ia SN
	if(m_1l_RG < mass && mass <= m_sun){
		return m_1l_RG;
	}

	/* (1.0 M_sun < M <= 8.0 M_sun)   */       //W.D. and companions, Type Ia SN
	if(m_sun < mass && mass <= m_2l){
		return m_sun;
	}

	/* (8.0 M_sun < M <= 25 M_sun)     */       //Type II SN
	if(m_2l < mass && mass <= m_2l_bh){
		return m_2l;
	}

	/* (25 M_sun < M <= 40 M_sun)      */       //Type II SN
	if(m_2l_bh < mass && mass <= m_2u){
		return m_2l_bh;
	}

	/* (40 M_sun < M <= 100 M_sun)     */				//Direct collapse to B.H.
	if(m_2u < mass && mass <= IMF_MAX){		
    return m_2u;
	}

  fprintf(stderr, 
          "ERROR!!! Something went wrong in lower_step_in_remnant_mass_frac()\n"
          "a lower step could not be found!\n");
  endrun(5021);

  /* Should NEVER get here! Hopefully large number would lead to epic failure*/
  return -1e100;   
}
#endif




/**********************************************************************************
	This linearly interpolates the yields of the table Type_II_SN. If your table has 
	different points you will DEFINITELY need to change this function. You will also 
	need to change 'sn_yield' in allvars.h.

	Returns mass fraction of metal yield
	
	If mass < 13 M_s, we pick the same mass fraction as 13 M_s.  
  CORRECTION : Nomoto 1997, p80c section 3, says that they assume no
  heavy element production below 10 M_sun and they interpolate between 10 - 13 M_sun

  All other values are interpolated. Yield = 0 for masses outside Type II SN range
  [m_2l, m_2u] which is usually [8,40].
 *********************************************************************************/
double TypeII_Zi_yield_mass_frac(char element[3], double mass)
{
	int i = -1;
	int j = 0;												//Index to iterate through *.Yields[]
	int mIndex = 0;										//Mass Index
	double x1,y1;											//Lower mass and yield for interpolation
	double x2,y2;											//Upper mass and yield for interpolation
	double m;													//Slope for interpolation
	double b;													//y-intercept for interpolation
	double zMassFrac  = 0;

	
	//Enforce limits for Type II SN explosion.  8 M_s < mass < 40 M_s
	if(mass < m_2l || mass > m_2u)
	{
		return 0;
	}
	i = find_SN_Table_index(element, 2);	


  //Mass outside interpolation range
  if(mass < SNIIMassTable[0])
  {
    //No metals produced. See Nomoto 1997, p80, he assumes m_ecsn = 10.
    if(mass <= m_ecsn)
    {
      zMassFrac = 0;
      return zMassFrac;
    }

    //If 10 < mass < 13, interpolate
    if(mass > m_ecsn)
    {
      //Get Points
      x1 = m_ecsn;
      x2 = SNIIMassTable[0];
      y1 = 0;
      y2 = Type_II_SN[i].Yield[0]/x2;
  
      //Interpolate
      m = (y2 - y1) / (x2 - x1);
	    b = y1 - m*x1;
	    zMassFrac = m * mass + b;
      return zMassFrac;
    }
  }



	//Mass w/in Interpolate table... Find lower bound, mIndex.
	j = 0;
	while(j < NMasses - 1)
	{
		//No need to interpolate.
		if(mass == SNIIMassTable[j])
		{
			zMassFrac = Type_II_SN[i].Yield[j] / SNIIMassTable[j];
			return zMassFrac;
		}
		mIndex = mass >= SNIIMassTable[j] ? j : mIndex;
		j++;
	}

	//Get points to interpolate, y = mx + b
	x1 = SNIIMassTable[mIndex];
	x2 = SNIIMassTable[mIndex + 1];
	y1 = Type_II_SN[i].Yield[mIndex] / x1;
	y2 = Type_II_SN[i].Yield[mIndex + 1] / x2;				

	//Interpolate
	m = (y2 - y1) / (x2 - x1);
	b = y1 - m*x1;
	zMassFrac = m * mass + b;

	return zMassFrac;
}



#ifdef USE_SIMPSON
/*********************************************************************
  This will help in breaking up Simpson's Rule integration from 
  integrate_pzII_imf() so it doesn't try to interpolate between discrete
  boundaries in TypeII_Zi_yield_mass_frac(). This will increase the
  overall accuracy.

  Given a mass, this function will return the lower bound of the current
  step of TypeII_Zi_yield_mass_frac().
**********************************************************************/
double lower_bound_in_cur_step_TypeII_Zi_yield(double mass)
{
  int j = 0;
  int mIndex = 0;
  double mass_returned = 0;

  //See which table value is immediately below 'mass'
  while(j < NMasses - 1)
  {
    mIndex = mass >= SNIIMassTable[j] ? j : mIndex;
    j++;
  }

  //If None, do this
  if(mass < SNIIMassTable[0]){
    if(mass < m_2l){
      fprintf(stderr, "ERROR!!! In lower_bound_in_cur_step_TypeII_Zi_yield() \n"
              "mass (%.3f) < m_2l (%.3f). This should never happen if the algorithm\n"
              "is correct. You can't have a TypeII SN yield BELOW the minimum TypeII\n"
              "SN mass (%.3f)\n", mass, m_2l, m_2l);
       endrun(5021);
    }
    if(mass > m_ecsn){
      mass_returned = m_ecsn;
    }else{
      mass_returned = m_2l;
    }
  }else{
    mass_returned = SNIIMassTable[mIndex];
  }
  return mass_returned;
}
#endif




/********************************************************************************
	Returns mass fraction of Type Ia metal yield
 ********************************************************************************/
double TypeIa_Zi_yield_mass_frac(char element[3])
{
	int i = 0;
	double zMassFrac = 0.0;
	i = find_SN_Table_index(element, 1);
	
	zMassFrac = Type_Ia_SN[i].Yield[0] / WhiteDwarfMass;

	return zMassFrac;
}





/***********************************************************************************
	Integrand used in eqn. 23 and 24 in K04. Use Trapezoidal Rule which is good enough.
	Can use GSL integration if desired. 

  integrate[(1-remn_mass_frac)*phi*dm]
************************************************************************************/
double integrate_1_minus_wm_imf(double lower, double upper)
{
	double area 	= 0.0;

  //default is trapezoidal method
  #if !defined(USE_GSL) && !defined(USE_SIMPSON)
  	int n		 	 		= N_INTEGRATION_STEPS;
  	int i 				= 0;
  	double dx 	 	= (upper - lower)/((float)n);
  	double x_i		= 0.0;
  	double x_j		= 0.0;
  	double y_i		= 0.0;
  	double y_j		= 0.0;

    x_i = lower;
    x_j = lower + dx;

    for(i=0; i<n; i++)
    {
		  y_i = (1 - remnant_mass_frac(x_i)) * imf(x_i);
		  y_j = (1 - remnant_mass_frac(x_j)) * imf(x_j);

		  area += (x_j - x_i) * y_i + 0.5 * (x_j - x_i) * (y_j - y_i);

		  x_i = x_j;
		  x_j += dx;
	  }
  #endif


  //Simpson's rule, section and theorem 4.4 in Numerical Analysis by Burden & Faires.
  #ifdef USE_SIMPSON
  	int n		  	 		= N_INTEGRATION_STEPS;
  	int i           = 0;
    int cont        = 1;      //flag to continue do-while loop.
    double x        = 0;
    double fx_low   = 0;      //Lower end point, f(lower)
    double fx_hi    = 0;      //Upper end point, f(upper)
    double dx       = 0; 
    double sum_fx_odd   = 0;      //summation of f(x_{2i-1})
    double sum_fx_even  = 0;      //summation of f(x_{2i})
    double sum_end_pts  = 0;
    double lower_bnd = 0;    //lower bound of current step.
    double upper_bnd = 0;    //lower bound of current step.

    upper_bnd = upper;
    lower_bnd = lower_bound_in_cur_step_of_remnant_mass_frac(upper);
    lower_bnd = dmax(lower, lower_bnd);


    /* Break up integral along discrete transisions in remnant_mass_frac().
     Otherwise it unneccessarily interpolates across transitions, reducing overall
     accuracy.*/
    do
    {
      //get endpoints
      fx_low = (1 - remnant_mass_frac(lower_bnd)) * imf(lower_bnd);
      fx_hi  = (1 - remnant_mass_frac(upper_bnd)) * imf(upper_bnd);
      sum_end_pts = fx_low + fx_hi;
      dx = (upper_bnd - lower_bnd) / ((float)n);
 
      for(i=1; i<n; i++)
      {
        x = lower_bnd + i * dx;

        if(i%2 == 0){
          sum_fx_even += (1 - remnant_mass_frac(x)) * imf(x);
        }else{
          sum_fx_odd += (1 - remnant_mass_frac(x)) * imf(x);
        }
      }
     
      //Move to the next step if needed 
      if( (lower_bnd - lower)/ lower > 1e-7){
        upper_bnd = lower_bnd;
        lower_bnd = lower_bound_in_cur_step_of_remnant_mass_frac(lower_bnd - 
                    lower_bnd * 1e-7);
        lower_bnd = dmax(lower, lower_bnd);
        cont = 1;
      }else{
        cont = 0;
      }

      area += dx * (sum_end_pts + 2.0 * sum_fx_even + 4.0 * sum_fx_odd) / 3.0;
      sum_fx_even = 0;
      sum_fx_odd = 0;

    }while(cont);

  #endif


  #ifdef USE_GSL
    F.function = &integrate_1_minus_wm_imf_gsl;
    area = gsl_integration_glfixed(&F, lower, upper, gl_table);
    /*int status;
    double abserr;        
    status = gsl_integration_qag(&F, lower, upper, 0.0, 1.0e-6,
                        WORKSIZE, GSL_INTEG_GAUSS15, workspace, &area, &abserr);
    */
  #endif

  return area;
}





#ifdef USE_GSL
/***********************************************************************************
Companion function integrate_1_minus_wm_imf() when GSL integration is desired
***********************************************************************************/
double integrate_1_minus_wm_imf_gsl(double mass, void *param)
{
  return (1 - remnant_mass_frac(mass)) * imf(mass);
}
#endif





/************************************************************************************
	Integrand used in eqn. 27 and 28 in K04. Use Trapezoidal Rule which is good enough.
  Can use GSL integration if desired.

	integrate[(1 - remn_mass_frac - metal_yield_mass_frac) * Z * phi * dm]
*************************************************************************************/
double integrate_1_minus_wm_minus_pzII_Z_imf(char element[3], double zMassFrac, 
																						 double lower, double upper)
{
	double area	= 0.0;

  //default is trapezoidal method
  #if !defined(USE_GSL) && !defined(USE_SIMPSON)
    int n 			= N_INTEGRATION_STEPS;
    int i				= 0;
    double dx		= (upper - lower) / ((float)n);
    double x_i	= 0.0;
    double x_j	= 0.0;
    double y_i	= 0.0;
    double y_j	= 0.0;

    x_i = lower;
    x_j = lower + dx;

    for(i=0; i<n; i++)
    {
      y_i = (1 - remnant_mass_frac(x_i) - 
             TypeII_Zi_yield_mass_frac(element, x_i)) * zMassFrac * imf(x_i);
      y_j = (1 - remnant_mass_frac(x_j) - 
             TypeII_Zi_yield_mass_frac(element, x_j)) * zMassFrac * imf(x_j);
		
      area += (x_j - x_i) * y_i + 0.5 * (x_j - x_i) * (y_j - y_i);
		
      x_i = x_j;
      x_j += dx;
    }
  #endif

  //Simpson's rule, section and theorem 4.4 in Numerical Analysis by Burden & Faires.
  #ifdef USE_SIMPSON
  	int n		  	 		= N_INTEGRATION_STEPS;
  	int i           = 0;
    int cont        = 1;      //flag to continue do-while loop.
    double x        = 0;
    double fx_low   = 0;      //Lower end point, f(lower)
    double fx_hi    = 0;      //Upper end point, f(upper)
    double dx       = 0; 
    double sum_fx_odd   = 0;      //summation of f(x_{2i-1})
    double sum_fx_even  = 0;      //summation of f(x_{2i})
    double sum_end_pts  = 0;
    double lower_bnd = 0;    //lower bound of current step.
    double upper_bnd = 0;    //lower bound of current step.

    upper_bnd = upper;
    lower_bnd = lower_bound_in_cur_step_of_remnant_mass_frac(upper);
    lower_bnd = dmax(lower, lower_bnd);


    /* Break up integral along discrete transisions in remnant_mass_frac().
     Otherwise it unneccessarily interpolates across transitions, reducing overall
     accuracy. */
    do
    {
      //get endpoints
      fx_low = (1 - remnant_mass_frac(lower_bnd) - 
             TypeII_Zi_yield_mass_frac(element, lower_bnd)) * zMassFrac*imf(lower_bnd);
      fx_hi  =  (1 - remnant_mass_frac(upper_bnd) - 
             TypeII_Zi_yield_mass_frac(element, upper_bnd)) * zMassFrac*imf(upper_bnd);
      sum_end_pts = fx_low + fx_hi;
      dx = (upper_bnd - lower_bnd) / ((float)n);
 
      for(i=1; i<n; i++)
      {
        x = lower_bnd + i * dx;

        if(i%2 == 0){
          sum_fx_even += (1 - remnant_mass_frac(x) - 
                          TypeII_Zi_yield_mass_frac(element, x)) * zMassFrac * imf(x);
        }else{
          sum_fx_odd += (1 - remnant_mass_frac(x) - 
                         TypeII_Zi_yield_mass_frac(element, x)) * zMassFrac * imf(x);
        }
      }
     
      //Move to the next step if needed 
      if( (lower_bnd - lower)/ lower > 1e-7){
        upper_bnd = lower_bnd;
        lower_bnd = lower_bound_in_cur_step_of_remnant_mass_frac(lower_bnd - 
                    lower_bnd * 1e-7);
        lower_bnd = dmax(lower, lower_bnd);
        cont = 1;
      }else{
        cont = 0;
      }

      area += dx * (sum_end_pts + 2.0 * sum_fx_even + 4.0 * sum_fx_odd) / 3.0;
      sum_fx_even = 0;
      sum_fx_odd = 0;

    }while(cont);


  #endif


  #ifdef USE_GSL
    params.element[0] = element[0];
    params.element[1] = element[1];
    params.element[2] = element[2];
    params.zMassFrac = zMassFrac;

    F.function = &integrate_1_minus_wm_minus_pzII_Z_imf_gsl;
    F.params = &params;
    area = gsl_integration_glfixed(&F, lower, upper, gl_table);
    /*int status;
    double abserr;
    status = gsl_integration_qag(&F, lower, upper, 0.0, 1.0e-6,
                     WORKSIZE, GSL_INTEG_GAUSS15, workspace, &area, &abserr);
    */
  #endif

	return area;
}




#ifdef USE_GSL
/***********************************************************************************
Companion to integrate_1_minus_wm_minus_pzII_Z_imf() when GSL integration desired
***********************************************************************************/
double integrate_1_minus_wm_minus_pzII_Z_imf_gsl(double mass, void *params)
{
  gsl_feedback_integ_params * p = (gsl_feedback_integ_params *)params;

  double result =  (1 - remnant_mass_frac(mass) - 
          TypeII_Zi_yield_mass_frac(p->element, mass)) * p->zMassFrac * 
          imf(mass);

  return result;

}
#endif




/***********************************************************************************
	Integrand used in eqn 28 in K04. Use Trapezoidal Rule which is good enough. 
  Can use GSL integration if desired.

	integrate[(pzII * phi * dm)]
***********************************************************************************/
double integrate_pzII_imf(char element[3], double lower, double upper)
{	
	double area	= 0.0;

  //default is trapezoidal method
  #if !defined(USE_GSL) && !defined(USE_SIMPSON)
    int n 			= N_INTEGRATION_STEPS;
    int i				= 0;
    double dx		= (upper - lower) / ((float)n);
    double x_i	= 0.0;
    double x_j	= 0.0;
    double y_i	= 0.0;
    double y_j	= 0.0;

    x_i = lower;
    x_j = lower + dx;

    for(i=0; i<n; i++)
    {
      y_i = TypeII_Zi_yield_mass_frac(element, x_i) * imf(x_i);
      y_j = TypeII_Zi_yield_mass_frac(element, x_j) * imf(x_j);

      area += (x_j - x_i) * y_i + 0.5 * (x_j - x_i) * (y_j - y_i);

      x_i = x_j;
      x_j += dx;
    }
  #endif

  //Simpson's rule, section and theorem 4.4 in Numerical Analysis by Burden & Faires.
  #ifdef USE_SIMPSON
  	int n		  	 		= N_INTEGRATION_STEPS;
  	int i           = 0;
    int cont        = 1;      //flag to continue do-while loop.
    double x        = 0;
    double fx_low   = 0;      //Lower end point, f(lower)
    double fx_hi    = 0;      //Upper end point, f(upper)
    double dx       = 0; 
    double sum_fx_odd   = 0;      //summation of f(x_{2i-1})
    double sum_fx_even  = 0;      //summation of f(x_{2i})
    double sum_end_pts  = 0;
    double lower_bnd = 0;    //lower bound of current step.
    double upper_bnd = 0;    //lower bound of current step.

    upper_bnd = upper;
    lower_bnd = lower_bound_in_cur_step_TypeII_Zi_yield(upper);
    lower_bnd = dmax(lower, lower_bnd);


    /* Break up integral along discrete transisions in TypeII_Zi_yield_mass_frac().
     Otherwise it unneccessarily interpolates across transitions, reducing overall
     accuracy.*/
    do
    {
      //get endpoints
      fx_low = TypeII_Zi_yield_mass_frac(element, lower_bnd) * imf(lower_bnd);
      fx_hi  = TypeII_Zi_yield_mass_frac(element, upper_bnd) * imf(upper_bnd);
      sum_end_pts = fx_low + fx_hi;
      dx = (upper_bnd - lower_bnd) / ((float)n);
 
      for(i=1; i<n; i++)
      {
        x = lower_bnd + i * dx;

        if(i%2 == 0){
          sum_fx_even += TypeII_Zi_yield_mass_frac(element, x) * imf(x);
        }else{
          sum_fx_odd  += TypeII_Zi_yield_mass_frac(element, x) * imf(x);
        }
      }
     
      //Move to the next step if needed 
      if( (lower_bnd - lower)/ lower > 1e-7){
        upper_bnd = lower_bnd;
        lower_bnd = lower_bound_in_cur_step_TypeII_Zi_yield(lower_bnd - 
                    lower_bnd * 1e-7);
        lower_bnd = dmax(lower, lower_bnd);
        cont = 1;
      }else{
        cont = 0;
      }

      area += dx * (sum_end_pts + 2.0 * sum_fx_even + 4.0 * sum_fx_odd) / 3.0;
      sum_fx_even = 0;
      sum_fx_odd = 0;

    }while(cont);

  #endif


  #ifdef USE_GSL
    params.element[0] = element[0];
    params.element[1] = element[1];
    params.element[2] = element[2];

    F.function = &integrate_pzII_imf_gsl;
    F.params = &params;
    area = gsl_integration_glfixed(&F, lower, upper, gl_table);

    /*int status;
    double abserr;
    status = gsl_integration_qag(&F, lower, upper, 0.0, 1.0e-6,
                     WORKSIZE, GSL_INTEG_GAUSS15, workspace, &area, &abserr);
    */
  #endif

	return area;
}




#ifdef USE_GSL
/***********************************************************************************
Companion function to integrate_pzII_imf() when GSL Integration scheme is desired.
***********************************************************************************/
double integrate_pzII_imf_gsl(double mass, void* params)
{
  gsl_feedback_integ_params * p = (gsl_feedback_integ_params *)params;

  return TypeII_Zi_yield_mass_frac(p->element, mass) * imf(mass);
}
#endif




/************************************************************************************
	Integrand used in eqn. 30, 31, 32 in K04. Use Trapezoidal Rule which is good enough.
  Can use GSL integration if desired.

	integrate[1/m * phi * dm]
************************************************************************************/
double integrate_imf_div_m(double lower, double upper)
{
	double area 	= 0.0;

  #if !defined(USE_GSL) && !defined(USE_SIMPSON)
    int n		 	 	= N_INTEGRATION_STEPS;
    int i 			= 0;
    double dx 	 	= (upper - lower)/((float)n);
    double x_i		= 0.0;
    double x_j		= 0.0;
    double y_i		= 0.0;
    double y_j		= 0.0;

    x_i = lower;
    x_j = lower + dx;

    for(i=0; i<n; i++)
    {
      y_i = imf(x_i) / x_i;
      y_j = imf(x_j) / x_j;

      area += (x_j - x_i) * y_i + 0.5 * (x_j - x_i) * (y_j - y_i);

      x_i = x_j;
      x_j += dx;
    }
  #endif

  //Simpson's rule, section and theorem 4.4 in Numerical Analysis by Burden & Faires.
  #ifdef USE_SIMPSON
  	int n		  	 		= N_INTEGRATION_STEPS;
  	int i           = 0;
    double x        = 0;
    double fx_low   = 0;      //Lower end point, f(lower)
    double fx_hi    = 0;      //Upper end point, f(upper)
    double dx       = (upper - lower) / ((float)n);
    double sum_fx_odd   = 0;      //summation of f(x_{2i-1})
    double sum_fx_even  = 0;      //summation of f(x_{2i})
    double sum_end_pts  = 0;

    //get endpoints
    fx_low = imf(lower) / lower;
    fx_hi  = imf(upper) / upper;
    sum_end_pts = fx_low + fx_hi;
 
    for(i=1; i<n; i++)
    {
      x = lower + i * dx;

      if(i%2 == 0)
      {
        sum_fx_even += imf(x) / x;
      }
      else
      {
        sum_fx_odd += imf(x) / x;
      }
    }
    area = dx * (sum_end_pts + 2.0 * sum_fx_even + 4.0 * sum_fx_odd) / 3.0;
  #endif


  #ifdef USE_GSL
  
    F.function = &integrate_imf_div_m_gsl;
    area = gsl_integration_glfixed(&F, lower, upper, gl_table);
    /*int status;
    double abserr;
    status = gsl_integration_qag(&F, lower, upper, 0.0, 1.0e-6,
                     WORKSIZE, GSL_INTEG_GAUSS15, workspace, &area, &abserr);
    */

  #endif


	return area;
}



#ifdef USE_GSL
/***********************************************************************************
Companion function to integrate_imf_div_m() when GSL Integration scheme is desired.
***********************************************************************************/
double integrate_imf_div_m_gsl(double mass, void* params)
{
  return imf(mass) / mass;
}
#endif



/***********************************************************************************
Integrate the DTD used by Vogelsberger et al. 2013. See eqn 10. He gets his values
from Maoz, Mannucci & Brandt 2012. 

Time units are in Myr. We will use the analytic solution. This function may break
if s < 1 or s == 1.
************************************************************************************/
double integrate_dtd(double star_age, double last_fb_time)
{
  double tau     = 42.5731;            // Lifetime of 8M_sun star.
  double N_0     = 0.0013;             // (SN / M_sun), Maoz et al 2012
  double s       = 1.12;               // power law index, Vogelsberger 13 eqn10
  double tmax    = phys_time - star_age;
  double tmin    = last_fb_time - star_age;
	double value   = 0.0;

  //PIECEWISE ::: Ensure if dt < tau, returns 0.  Eqn. 10 Vogelsberger 2013
  if(phys_time - star_age < tau){
    return 0;
  }

  //Ensure integration is over valid range for g(t)
  if(tmin < tau){
    tmin = tau;
  }

  value = N_0 * (s - 1) * tmax * pow(tmax / tau, -s) / ((1 - s) * tau) - 
          N_0 * (s - 1) * tmin * pow(tmin / tau, -s) / ((1 - s) * tau);
  
  return value;
}




/************************************************************************************
	Given an element, find the appropriate index in either Type_II_SN[] or Type_Ia_SN[]

	mode == 1 for Type Ia SN
	mode == 2 for Type II SN
************************************************************************************/
int find_SN_Table_index(char element[3], int mode)
{
	int i = 0;
	int index = -1;
	int nElements = 0;

	if(mode == 1)
	{
		nElements = NTypeIaElements;
	}else if(mode == 2){
		nElements = NTypeIIElements;
	}else{
		fprintf(stderr, "ERROR!! Please specify correct 'mode' for find_SN_index\n");
		endrun(674);
	}


	while(i < nElements)
	{
		if( strcmp(element, Type_II_SN[i].Element) == 0)
		{
			//Error check, be sure element only occurs once in array
			if(index >= 0)
			{
				fprintf(stderr, "ERROR!! Element occurs multiple times in "
												"SN yields data files\n");
				endrun(672);
			}
			else
			{
				index = i;
			}
		}
		i++;
	}

	return index;
}	






/************************************************************************************
	Given a target index, it finds appropriate metal mass fraction from P[].*Mass
************************************************************************************/
double get_Zi_mass_frac(int target, char element[3], int mode)
{
	double metalMassFrac = -999.0;

	if(P[target].Type != 0 && P[target].Type != 4){
		fprintf(stderr, "ERROR!!! Trying to change metal abundance of particle that is "
										"neither a star nor gas particle\n");
		endrun(675);
	}



	/* mode == 0 for local particles, mode == 1 for imported particles */
	if( mode == 0 )
	{
		#ifdef CARBON
			if(strcmp(element, "C") == 0){
				metalMassFrac = P[target].CarbonMass / P[target].Mass;
			}
		#endif
		#ifdef NITROGEN
			if(strcmp(element, "N") == 0){
				metalMassFrac = P[target].NitrogenMass / P[target].Mass;
			}
		#endif
		#ifdef OXYGEN
			if(strcmp(element, "O") == 0){
				metalMassFrac = P[target].OxygenMass / P[target].Mass;
			}
		#endif
		#ifdef FLORINE
			if(strcmp(element, "F") == 0){
				metalMassFrac = P[target].FlorineMass / P[target].Mass;
			}
		#endif
		#ifdef NEON
			if(strcmp(element, "Ne") == 0){
				metalMassFrac = P[target].NeonMass / P[target].Mass;
			}
		#endif
		#ifdef SODIUM
			if(strcmp(element, "Na") == 0){
				metalMassFrac = P[target].SodiumMass / P[target].Mass;
			}
		#endif
		#ifdef MAGNESIUM
			if(strcmp(element, "Mg") == 0){
				metalMassFrac = P[target].MagnesiumMass / P[target].Mass;
			}
		#endif
		#ifdef ALUMINUM
			if(strcmp(element, "Al") == 0){
				metalMassFrac = P[target].AluminumMass / P[target].Mass;
			}
		#endif
		#ifdef SILICON
			if(strcmp(element, "Si") == 0){
				metalMassFrac = P[target].SiliconMass / P[target].Mass;
			}
		#endif
		#ifdef PHOSPHORUS
			if(strcmp(element, "P") == 0){
				metalMassFrac = P[target].PhosphorusMass / P[target].Mass;
			}
		#endif
		#ifdef SULFUR
			if(strcmp(element, "S") == 0){
				metalMassFrac = P[target].SulfurMass / P[target].Mass;
			}
		#endif
		#ifdef CHLORINE
			if(strcmp(element, "Cl") == 0){
				metalMassFrac = P[target].ChlorineMass / P[target].Mass;
			}
		#endif
		#ifdef ARGON
			if(strcmp(element, "Ar") == 0){
				metalMassFrac = P[target].ArgonMass / P[target].Mass;
			}
		#endif
		#ifdef POTASSIUM
			if(strcmp(element, "K") == 0){
				metalMassFrac = P[target].PotassiumMass / P[target].Mass;
			}
		#endif
		#ifdef CALCIUM
			if(strcmp(element, "Ca") == 0){
				metalMassFrac = P[target].CalciumMass / P[target].Mass;
			}
		#endif
		#ifdef SCANDIUM
			if(strcmp(element, "Sc") == 0){
				metalMassFrac = P[target].ScandiumMass / P[target].Mass;
			}
		#endif
		#ifdef TITANIUM
			if(strcmp(element, "Ti") == 0){
				metalMassFrac = P[target].TitaniumMass / P[target].Mass;
			}
		#endif
		#ifdef VANADIUM
			if(strcmp(element, "V") == 0){
				metalMassFrac = P[target].VanadiumMass / P[target].Mass;
			}
		#endif
		#ifdef CHROMIUM
			if(strcmp(element, "Cr") == 0){
				metalMassFrac = P[target].ChromiumMass / P[target].Mass;
			}
		#endif
		#ifdef MANGANESE
			if(strcmp(element, "Mn") == 0){
				metalMassFrac = P[target].ManganeseMass / P[target].Mass;
			}
		#endif
		#ifdef IRON
			if(strcmp(element, "Fe") == 0){
				metalMassFrac = P[target].IronMass / P[target].Mass;
			}
		#endif
		#ifdef COBALT
			if(strcmp(element, "Co") == 0){
				metalMassFrac = P[target].CobaltMass / P[target].Mass;
			}
		#endif
		#ifdef NICKEL
			if(strcmp(element, "Ni") == 0){
				metalMassFrac = P[target].NickelMass / P[target].Mass;
			}
		#endif
		#ifdef COPPER
			if(strcmp(element, "Cu") == 0){
				metalMassFrac = P[target].CopperMass / P[target].Mass;
			}
		#endif
		#ifdef ZINC
			if(strcmp(element, "Zn") == 0){
				metalMassFrac = P[target].ZincMass / P[target].Mass;
			}
		#endif
	}
	return metalMassFrac;
}




/******************************************************************************
	Updates P[target].*MassFrac. If you want to update it with only partial amounts
	of the metals, adjust the fraction appropriately. This assumes that P[target].Mass
	is uptodate. 

	This also updates P[target].MetalMassFrac

	cofactor = -1.0 if you want to subract
	cofactor = +1.0 if you want to add.

	P[target] -> Particle to be updated
	dtzmass		-> Contains mass of each element to be updated.
	cofactor	-> what fraction of dtzmass to update with.

*******************************************************************************/
void update_Zi_mass(int target, double dtzmass[NELEMENTS], double cofactor)
{
	int i=0;
	char element[3];
	double totalDtMetalMass = 0.0;

	if(P[target].Type != 0 && P[target].Type != 4){
		fprintf(stderr, "ERROR!!! Trying to change metal abundance of particle that is "
										"neither a star nor gas particle\n");
		endrun(675);
	}


	for(i=0; i<NELEMENTS; i++)
	{
		strcpy(element, MetalsPresent[i].Element);
		totalDtMetalMass += cofactor * dtzmass[i];

		#ifdef CARBON
			if(strcmp(element, "C") == 0){
				P[target].CarbonMass += cofactor * dtzmass[i];
			}
		#endif
		#ifdef NITROGEN
			if(strcmp(element, "N") == 0){
				P[target].NitrogenMass += cofactor * dtzmass[i];
			}
		#endif
		#ifdef OXYGEN
			if(strcmp(element, "O") == 0){
				P[target].OxygenMass += cofactor * dtzmass[i];
			}
		#endif
		#ifdef FLORINE
			if(strcmp(element, "F") == 0){
				P[target].FlorineMass += cofactor * dtzmass[i];
			}
		#endif
		#ifdef NEON
			if(strcmp(element, "Ne") == 0){
				P[target].NeonMass += cofactor * dtzmass[i];
			}
		#endif
		#ifdef SODIUM
			if(strcmp(element, "Na") == 0){
				P[target].SodiumMass += cofactor * dtzmass[i];
			}
		#endif
		#ifdef MAGNESIUM
			if(strcmp(element, "Mg") == 0){
				P[target].MagnesiumMass += cofactor * dtzmass[i];
			}
		#endif
		#ifdef ALUMINUM
			if(strcmp(element, "Al") == 0){
				P[target].AluminumMass += cofactor * dtzmass[i];
			}
		#endif
		#ifdef SILICON
			if(strcmp(element, "Si") == 0){
				P[target].SiliconMass += cofactor * dtzmass[i];
			}
		#endif
		#ifdef PHOSPHORUS
			if(strcmp(element, "P") == 0){
				P[target].PhosphorusMass += cofactor * dtzmass[i];
			}
		#endif
		#ifdef SULFUR
			if(strcmp(element, "S") == 0){
				P[target].SulfurMass += cofactor * dtzmass[i];
			}
		#endif
		#ifdef CHLORINE
			if(strcmp(element, "Cl") == 0){
				P[target].ChlorineMass += cofactor * dtzmass[i];
			}
		#endif
		#ifdef ARGON
			if(strcmp(element, "Ar") == 0){
				P[target].ArgonMass += cofactor * dtzmass[i];
			}
		#endif
		#ifdef POTASSIUM
			if(strcmp(element, "K") == 0){
				P[target].PotassiumMass += cofactor * dtzmass[i];
			}
		#endif
		#ifdef CALCIUM
			if(strcmp(element, "Ca") == 0){
				P[target].CalciumMass += cofactor * dtzmass[i];
			}
		#endif
		#ifdef SCANDIUM
			if(strcmp(element, "Sc") == 0){
				P[target].ScandiumMass += cofactor * dtzmass[i];
			}
		#endif
		#ifdef TITANIUM
			if(strcmp(element, "Ti") == 0){
				P[target].TitaniumMass += cofactor * dtzmass[i];
			}
		#endif
		#ifdef VANADIUM
			if(strcmp(element, "V") == 0){
				P[target].VanadiumMass += cofactor * dtzmass[i];
			}
		#endif
		#ifdef CHROMIUM
			if(strcmp(element, "Cr") == 0){
				P[target].ChromiumMass += cofactor * dtzmass[i];
			}
		#endif
		#ifdef MANGANESE
			if(strcmp(element, "Mn") == 0){
				P[target].ManganeseMass += cofactor * dtzmass[i];
			}
		#endif
		#ifdef IRON
			if(strcmp(element, "Fe") == 0){
				P[target].IronMass += cofactor * dtzmass[i];
			}
		#endif
		#ifdef COBALT
			if(strcmp(element, "Co") == 0){
				P[target].CobaltMass += cofactor * dtzmass[i];
			}
		#endif
		#ifdef NICKEL
			if(strcmp(element, "Ni") == 0){
				P[target].NickelMass += cofactor * dtzmass[i];
			}
		#endif
		#ifdef COPPER
			if(strcmp(element, "Cu") == 0){
				P[target].CopperMass += cofactor * dtzmass[i];
			}
		#endif
		#ifdef ZINC
			if(strcmp(element, "Zn") == 0){
				P[target].ZincMass += cofactor * dtzmass[i];
			}
		#endif
	}

	P[target].MetalMass += totalDtMetalMass;		//Already accounted for cofactor 
}



/*
	This function finds the R_SW, R_Ia and R_II rates for the target particle.  This
	is particularly useful so that we can avoid updating the P[].starHsml, 
	FeedbackNorm and distributing_metals_feedback for particles that do not need to 
	run on stars that lack feedback.
*/
void get_feedback_rates(int target)
{
	double dt = 0;
	double m_u	 	= 0;											// Most massive stars currently
	double upper 	= 0;											// Upper lim passed to integration routines
	double lower 	= 0;											// Lower lim passed to integration routines
	double star_age = 0;
	double m_turnoff  	= 0;								// Upper-most mass ON the Main Sequence
	double R_SW				= 0;									// Rate	of SW	per SOLAR mass
	double R_II				= 0;									// Rate of Type II SN per SOLAR mass
	double R_Ia				= 0;									// Rate of Type Ia SN per SOLAR mass
  double R_AGB      = 0;
  double last_fb_time = 0;                // Last feedback time, used for R_Ia

	//Error check.
	if(P[target].Type != 4){
		fprintf(stderr, "ERROR!! Can only get feedback rates for star particles!\n");
		endrun(5008);
	}

	star_age				= P[target].StarAge;
	m_u 						= P[target].TurnOffMass;
	dt 							= phys_time - star_age;
 
  //Find last_fb_time for R_Ia.  
  if(m_u >= 0.999 * IMF_MAX){      
    last_fb_time  = star_age;   //Stellar life times is works only when m_u != IMF_MX.
  }else{
    last_fb_time  = star_age + 
                    pow(10, 10.0 + (-3.42 + 0.88 * log10(m_u)) * log10(m_u)) / 1e6; 
  }

	//Get turnoff mass, solve eqn 26, K04 for log(m). Select physical root.
	if(dt > 5.0){											//prevent sqrt(-1)
		m_turnoff = (3.42 - sqrt(3.42*3.42 - 4.0*0.88*(10 - log10(dt * 1.0e6)))) / 1.76;
		m_turnoff = pow(10, m_turnoff);
	}else{
		//No feedback before 5 mega-years
		return;
	}

	/************************** Feedback Rates ******************************/
	/* Stellar Winds */
	if(m_u > m_2u){
		upper = m_u;
		lower = dmax(m_turnoff, m_2u);
		R_SW  = integrate_imf_div_m(lower, upper);											//eqn 30 in K04
	}else{
		R_SW  = 0.0;
	}	


	/* Type II SN */
	if(m_2l <= m_u && m_u <= m_2u){
		upper = m_u;
		lower = dmax(m_turnoff, m_2l);
		R_II  = integrate_imf_div_m(lower, upper);											//eqn 31 in K04
	}
	else if(m_turnoff < m_2u && m_u > m_2u){		// Cross SW -> Type II region
		upper = m_2u;
		lower = dmax(m_turnoff, m_2l);
		R_II  = integrate_imf_div_m(lower, upper);											//eqn 31 in K04
	}else{
		R_II = 0.0;
	}


  /* Type Ia SN */           
  R_Ia = integrate_dtd(star_age, last_fb_time);   // Returns 0 if outside range


  /* AGB and sAGB */
  if(m_u < m_2l){
    upper = m_u;
    lower = dmax(m_turnoff, m_1l_RG);
    R_AGB = integrate_imf_div_m(lower, upper);
  }
  else if(m_turnoff < m_2l && m_u > m_2l){    //Cross Type II -> AGB region
    upper = m_2l;
    lower = dmax(m_turnoff, m_1l_RG);
    R_AGB = integrate_imf_div_m(lower, upper);
  }else{
    R_AGB = 0.0;
  }


	
	//Error Check.
	if(R_Ia < 0.0 || R_II < 0.0 || R_SW < 0.0 || R_AGB < 0.0){
		fprintf(stderr, "ERROR!!! Rates can't be less than zero!\nR_SW : %.2e "
						"R_II : %.2e  R_Ia : %.2e\n", R_SW, R_II, R_Ia);
		endrun(678);
	}

	P[target].R_II  = R_II;
	P[target].R_Ia  = R_Ia;
	P[target].R_SW  = R_SW;
  P[target].R_AGB = R_AGB;
}

#endif
#endif
#endif
#endif
#endif
