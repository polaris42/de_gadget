/***************************************************************
Author: Ali Snedden
Date: 1/15/13

NSH 341a
Dept. of Physics
Univ. of Notre Dame

Purpose:
	Here I create a function that will output in a space deliminated
	format that I can compare to the gadget2csv file. Only works 
	for file format = 1.

	I simply copied io.c and am modifying it appropriately. I don't 
	think that it is worth my time rewriting similar routines.

	I have eliminated the ability to write multiple files at once for
	the sake of my own clarity.

	THIS IS NOT COMPLETE!!

****************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <mpi.h>
#include <errno.h>

#include "allvars.h"
#include "proto.h"



static int n_type[6];												//Number of LOCAL particles per type
//static long long ntot_type_all[6];					//Number of TOTAL particles per type

//Output Particle Structure
typedef struct
{
	float Pos[3];
	float Vel[3];
	float Mass;
	int 	Type;
	int 	Id;

	float Rho;
	float U;
	float Temp;
	float Ne;												//Electron fraction?
	float Hsml;											//Smoothing length

  float StarAge;

  double dN_II;
  double dN_Ia;

	#ifdef METALS
	float MetalMass;
		#ifdef CARBON
			float CarbonMass;
		#endif
		#ifdef NITROGEN
			float NitrogenMass;
		#endif
		#ifdef OXYGEN
			float OxygenMass;
		#endif
		#ifdef FLORINE
			float FlorineMass;
		#endif
		#ifdef NEON
			float NeonMass;
		#endif
		#ifdef SODIUM
			float SodiumMass;
		#endif
		#ifdef MAGNESIUM
			float MagnesiumMass;
		#endif
		#ifdef ALUMINUM
			float AluminumMass;
		#endif
		#ifdef SILICON
			float SiliconMass;
		#endif
		#ifdef PHOSPHORUS
			float PhosphorusMass;
		#endif
		#ifdef SULFUR
			float SulfurMass;
		#endif
		#ifdef CHLORINE
			float ChlorineMass;
		#endif
		#ifdef ARGON
			float ArgonMass;
		#endif
		#ifdef POTASSIUM
			float PotassiumMass;
		#endif
		#ifdef CALCIUM
			float CalciumMass;
		#endif
		#ifdef SCANDIUM
			float ScandiumMass;
		#endif
		#ifdef TITANIUM
			float TitaniumMass;
		#endif
		#ifdef VANADIUM
			float VanadiumMass;
		#endif
		#ifdef CHROMIUM
			float ChromiumMass;
		#endif
		#ifdef MANGANESE
			float ManganeseMass;
		#endif
		#ifdef IRON
			float IronMass;
		#endif
		#ifdef COBALT
			float CobaltMass;
		#endif
		#ifdef NICKEL
			float NickelMass;
		#endif
		#ifdef COPPER
			float CopperMass;
		#endif
		#ifdef ZINC
			float ZincMass;
		#endif
	#endif

}	particle_output_data;




 /*
	Here I write out the file to compare to my gadget2csv. Each Processor outputs
	its own file and you can cut and paste the resulting files to achieve the same
	output file as gadget2csv. I would make a version of this code that would 
	output the csv file in the same way that io.c does, however, that is just too 
	much damn work. 

	In io.c each processor writes to the same block of data in the snapshot, which
	does not make it conducive to writing out ALL of a single particle's data 
	(i.e. Pos, Vel, Mass, Type, Id, Rho, etc.) on one line. Likewise, it would take
	me too much time to develop a code that uses MPI to reduce this data on one
	processor.
 
 */
void writeCsvData(char * fname)
{
	int i = 0;
	int k = 0;
	int n = 0;
	double dt_gravkick = 0.0; 
	double dt_hydrokick = 0.0; 
	double dt_gravkick_pm = 0.0;
	char localTaskFileName[500];
	float a3inv = 1 / (All.Time * All.Time * All.Time);
	FILE * f = NULL;													//Output file
	particle_output_data * P_out = NULL;				//Local output data.		

	#ifdef PERIODIC
		double boxSize = 0.0; 
	#endif

	//Let root node open create the file.
	sprintf(localTaskFileName, "%s.Task_%i", fname, ThisTask);
	f = fopen(localTaskFileName,"w+");

	//Allocate memory
	P_out = (particle_output_data *)malloc(sizeof(particle_output_data) * NumPart);
  for(n = 0; n < 6; n++){
  	n_type[n] = 0;
	}

	//Determine local particle numbers, initialize P_out.* = 0
  for(i = 0; i < NumPart; i++){
  	n_type[P[i].Type]++;

		P_out[i].Type = P[i].Type;
		P_out[i].Pos[0] = P_out[i].Pos[1] = P_out[i].Pos[2] = 0.0;
		P_out[i].Vel[0] = P_out[i].Vel[1] = P_out[i].Vel[2] = 0.0;
		P_out[i].Mass 	= 0.0;
		P_out[i].Id 		= P[i].ID;
		P_out[i].Rho 		= 0.0;
		P_out[i].U 			= 0.0;
		P_out[i].Temp 	= 0.0;
		P_out[i].Ne 		= 0.0;
		P_out[i].Hsml 	= 0.0;
    P_out[i].StarAge= 0.0;
    P_out[i].dN_II   = 0.0;
    P_out[i].dN_Ia   = 0.0;

	#ifdef METALS
		P_out[i].MetalMass = 0;
		#ifdef CARBON
			P_out[i].CarbonMass = 0;
		#endif
		#ifdef NITROGEN
			P_out[i].NitrogenMass = 0;
		#endif
		#ifdef OXYGEN
			P_out[i].OxygenMass = 0;
		#endif
		#ifdef FLORINE
			P_out[i].FlorineMass = 0;
		#endif
		#ifdef NEON
			P_out[i].NeonMass = 0;
		#endif
		#ifdef SODIUM
			P_out[i].SodiumMass = 0;
		#endif
		#ifdef MAGNESIUM
			P_out[i].MagnesiumMass = 0;
		#endif
		#ifdef ALUMINUM
			P_out[i].AluminumMass = 0;
		#endif
		#ifdef SILICON
			P_out[i].SiliconMass = 0;
		#endif
		#ifdef PHOSPHORUS
			P_out[i].PhosphorusMass = 0;
		#endif
		#ifdef SULFUR
			P_out[i].SulfurMass = 0;
		#endif
		#ifdef CHLORINE
			P_out[i].ChlorineMass = 0;
		#endif
		#ifdef ARGON
			P_out[i].ArgonMass = 0;
		#endif
		#ifdef POTASSIUM
			P_out[i].PotassiumMass = 0;
		#endif
		#ifdef CALCIUM
			P_out[i].CalciumMass = 0;
		#endif
		#ifdef SCANDIUM
			P_out[i].ScandiumMass = 0;
		#endif
		#ifdef TITANIUM
			P_out[i].TitaniumMass = 0;
		#endif
		#ifdef VANADIUM
			P_out[i].VanadiumMass = 0;
		#endif
		#ifdef CHROMIUM
			P_out[i].ChromiumMass = 0;
		#endif
		#ifdef MANGANESE
			P_out[i].ManganeseMass = 0;
		#endif
		#ifdef IRON
			P_out[i].IronMass = 0;
		#endif
		#ifdef COBALT
			P_out[i].CobaltMass = 0;
		#endif
		#ifdef NICKEL
			P_out[i].NickelMass = 0;
		#endif
		#ifdef COPPER
			P_out[i].CopperMass = 0;
		#endif
		#ifdef ZINC
			P_out[i].ZincMass = 0;
		#endif
	#endif
	}

	#ifdef PMGRID
		if(All.ComovingIntegrationOn){
   		dt_gravkick_pm =
   										get_gravkick_factor(All.PM_Ti_begstep, All.Ti_Current) -
                      get_gravkick_factor(All.PM_Ti_begstep,
                      (All.PM_Ti_begstep + All.PM_Ti_endstep) / 2);
   	}
		else
		{
   		dt_gravkick_pm = (All.Ti_Current - (All.PM_Ti_begstep + All.PM_Ti_endstep) 
														/ 2) * All.Timebase_interval;
		}
	#endif


	//Initialize P_out
	for(i=0; i<NumPart; i++)
	{
		//Position		
   	for(k = 0; k < 3; k++)
	  {
    	P_out[i].Pos[k] = P[i].Pos[k];
			#ifdef PERIODIC
     		boxSize = All.BoxSize;
				#ifdef LONG_X
     			if(k == 0)
     	  		boxSize = All.BoxSize * LONG_X;
				#endif
				#ifdef LONG_Y
     			if(k == 1)
     	  		boxSize = All.BoxSize * LONG_Y;
				#endif
				#ifdef LONG_Z
     			if(k == 2)
     			  boxSize = All.BoxSize * LONG_Z;
				#endif
     			while(P_out[i].Pos[k] < 0)
     			  P_out[i].Pos[k] += boxSize;
     			while(P_out[i].Pos[k] >= boxSize)
     			  P_out[i].Pos[k] -= boxSize;
				#endif
	  }

		//Velocity
	  if(All.ComovingIntegrationOn)
	  {
			/* I suspect below ALL the remaining particles that have
				 not been updated due to being in the "drift" portion of the integration
				 scheme.*/
	   	dt_gravkick =
	 	  							get_gravkick_factor(P[i].Ti_begstep,
		                All.Ti_Current) -	 get_gravkick_factor(P[i].Ti_begstep,
	 	                (P[i].Ti_begstep + P[i].Ti_endstep) / 2);
	    dt_hydrokick =
	                  get_hydrokick_factor(P[i].Ti_begstep,
		                All.Ti_Current) - get_hydrokick_factor(P[i].Ti_begstep,
	  	              (P[i].Ti_begstep + P[i].Ti_endstep) / 2);
	  }
	  else
		{
	   	dt_gravkick = dt_hydrokick =
		                (All.Ti_Current - (P[i].Ti_begstep + P[i].Ti_endstep)/2)
 										* All.Timebase_interval;
		}

	  for(k = 0; k < 3; k++)
	  {
		  P_out[i].Vel[k] = P[i].Vel[k] + P[i].GravAccel[k] * dt_gravkick;
		  if(P[i].Type == 0){
		    P_out[i].Vel[k] += SphP[i].HydroAccel[k] * dt_hydrokick;
			}
	  }
		#ifdef PMGRID
	   	for(k = 0; k < 3; k++){
 				

	   	  P_out[i].Vel[k] += P[i].GravPM[k] * dt_gravkick_pm;
			}
		#endif
	  for(k = 0; k < 3; k++){
	    P_out[i].Vel[k] *= sqrt(a3inv);				// Maintain GADGET-1 compatability, 
		}																				// see User Manual p40 

	
		//Mass
		P_out[i].Mass = P[i].Mass;

		//Energy
	  if(P[i].Type == 0)
	  {
			#ifdef ISOTHERM_EQS
	      P_out[i].U = SphP[i].Entropy;  /***** this line is not changed yet *****/
			#else
				#ifdef OUTPUT_TEMP_FOR_U
	   			/***** here output temperature instead of internal energy *****/
					/* Photo-ionized even at low temps. Self shielding ignored. See
						 p1214 in Schaye & Dalla Vecchia 2007 */
	 	     	double molecular_weight;
					#ifndef UVBACKGROUND
	       	if(All.InitGasTemp > 1.0e4)   /* assuming FULL ionization */
	   	  	  molecular_weight = 4 / (8 - 5 * (1 - HYDROGEN_MASSFRAC));
	     		else                          /* assuming NEUTRAL GAS */
	     		  molecular_weight = 4 / (1 + 3 * HYDROGEN_MASSFRAC);
					#endif
					#ifdef UVBACKGROUND
	   	  	  molecular_weight = 4 / (8 - 5 * (1 - HYDROGEN_MASSFRAC));
					#endif

	     		P_out[i].U = dmax(All.MinEgySpec,SphP[i].Entropy / 
             GAMMA_MINUS1 * pow(SphP[i].Density * a3inv, GAMMA_MINUS1))*molecular_weight
	     		   *GAMMA_MINUS1/(All.UnitMass_in_g/All.UnitEnergy_in_cgs)/(BOLTZMANN / 
             PROTONMASS);
				#else
	      	P_out[i].U =
	         	  dmax(All.MinEgySpec,
	          	SphP[i].Entropy / GAMMA_MINUS1 * pow(SphP[i].Density * a3inv,
             	GAMMA_MINUS1));
				#endif
			#endif
	   }
		

		//Density
		if(P[i].Type == 0){
			P_out[i].Rho = SphP[i].Density;
		}


		//SPH Smoothing Length
		if(P[i].Type == 0){
			P_out[i].Hsml = SphP[i].Hsml;
		}


    //Star Age
    #ifdef SFR
    if(P[i].Type == 4){
        P_out[i].StarAge = P[i].StarAge;
    }
    #endif

    //Type II
    #ifdef FEEDBACK
		if(P[i].Type == 4)
		{
      double last_fb_time = 0;           //Find the last feedback time
      double dN_II        = 0;           //Number of Type II SN per year.
      double star_age     = 0;
      double dt           = 0;
      double origStarMass = 0;
      double m_2u         = 40;
      double m_2l         = 8;
      double m_u          = 0;
      double m_turnoff    = 0;
      double upper        = 0;
      double lower        = 0;

      star_age     = P[i].StarAge;
      origStarMass = P[i].origStarMass;  
      m_u          = P[i].TurnOffMass;
      dt           = phys_time - star_age;


      //Convert origStarMass to solar masses
      if(All.ComovingIntegrationOn)
      {
        origStarMass = origStarMass * All.UnitMass_in_g / 
                       (SOLAR_MASS * All.HubbleParam);
      }else{
        origStarMass = origStarMass * All.UnitMass_in_g / (SOLAR_MASS);
      }


      //Find last_fb_time for R_II
      if(m_u >= 0.999 * IMF_MAX){
        last_fb_time = star_age;
      }else{
        last_fb_time = star_age + 
                  pow(10, 10.0 + (-3.42 + 0.88 * log10(m_u)) * log10(m_u)) / 1e6;
      }

      //Get turnoff mass, solve eqn 26, K04 for log(m). Select physical root.
      if(dt > 5.0){                     //prevent sqrt(-1)
        m_turnoff = (3.42 - sqrt(3.42*3.42 - 4.0*0.88*(10 - log10(dt * 1.0e6)))) / 1.76;
        m_turnoff = pow(10, m_turnoff);

        /* Type II SN */
        if(m_2l <= m_u && m_u <= m_2u){
          upper = m_u;
          lower = dmax(m_turnoff, m_2l);
          dN_II = origStarMass * integrate_imf_div_m(lower, upper); //eqn 31, K04
        }
        else if(m_turnoff < m_2u && m_u > m_2u){    // Cross SW -> Type II region
          upper = m_2u;
          lower = dmax(m_turnoff, m_2l);
          dN_II = origStarMass * integrate_imf_div_m(lower, upper); //eqn 31, K04
        }else{
          dN_II = 0.0;
        }
      }else{
        //No feedback before 5 mega-years
        dN_II = 0.0;
      }

      //Convert to SNe per year.
      dN_II = dN_II /( (phys_time - last_fb_time) * 1.0e6);
    
      P_out[i].dN_II = dN_II;
    }
    #endif



    //Type Ia
    #ifdef FEEDBACK
		if(P[i].Type == 4)
		{
      double last_fb_time = 0;           //Find the last feedback time
      double dN_Ia        = 0;           //Number of Type II SN per year.
      double star_age     = 0;
      double dt           = 0;
      double origStarMass = 0;
      double m_u          = 0;

      star_age     = P[i].StarAge;
      origStarMass = P[i].origStarMass;  
      m_u          = P[i].TurnOffMass;
      dt           = phys_time - star_age;

      //Convert origStarMass to solar masses
      if(All.ComovingIntegrationOn)
      {
        origStarMass = origStarMass * All.UnitMass_in_g / 
                       (SOLAR_MASS * All.HubbleParam);
      }else{
        origStarMass = origStarMass * All.UnitMass_in_g / SOLAR_MASS;
      }

      //Find last_fb_time for R_II
      if(m_u >= 0.999 * IMF_MAX){
        last_fb_time = star_age;
      }else{
        last_fb_time = star_age + 
                  pow(10, 10.0 + (-3.42 + 0.88 * log10(m_u)) * log10(m_u)) / 1e6;
      }

      dN_Ia = origStarMass * integrate_dtd(star_age, last_fb_time);
      dN_Ia = dN_Ia /( (phys_time - last_fb_time) * 1.0e6);

      P_out[i].dN_Ia = dN_Ia;
    }
    #endif



		//Metals
		#ifdef METALS
			P_out[i].MetalMass = P[i].MetalMass ;
			#ifdef CARBON
				P_out[i].CarbonMass = P[i].CarbonMass ;
			#endif
			#ifdef NITROGEN
				P_out[i].NitrogenMass = P[i].NitrogenMass;
			#endif
			#ifdef OXYGEN
				P_out[i].OxygenMass = P[i].OxygenMass;
			#endif
			#ifdef FLORINE
				P_out[i].FlorineMass = P[i].FlorineMass;
			#endif
			#ifdef NEON
				P_out[i].NeonMass = P[i].NeonMass;
			#endif
			#ifdef SODIUM
				P_out[i].SodiumMass = P[i].SodiumMass;
			#endif
			#ifdef MAGNESIUM
				P_out[i].MagnesiumMass = P[i].MagnesiumMass;
			#endif
			#ifdef ALUMINUM
				P_out[i].AluminumMass = P[i].AluminumMass;
			#endif
			#ifdef SILICON
				P_out[i].SiliconMass = P[i].SiliconMass;
			#endif
			#ifdef PHOSPHORUS
				P_out[i].PhosphorusMass = P[i].PhosphorusMass;
			#endif
			#ifdef SULFUR
				P_out[i].SulfurMass = P[i].SulfurMass;
			#endif
			#ifdef CHLORINE
				P_out[i].ChlorineMass = P[i].ChlorineMass;
			#endif
			#ifdef ARGON
				P_out[i].ArgonMass = P[i].ArgonMass;
			#endif
			#ifdef POTASSIUM
				P_out[i].PotassiumMass = P[i].PotassiumMass;
			#endif
			#ifdef CALCIUM
				P_out[i].CalciumMass = P[i].CalciumMass;
			#endif
			#ifdef SCANDIUM
				P_out[i].ScandiumMass = P[i].ScandiumMass;
			#endif
			#ifdef TITANIUM
				P_out[i].TitaniumMass = P[i].TitaniumMass;
			#endif
			#ifdef VANADIUM
				P_out[i].VanadiumMass = P[i].VanadiumMass;
			#endif
			#ifdef CHROMIUM
				P_out[i].ChromiumMass = P[i].ChromiumMass;
			#endif
			#ifdef MANGANESE
				P_out[i].ManganeseMass = P[i].ManganeseMass;
			#endif
			#ifdef IRON
				P_out[i].IronMass = P[i].IronMass;
			#endif
			#ifdef COBALT
				P_out[i].CobaltMass = P[i].CobaltMass;
			#endif
			#ifdef NICKEL
				P_out[i].NickelMass = P[i].NickelMass;
			#endif
			#ifdef COPPER
				P_out[i].CopperMass = P[i].CopperMass;
			#endif
			#ifdef ZINC
				P_out[i].ZincMass = P[i].ZincMass;
			#endif
		#endif
	}



	/* determine global and local particle numbers. Put output values in same units
		 as io.c does. Handle Periodic B.C. w/r/t Position and be sure all the velocities
		 have been updated to the current timestep
	*/




	//Print Header
	fprintf(f, "#%*s%*s%*s%*s%*s%*s%*s", 
			9, "x", 10, "y", 10, "z", 7, "ID", 13, "Mass", 13, "U/Temp", 6, "Type");
		
	#ifdef METALS
		fprintf(f,"%*s", 10, "MtlFrac");
		#ifdef CARBON
			fprintf(f,"%*s", 10, "CFrac");
		#endif
		#ifdef NITROGEN
			fprintf(f,"%*s", 10, "NFrac");
		#endif
		#ifdef OXYGEN
			fprintf(f,"%*s", 10, "OFrac");
		#endif
		#ifdef FLORINE
			fprintf(f,"%*s", 10, "FFrac");
		#endif
		#ifdef NEON
			fprintf(f,"%*s", 10, "NeFrac");
		#endif
		#ifdef SODIUM
			fprintf(f,"%*s", 10, "NaFrac");
		#endif
		#ifdef MAGNESIUM
			fprintf(f,"%*s", 10, "MgFrac");
		#endif
		#ifdef ALUMINUM
			fprintf(f,"%*s", 10, "AlFrac");
		#endif
		#ifdef SILICON
			fprintf(f,"%*s", 10, "SiFrac");
		#endif
		#ifdef PHOSPHORUS
			fprintf(f,"%*s", 10, "PFrac");
		#endif
		#ifdef SULFUR
			fprintf(f,"%*s", 10, "SFrac");
		#endif
		#ifdef CHLORINE
			fprintf(f,"%*s", 10, "ClFrac");
		#endif
		#ifdef ARGON
			fprintf(f,"%*s", 10, "ArFrac");
		#endif
		#ifdef POTASSIUM
			fprintf(f,"%*s", 10, "KFrac");
		#endif
		#ifdef CALCIUM
			fprintf(f,"%*s", 10, "CaFrac");
		#endif
		#ifdef SCANDIUM
			fprintf(f,"%*s", 10, "ScFrac");
		#endif
		#ifdef TITANIUM
			fprintf(f,"%*s", 10, "TiFrac");
		#endif
		#ifdef VANADIUM
			fprintf(f,"%*s", 10, "VFrac");
		#endif
		#ifdef CHROMIUM
			fprintf(f,"%*s", 10, "CrFrac");
		#endif
		#ifdef MANGANESE
			fprintf(f,"%*s", 10, "MnFrac");
		#endif
		#ifdef IRON
			fprintf(f,"%*s", 10, "FeFrac");
		#endif
		#ifdef COBALT
			fprintf(f,"%*s", 10, "CoFrac");
		#endif
		#ifdef NICKEL
			fprintf(f,"%*s", 10, "NiFrac");
		#endif
		#ifdef COPPER
			fprintf(f,"%*s", 10, "CuFrac");
		#endif
		#ifdef ZINC
			fprintf(f,"%*s", 10, "ZnFrac");
		#endif
	#endif		//End Print Header
	fprintf(f,"\n");




	

	for(i=0; i<NumPart; i++)
	{
			/*fprintf(f, "%*.2f%*.2f%*.2f%*i%*.5e%*.5e%*i", 
					9, P_out[i].Pos[0], 9, P_out[i].Pos[1], 9, P_out[i].Pos[2],  
					10, P_out[i].Id, 13, P_out[i].Mass, 13, P_out[i].U, 
					6, P_out[i].Type);
      */
      fprintf(f, "%*i %i %.6e %.6e %.6e %.6e %.6e %.6e %.6e %.6e %.6e %.6e %.6e "
              "%.6e %.6e\n", 10, P_out[i].Id, P_out[i].Type, P_out[i].Pos[0], 
              P_out[i].Pos[1], P_out[i].Pos[2], P_out[i].Vel[0], P_out[i].Vel[1], 
              P_out[i].Vel[2], P_out[i].Mass, P_out[i].U, P_out[i].Rho, P_out[i].Hsml, 
              P_out[i].dN_II, P_out[i].dN_Ia, P_out[i].StarAge);


			//if(P_out[i].Type == 0 || P_out[i].Type == 4)
			{
			#ifdef METALS
				//fprintf(f,"%*.2e", 10, P_out[i].MetalMass);
				#ifdef CARBON
					fprintf(f,"%*.6e", 10, P_out[i].CarbonMass);
				#endif
				#ifdef NITROGEN
					fprintf(f,"%*.6e", 10, P_out[i].NitrogenMass);
				#endif
				#ifdef OXYGEN
					fprintf(f,"%*.6e", 10, P_out[i].OxygenMass);
				#endif
				#ifdef FLORINE
					fprintf(f,"%*.6e", 10, P_out[i].FlorineMass);
				#endif
				#ifdef NEON
					fprintf(f,"%*.6e", 10, P_out[i].NeonMass);
				#endif
				#ifdef SODIUM
					fprintf(f,"%*.6e", 10, P_out[i].SodiumMass);
				#endif
				#ifdef MAGNESIUM
					fprintf(f,"%*.6e", 10, P_out[i].MagnesiumMass);
				#endif
				#ifdef ALUMINUM
					fprintf(f,"%*.6e", 10, P_out[i].AluminumMass);
				#endif
				#ifdef SILICON
					fprintf(f,"%*.6e", 10, P_out[i].SiliconMass);
				#endif
				#ifdef PHOSPHORUS
					fprintf(f,"%*.6e", 10, P_out[i].PhosphorusMass);
				#endif
				#ifdef SULFUR
					fprintf(f,"%*.6e", 10, P_out[i].SulfurMass);
				#endif
				#ifdef CHLORINE
					fprintf(f,"%*.6e", 10, P_out[i].ChlorineMass);
				#endif
				#ifdef ARGON
					fprintf(f,"%*.6e", 10, P_out[i].ArgonMass);
				#endif
				#ifdef POTASSIUM
					fprintf(f,"%*.6e", 10, P_out[i].PotassiumMass);
				#endif
				#ifdef CALCIUM
					fprintf(f,"%*.6e", 10, P_out[i].CalciumMass);
				#endif
				#ifdef SCANDIUM
					fprintf(f,"%*.6e", 10, P_out[i].ScandiumMass);
				#endif
				#ifdef TITANIUM
					fprintf(f,"%*.6e", 10, P_out[i].TitaniumMass);
				#endif
				#ifdef VANADIUM
					fprintf(f,"%*.6e", 10, P_out[i].VanadiumMass);
				#endif
				#ifdef CHROMIUM
					fprintf(f,"%*.6e", 10, P_out[i].ChromiumMass);
				#endif
				#ifdef MANGANESE
					fprintf(f,"%*.6e", 10, P_out[i].ManganeseMass);
				#endif
				#ifdef IRON
					fprintf(f,"%*.6e", 10, P_out[i].IronMass);
				#endif
				#ifdef COBALT
					fprintf(f,"%*.6e", 10, P_out[i].CobaltMass);
				#endif
				#ifdef NICKEL
					fprintf(f,"%*.6e", 10, P_out[i].NickelMass);
				#endif
				#ifdef COPPER
					fprintf(f,"%*.6e", 10, P_out[i].CopperMass);
				#endif
				#ifdef ZINC
					fprintf(f,"%*.6e", 10, P_out[i].ZincMass);
				#endif
			#endif
			}

			fprintf(f,"\n");	
			fflush(stdout);
	}

	fclose(f);
	free(P_out);
}
