#!/Users/ali/Research/yt-i386/bin/python
#===============================================================
#	Author:	Ali Snedden
#		
#	Purpose:
#		This will output the Metal_Cooling_Rate.txt for instances where 
#		loop_coolingcurve_w_UV.py fails. Who knows why it fails.
#
#		This will loop through the metals in a particular ./redshift/density 
#		and generate the appropriate Metal_Cooling_Rate.txt
#
#================================================================
import string
import struct
import sys
import os
import subprocess
import numpy as np
from time import time

startTime = time()
argc = len(sys.argv)


# logT limits...ex 400 -> T=10^(400/100)
logT_min = 400
#logT_min = 695
logT_max = 855
#logT_max = 700
dlogT		 = 5 									#step size

# metal limits...ex -3 is 1/1000 of solar metalicity
metal_min =   -3.0
metal_max =   1.5
dmetal    =   0.25    # =0.25

#density limits


#-------------------------Help Section-----------------------------
if argc > 1 and sys.argv[1][0] == '-' and sys.argv[1][1] == 'h':
	print "USAGE: ./Output_Metals_4_hden path hden\n"
	print "where 'path' is the pathname to the density directory and "
	print "'hden' is the log10(hdensity)"
	print "To change the temperature and metal range, you must edit"
	print "the executable."
	sys.exit()

if argc != 3:
	print "USAGE: ./Output_Metals_4_hden path hden\n"
	print "where 'path' is the pathname to the density directory and "
	print "'hden' is the log10(hdensity)"
	print "To change the temperature and metal range, you must edit"
	print "the executable."
	sys.exit()



#------------------------ Get File Stems --------------------------
file_base = 'cc'           							        #stem for all filenames
path = sys.argv[1]
hden = float(sys.argv[2])
density_dir = path

temp = []
coolRate = []							#Cooling Rate for particular metallicity
heatRate = []							#Heating Rate for particular metallicity





#-------------Put all the cooling rates into one file per hden------------
#Open file
f2 = open("{0}/Metal_Cooling_Rate.txt".format(density_dir),'w+')
f2.write("# This was generated using Cloudy10 and the columns are ordered \n")
f2.write("# as log10 of the fraction of the metallicity to the solar metallicity\n")
f2.write("# \'h_*_Z\' and \'c_*_Z\' stands for cooling and heating respectively \n")
f2.write("# at * metalicity\n#\n")
f2.write("# NOTE: this was generated separately from loop_coolingcurve_w_UV.py\n")
f2.write("# b/c it broke for some reason.\n")
f2.write("# Redshift = \n# hden = {0}\n".format(hden))
f2.write("#{:>9} ".format("temp"))

#Write Header
for i in np.arange(metal_min, metal_max + dmetal, dmetal):
    f2.write("{0:>9}_Z".format("h_{0:.2f}".format(i)))
    f2.write("{0:>10}_Z ".format("c_{0:.2f}".format(i)))
f2.write("\n");


for logT in range(logT_min, logT_max, dlogT):
    coolRate = []
    heatRate = []

    #Read all files at same temperature.
    for metals in np.arange(metal_min, metal_max + dmetal, dmetal):
        dirName = density_dir + "/metal_{0:.2f}".format(metals)
        baseFileName = dirName + "/" + file_base + str(logT) + '.col'

        #print baseFileName
        f3 = open(baseFileName,'r')
        fileLine = f3.readlines()											#Read all lines of file
        f3.close()

        #Parse data line.....Second line in file
        #dataLine = fileLine[1].split( )			#w/o args this splits by ALL whitespace
        dataLine = fileLine[len(fileLine)-1].split( )	#splits by ALL whitespace
        coolRate = coolRate + [dataLine[3]]	#All cooling rates per specific temp
        heatRate = heatRate + [dataLine[2]]
        temp 		 = dataLine[1]


    #Write all NORMALIZED cooling/heating rates for all metallicities to file
    f2.write("  {:>8.2f}".format( np.log10(float(temp)) ))
    for i in range(0, len(coolRate), 1):
        f2.write("  {0:>9.4e}  {1:>9.4e}".format(float(heatRate[i])/(10**hden * 10**hden), float(coolRate[i])/(10**hden * 10**hden)))

    f2.write("\n")


f2.close()
endTime = time()
print "Time to run: %f\n" %(endTime - startTime)




#---------------Extra Non-Essential Cloudy Options-----------------
#		command = "table read \"input_flux_z3.0.con\"\n" +\
#							"intensity -5.8 range 0.123737 to 4.99735e7 Ryd\n" +\
#							"metals " + str(metals) + "\n"+\
#							"stop zone 1\n" +\
#							"hden 0\n" +\
#							"constant temperature, t=" + str(10**(logT/100.0)) + " K [linear]\n" +\
#							"save cooling \"" + baseFileName + ".col\"\n" +\
#							"save continuum file = \"" + dirName + "/input_spectrum.con\"\n" +\


#"abundances primordial\n" +\
#"save overview \"" + baseFileName + ".ovr\"\n" +\
#"metals " + str(metals) +\
#"save continuum \"" + baseFileName + ".con\" units microns\n" +\
#"save transmitted continuum file = \"" + dirName + "/trans_spectrum.con\" last\n"	#only want 1 spectra


#"stop neutral column density 14.0\n" +\



#-------------Running Integrator is Unnecessary-------------------
#Run Integrator on columns 0,6 on *.con file, we're indexing from 0...
#command = "~/Research/code/integrator/integrator " + baseFileName + ".con 0 6"
#a = subprocess.Popen([command], stdout=subprocess.PIPE, shell=True)
#(out, err) = a.communicate()
#out = "%.6s" % (string.strip(out))

#Save Temperature and Integrated spectra
#f.write("%s %s\n" % (logT/10.0, out))

