#!/Users/ali/Research/yt-i386/bin/python
#===============================================================
#	Author:	Ali Snedden
#		
#	Purpose:
#		This will output the Metal_Cooling_Rate.txt for instances where 
#		loop_coolingcurve_w_UV.py fails. Who knows why it fails.
#
#		This will loop through the metals in a particular ./redshift/ 
#		and generate the appropriate Metal_Cooling_Rate.txt for ALL 
#   the densities
#
#================================================================
import string
import struct
import sys
import os
import subprocess
import numpy as np
from time import time

startTime = time()
argc = len(sys.argv)


# logT limits...ex 400 -> T=10^(400/100)
logT_min = 400
logT_max = 850
dlogT		 = 5 									#step size

# metal limits...ex -3 is 1/1000 of solar metalicity
metal_min =   -3.0
metal_max =   1.5
dmetal    =   0.25    # =0.25

#density limits
hden_min  =   -10
hden_max  =   1.5
dhden     =   0.5

#-------------------------Help Section-----------------------------
if argc > 1 and sys.argv[1][0] == '-' and sys.argv[1][1] == 'h':
    print "USAGE: ./Output_Metals_4_hden path\n"
    print "where 'path' is the pathname to the redshit directory and "
    print "To change the temperature, metal, and density range, you must edit"
    print "the executable."
    print "NOTE: if these temp, metals, and density range don't exactly match "
    print "this script will NOT work!!!!"
    sys.exit()

if argc != 2:
    print "USAGE: ./Output_Metals_4_hden path\n"
    print "where 'path' is the pathname to the redshit directory and "
    print "To change the temperature, metal, and density range, you must edit"
    print "the executable."
    print "NOTE: if these temp, metals, and density range don't exactly match "
    print "this script will NOT work!!!!"
    sys.exit()




#------------------------ Get File Stems --------------------------
file_base = 'cc'           							        #stem for all filenames
path = sys.argv[1]
redshift_dir = path

temp = []
coolRate = []							#Cooling Rate for particular metallicity
heatRate = []							#Heating Rate for particular metallicity





#-------------Put all the cooling rates into one file per hden------------
for hden in np.arange(hden_min, hden_max + dhden, dhden):
    density_dir = redshift_dir + "/hden_{0:.2f}".format(hden)
    dbeg_time = time()             #Get Time

    #Get redshift
    f3 = open("{0}/redshift.txt".format(redshift_dir), 'r')
    redshift = f3.readlines()
    f3.close()

    #Open file
    f2 = open("{0}/Metal_Cooling_Rate.txt".format(density_dir),'w+')
    f2.write("# This was generated using Cloudy10 and the columns are ordered \n")
    f2.write("# as log10 of the fraction of the metallicity to the solar metallicity\n")
    f2.write("# \'h_*_Z\' and \'c_*_Z\' stands for cooling and heating respectively \n")
    f2.write("# at * metalicity\n#\n")
    f2.write("# NOTE: This was generated separately from loop_coolingcurve_w_UV.py\n")
    f2.write("#       using Output_metals_w_redshift.py.\n#\n")
    f2.write("# Redshift = {1}\n# hden = {0}\n#\n".format(hden,float(redshift[0])))
    f2.write("#{:>9} ".format("temp"))

    #Write Header
    for i in np.arange(metal_min, metal_max + dmetal, dmetal):
        f2.write("{0:>9}_Z".format("h_{0:.2f}".format(i)))
        f2.write("{0:>10}_Z ".format("c_{0:.2f}".format(i)))
    f2.write("\n");


    for logT in range(logT_min, logT_max + dlogT, dlogT):
        coolRate = []
        heatRate = []

        #Read all files at same temperature.
        for metals in np.arange(metal_min, metal_max + dmetal, dmetal):
            dirName = density_dir + "/metal_{0:.2f}".format(metals)
            baseFileName = dirName + "/" + file_base + str(logT) + '.col'

            #print baseFileName
            f3 = open(baseFileName,'r')
            fileLine = f3.readlines()											#Read all lines of file
            f3.close()

            #Parse data line.....Get last iteration of line
            dataLine = fileLine[len(fileLine)-1].split( )	#splits by ALL whitespace
            coolRate = coolRate + [dataLine[3]]	#All cooling rates per specific temp
            heatRate = heatRate + [dataLine[2]]
            temp 		 = dataLine[1]


        #Write all NORMALIZED cooling/heating rates for all metallicities to file
        f2.write("  {:>8.2f}".format( np.log10(float(temp)) ))
        for i in range(0, len(coolRate), 1):
            f2.write("  {0:>9.4e}  {1:>9.4e}".format(float(heatRate[i])/(10**hden * 10**hden), float(coolRate[i])/(10**hden * 10**hden)))

        f2.write("\n")

    f2.close()
    dend_time = time()
    print "{0}/Metal_Cooling_Rate.txt : {1:.4f} s".format(density_dir,dend_time - dbeg_time)


endTime = time()
print "TOTAL: Time to run: %f\n" %(endTime - startTime)

