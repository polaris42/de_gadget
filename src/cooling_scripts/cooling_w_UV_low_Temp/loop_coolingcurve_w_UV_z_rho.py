#!/Users/ali/Research/yt-i386/bin/python
#===============================================================
#	Author:	Ali Snedden
#		
#	Purpose:
#		Here I am creating a script to find the emitted spectra of 
#		a cloud of gas so that I can generate the nifty cooling curves
#		presented in Katz et al (1996) and Binney and Tremain (1980s)
#		p580.
#				
#		1. Run Cloudy at different temperatures
#		2. Integrate spectra
#		3. Write Log10(T) and total emitted energy to file.
#		
#		NOTE: This includes the HM01 photoionization.
#
#		This creates the file structure for a particular redshift.
#		The thought is that I'll submit 30+ jobs to the CRC at once.
#
#   NOTE: All path names are relative from where this script
#   is executed.
#
#   NOTE: Also, I expanded the range on this run so that it 
#
#================================================================
import string
import struct
import sys
import os
import subprocess
import numpy as np
from time import time

startTime = time()
argc = len(sys.argv)


# logT limits...ex 400 -> T=10^(400/100)
logT_min = 100
logT_max = 395
dlogT		 = 5 									#step size

# metal limits...ex -3 is 1/1000 of solar metalicity
metal_min =   -3.0
metal_max =   1.5
dmetal    =   0.25    # =0.25

#density limits
hden_max =   1.5
hden_min = -10.0
dhden    =   0.5   #change in hdensity

#-------------------------Help Section-----------------------------
if argc > 1 and sys.argv[1][0] == '-' and sys.argv[1][1] == 'h':
	print "USAGE: ./loop_coolingcurve.py redshift"
	print "where redshift is an 'int'"
	sys.exit()

if argc != 2 :
	print "USAGE: ./loop_coolingcurve.py redshift"
	print "where redshift is an 'int'"
	sys.exit()



#------------------------ Get File Stems --------------------------
REDSHIFT = float(sys.argv[1])                           #Change this for each crc run
redshift_dir = "z_{0:.2f}".format(REDSHIFT)
file_base = 'cc'           							        #stem for all filenames
command = "mkdir " + redshift_dir                       #create redshift directory
os.system(command)



#-------------------------Open Files-----------------------------
f  = open("{0}/redshift.txt".format(redshift_dir), 'w+')
f.write("{0}".format(REDSHIFT))
f.close()



#--------------------------Run cloudy----------------------------
#Loop over densities
for hden in np.arange(hden_min, hden_max + dhden, dhden):
    density_dir = redshift_dir + "/hden_{0:.2f}".format(hden)
    command = "mkdir " + density_dir
    os.system(command)                                 #create density directory

    #Create files.
    f3 = open("{0}/density.txt".format(density_dir), 'w+')
    f3.write("{0}".format(hden))
    f3.close()

    #Loop over metals
    for metals in np.arange(metal_min, metal_max + dmetal, dmetal):
        dirName = density_dir + "/metal_{0:.2f}".format(metals)
        command = "mkdir " + str(dirName)
        os.system(command)                             #create metals directory

        #Loop over Temperature
        for logT in range(logT_min, logT_max + dlogT, dlogT):
            baseFileName = dirName + "/" + file_base + str(logT)
            command = "CMB redshift {0:.2f}\n".format(REDSHIFT) +\
                      "table HM05 z={0:.2f}\n".format(REDSHIFT) +\
                      "print last\n" +\
                      "hden {0:.2f}\n".format(hden) +\
                      "metals " + str(metals) + " log\n"+\
											"element abundance helium -1.1029\n"+\
                      "stop zone 1\n" +\
                      "set trimming -10\n" +\
                      "constant temperature, t=" + str(10**(logT/100.0)) +\
                           " K [linear]\n" +\
                      "iterate to convergence\n" +\
                      "save cooling \"" + baseFileName + ".col\"\n" +\
                      "save continuum file = \"" + dirName + "/input_spectrum.con\"\n"
            cloudy_param_file = baseFileName + ".in"
            inFile = open(cloudy_param_file,'w+')		  #Make file to pass to Cloudy
            inFile.write(command)
            inFile.close()

            #Run Cloudy
            command ="~/Research/GADGET/Cloudy_10.0/source/cloudy.exe < " +\
                     cloudy_param_file + " &> " + dirName + "/cc.out"
            os.system(command)


temp = []
coolRate = []							#Cooling Rate for particular metallicity
heatRate = []							#Heating Rate for particular metallicity





#-------------Put all the cooling rates into one file per hden------------
for hden in np.arange(hden_min, hden_max, dhden):
    density_dir = redshift_dir + "/hden_{0:.2f}".format(hden)

    #Open file
    f2 = open("{0}/Metal_Cooling_Rate.txt".format(density_dir),'w+')
    f2.write("# This was generated using Cloudy10 and the columns are ordered \n")
    f2.write("# as log10 of the fraction of the metallicity to the solar metallicity\n")
    f2.write("# \'h_*_Z\' and \'c_*_Z\' stands for cooling and heating respectively \n")
    f2.write("# at * metalicity\n#\n")
    f2.write("# Redshift = {0}\n# hden = {1}\n".format(REDSHIFT,hden))
    f2.write("#{:>9} ".format("temp"))
    for i in np.arange(metal_min, metal_max + dmetal, dmetal):
        f2.write("{0:>9}_Z".format("h_{0:.2f}".format(i)))
        f2.write("{0:>10}_Z ".format("c_{0:.2f}".format(i)))
    f2.write("\n");


    for logT in range(logT_min, logT_max, dlogT):
        coolRate = []
        heatRate = []

        #Read all files at same temperature.
        for metals in np.arange(metal_min, metal_max + dmetal, dmetal):
           dirName = density_dir + "/metal_{0:.2f}".format(metals)
           baseFileName = dirName + "/" + file_base + str(logT) + '.col'
           f3 = open(baseFileName,'r')
           fileLine = f3.readlines()											#Read all lines of file

           #Parse data line.....Second line in file
           dataLine = fileLine[1].split( )			#w/o args this splits by ALL whitespace
           coolRate = coolRate + [dataLine[3]]	#All cooling rates per specific temp
           heatRate = heatRate + [dataLine[2]]
           temp 		 = dataLine[1]


        #Write all NORMALIZED cooling/heating rates for all metallicities to file
        f2.write("  {:>8.2f}".format( np.log10(float(temp)) ))
        for i in range(0, len(coolRate), 1):
           f2.write("  {0:>9}  {1:>9}".format(float(heatRate[i])/(10**hden * 10**hden), float(coolRate[i])/(10**hden * 10**hden)))

        f2.write("\n")
    f2.close()



endTime = time()
print "Time to run: %f\n" %(endTime - startTime)




#---------------Extra Non-Essential Cloudy Options-----------------
#		command = "table read \"input_flux_z3.0.con\"\n" +\
#							"intensity -5.8 range 0.123737 to 4.99735e7 Ryd\n" +\
#							"metals " + str(metals) + "\n"+\
#							"stop zone 1\n" +\
#							"hden 0\n" +\
#							"constant temperature, t=" + str(10**(logT/100.0)) + " K [linear]\n" +\
#							"save cooling \"" + baseFileName + ".col\"\n" +\
#							"save continuum file = \"" + dirName + "/input_spectrum.con\"\n" +\


#"abundances primordial\n" +\
#"save overview \"" + baseFileName + ".ovr\"\n" +\
#"metals " + str(metals) +\
#"save continuum \"" + baseFileName + ".con\" units microns\n" +\
#"save transmitted continuum file = \"" + dirName + "/trans_spectrum.con\" last\n"	#only want 1 spectra


#"stop neutral column density 14.0\n" +\



#-------------Running Integrator is Unnecessary-------------------
#Run Integrator on columns 0,6 on *.con file, we're indexing from 0...
#command = "~/Research/code/integrator/integrator " + baseFileName + ".con 0 6"
#a = subprocess.Popen([command], stdout=subprocess.PIPE, shell=True)
#(out, err) = a.communicate()
#out = "%.6s" % (string.strip(out))

#Save Temperature and Integrated spectra
#f.write("%s %s\n" % (logT/10.0, out))

