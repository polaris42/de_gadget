#!/Users/ali/Research/yt-i386/bin/python
#===============================================================
#	Author:	Ali Snedden
#		
#	Purpose:
#		Here I am creating a script to find the emitted spectra of 
#		a cloud of gas so that I can generate the nifty cooling curves
#		presented in Katz et al (1996) and Binney and Tremain (1980s)
#		p580.
#				
#		1. Run Cloudy at different temperatures
#		2. Integrate spectra
#		3. Write Log10(T) and total emitted energy to file.
#================================================================
import string
import struct
import sys
import os
import subprocess
import numpy as np
from time import time

startTime = time()
argc = len(sys.argv)
file_base = 'cc'							#stem for all filenames
dir_base = 'metal'						#stem for all directories

# logT limits...ex 400 -> T=10^(400/100)
logT_min = 400
logT_max = 855
dlogT		 = 5 									#step size

# metal limits...ex -3 is 1/1000 of solar metalicity
metal_min =  -3.0
metal_max =   1.5
dmetal    =   0.25

#-------------------------Help Section-----------------------------
if argc > 1 and sys.argv[1][0] == '-' and sys.argv[1][1] == 'h':
	print "USAGE: ./loop\n"
	print "More stuff\n"
	exit()



#--------------------------Run cloudy----------------------------
#Loop over metals
for metals in np.arange(metal_min, metal_max + dmetal, dmetal):
	dirName = dir_base + str(metals)
	command = "mkdir " + str(dirName)
	os.system(command)

	#Loop over Temperature
	for logT in range(logT_min, logT_max, dlogT):
		baseFileName = dirName + "/" + file_base + str(logT)
		command = "CMB redshift 0\n" +\
							"metals " + str(metals) + " log\n"+\
							"element abundance helium -1.1029\n"+\
							"stop zone 1\n" +\
							"hden 0\n" +\
              "print last\n" +\
              "set trimming -10\n" +\
              "iterate to convergence\n" +\
							"constant temperature, t=" + str(10**(logT/100.0)) + " K [linear]\n" +\
							"coronal equilibrium, t=" + str(10**(logT/100.0)) + " K [linear]\n" +\
							"no photoionization\n" +\
							"save cooling \"" + baseFileName + ".col\"\n" +\
              "save continuum file = \"" + dirName + "/input_spectrum.con\"\n"
		cloudy_param_file = baseFileName + ".in"
		inFile = open(cloudy_param_file,'w+')						#Make file to pass to Cloudy
		inFile.write(command)
		inFile.close()

		#Run Cloudy
		command ="~/Research/GADGET/Cloudy_10.0/source/cloudy.exe < " + cloudy_param_file +\
							" &> " + dirName + "/cc.out"
		os.system(command)

temp = []
coolRate = []							#Cooling Rate for particular metallicity
heatRate = []

#-----------------------Write File Header------------------------
f2 = open("Metal_Cooling_Rate.txt",'w+')
f2.write("# This was generated using Cloudy10 and the columns are ordered \n")
f2.write("# as a log10 of the fraction of the metallicity to the solar metallicity\n")
f2.write("# \'c_*_Z\' and \'h_*_Z\' stands for cooling and heating respectively \n")
f2.write("# at * metalicity\n#\n")
f2.write("#{:>9} ".format("temp"))
for i in np.arange(metal_min, metal_max + dmetal, dmetal):
	f2.write("{0:>9}_Z".format("h_{0:.2f}".format(i)))
	f2.write("{0:>10}_Z ".format("c_{0:.2f}".format(i)))
f2.write("\n");


#-------------Put all the cooling rates into one file------------
for logT in range(logT_min, logT_max, dlogT):
	coolRate = []

	#Read all files at same temperature.
	for metals in np.arange(metal_min, metal_max + dmetal, dmetal):

		dirName = dir_base + str(metals)
		baseFileName = dirName + "/" + file_base + str(logT) + '.col'
		f3 = open(baseFileName,'r')
		fileLine = f3.readlines()											#Read all lines of file
		f3.close()

		#Parse data line.....Second line in file
		dataLine = fileLine[1].split( )						#w/o args this splits by ALL whitespace
		coolRate = coolRate + [dataLine[3]]				#All metal cooling rates per specific temp
		temp 		 = dataLine[1]
		heatRate = heatRate + [dataLine[2]]


	#Write all cooling rates to file
	f2.write("  {:>9.2f}".format( np.log10(float(temp)) ))
	for i in range(0, len(coolRate), 1):
		f2.write("  {0:>9}  {1:>9}".format(heatRate[i], coolRate[i]))

	f2.write("\n")


endTime = time()
print "Time to run: %f\n" %(endTime - startTime)




#---------------Extra Non-Essential Cloudy Options-----------------
#"abundances primordial\n" +\
#"save overview \"" + baseFileName + ".ovr\"\n" +\
#"metals " + str(metals) +\
#"save continuum \"" + baseFileName + ".con\" units microns\n" +\


#-------------Running Integrator is Unnecessary-------------------
#Run Integrator on columns 0,6 on *.con file, we're indexing from 0...
#command = "~/Research/code/integrator/integrator " + baseFileName + ".con 0 6"
#a = subprocess.Popen([command], stdout=subprocess.PIPE, shell=True)
#(out, err) = a.communicate()
#out = "%.6s" % (string.strip(out))

#Save Temperature and Integrated spectra
#f.write("%s %s\n" % (logT/10.0, out))

