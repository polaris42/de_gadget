/***************************************************************
Author: Ali Snedden
Date: 10/5/12

NSH 341a
Dept. of Physics
Univ. of Notre Dame

Purpose:
	Here I create a function that will output either the gas or 
	dark matter or star particles (or some combination thereof) 
	into a format that can be read by plot.gnp (a gnuplot script).
	
	It will output data for a for 3D scatter plot. It will also output
	data for a 2D contour plot that can be used to generate plots 
	similar to those given by splash.

	I created these to trouble shoot my snapshot* files when star
	formation is enabled.

	I picked variable names to avoid name conflicts. Also I assumed that the box
	is cubed w/ periodic BC so I can use All.BoxSize. Also I am assuming that the 
	contour plot is the same dimension in x and y (i.e. xDIM = yDIM). I am 
	assuming that we are looking down the z-axis.
****************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <mpi.h>
#include <unistd.h>

#include "allvars.h"
#include "proto.h"

// pType = 0,1,2,3,4,5,6 = P[*].Type as defined in allvars.h
#ifdef WRITEGNUPLOT
void writeGnuplotData(int pType){
	FILE * f = NULL;											//Particle Scatter plot
	FILE * fc = NULL; 										//Contour plot 
	FILE * fcSum = NULL;									//Contour plot 
	char pFileName[300];									//Name for particle file
	char cFileName[300];									//Name for contour file
	int i=0;															//index for P[]
	int x=0;															//contour's x index
	int y=0;															//contour's y index
	int xDIM = 100;												//# of contour grids in x
	int yDIM = 100;												//# of contour grids in y
	int totalParticles =0;								//Get total Number of particles. 
	float xMax = All.BoxSize;							//You'd be screwed if it wasn't a cube
	float yMax = All.BoxSize;
	float scaleLengthX = xMax / xDIM;			//Resolution of grid in X
	float scaleLengthY = yMax / yDIM;			//Resolution of grid in Y
	float contour[xDIM][yDIM];
	float contourSum[xDIM][yDIM];					//Sum of contours from both processors

	//This is a problem is you are trying to sum over a non MPI data type, like 'long'
	MPI_Reduce(&NumPart,&totalParticles,1,MPI_INT,MPI_SUM,0,MPI_COMM_WORLD);


	//Root Task ONLY!
	if(ThisTask ==0){
		//Remove later...
		printf("totalParticles: %i   \n",totalParticles);
	}

	//Initialize to zero.
	for(x=0; x<xDIM; x++){
		for(y=0; y<yDIM; y++){
			contour[x][y] = 0.0;
			contourSum[x][y] = 0.0;
		}
	}

	/************** Below is done on each processor individually ******************/

	//Setup output files for each processor
	sprintf(pFileName,"%sparticles_task%i.dat",All.OutputDir, ThisTask);
	sprintf(cFileName,"%scontours_task%i.dat",All.OutputDir, ThisTask);
	f = fopen(pFileName,"w+");
	fc = fopen(cFileName,"w+"); 							//Mistake?  Gets opened below

	fprintf(f,"#iex\tx\ty\tz\ttype\n");
	fprintf(f,"#NumPart: %i N_gas: %i NtypeLocal: (%i %i %i %i %i %i)\n",
					NumPart, N_gas, NtypeLocal[0], NtypeLocal[1], NtypeLocal[2], NtypeLocal[3],
					NtypeLocal[4], NtypeLocal[5]);

	//Print Scatter Plot info
	for(i=0; i<NumPart; i++){
		fprintf(f, "%e %e %e %i %e\n",P[i].Pos[0], P[i].Pos[1], 
						P[i].Pos[2], P[i].Type, SphP[i].Density);

		//Sum up contours.
		if(P[i].Type==pType){
			//Get grid location
			x = (int)floor(P[i].Pos[0]/scaleLengthX);
			y = (int)floor(P[i].Pos[1]/scaleLengthY);
			contour[x][y]++;	
		}
	}


	//Print to contour file.
	for(x=0; x<xDIM; x++){
		for(y=0; y<yDIM; y++){
			fprintf(fc,"%*i %*i %*f \n", 9,(int)floor(x * scaleLengthX), 9,
							(int)floor(y * scaleLengthY), 9,contour[x][y]);
		}
		fprintf(fc,"\n");
	}

	//Sum up all contours[][] onto Task 0, This is highly inefficient but easy to code
	MPI_Reduce(contour, contourSum, xDIM*yDIM, MPI_FLOAT, MPI_SUM, 0, MPI_COMM_WORLD); 

	if(ThisTask==0){
		sprintf(cFileName,"%s/contour_sum.dat",All.OutputDir);
		fcSum = fopen(cFileName,"w+");    

		//Print out contourSum from Task 0
		for(x=0; x<xDIM; x++){
			for(y=0; y<yDIM; y++){
				fprintf(fcSum, "%*i %*i %*f \n", 9, (int)floor(x * scaleLengthX), 9,
								(int)floor(y * scaleLengthY), 9, contourSum[x][y]);
			}
			fprintf(fcSum,"\n");
		}
		fclose(fcSum);
	}

	fclose(f);
	fclose(fc);
}
#endif




