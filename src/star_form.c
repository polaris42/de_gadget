/***********************************************************
Author:  Ali Snedden
Date: 9/10/12

NSH 341a
Dept. of Physics
Univ. of Notre Dame

Purpose: It is to create star particles following the logic
  of Kobayashi(2004). I (Ali) am taking the effort to be 
  sure that this is implemented correctly. This is a large 
  endevor that this was (supposedly) written by Xinghai.  
  I am documenting this, unlike Xinghai.

	I think that the issue could arise where this routine is run,
	screws up the tree, and the tree doesn't get reconstructed. To 
	prevent this (or at least try), I set TreeReconstructFlag = 1.
	

	NOTE : Katz, Weinberg, and Hernquist (1996) have a good discussion of
		 		 the c_star. 

	WARNING : SphP[i].cooling_rate is only updated at each Kick. This means 
				 	that it is only known when a particles' P[i].Ti_endstep == All.Ti_Current.
					I'm not sure if I should qualify star formation upon this condition.
					Katz et al (1996), seem to imply that this computation is run at EVERY
					timestep. So perhaps this isn't as bad as it seems.

  Recall:
    memmove(&destination, &source, size_in_bytes);

  K04 = Kobayashi 2004


	CHANGES: 
	  BEFORE:	t_dyn = 1.0 / sqrt(4.0 * M_PI * All.G);  
		AFTER: 	t_dyn = 1.0 / sqrt(4.0 * M_PI * ALl.G * SphP[i].Density); 

		BEFORE: c_star = 0.1;
		AFTER:  c_star = 1.0;
************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <mpi.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_integration.h>

#include "allvars.h"
#include "proto.h"

#ifdef COOLING            //ALI added
#ifdef SFR                //ALI added

double c_star = 0.015;			//SF parameter, K04 sets = 1.0, K07 = 0.1 see K07 p1472
													//Also see Katz (1996)
#if defined(NO_JEANS_CRITERION) && defined(NH_MIN) && defined(T_MAX) && \
defined(OVERDENSITY)
static double nH_min = NH_MIN;
static double T_max  = T_MAX;
static double minOverdensity = OVERDENSITY;
#endif


void star_form(void)
{
	/*************************** Define Variables *****************************/
	char pathName[300];											//Output file name for sfr_rate*.txt
  int i;
	int starsFormed = 0;										//Local stars formed in this run
	int totalStarsFormed=0; 								//All (global) stars formed in this run
	int isStarForming = 0;
	double star_prob;
	double star_random;
	double massOfStarsFormed = 0.0;					//LOCAL mass of stars formed at this step
	double totalMassOfStarsFormed = 0.0;		//GLOBAL mass of stars formed.
	double redshift;												// = 1/All.Time - 1;
 	double gas_entropy;											//=pressure / rho^gamma, in cart. units
  double a3 = All.Time * All.Time * All.Time;
  double a3inv = 1.0 / a3;

	#if defined(NO_JEANS_CRITERION) && defined(NH_MIN) && defined(T_MAX) && \
	defined(OVERDENSITY)
		double T 										= 0;
		double nH 									= 0;
		double rho									= 0;
		double overdensity					= 0;			//Relative overdensity
		double meanDensity 					= 0;			//Average comoving density.
		double globalGasMassSum 		= 0;
		double globalDensitySum 		= 0;
		double localDensitySum			= 0;			//Local density sum
		double localGasMassSum 			= 0;
	#else
		double soundspeed;			// Local speed of sound 				   (cartesian, internal!)
 		double t_sound;					// = smoothing_length / soundspeed (cartesian, internal!)
		double t_dyn;						// = 1/ sqrt(4 * PI * G * density) (cartesian, internal!)
		double gas_u;						//gas energy in (internal energy / internal mass)
		double t_cool;					// 	= rho * U / Lambda						 (cartesian, internal)
	#endif

	#ifdef PERIODIC
		//double sfr_density;											//SFR (M_sun / (year * (Mpc/h)^3)
  	FILE *sfr_rate_density = NULL;
	#else 
 		FILE *sfr_rate         = NULL;
	#endif  

  #ifdef FEEDBACK_W_SPH_KERNAL
    FILE *typeII_snr       = NULL;
    FILE *typeIa_snr       = NULL;
    double dN_II           = 0;       //LOCAL  Number of TypeII / year
    double dN_Ia           = 0;       //LOCAL  Number of TypeIa / year
    double tot_dN_II       = 0;       //GLOBAL Number of TypeII / year
    double tot_dN_Ia       = 0;       //GLOBAL Number of TypeIa / year
  #endif

	//Get Redshift
	if(All.ComovingIntegrationOn){
		redshift = 1/All.Time - 1;
	}else{
		#ifdef UVBACKGROUND
			redshift = mapTime2Redshift(All.Time);
		#else
			redshift = phys_time;		//This is meaningless in this case. but it needs a value.
		#endif	
	}




	//Get average density.
	#if defined(NO_JEANS_CRITERION) && defined(NH_MIN) && defined(T_MAX) && \
	defined(OVERDENSITY)
		for(i=0; i<N_gas; i++)
		{
			localDensitySum += SphP[i].Density * P[i].Mass;
			localGasMassSum += P[i].Mass;
		}
		MPI_Allreduce(&localDensitySum, &globalDensitySum, 1, MPI_DOUBLE, MPI_SUM,
									MPI_COMM_WORLD);
		MPI_Allreduce(&localGasMassSum, &globalGasMassSum, 1, MPI_DOUBLE, MPI_SUM,
									MPI_COMM_WORLD);

		/* BoxSize is meaningless when it isn't periodic. So use mass-weighted density
			 in that case */
		#ifdef PERIODIC
			meanDensity = globalGasMassSum / pow(All.BoxSize, 3);
		#else
			meanDensity = globalDensitySum / globalGasMassSum;
		#endif
		//printf("meanDensity = %e\n",meanDensity);
	#endif


	//Get Physical Time
  phys_time = get_phys_time(All.Time);    //In units of My

  if(ThisTask == 0)
	{
 	  printf("\nphys_time = %f My.  dt_{sf} = %fMy\n", phys_time, 
						phys_time - star_form_time);
 	}





  //Allow at least 2 Million years to pass between Star Formation Runs
  if((phys_time - star_form_time) >= 2.0)
 	{
    /******************* Loop through all the SPH Particles ********************/
 	  for(i = 0; i < N_gas; i++)
    {

			/* 
				NOTE : during drift (find_next_sync_point_and_drift() -> move_particles()),
 				SphP[].Pressure and SphP[].Density have been updated, but SphP[].Entropy 
				has not been updated. 

        RECALL : That pressure has a goofy-ass hybrid of comoving 
        and cartesian coordinates and that P_code = P_cart * a^(3*GAMMA) and that

                  SphP[].Pressure = Entropy_phys * Density_comov^Gamma      
			*/			
			isStarForming = 0;
			gas_entropy = SphP[i].Pressure / pow(SphP[i].Density, GAMMA);//Put in cart. units


			#if defined(NO_JEANS_CRITERION) && defined(NH_MIN) && defined(T_MAX) && \
			defined(OVERDENSITY)
				rho = SphP[i].Density;
				nH = get_nH(rho);
				T  = get_gas_temp(rho, gas_entropy);
				overdensity = SphP[i].Density / meanDensity;

				if(SphP[i].DivVel < 0 && nH_min <= nH && T_max >= T && 
				minOverdensity <= overdensity){
					isStarForming = 1;
				}else{
					isStarForming = 0;
				}
			#else
				/* 
				NOTE : soundspeed, t_sound, and t_dyn are all in INTERNAL time units!!!
		 	 	SphP[].Pressure and SphP[].Density[] are COMOVING! 
				*/
				soundspeed = sqrt(GAMMA * SphP[i].Pressure / 
                          (SphP[i].Density * pow(a3, GAMMA_MINUS1)) );  
				gas_u = gas_entropy * pow(SphP[i].Density * a3inv, GAMMA_MINUS1) / GAMMA_MINUS1;

				t_sound = SphP[i].Hsml * All.Time / soundspeed;                 //Eqn. 11 in K04
				t_dyn = 1.0 / sqrt(4.0* M_PI * All.G * SphP[i].Density * a3inv);//Eqn. 10 in K04
				t_cool = (gas_u* SphP[i].Density * a3inv) / SphP[i].cooling_rate;//Eqn. 9 in K04

				//Is gas star forming?
				if(SphP[i].DivVel < 0 && t_cool < t_dyn && t_dyn < t_sound){
					isStarForming = 1;
				}else{
					isStarForming = 0;
				}
			#endif

	    //Later move inside of if(isStarForming)
	    //star_random = gsl_rng_uniform(random_generator);

 
      /**************** CHECK that Star Formation Criteria is met *****************/
      if(isStarForming == 1)
			{
				//Eqn. 14 K04
				if(All.ComovingIntegrationOn)
				{
		      star_prob = 1.0 - exp(-c_star * sqrt(4.0 * M_PI * GRAVITY * SphP[i].Density *
											a3inv *  All.UnitDensity_in_cgs) * (phys_time - star_form_time) * 
                      SEC_PER_MEGAYEAR * All.HubbleParam); 
				}
				else
				{
		      star_prob = 1.0 - exp(-c_star * sqrt(4.0 * M_PI * GRAVITY * SphP[i].Density *
										  All.UnitDensity_in_cgs) * (phys_time - star_form_time) * 
											SEC_PER_MEGAYEAR);	//No 'h' b/c not cosmological!
				}
	      star_random = gsl_rng_uniform(random_generator);


				//Gas -> Star
				if(star_prob > star_random)
				{
					TreeReconstructFlag = 1;			  //ALI: This routine could screw up the tree 
	      	P[i].Type = 4;        	        //Change from Sph Part. to Star Part.
      		P[i].StarAge = phys_time;  		  //Units = Millions Years.
					massOfStarsFormed += P[i].Mass;
						
					#ifdef FEEDBACK_W_SPH_KERNAL
            P[i].origStarMass = P[i].Mass;  //Preserve star mass for feedback eqns later
						P[i].StarHsml = SphP[i].Hsml;
						P[i].NumGasNearby = SphP[i].NumNgb;
					#endif

         	//Copy Particle to StarP[]
	      	memmove(&StarP[0], &P[i], sizeof(struct particle_data));
         	//Shift other particles up in the list P[] 
	      	memmove(&P[i], &P[i + 1], (NumPart - 1 - i) * sizeof(struct particle_data));
         	//Place StarP[] at the end of list P[]
	      	memmove(&P[NumPart - 1], &StarP[0], sizeof(struct particle_data));
 					//Remove relevant Sph Particle
	      	if(i < (N_gas - 1)){         				//No need to move last SphP 
 	      	  memmove(&SphP[i], &SphP[i + 1], 
										(N_gas - 1 - i)*sizeof(struct sph_particle_data));
					}
				
					//Check for errors	
					if(i >= N_gas){
						printf("WARNING :::: i >= N_gas :::: Check star_form.c\n");
						endrun(1000);
					}

					starsFormed += 1;
	      	N_star += 1;
         	N_gas -= 1;		
				}
	    }
			else
			{
				//SphP[i].Sfr = 0.0;
			}
    }

    MPI_Allreduce(&starsFormed, &totalStarsFormed,1,MPI_INT, MPI_SUM, MPI_COMM_WORLD);
    MPI_Allreduce(&massOfStarsFormed, &totalMassOfStarsFormed,1,MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);

	  
		//Update the "global" variables on all processors simultaneously
    All.TotN_gas  -= totalStarsFormed;
		All.TotN_star += totalStarsFormed;
	  
    if(ThisTask == 0)
    {
      printf("\n%d stars formed in whole"
						 "simulation.\n", totalStarsFormed);
    }

		//This currently only works with periodic boundary conditions
    if(ThisTask == 0 && totalStarsFormed > 0)
    {
			#ifdef PERIODIC
				sprintf(pathName,"%ssfr_rate_density.txt",All.OutputDir);
      	sfr_rate_density = fopen(pathName,"a+");

				if(All.ComovingIntegrationOn)
				{
					totalMassOfStarsFormed = totalMassOfStarsFormed * All.UnitMass_in_g / 
																	(All.HubbleParam * SOLAR_MASS);  //Convert to M_sun
					//sfr = totalMassOfStarsFormed / ((phys_time - star_form_time) * 
						//		SEC_PER_MEGAYEAR / SEC_PER_YEAR); 
					//sfr_density= sfr/pow(All.BoxSize * All.Time / (1000.0 * All.HubbleParam),3);
				}
				else
				{
					totalMassOfStarsFormed = totalMassOfStarsFormed * All.UnitMass_in_g / 
																	 SOLAR_MASS;  									//Convert to M_sun
					//sfr = totalMassOfStarsFormed / ((phys_time - star_form_time) * 
					//			SEC_PER_MEGAYEAR / SEC_PER_YEAR); 
					//sfr_density= sfr/pow(All.BoxSize / (1000.0), 3);
				}

      	fprintf(sfr_rate_density, "%e\t%e\t%e\t%e\n", redshift, 
								phys_time, totalMassOfStarsFormed, phys_time - last_sf_event_time); 
      	fclose(sfr_rate_density);

			#else
				sprintf(pathName,"%ssfr_rate.txt",All.OutputDir);
      	sfr_rate = fopen(pathName,"a+");

				if(All.ComovingIntegrationOn)
				{
					totalMassOfStarsFormed = totalMassOfStarsFormed * All.UnitMass_in_g / 
																	(All.HubbleParam * SOLAR_MASS);  //Convert to M_sun
				}
				else
				{
					totalMassOfStarsFormed = totalMassOfStarsFormed * All.UnitMass_in_g / 
																	(SOLAR_MASS);  										//Convert to M_sun
				}

				//sfr = totalMassOfStarsFormed / ((phys_time - star_form_time) * 
				//			SEC_PER_MEGAYEAR / SEC_PER_YEAR); 
      	fprintf(sfr_rate, "%e\t%e\t%e\t%e\n", redshift, phys_time, 
								totalMassOfStarsFormed, phys_time - last_sf_event_time);
      	fclose(sfr_rate);
			#endif
    }

	
		//Update times
		if(totalStarsFormed > 0){
			last_sf_event_time = phys_time;
		}
    star_form_time = phys_time;


    /* ensures that new tree will be constructed only if particles are deleted */
		if(totalStarsFormed > 0){
    	All.NumForcesSinceLastDomainDecomp = 1 + All.TreeDomainUpdateFrequency * All.TotNumPart;
		}



    //Record the TypeII and TypeIa Supernova rates
    #ifdef FEEDBACK_W_SPH_KERNAL
      if(All.TotN_star > 0)
      {
        //Loop over stars.
        for(i=N_gas; i<NumPart; i++)
        {
          if(P[i].Type == 4)
          {
            dN_II += get_typeII_snr(i);
            dN_Ia += get_typeIa_snr(i);
          }
        }

        MPI_Allreduce(&dN_II, &tot_dN_II, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
        MPI_Allreduce(&dN_Ia, &tot_dN_Ia, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);

        //Comment later
        //printf("Writing GLOBAL TypeII and TypeIa SNR\n"); fflush(stdout);

        if(ThisTask == 0 && All.TotN_star > 0)
        {

          sprintf(pathName, "%sTypeII_SNR.txt", All.OutputDir);
          typeII_snr = fopen(pathName, "a+");

          sprintf(pathName, "%sTypeIa_SNR.txt", All.OutputDir);
          typeIa_snr = fopen(pathName, "a+");

          fprintf(typeII_snr, "%e\t%e\t%e\n", redshift, phys_time, tot_dN_II);
          fprintf(typeIa_snr, "%e\t%e\t%e\n", redshift, phys_time, tot_dN_Ia);
  
          fclose(typeII_snr);
          fclose(typeIa_snr);
        }    
      }
    #endif
  }
}
#endif                //End of SFR





/********************************************************************************
	Get's hydrogen density
********************************************************************************/
double get_nH(double rho)
{
	double nH = 0;

	if(All.ComovingIntegrationOn){
		nH =  rho/pow(All.Time, 3) * All.UnitDensity_in_cgs * HYDROGEN_MASSFRAC *
          pow(All.HubbleParam, 2) / PROTONMASS;
	}else{
		nH = rho * All.UnitDensity_in_cgs * HYDROGEN_MASSFRAC / PROTONMASS;
	}
	
	return nH;
}






/***********************************************************************************
ALI: Here we use friedman's eqn where we know H(a) = adot/a. Separate and integrate

	da/dt = a * H(a)  ---->  dt = da / (a * H(a)).  

We take H(a) from Carrol & Ostlie (2nd) eqn 29.128. Recall Omega0 = Omega_matter.

NOTE: Omega_k = 1 - All.Omega0 - All.OmegaLambda 
			Omega_r = 0.

NOTE: HUBBLE is in (h/sec), All.HubbleParam is little 'h'
***********************************************************************************/
double phys_time_integrand(double a, void *param)
{
   double phys_time_int;

   #ifndef DARKENERGY
      phys_time_int = 1.0 / (HUBBLE * All.HubbleParam) * pow(All.Omega0 / a + All.OmegaLambda * pow(a, 2.0) + (1 - All.Omega0 - All.OmegaLambda), -0.5);
   #endif

   #ifdef DARKENERGY
      #ifdef SCALARFIELD
         All.dark_factor = get_dark_factor(a);

         phys_time_int = 1.0 / (HUBBLE * All.HubbleParam) * pow(All.Omega0 / a + (All.OmegaLambda * pow(a, 2.0) * All.dark_factor) + (1 - All.Omega0 - All.OmegaLambda), -0.5);
      #endif

      #ifdef DARKPARAM
         All.dark_factor = get_dark_factor(a);

         phys_time_int = 1.0 / (HUBBLE * All.HubbleParam) * pow(All.Omega0 / a + (All.OmegaLambda * pow(a, 2.0) * All.dark_factor) + (1 - All.Omega0 - All.OmegaLambda), -0.5);
      #endif

      #ifdef GCG
         All.dark_factor = get_dark_factor(a);

         phys_time_int = 1.0 / (HUBBLE * All.HubbleParam) * pow(All.Omega0 / a + (All.OmegaLambda * pow(a, 2.0) * All.dark_factor) + (1 - All.Omega0 - All.OmegaLambda), -0.5);
      #endif
   #endif

  return phys_time_int;
}




/******************************************************************************
This sets phys_time to the physical time since the beginning of simulation in 
megayears.

Creates a Gnu Scientific Library Function. See gsl_math.h for documentation. 
A function pointer is passed as an argument to this struct. This is similar to the
qsort() function in stdlib.h (see cplusplus.com). 

Returns time in MegaYears.
*******************************************************************************/
double get_phys_time(double atime)
{
	double time;

  /************************ For cosmological simulations **********************/
  if(All.ComovingIntegrationOn)
  {
		#define WORKSIZE 1000
    double result, abserr;
    gsl_function F;
    gsl_integration_workspace *workspace;

    F.function = &phys_time_integrand;           //Pointing to a FUNCTION
    workspace = gsl_integration_workspace_alloc(WORKSIZE);

    //Integrate from beginning to current Time / Scale factor.

    // The reason for the ifdefs here is that, when using SCALARFIELD, dark_factor is obtained using an interpolation of data that
    // has been read in from a file. That file does NOT have a starting at 0 (because of the way the dynamical eqs. are solved. N = ln(a),
    // and if a = 0, N is undefined). This means that, when trying to integrate from 0^a, as the vanilla version does (which is what the limits
    // should be, strictly speaking), I get a gsl error because I'm trying to use a limit that is not within the bounds of the read-in data. Hence,
    // in the ifdef SCALARFIELD block, the lower limit is All.scale_factor[0], which is the earliest time I have.

    #ifndef SCALARFIELD
      gsl_integration_qag(&F, 0.0, atime, 1.0e-16, 1.0e-8, WORKSIZE, GSL_INTEG_GAUSS41, workspace, &result, &abserr);
    #endif

    #ifdef SCALARFIELD 
      gsl_integration_qag(&F, All.scale_factor[0], atime, 1.0e-16, 1.0e-8, WORKSIZE, GSL_INTEG_GAUSS41, workspace, &result, &abserr);
    #endif  

    time = result;      

    gsl_integration_workspace_free(workspace);

  }else{
    time = atime * All.UnitTime_in_s; 	// in (seconds/h)
	}

	time /= SEC_PER_MEGAYEAR;
	return time;
}


#endif                //ALI added
