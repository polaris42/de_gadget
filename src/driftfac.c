#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <mpi.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_integration.h>

#include "allvars.h"
#include "proto.h"

/*! \file driftfac.c
 *  \brief compute look-up tables for prefactors in cosmological integration
 */

static double logTimeBegin;
static double logTimeMax;


/*! This function computes look-up tables for factors needed in
 *  cosmological integrations. The (simple) integrations are carried out
 *  with the GSL library.  Separate factors are computed for the "drift",
 *  and the gravitational and hydrodynamical "kicks".  The lookup-table is
 *  used for reasons of speed.
 *
 *  This initializes the Look up tables (DriftTable, GravKickTable, and 
 *  HydroKickTable). These tables represent the integrand in eqn 27 & 28.
 *  Recall that H(a) = (1/a) * da/dt. See comments for respective integrals 
 *  below.
 */
void init_drift_table(void)
{
  #define WORKSIZE 100000
  int i;									
  double result;										//Integral from integration in eqns 27 & 28
	double abserr;
  gsl_function F;
  gsl_integration_workspace *workspace;

  logTimeBegin = log(All.TimeBegin);
  logTimeMax = log(All.TimeMax);

  workspace = gsl_integration_workspace_alloc(WORKSIZE);

	//Initialize the appropriate integrands from eqns 27 & 28 (Springel '05)
  for(i = 0; i < DRIFT_TABLE_LENGTH; i++)
  {
		//Drift Table
   #ifdef DARKENERGY
      #ifdef SCALARFIELD
         printf("%d of %d\n", i, (int)DRIFT_TABLE_LENGTH);
      #endif
   #endif
    F.function = &drift_integ;
    gsl_integration_qag(&F, exp(logTimeBegin), exp(logTimeBegin + ((logTimeMax - logTimeBegin) / DRIFT_TABLE_LENGTH) * (i + 1)), 
        0, 1.0e-8, WORKSIZE, GSL_INTEG_GAUSS41, workspace, &result, &abserr);
    DriftTable[i] = result;
		
		//Gravity Kick Table
    F.function = &gravkick_integ;
    gsl_integration_qag(&F, exp(logTimeBegin), exp(logTimeBegin + ((logTimeMax - logTimeBegin) / DRIFT_TABLE_LENGTH) * (i + 1)), 
        0, 1.0e-8, WORKSIZE, GSL_INTEG_GAUSS41, workspace, &result, &abserr);
    GravKickTable[i] = result;

		//Hydrodynamics Kick Table
    F.function = &hydrokick_integ;
    gsl_integration_qag(&F, exp(logTimeBegin), exp(logTimeBegin + ((logTimeMax - logTimeBegin) / DRIFT_TABLE_LENGTH) * (i + 1)), 0,
			  1.0e-8, WORKSIZE, GSL_INTEG_GAUSS41, workspace, &result, &abserr);
    HydroKickTable[i] = result;
  }

  gsl_integration_workspace_free(workspace);
}


/*! This function integrates the cosmological prefactor for a drift step
 *  between time0 and time1. The value returned is * \f[ \int_{a_0}^{a_1}
 *  \frac{{\rm d}a}{H(a)} * \f]
 */
double get_drift_factor(int time0, int time1)
{
  double a1, a2, df1, df2, u1, u2;
  int i1, i2;

  /* note: will only be called for cosmological integration */

  a1 = logTimeBegin + time0 * All.Timebase_interval;
  a2 = logTimeBegin + time1 * All.Timebase_interval;

  u1 = (a1 - logTimeBegin) / (logTimeMax - logTimeBegin) * DRIFT_TABLE_LENGTH;
  i1 = (int) u1;
  if(i1 >= DRIFT_TABLE_LENGTH)
    i1 = DRIFT_TABLE_LENGTH - 1;

  if(i1 <= 1)
    df1 = u1 * DriftTable[0];
  else
    df1 = DriftTable[i1 - 1] + (DriftTable[i1] - DriftTable[i1 - 1]) * (u1 - i1);


  u2 = (a2 - logTimeBegin) / (logTimeMax - logTimeBegin) * DRIFT_TABLE_LENGTH;
  i2 = (int) u2;
  if(i2 >= DRIFT_TABLE_LENGTH)
    i2 = DRIFT_TABLE_LENGTH - 1;

  if(i2 <= 1)
    df2 = u2 * DriftTable[0];
  else
    df2 = DriftTable[i2 - 1] + (DriftTable[i2] - DriftTable[i2 - 1]) * (u2 - i2);

  return df2 - df1;
}


/*! This function integrates the cosmological prefactor for a kick step of
 *  the gravitational force. The integration bounds are from "time0 -> time1".
 *  The GravKickTable stores the integral from eqn 28 (Springel '05). So 
 *  GravKickTable[10] is equivalent to "integrate[da/( a^2*H(a) ), {0,10}]"
 *  So, to integrate from i=10 to i=23, simply GravKickTable[10] - GravKickTable[23]
 *  Of course, if the times don't fall nicely onto the HydroKickTable, some
 *  interpolation is required.
 */
double get_gravkick_factor(int time0, int time1)
{
  double a1;							//Initial time
	double a2;							//Final time
	double df1;             //Integral at time0
	double df2;             //Integral at time1
	double u1;							//use to find location of time0 w/in GravKickTable
	double u2;							//use to find location of time1 w/in GravKickTable
  int i1;									// (int)u1
	int i2;                 // (int)u2

  /* note: will only be called for cosmological integration */

  a1 = logTimeBegin + time0 * All.Timebase_interval;
  a2 = logTimeBegin + time1 * All.Timebase_interval;

  u1 = (a1 - logTimeBegin) / (logTimeMax - logTimeBegin) * DRIFT_TABLE_LENGTH;
  i1 = (int) u1;
  if(i1 >= DRIFT_TABLE_LENGTH)
    i1 = DRIFT_TABLE_LENGTH - 1;

	/* Use i1 to get close to the actual position on GravKickTable. Use the remainder 
     to interpolate the actual location of the integral. This saves time. */
  if(i1 <= 1)
    df1 = u1 * GravKickTable[0];
  else
    df1 = GravKickTable[i1 - 1] + (GravKickTable[i1] - GravKickTable[i1 - 1]) * (u1 - i1);


  u2 = (a2 - logTimeBegin) / (logTimeMax - logTimeBegin) * DRIFT_TABLE_LENGTH;
  i2 = (int) u2;
  if(i2 >= DRIFT_TABLE_LENGTH)
    i2 = DRIFT_TABLE_LENGTH - 1;

	/* Use i2 to get close to the actual position on GravKickTable. Use the remainder 
     to interpolate the actual location of the integral. This saves time. */
  if(i2 <= 1)
    df2 = u2 * GravKickTable[0];
  else
    df2 = GravKickTable[i2 - 1] + (GravKickTable[i2] - GravKickTable[i2 - 1]) * (u2 - i2);

  return df2 - df1;
}


/*! This function integrates the cosmological prefactor for a kick step of
 *  the hydrodynamical force. Integration bounds are from "time0 -> time1"
 *  The HydroKickTable stores the integral from eqn 28 (Springel '05). So
 *  HydroKickTable[10] is equivalent to "integrate[da/(a^(3*gamma - 2)*H(a)), {0,10}]"
 *  So to integrate from i=10 to i=23, simply HydroKickTable[10] - HydroKickTable[23].
 *  Of course, if the times don't fall nicely onto the HydroKickTable, some
 *  interpolation is required.
 */
double get_hydrokick_factor(int time0, int time1)
{
  double a1;							//Initial time
	double a2;							//FInal time
	double df1;							//Integral at time0
	double df2; 						//Integral at time1
	double u1; 							//use to find location of time0 w/in HydroKickTable
	double u2;							//use to find location of time1 w/in HydroKickTable
  int i1; 								// (int)u1
	int i2;									// (int)u2

  /* note: will only be called for cosmological integration */

  a1 = logTimeBegin + time0 * All.Timebase_interval;
  a2 = logTimeBegin + time1 * All.Timebase_interval;

  u1 = (a1 - logTimeBegin) / (logTimeMax - logTimeBegin) * DRIFT_TABLE_LENGTH;
  i1 = (int) u1;
  if(i1 >= DRIFT_TABLE_LENGTH)
    i1 = DRIFT_TABLE_LENGTH - 1;

	/* Use i1 to get close to the actual position on HydroKickTable. Use the remainder 
     to interpolate the actual location of the integral. This saves time. */
  if(i1 <= 1)
    df1 = u1 * HydroKickTable[0];
  else
    df1 = HydroKickTable[i1 - 1] + (HydroKickTable[i1] - HydroKickTable[i1 - 1]) * (u1 - i1);


  u2 = (a2 - logTimeBegin) / (logTimeMax - logTimeBegin) * DRIFT_TABLE_LENGTH;
  i2 = (int) u2;
  if(i2 >= DRIFT_TABLE_LENGTH)
    i2 = DRIFT_TABLE_LENGTH - 1;

  if(i2 <= 1)
    df2 = u2 * HydroKickTable[0];
  else
    df2 = HydroKickTable[i2 - 1] + (HydroKickTable[i2] - HydroKickTable[i2 - 1]) * (u2 - i2);

  return df2 - df1;
}


/*! Integration kernel for drift factor computation. Integrand in eqn 27 (Springel '05)
 *  Recall that H(a) = (1/a) * (da/dt)
 */
double drift_integ(double a, void *param)
{
  double h;

  #ifndef DARKENERGY
      h = All.Omega0 / (a * a * a) + (1 - All.Omega0 - All.OmegaLambda) / (a * a) + All.OmegaLambda;
  #endif

  #ifdef DARKENERGY
      #ifdef SCALARFIELD
         All.dark_factor = get_dark_factor(a);
         h = All.Omega0 / (a * a * a) + (1 - All.Omega0 - All.OmegaLambda) / (a * a) + 
             (All.OmegaLambda * All.dark_factor);
      #endif 

      #ifdef DARKPARAM
         All.dark_factor = get_dark_factor(a);
         h = All.Omega0 / (a * a * a) + (1 - All.Omega0 - All.OmegaLambda) / (a * a) + 
             (All.OmegaLambda * All.dark_factor);
      #endif 

      #ifdef GCG
         All.dark_factor = get_dark_factor(a);
         h = All.Omega0 / (a * a * a) + (1 - All.Omega0 - All.OmegaLambda) / (a * a) + 
             (All.OmegaLambda * All.dark_factor);
      #endif 
  #endif

  h = All.Hubble * sqrt(h);

  return 1 / (h * a * a * a);
}


/*! Integration kernel for gravitational kick factor computation. Integrand in 
 *  eqn 28 (Springel '05).  Recall that H(a) = (1/a) * da/dt and that we are 
 *  integrating the gravity portion of hamilton's eqn of motion... 
 *  dp_{i} = -dH_{grav}/dq_{i}
 */
double gravkick_integ(double a, void *param)
{
  double h;

  #ifndef DARKENERGY
      h = All.Omega0 / (a * a * a) + (1 - All.Omega0 - All.OmegaLambda) / (a * a) + All.OmegaLambda;
  #endif

  #ifdef DARKENERGY
      #ifdef SCALARFIELD
         All.dark_factor = get_dark_factor(a);
         h = All.Omega0 / (a * a * a) + (1 - All.Omega0 - All.OmegaLambda) / (a * a) + 
             (All.OmegaLambda * All.dark_factor);
      #endif

      #ifdef DARKPARAM
         All.dark_factor = get_dark_factor(a);
         h = All.Omega0 / (a * a * a) + (1 - All.Omega0 - All.OmegaLambda) / (a * a) + 
             (All.OmegaLambda * All.dark_factor);
      #endif

      #ifdef GCG
         All.dark_factor = get_dark_factor(a);
         h = All.Omega0 / (a * a * a) + (1 - All.Omega0 - All.OmegaLambda) / (a * a) + 
             (All.OmegaLambda * All.dark_factor);
      #endif
  #endif

  h = All.Hubble * sqrt(h);

  return 1 / (h * a * a);
}


/*! Integration kernel for hydrodynamical kick factor computation.
 *  Integrand in eqn 28 combined with eqn 35 (Springel '05). 
 *  Recall that H(a) = (1/a) * da/dt. Since it is a "reversible" reaction, 
 *  A_i is condsidered a constant. Here we are integrating the hydrodynamic portion
 *  of Hamilton's eqns of motion .... dp_{i} = -dH_{hydro}/dq_{i}
 */
double hydrokick_integ(double a, void *param)
{
  double h;

  #ifndef DARKENERGY
      h = All.Omega0 / (a * a * a) + (1 - All.Omega0 - All.OmegaLambda) / (a * a) + All.OmegaLambda;
  #endif

  #ifdef DARKENERGY
      #ifdef SCALARFIELD
         All.dark_factor = get_dark_factor(a);
         h = All.Omega0 / (a * a * a) + (1 - All.Omega0 - All.OmegaLambda) / (a * a) + 
             (All.OmegaLambda * All.dark_factor);
      #endif

      #ifdef DARKPARAM
         All.dark_factor = get_dark_factor(a);
         h = All.Omega0 / (a * a * a) + (1 - All.Omega0 - All.OmegaLambda) / (a * a) + 
             (All.OmegaLambda * All.dark_factor);
      #endif

      #ifdef GCG
         All.dark_factor = get_dark_factor(a);
         h = All.Omega0 / (a * a * a) + (1 - All.Omega0 - All.OmegaLambda) / (a * a) + 
             (All.OmegaLambda * All.dark_factor);
      #endif
  #endif

  h = All.Hubble * sqrt(h);

  return 1 / (h * pow(a, 3 * GAMMA_MINUS1) * a);
}

double growthfactor_integ(double a, void *param)
{
  double s;

  // I don't know why, but this is the same Hubble eq he's used throughout this file, but
  // he's multiplied through by a^3. I'm pretty sure I still need to modify this.
  // THIS FUNCTION IS NEVER CALLED IN GADGET... IT"S NOT EVEN IN PROTO.H. WTF? That's how it
  // is in vanilla gadget, too... Weird. I'm not going to touch it. I should try and delete or 
  // comment it out and see if the code still compiles and runs.
  s = All.Omega0 + (1 - All.Omega0 - All.OmegaLambda) * a + All.OmegaLambda * a * a * a;
  s = sqrt(s);

  return pow(sqrt(a) / s, 3);
}


