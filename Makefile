
#----------------------------------------------------------------------
# From the list below, please activate/deactivate the options that     
# apply to your run. If you modify any of these options, make sure     
# that you recompile the whole code by typing "make clean; make".      
#                                                                      
# Look at end of file for a brief guide to the compile-time options.   
# After making... run with
# mpirun -np 2 ./Gadget2 parameterfiles/lcdm_2lpt.param
#----------------------------------------------------------------------


#--------------------------------------- Basic operation mode of code
OPT   +=  -DPERIODIC 
OPT   +=  -DUNEQUALSOFTENINGS


#--------------------------------------- Things that are always recommended
OPT   +=  -DPEANOHILBERT
OPT   +=  -DWALLCLOCK   


#--------------------------------------- Astrophysics Options
#OPT += -DCOOLING       
#OPT += -DSFR                   # COOLING must be defined for SFR to work
#OPT += -DSTELLARAGE            # SFR must also be defined. Outputs Star Age to file
#OPT += -DMETALS				    # Must specify which metals you track below
#OPT += -DFEEDBACK			       # Uses only feedback radius passed in parameter file.
#OPT += -DFEEDBACK_W_SPH_KERNAL #Let FeedbackRadius = smoothing length...Not working yt
#OPT += -DUVBACKGROUND
#OPT += -DBLACK_HOLES


#--------------------------------------- Dark Energy Options
# These are the options for time-dependent DE. The SCALARFIELD option 
# is for both quint and kessence. Model details are given in the logging file. 
# This info should be at the very top of the w_vs_a.txt file that the code 
# reads in. Only need one option for both because the Hubble eq is the same 
# for both of them, it's just that the w_vs_a.txt will be different. 
#----------------------------------------

#OPT += -DWRITE_COM            # This MUST be enabled for use with bexspec

#OPT += -DDARKENERGY           # This MUST be enabled to use time-dependent DE
#OPT += -DSCALARFIELD          # Enable when using quintessence or k-essence
#OPT += -DGCG                  # Enable when using generalized Chapylgin Gas
#OPT += -DDARKPARAM            # Enable when using a parameterization of w(a) (i.e. Linder02)
                               # This is what Planck used when exploring time-dependent DE, so 
                               # I can use their bounds. It's also model independent. 


#---------------------------------------------------------- Star Formation Options
# If disabling the Jeans Criterion, NO_JEANS_CRITERION, 
# NH_MIN, T_MAX, OVERDENSITY all need to be simultaneously 
# enabled. If these options are enabled, assume that we 
# are using the criterion form Stinson 2006. 
#
# If using the Jeans Criterion (See Kobayashi 2004).
#
# IMPORTANT: If simulating a virialized structure (i.e. isolated galaxy)
# set OVERDENSITY = 0. The purpose of OVERDENSITY, is to ensure
# SF only occurs in virialized regions.
#----------------------------------------------------------
#
#---------------No Jeans Criterion Options ----------------
#OPT += -DNO_JEANS_CRITERION   #Use T_max and (n_H)_min and converging flow
#OPT += -DNH_MIN=0.1				 #Hydrogen Density in cm^-3
#OPT += -DT_MAX=15000		    #Max Temperature in K
#OPT += -DOVERDENSITY=55		 #Relative overdensity necessary to form star.

#-------------------- IMF Options ------------------------
# Default IMF is the Salpeter IMF, only one IMF below can 
# be specified at a time.
#---------------------------------------------------------
OPT += -DSAL_A_IMF
#OPT += -DCHABRIER_IMF



#---------------------------------------------------------- Feedback Integration Options
# USE_GSL and USE_SIMPSON should be exclusive in operation.
# Default is trapezoidal method, N_INTEGRATION_STEPS is used
# Simpson and trapezoidal methods. N_INTEGRATION_STEPS = 100
# seems to be adequate.
#
# As of 4/2/14 USE_GSL is non-operational. It fails b/c I
# floor the metal yield in TypeII_Zi_yield_mass_frac() for
# mass < 13. To fix this you need to add an interpolation point 
# for mass = 8. Good luck finding appropriate values there!
#----------------------------------------------------------
#OPT += -DUSE_GSL                   #Use GSL integration instead of trap. rule...
OPT += -DUSE_SIMPSON                #Use Simpson's Rule. RECOMMENDED
#OPT += -DN_INTEGRATION_STEPS=1000  #Default = 100, set in feedback_w_kernal.c


#---------------------------------------------------------- Additional Feedback Options
# NOTE: that the code is NOT optimized for these options.
# I added them in the easiest way possible. Essentially ALL
# the feedback computations are run but not ALL are used.
# to improve memory and computational efficiency, there is
# FEEDBACK_W_SPH_KERNAL must be enabled for these to do anything.
# NOTE: Cannot have DISABLE_COOLING_4_ALL_NGB_GAS and 
# METAL_FEEDBACK_ONLY enabled at the same time.
#-----------------------------------------------------------
#OPT += -DADIABATIC_SWITCH_ON_4_ALL_NGB_GAS  #No BlastRadius, turn all Ngb_gas cool off
#OPT += -DMETAL_FEEDBACK_ONLY                #Do not include energy feedback.
#OPT += -DTURNOFF_ADIABATIC_SWITCH           #No longer turn off ANY Ngb_gas's cooling


#--------------------------------------- Metals to Track Options.
#OPT += -DCARBON	       #USE
#OPT += -DNITROGEN
#OPT += -DOXYGEN          #USE
#OPT += -DFLORINE
#OPT += -DNEON
#OPT += -DSODIUM
#OPT += -DMAGNESIUM
#OPT += -DALUMINUM
#OPT += -DSILICON
#OPT += -DPHOSPHORUS
#OPT += -DSULFUR
#OPT += -DCHLORINE
#OPT += -DARGON
#OPT += -DPOTASSIUM
#OPT += -DCALCIUM         #USE
#OPT += -DSCANDIUM
#OPT += -DTITANIUM
#OPT += -DVANADIUM
#OPT += -DCHROMIUM        #USE
#OPT += -DMANGANESE       #USE
#OPT += -DIRON            #USE
#OPT += -DCOBALT
#OPT += -DNICKEL
#OPT += -DCOPPER
#OPT += -DZINC



#--------------------------------------- TreePM Options
OPT   +=  -DPMGRID=128
#OPT   +=  -DPLACEHIGHRESREGION=3
#OPT   +=  -DENLARGEREGION=1.2
#OPT   +=  -DASMTH=1.25
#OPT   +=  -DRCUT=4.5


#--------------------------------------- Single/Double Precision
#OPT   +=  -DDOUBLEPRECISION      
#OPT   +=  -DDOUBLEPRECISION_FFTW      


#--------------------------------------- Time integration options
OPT   +=  -DSYNCHRONIZATION
#OPT   +=  -DFLEXSTEPS
#OPT   +=  -DPSEUDOSYMMETRIC
#OPT   +=  -DNOSTOP_WHEN_BELOW_MINTIMESTEP
#OPT   +=  -DNOPMSTEPADJUSTMENT


#--------------------------------------- Output 
#OPT	 +=  -DWRITEGNUPLOT	 			     #To create contour plot files for gnuplot 
#OPT	 +=  -DCSVOUTPUT						  #Output local task particles csv format
#OPT   +=  -DHAVE_HDF5  
#OPT   +=  -DOUTPUTPOTENTIAL             #ALI Commented
#OPT   +=  -DOUTPUTACCELERATION
#OPT   +=  -DOUTPUTCHANGEOFENTROPY       #ALI Commented
#OPT   +=  -DOUTPUTTIMESTEP
OPT   +=   -DOUTPUT_TEMP_FOR_U           #ALI Commented
#OPT   +=  -DOUTPUT_AGE_FOR_POTENTIAL    #ALI Commented

#--------------------------------------- Things for special behaviour
#OPT   +=  -DNOGRAVITY     
#OPT   +=  -DNOTREERND 
OPT   +=  -DNOTYPEPREFIX_FFTW        
#OPT   +=  -DLONG_X=60
#OPT   +=  -DLONG_Y=5
#OPT   +=  -DLONG_Z=0.2
#OPT   +=  -DTWODIMS
#OPT   +=  -DSPH_BND_PARTICLES
#OPT   +=  -DNOVISCOSITYLIMITER
#OPT   +=  -DCOMPUTE_POTENTIAL_ENERGY
#OPT   +=  -DLONGIDS
#OPT   +=  -DISOTHERM_EQS
#OPT   +=  -DADAPTIVE_GRAVSOFT_FORGAS
#OPT   +=  -DSELECTIVE_NO_GRAVITY=2+4+8+16

#--------------------------------------- Testing and Debugging options
#OPT   +=  -DFORCETEST=0.1


#--------------------------------------- Glass making
#OPT   +=  -DMAKEGLASS=262144


#----------------------------------------------------------------------
# Here, select compile environment for the target machine. This may need 
# adjustment, depending on your local system. Follow the examples to add
# additional target platforms, and to get things properly compiled.
#----------------------------------------------------------------------

#--------------------------------------- Select some defaults

#CC       =  mpicc               # sets the C-compiler
#OPTIMIZE =  -O2 -Wall -g        # sets optimization and warning flags
#MPICHLIB =  -lmpich


#--------------------------------------- Select target computer

#SYSTYPE="CRC-Opteron-long"
#SYSTYPE="jared_home"
SYSTYPE="CRC-EPYC-IB"

#--------------------------------------- Adjust settings for target computer

ifeq ($(SYSTYPE),"jared_home")
CC = mpicc
OPTIMIZE  = -O3 -Wall
#OPTIMIZE = -Wall -g3
GSL_INCL  = -I/usr/local/include/gsl
GSL_LIBS  = -L/usr/local/lib
FFTW_INCL = -I/usr/local/include
FFTW_LIBS = -L/usr/local/lib
MPICHLIB  = -L/usr/local/lib
endif



ifeq ($(SYSTYPE),"CRC-Opteron-long")
CC       = /afs/crc.nd.edu/user/j/jcoughl2/.local/bin/mpicc
OPTIMIZE =  -O3 -Wall
GSL_INCL = -I/afs/crc.nd.edu/user/j/jcoughl2/.local/include
GSL_LIBS = -L/afs/crc.nd.edu/user/j/jcoughl2/.local/lib
FFTW_INCL=  -I/afs/crc.nd.edu/user/j/jcoughl2/.local/include
FFTW_LIBS=  -L/afs/crc.nd.edu/user/j/jcoughl2/.local/lib
MPICHLIB =	-L/afs/crc.nd.edu/user/j/jcoughl2/.local/lib
HDF5INCL =
HDF5LIB  =
endif



ifeq ($(SYSTYPE),"CRC-EPYC-IB")
CC       =  mpicc
#OPTIMIZE =  -O0 -g3 -Wall -traceback
OPTIMIZE =  -O3 -Wall
GSL_INCL =  -I/opt/crc/g/gsl/2.3/intel/15.0/include
GSL_LIBS =  -L/opt/crc/g/gsl/2.3/intel/15.0/lib -lgsl -lgslcblas
FFTW_INCL=  -I/opt/crc/f/fftw/2.1.5/mvapich2-2.2/intel-18.0-mlx/include
FFTW_LIBS=  -L/opt/crc/f/fftw/2.1.5/mvapich2-2.2/intel-18.0-mlx/lib
MPICHLIB =  -L/opt/crc/m/mvapich2/2.2/intel/18.0/lib -lmpich
HDF5INCL =
HDF5LIB  =
endif



PREFIX = ./src
OBJ_DIR = $(PREFIX)/obj

OPTIONS =  $(OPTIMIZE) $(OPT)

EXEC   = Gadget2

OBJS   = $(OBJ_DIR)/main.o  $(OBJ_DIR)/run.o  $(OBJ_DIR)/predict.o \
	 $(OBJ_DIR)/begrun.o $(OBJ_DIR)/endrun.o $(OBJ_DIR)/global.o  \
	 $(OBJ_DIR)/timestep.o  $(OBJ_DIR)/init.o $(OBJ_DIR)/restart.o  $(OBJ_DIR)/io.o    \
	 $(OBJ_DIR)/accel.o   $(OBJ_DIR)/read_ic.o  $(OBJ_DIR)/ngb.o  \
	 $(OBJ_DIR)/system.o  $(OBJ_DIR)/allocate.o  $(OBJ_DIR)/density.o  \
	 $(OBJ_DIR)/gravtree.o $(OBJ_DIR)/hydra.o  $(OBJ_DIR)/driftfac.o  \
	 $(OBJ_DIR)/domain.o  $(OBJ_DIR)/allvars.o $(OBJ_DIR)/potential.o  \
    $(OBJ_DIR)/forcetree.o   $(OBJ_DIR)/peano.o $(OBJ_DIR)/gravtree_forcetest.o \
	 $(OBJ_DIR)/pm_periodic.o $(OBJ_DIR)/pm_nonperiodic.o $(OBJ_DIR)/longrange.o \
    $(OBJ_DIR)/star_form.o  $(OBJ_DIR)/writeGnuplotData.o \
    $(OBJ_DIR)/writeCsvData.o $(OBJ_DIR)/feedback.o $(OBJ_DIR)/cooling.o \
    $(OBJ_DIR)/feedback_w_kernal.o  $(OBJ_DIR)/printOptionsEnabled.o\
    $(OBJ_DIR)/phystime2redshift.o  $(OBJ_DIR)/snr.o $(OBJ_DIR)/dark_energy.o
   

INCL   = $(PREFIX)/allvars.h  $(PREFIX)/proto.h  $(PREFIX)/tags.h  Makefile

INCLUDE   = $(GSL_INCL) $(FFTW_INCL)


CFLAGS = $(OPTIONS) $(GSL_INCL) $(FFTW_INCL) #$(HDF5INCL)


ifeq (NOTYPEPREFIX_FFTW,$(findstring NOTYPEPREFIX_FFTW,$(OPT)))    # fftw installed with type prefix?
  FFTW_LIB = $(FFTW_LIBS) -lrfftw_mpi -lfftw_mpi -lrfftw -lfftw
else
ifeq (DOUBLEPRECISION_FFTW,$(findstring DOUBLEPRECISION_FFTW,$(OPT)))
  FFTW_LIB = $(FFTW_LIBS) -ldrfftw_mpi -ldfftw_mpi -ldrfftw -ldfftw
else
  FFTW_LIB = $(FFTW_LIBS) -lsrfftw_mpi -lsfftw_mpi -lsrfftw -lsfftw
endif
endif

all: $(EXEC) 

LIBS   =   $(HDF5LIB) -g  $(MPICHLIB)  $(GSL_LIBS) -lgsl -lgslcblas -lm $(FFTW_LIB) 

#ALI Added 2/6/13  see http://www.apl.jhu.edu/Misc/Unix-info/make/make_10.html#SEC90
#                  for logic 
$(OBJ_DIR)/%.o : $(PREFIX)/%.c $(INCL)
	$(CC) $(OPTIONS) $(INCLUDE) -c $< -o $@

$(EXEC): $(OBJS) 
	$(CC) $(OBJS) $(LIBS)  -o  $(EXEC)  

#ALI Commented 2/6/13
#$(OBJS): $(INCL) 

clean:
	rm -f $(OBJS) *.gch 


#-----------------------------------------------------------------------
#
#   Brief guide to compile-time options of the code. More information
#   can be found in the code documentation.
#
# - PERIODIC:   
#     Set this if you want to have periodic boundary conditions.
#
# - UNEQUALSOFTENINGS:
#     Set this if you use particles with different gravitational
#     softening lengths.
#
# - PEANOHILBERT:    
#     This is a tuning option. When set, the code will bring the
#     particles after each domain decomposition into Peano-Hilbert
#     order. This improves cache utilization and performance.
#  
# - WALLCLOCK:       
#     If set, a wallclock timer is used by the code to measure internal
#     time consumption (see cpu-log file).  Otherwise, a timer that
#     measures consumed processor ticks is used.
#
# - PMGRID:     
#     This enables the TreePM method, i.e. the long-range force is
#     computed with a PM-algorithm, and the short range force with the
#     tree. The parameter has to be set to the size of the mesh that
#     should be used, (e.g. 64, 96, 128, etc). The mesh dimensions need
#     not necessarily be a power of two.  Note: If the simulation is
#     not in a periodic box, then a FFT method for vacuum boundaries is
#     employed, using an actual mesh with dimension twice(!) that
#     specified by PMGRID.
#
# - PLACEHIGHRESREGION: 
#     If this option is set (will only work together with PMGRID), then
#     the long range force is computed in two stages: One Fourier-grid
#     is used to cover the whole simulation volume, allowing the
#     computation of the longe-range force.  A second Fourier mesh is
#     placed on the region occupied by "high-resolution" particles,
#     allowing the computation of an intermediate scale force. Finally,
#     the force on short scales is computed with the tree. This
#     procedure can be useful for "zoom-simulations", provided the
#     majority of particles (the high-res particles) are occupying only
#     a small fraction of the volume. To activate this option, the
#     parameter needs to be set to an integer bit mask that encodes the
#     particle types that make up the high-res particles.
#     For example, if types 0, 1, and 4 form the high-res
#     particles, set the parameter to PLACEHIGHRESREGION=19, because
#     2^0 + 2^1 + 2^4 = 19. The spatial region covered by the high-res 
#     grid is determined automatically from the initial conditions. 
#     Note: If a periodic box is used, the high-res zone may not intersect
#     the box boundaries.
#
# - ENLARGEREGION: 
#     The spatial region covered by the high-res zone has a fixed size
#     during the simulation, which initially is set to the smallest
#     region that encompasses all high-res particles. Normally, the
#     simulation will be interrupted if high-res particles leave this
#     region in the course of the run. However, by setting this
#     parameter to a value larger than one, the size of the high-res
#     region can be expanded, providing a buffer region.  For example, 
#     setting it to 1.4 will enlarge its side-length by 40% (it remains
#     centered on the high-res particles). Hence, with this setting, the 
#     high-res region may expand or move by a limited amount. 
#     Note: If SYNCHRONIZATION is activated, the code will be able to
#     continue even if high-res particles leave the initial high-res
#     grid. In this case, the code will update the size and position of
#     the grid that is placed onto the high-resolution region
#     automatically. To prevent that this potentially happens every
#     single PM step, one should nevertheless assign a value slightly
#     larger than 1 to ENLARGEREGION.
#
# - ASMTH: 
#     This can be used to override the value assumed for the scale that
#     defines the long-range/short-range force-split in the TreePM
#     algorithm. The default value is 1.25, in mesh-cells.
#
# - RCUT: 
#     This can be used to override the maximum radius in which the
#     short-range tree-force is evaluated (in case the TreePM algorithm
#     is used). The default value is 4.5, given in mesh-cells.
#
# - DOUBLEPRECISION: 
#     This makes the code store and compute internal particle data in
#     double precision. Note that output files are nevertheless written
#     by converting the particle data to single precision.
#
# - DDOUBLEPRECISION_FFTW:
#     If this is set, the code will use the double-precision version of
#     FTTW, provided the latter has been explicitly installed with a
#     "d" prefix, and NOTYPEPREFIX_FFTW is not set. Otherwise the
#     single precision version ("s" prefix) is used.
#
# - SYNCHRONIZATION: 
#     When this is set, particles are kept in a binary hierarchy of
#     timesteps and may only increase their timestep if the new
#     timestep will put them into synchronization with the higher time
#     level.
#
# - FLEXSTEPS: 
#     This is an alternative to SYNCHRONIZATION. Particle timesteps are
#     here allowed to be integer multiples of the minimum timestep that
#     occurs among the particles, which in turn is rounded down to the
#     nearest power-of-two devision of the total simulated
#     timespan. This option distributes particles more evenly over
#     individual system timesteps, particularly once a simulation has
#     run for a while, and may then result in a reduction of work-load
#     imbalance losses.
#
# - PSEUDOSYMMETRIC: 
#     When this option is set, the code will try to "anticipate"
#     timestep changes by extrapolating the change of the acceleration
#     into the future. This can in certain idealized cases improve the
#     long-term integration behaviour of periodic orbits, but should
#     make little or no difference in most real-world applications. May
#     only be used together with SYNCHRONIZATION.
#
# - NOSTOP_WHEN_BELOW_MINTIMESTEP: 
#     If this is activated, the code will not terminate when the
#     timestep falls below the value of MinSizeTimestep specified in
#     the parameterfile. This is useful for runs where one wants to
#     enforce a constant timestep for all particles. This can be done
#     by activating this option, and by setting MinSizeTimestep and
#     MaxSizeTimestep to an equal value.
#
# - NOPMSTEPADJUSTMENT: 
#     When this is set, the long-range timestep for the PM-force
#     computation (when the TreePM algorithm is used) is always
#     determined by MaxSizeTimeStep.  Otherwise, it is determined by
#     the MaxRMSDisplacement parameter, or MaxSizeTimeStep, whichever
#     gives the smaller step.
#
# - HAVE_HDF5:
#     If this is set, the code will be compiled with support for input
#     and output in the HDF5 format. You need to have the HDF5
#     libraries and headers installed on your computer for this option
#     to work. The HDF5 format can then be selected as format "3" in
#     Gadget's parameterfile.
#
# - OUTPUTPOTENTIAL: 
#     This will make the code compute gravitational potentials for
#     all particles each time a snapshot file is generated. The values
#     are then included in the snapshot file. Note that the computation
#     of the values of the gravitational potential costs additional CPU.
#
# - OUTPUTACCELERATION: 
#     This will include the physical acceleration of each particle in
#     snapshot files.
#
# - OUTPUTCHANGEOFENTROPY: 
#     This will include the rate of change of entropy of gas particles
#     in snapshot files.
#
# - OUTPUTTIMESTEP:  
#     This will include the current timesteps of all particles in the 
#     snapshot files.
#
# - NOGRAVITY      
#     This switches off gravity. Useful only for pure SPH simulations 
#     in non-expanding space.
#
# - NOTREERND:       
#     If this is not set, the tree construction will succeed even when
#     there are a few particles at identical locations. This is done by
#     `rerouting' particles once the node-size has fallen below 1.0e-3
#     of the softening length. When this option is activated, this will
#     be surpressed and the tree construction will always fail if there
#     are particles at extremely close coordinates.
#
# - NOTYPEPREFIX_FFTW:
#     This is an option that signals that FFTW has been compiled
#     without the type-prefix option, i.e. no leading "d" or "s"
#     characters are used to access the library.
#
# - LONG_X/Y/Z: 
#     These options can be used together with PERIODIC and NOGRAVITY only.
#     When set, the options define numerical factors that can be used to
#     distorts the periodic simulation cube into a parallelepiped of 
#     arbitrary aspect ratio. This can be useful for idealized SPH tests.
#
# - TWODIMS:
#     This effectively switches of one dimension in SPH, i.e. the code
#     follows only 2d hydrodynamics in the xy-, yz-, or xz-plane. This
#     only works with NOGRAVITY, and if all coordinates of the third
#     axis are exactly equal. Can be useful for idealized SPH tests.
#
# - SPH_BND_PARTICLES:
#     If this is set, particles with a particle-ID equal to zero do not
#     receive any SPH acceleration. This can be useful for idealized
#     SPH tests, where these particles represent fixed "walls".
#
# - NOVISCOSITYLIMITER:   
#     If this is set, the code will not try to put an upper limit on
#     the viscous force in case an implausibly high pair-wise viscous
#     force (which may lead to a particle 'reflection' in case of poor
#     timestepping) should arise. Note: For proper settings of the
#     timestep parameters, this situation should not arise.
#
# - COMPUTE_POTENTIAL_ENERGY:
#     When this option is set, the code will compute the gravitational
#     potential energy each time a global statistics is computed. This
#     can be useful for testing global energy conservation.
#
# - LONGIDS: 
#     If this is set, the code assumes that particle-IDs are stored as
#     64-bit long integers. This is only really needed if you want to
#     go beyond ~2 billion particles.
#
# - ISOTHERM_EQS:
#     This special option makes the gas behave like an isothermal gas
#     with equation of state P = cs^2 * rho. The sound-speed cs is set by 
#     the thermal energy per unit mass in the intial conditions, 
#     i.e. cs^2=u. If the value for u is zero, then the initial gas 
#     temperature in the parameter file is used to define the sound speed
#     according to cs^2 = 3/2 kT/mp, where mp is the proton mass.
#
# - ADAPTIVE_GRAVSOFT_FORGAS:
#     When this option is set, the gravitational softening lengths used for
#     gas particles is tied to their SPH smoothing length. This can be useful
#     for dissipative collapse simulations. The option requires the setting
#     of UNEQUALSOFTENINGS.
#
# - SELECTIVE_NO_GRAVITY:
#     This can be used for special computations where one wants to 
#     exclude certain particle types from receiving gravitational
#     forces. The particle types that are excluded in this fashion are 
#     specified by a bit mask, in the same as for the PLACEHIGHRESREGION 
#     option.
#
# - FORCETEST:       
#     This can be set to check the force accuracy of the code. The
#     option needs to be set to a number between 0 and 1 (e.g. 0.01),
#     which is taken to specify a random fraction of particles for
#     which at each timestep forces by direct summation are
#     computed. The normal tree-forces and the correct direct
#     summation forces are collected in a file. Note that the
#     simulation itself is unaffected by this option, but it will of
#     course run much(!) slower, especially if 
#     FORCETEST*NumPart*NumPart >> NumPart. Note: Particle IDs must 
#     be set to numbers >=1 for this to work.
#
# - MAKEGLASS
#     This option can be used to generate a glass-like particle 
#     configuration. The value assigned gives the particle load,
#     which is initially generated as a Poisson sample and then
#     evolved towards a glass with the sign of gravity reversed.
#
#-----------------------------------------------------------------------

